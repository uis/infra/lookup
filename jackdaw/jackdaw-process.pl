#!/usr/bin/perl

# $Id$

use warnings;
use strict;

use English;
use Text::CSV_XS;
use Getopt::Std;

use Ucam::Directory;
use Ucam::Directory::Person qw/map_title/;

use Ucam::Lookup::JackdawUtils qw/create_inst   update_inst
                                  create_person update_person
                                  cancel_entry/;

use constant CANCEL   => 1;
use constant UNCANCEL => 0;

# The expected number of CVS fields for each action
our %expected_fields =
    ( 'inst-delta'   => 5,
      'person-delta' => 10,
      'inst-full'    => 3,
      'person-full'  => 8,
 );

# Command line/parsing
our ($opt_c, $opt_u, $opt_a);
getopts ('c:ua:') || die "Failed to parse arguments";
die "A config file must be specified (with -c)" unless $opt_c;
die "A action must be specified (with -a)" unless $opt_a;
die "The specified action ('$opt_a') isn't valid - expecting one of " .
    join(", ",keys %expected_fields) unless $expected_fields{$opt_a};
my $do_update = $opt_u;
my $action = $opt_a;



# Config file
my $config;
unless ($config = do $opt_c) {
    die "couldn't parse config file: $@" if $@;
    die "couldn't read config file: $!" unless defined $config;
}

# FIXME the logfile used here needs to be the same as the one used by
# FIXME the webserver (or at least they need to be merged)
open my $log, ">>", "logfile";
my @config =
    ( log        => $log,
      godmode    => 1,
      @$config
 );
my $dir = eval { new Ucam::Directory(@config) };
die "$EVAL_ERROR\n" if $EVAL_ERROR;

my $csv = Text::CSV_XS->new();

print "START dry run\n" unless $do_update;

# Open token file if processing deltas
my $token_file;
if ($action =~ /-delta/) {
    open $token_file, ">>", "TOKENS" or die "Cannot open TOKENS: $!";
}

my (%seen,$lines, $err_lines);

# loop over the input file
$lines = 0;
$err_lines = 0;
while(my $line = <>) {
    chomp($line);
    next if $line =~ /^\s*$/;
    ++$lines;

    eval {

        $csv->parse($line)
            or die "Failed to parse line: ", $csv->error_input();

        # See if we got what we expected
        die sprintf("Wrong number of fields for %s (expecting %s, got %s)\n",
                    $action,
                    $expected_fields{$action},
                    scalar($csv->fields))
            unless $expected_fields{$action} == scalar($csv->fields);

        # Dispatch to something that knows what to do
        if ($action eq 'inst-delta') {
            do_delta_inst($dir,$csv,$do_update);
            print $token_file ($csv->fields)[0], "\n"
                or die "Failed to print token to token file: $!";
        } elsif ($action eq 'person-delta') {
            do_delta_person($dir,$csv,$do_update);
            print $token_file ($csv->fields)[0], "\n"
                or die "Failed to print token to token file: $!";
        } elsif ($action eq 'inst-full') {
            $seen{($csv->fields)[0]} = 1;
            do_full_inst($dir,$csv,$do_update);
        } elsif ($action eq 'person-full') {
            $seen{lc((($csv->fields)[0]))} = 1;
            do_full_person($dir,$csv,$do_update);
        } else {
            die "The specified action ('$action') isn't valid";
        }

    };

    # Uncaught exceptions are logged and we move on to the next line
    if ($EVAL_ERROR) {
        ++$err_lines;
        my $msg = $EVAL_ERROR; 
        chomp $msg;
        print STDERR "Error doing $action, input line $.: $msg\n";
        print STDERR "  line was '$line'\n";
    }

}

# Close the token file
if (defined($token_file)) {
    close $token_file or die "Failed to close TOKENS: $!";
}

# Check for entries in the directory that _didn't_ appear in the input
# file, but only if we didn't have to skip any lines due to error
if ($action =~ /-full/ and $err_lines) {
    print STDERR "Errors detected in input - cleanup not run\n";
}
else {
    if ($action eq 'inst-full') {
        do_full_inst_cleanup($dir,\%seen,$do_update);
    } elsif ($action eq 'person-full') {
        do_full_person_cleanup($dir,\%seen,$do_update);
    }
}

print "Processed $action, $lines lines\n";
print "END dry run\n" unless $do_update;

# ---

# Process institution deltas
sub do_delta_inst {

    my ($dir,$csv,$do_update) = @_;

    my ($token,$valid,$id,$father,$name) = $csv->fields;

    # Attempt to retrieve the corresponding inst object
    my $inst = eval { $dir->lookup_inst($id) };

    # For a valid institution create it if needed, but always
    # attempt un-cancel and update
    if ($valid eq 'Y') {
        $inst = create_inst($dir,$do_update,$id,$name,$father)
            if !defined($inst);
        cancel_entry($dir,$inst,UNCANCEL,$do_update);
        update_inst($dir,$inst,$do_update,$name,$father);
    }
    # Otherwise cancel it
    elsif ($valid eq 'N') {
        if (defined($inst) and !$inst->cancelled) {
            cancel_entry($dir,$inst,CANCEL,$do_update);
        } else {
            #print "Inst $id: already cancelled, invalidation ignored\n";
        }
    }
    # Otherwise worry
    else {
        die "Unexpected validity flag: $valid";
    }

}

# ---

# Process person deltas
sub do_delta_person {

    my ($dir,$csv,$do_update) = @_;

    my ($token,$valid,$id,$name,$inst,
        $coll,$cammail,$ex_dir,$staff,$student) = $csv->fields;
    $id = lc($id);

    # Attempt to retrieve the corresponding person object
    my $person = eval { $dir->lookup_person($id) };

    # If it's valid, create it if needed, and always un-cancel and update
    if ($valid eq 'Y') {
        $person = create_person($dir,$do_update,$id,$name)
            if !defined($person);
        cancel_entry($dir,$person,UNCANCEL,$do_update);
        update_person($dir,$person,$do_update,$name,
                      $inst,$cammail,$ex_dir,$coll,$staff,$student);
    }
    # Otherwise cancel if necessary
    elsif ($valid eq 'N') {
        if (defined($person) and !$person->cancelled) {
            cancel_entry($dir,$person,CANCEL,$do_update);
        } else {
            #print "Person $id: already cancelled, invalidation ignored\n";
        }
    }
    # Otherwise worry
    else {
        die "Unexpected validity flag: $valid";
    }

}

# ---

# Process full institution list
sub do_full_inst {

    my ($dir,$csv,$do_update) = @_;

    my ($id,$father,$name) = $csv->fields;

    # Attempt to retrieve the corresponding inst object
    my $inst = eval { $dir->lookup_inst($id) };

    # Create the institution if needed, always un-cancel and update
    $inst = create_inst($dir,$do_update,$id,$name,$father) if !defined($inst);
    cancel_entry($dir,$inst,UNCANCEL,$do_update);
    update_inst($dir,$inst,$do_update,$name,$father);

}

# ---

# Process full people list
sub do_full_person {

    my ($dir,$cvs, $do_update) = @_;

    my ($id,$name,$inst,$coll,
        $cammail,$ex_dir,$staff,$student) = $csv->fields;;
    $id = lc($id);

    # Attempt to retrieve the corresponding person object
    my $person = eval { $dir->lookup_person($id) };

    # Create the person if needed, always un-cancel and update
    $person = create_person($dir,$do_update,$id,$name) if !defined($person);
    cancel_entry($dir,$person,UNCANCEL,$do_update);
    update_person($dir,$person,$do_update,$name,
                  $inst,$cammail,$ex_dir,$coll,$staff,$student);

}

# ---

# Cleanup invalid inst entries
sub do_full_inst_cleanup {

    my ($dir,$seen,$do_update) = @_;

    # Retrieve and check all insts, cancelling ones we haven't heard of
    # (except for college UG/PG insts)
    foreach my $inst
        ($dir->search_for_insts("objectClass=camAcUkOrganizationalUnit")) {
            eval {
                cancel_entry($dir,$inst,CANCEL,$do_update)
                    unless $inst->cancelled or $seen->{$inst->instID};
            };
            if ($EVAL_ERROR) {
                my $msg = $EVAL_ERROR;
                chomp $msg;
                print STDERR "Error cancelling ", $inst->instID, ": $msg\n";
            }
        }

}

# ---

# Cleanup invalid people entries
sub do_full_person_cleanup {

    my ($dir,$seen,$do_update) = @_;

    # Retrieve and check all people, cancelling ones we haven't heard of
    # Split by sn initial to keep blocks manageable and 'cos sn is required
    foreach my $initial ('A'..'Z') {
        foreach my $person ($dir->search_for_people("sn=$initial*")) {
            eval {
                cancel_entry($dir,$person,CANCEL,$do_update)
                    unless $person->cancelled or $seen->{$person->uid};
            };
            if ($EVAL_ERROR) {
                my $msg = $EVAL_ERROR;
                chomp $msg;
                print STDERR "Error cancelling ", $person->uid, ": $msg\n";
            }
        }
    }

}
