#!/usr/bin/perl

# $Id$

use warnings;
use strict;

use English;
use Getopt::Std;
use DateTime;
use Net::SMTP;
use Text::Wrap;

use Ucam::Directory;

use Ucam::Lookup::JackdawUtils qw/cancel_entry/;

use constant CANCEL   => 1;
use constant UNCANCEL => 0;

use constant MAIL_FROM => 'lookup-support@ucs.cam.ac.uk';
use constant SMATRHOST => 'ppsw.cam.ac.uk';

# Command line/parsing: -c <config_file> [-u] [-d <yyyy-mm-dd>]
our ($opt_c, $opt_u, $opt_d);
getopts ('c:ud:') || die "Failed to parse arguments";
die "A config file must be specified (with -c)" unless $opt_c;
my $do_update = $opt_u;

# Config file
my $config;
unless ($config = do $opt_c) {
    die "couldn't parse config file: $@" if $@;
    die "couldn't read config file: $!" unless defined $config;
}

# Date
my $now = DateTime->now;
if ($opt_d) {
    print "Running with overridden date $opt_d\n";
    my $fmt = DateTime::Format::Strptime->new(pattern=>'%Y-%m-%d',
                                              on_error=>'croak');
    $now = $fmt->parse_datetime($opt_d);
}

# FIXME the logfile used here needs to be the same as the one used by
# FIXME the webserver (or at least they need to be merged)
open my $log, ">>", "logfile";
my @config =
    ( log        => $log,
      godmode    => 1,
      @$config
 );
my $dir = eval { new Ucam::Directory(@config) };
die "$EVAL_ERROR\n" if $EVAL_ERROR;

print "START dry run\n" unless $do_update;

foreach my $group ($dir->search_for_groups("objectClass=camAcUkGroup")) {

    # ignore anything that's already cancelled
    next if $group->cancelled;

    # groups without expiryTimestamp don't expire
    if (!$group->expiryTimestamp) {
        print "Group ", $group->groupID, ": doesn't expire\n" if !$do_update;
        next;
    }

    # how long until expiry?
    my $days = $group->expiryTimestamp->delta_days($now)->in_units('days');
    $days = -$days if $group->expiryTimestamp < $now;

    print "Group ", $group->groupID, ": expires in $days days\n"
        if !$do_update;

    eval {
        # Cancel if expired
        if ($days < 0) {
            cancel_entry($dir,$group,CANCEL,$do_update);
        }

        # Otherwise warn administrators if expiry within 90 days and
        # we haven't done so recently
        elsif ($days <= 90 and
           (!$group->warnTimestamp or 
            $group->warnTimestamp <= $now)) {
            my $next = 60;
            $next = 30 if $days < 60;
            $next =  7 if $days < 30;
            $next =  1 if $days < 7;
            $next =  0 if $days < 1;
            email($dir,$group,$days,$do_update);
            if ($next > 0 ) {
                my $nextwarn =
                    $group->expiryTimestamp->clone->subtract(days=>$next);
                print "Group ", $group->groupID, 
                    ": next warning after ", $nextwarn, "\n";
                if ($do_update) {
                    $group->replace(attr=>'warnTimestamp',val=>$nextwarn);
                    $group->commit;
                }
            }
        }

    };
    if ($EVAL_ERROR) {
        my $msg = $EVAL_ERROR;
        chomp $msg;
        print STDERR "Error processing ", $group->groupID, ": $msg\n";
    }
}

print "END dry run\n" unless $do_update;

sub email {

    my ($dir,$group,$days,$do_update) = @_;

    # For each group that manages this one, collect the group's mail
    # address if there is one, and the preferred mail address of all
    # its members otherwise
    my @addrs;
    foreach my $mgr ($group->mgrgroups) {
        if ($mgr->mail) {
            push @addrs,$mgr->mail;
        }
        else {
            foreach my $person ($mgr->members) {
                push @addrs,$person->mail;
            }
        }
    }

    print "Group ", $group->groupID, ": sending warning\n";
    print "Group ", $group->groupID, ": recipients: @addrs\n";

    return unless $do_update;
    return unless @addrs;

    my $from    = MAIL_FROM;
    my $groupid = $group->groupID;
    my $title   = $group->groupTitle;
    my $units   = $days > 1 ? 'days' : 'day';

    my $message = 
"From: lookup Support <$from>
To: Managers of lookup group $groupid: ;
Subject: lookup group $groupid expires in $days $units" .
"\n" .
wrap("","",
"You are receiving this message becasue you are a manager of group
$groupid - $title - in the lookup directory, see:") .
"
  http://www.lookup.cam.ac.uk/group/$groupid/

This group expires in $days $units and will then be cancelled if not
renewed before then. Any group manager can renew the group for a further
year by going to the groups edit page and clicking 'OK'.";

    eval {
        my $smtp = Net::SMTP->new('ppsw.cam.ac.uk') or die "connect";
        $smtp->mail($from)                          or die "mail $from";
        $smtp->to(@addrs)                           or die "to " . @addrs;
        $smtp->data()                               or die "data";
        $smtp->datasend("$message")                 or die "datasend ...";
        $smtp->dataend()                            or die "dataend";
        $smtp->quit()                               or die "quit";
        print "Group ", $group->groupID, ": message sent\n";
    };
    if ($@) {
        die "Message not sent: $@ failed\n";
    }

}

