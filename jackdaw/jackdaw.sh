#!/bin/bash -e

SSH='ssh -x ldap@jackdaw.cam.ac.uk'

stamp=$(date +'%Y%m%d%H%m')

[ -e LDAP_INST_DELTA.csv ] && \
      mv LDAP_INST_DELTA.csv "LDAP_INST_DELTA.csv.${stamp}"
[ -e LDAP_PERSON_DELTA.csv ] && \
      mv LDAP_PERSON_DELTA.csv "LDAP_PERSON_DELTA.csv.${stamp}"
[ -e LDAP_INST_DELTA.ack ] && \
      mv LDAP_INST_DELTA.ack "LDAP_INST_DELTA.ack.${stamp}"
[ -e LDAP_PERSON_DELTA.ack ] && \
      mv LDAP_PERSON_DELTA.ack "LDAP_PERSON_DELTA.ack.${stamp}"
[ -e logfile ] && \
      mv logfile "logfile.${stamp}"
[ -e deltas.log ] && \
      mv deltas.log "deltas.log.${stamp}"

# Get instructions
${SSH} -n newcsv LDAP_INST_DELTA TOKEN VALID ID FATHER NAME > LDAP_INST_DELTA.csv 2> LDAP_INST_DELTA.err
${SSH} -n newcsv LDAP_PERSON_DELTA TOKEN VALID ID NAME INST COLL CAMMAIL EX_DIR STAFF_FLAG STUDENT_FLAG > LDAP_PERSON_DELTA.csv 2> LDAP_PERSON_DELTA.err

#./jackdaw-process creates a TOKENS file 
./jackdaw-process.pl -u -c lookup-jackdaw-config -a inst-delta \
              < LDAP_INST_DELTA.csv 1>> deltas.log 2>> LDAP_INST_DELTA.err
mv TOKENS LDAP_INST_DELTA.ack
./jackdaw-process.pl -u -c lookup-jackdaw-config -a person-delta \
              < LDAP_PERSON_DELTA.csv 1>> deltas.log 2>> LDAP_PERSON_DELTA.err
mv TOKENS LDAP_PERSON_DELTA.ack

# Return tokens
${SSH} ack LDAP_INST_DELTA < LDAP_INST_DELTA.ack 2>> LDAP_INST_DELTA.err
${SSH} ack LDAP_PERSON_DELTA < LDAP_PERSON_DELTA.ack 2>> LDAP_PERSON_DELTA.err

