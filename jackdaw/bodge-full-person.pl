#!/usr/bin/perl

# $Id$

use warnings;
use strict;

use English;
use Text::CSV_XS;
use Getopt::Std;

use Ucam::Directory;
use Ucam::Lookup::JackdawUtils qw/sortout_name/;

our ($opt_c,$opt_u);
getopts ('c:u') || die "Failed to parse arguments";
my $do_update = $opt_u;

# Config file
my $config;
unless ($config = do $opt_c) {
    die "couldn't parse config file: $@" if $@;
    die "couldn't read config file: $!" unless defined $config;
}

# FIXME the logfile used here needs to be the same as the one used by
# FIXME the webserver (or at least they need to be merged)
open my $log, ">>", "logfile";
my @config =
    ( log        => $log,
      @$config
 );

my $dir = eval { new Ucam::Directory(@config) };
die "$EVAL_ERROR\n" if $EVAL_ERROR;

my $csv = Text::CSV_XS->new();
my %seen;
my %valid;

print "START dry run\n" unless $do_update;

while(my $line = <>) {
    chomp($line);
    next if $line =~ /^\s*$/;

    eval {

        $csv->parse($line)
            or die "Failed to parse line: ", $csv->error_input();

        # See if we got what we expected
        die sprintf("Wrong number of fields for %s (expecting %s, got %s)\n",
                    'check-full-inst',
                    6,
                    scalar($csv->fields))
            unless 6 == scalar($csv->fields);

        my ($id,$name,$inst,$coll,$cammail,$ex_dir) = $csv->fields;
        $id = lc($id);
        $seen{$id} = 1;

        # Attempt to retrieve the corresponding person object
        my $person = eval { $dir->lookup_person($id) };

        if (!$person) {
            #print "$id: not in the directory\n";
        }
        else {

            if ($person->cancelled) {
                #print "$id: should not be cancelled, but is\n";
            }

            # Name?
            my ($cn,$title,$initials,$sn) = sortout_name($name);
            if (!$person->sn or $person->sn ne $sn) {
                print "$id: sn is '", $person->sn, "', should be '$sn'\n";
                $person->replace(attr=>'sn',val=>$sn) if $do_update;
            }
            if (!$person->cn or $person->cn ne $cn) {
                print "$id: cn is '", $person->cn, "', should be '$cn'\n";
                $person->replace(attr=>'cn',val=>$cn) if $do_update;
            }
            $person->replace(attr=>'jdName',val=>$name) if $do_update;

            # Affiliation?
            $person->replace(attr=>'jdInst',val=>$inst) if $do_update;

            # @cam email status
            $ex_dir = '-' if $ex_dir eq '';
            $person->replace(attr =>'jdCamMail', val=>$cammail) if $do_update;
            $person->replace(attr =>'jdExDir', val=>$ex_dir) if $do_update;

            $person->commit if $do_update;

        }


    };
    if ($EVAL_ERROR) {
        my $msg = $EVAL_ERROR;
        chomp $msg;
        print STDERR "Error at input line $.: $msg\n";
        print STDERR "  ($line)\n";
    }

}

print "END dry run\n" unless $do_update;
