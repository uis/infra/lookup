#!/bin/bash

CONFIG=$1

if [ -z $CONFIG ]
then
  echo "Usage: process <config_file>" >&2
  exit 1
fi

if [ ! -e $1 ]
then
  echo "$CONFIG: not found" >&2
  exit 1
fi

source $CONFIG

sed -e "s/@@HOST_NAME@@/$HOST_NAME/" \
    -e "s/@@EXT_HOST_ADDRESS@@/$EXT_HOST_ADDRESS/" \
    -e "s|@@ROOT_PASSWORD@@|$ROOT_PASSWORD|"

