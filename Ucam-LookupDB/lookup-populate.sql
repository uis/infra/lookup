
INSERT INTO people 
  VALUES ('fjc55');

INSERT INTO insts 
  VALUES ('IS');
INSERT INTO insts
  VALUES ('BOT');

INSERT INTO groups 
  VALUES ('123456');

--- ===Relations between objects===

INSERT INTO person_inst 
  VALUES ('fjc55','IS',TRUE,1);

INSERT INTO person_inst_addable
  VALUES ('fjc55','IS',TRUE,TRUE,FALSE);

INSERT INTO group_member 
  VALUES ('123456','fjc55',TRUE,2);

INSERT INTO group_member_addable
  VALUES ('123456',TRUE);

INSERT INTO group_mgrgroupid 
  VALUES ('123456','123456',TRUE,3);

INSERT INTO group_mgrgroupid_addable
  VALUES ('123456',TRUE);

INSERT INTO group_rdrgroupid 
  VALUES ('123456','123456',TRUE,3);

INSERT INTO group_rdrgroupid_addable
  VALUES ('123456',TRUE);

INSERT INTO group_visibility 
  VALUES ('123456','cam',TRUE,29);

INSERT INTO group_visibility_addable
  VALUES ('123456',TRUE);

--- ===Attributes of people===

INSERT INTO person_title 
  VALUES ('fjc55','IS','Director',FALSE,4,FALSE,TRUE);
INSERT INTO person_title
  VALUES ('fjc55','BOT','Dogsbody',FALSE,23,FALSE,TRUE);

INSERT INTO person_title_addable 
  VALUES ('fjc55','IS',TRUE,TRUE,FALSE);

INSERT INTO person_registeredname 
  VALUES ('fjc55','IS','Clark','FJ','Dr',FALSE,5,FALSE,TRUE);

INSERT INTO person_registeredname_addable 
  VALUES ('fjc55','IS',TRUE,TRUE,FALSE);

INSERT INTO person_displayname 
  VALUES ('fjc55','IS','Fred Clark',FALSE,6,FALSE,TRUE);

INSERT INTO person_displayname_addable 
  VALUES ('fjc55','IS',TRUE,TRUE,FALSE);

INSERT INTO person_mail 
  VALUES ('fjc55','IS','fjc55@cam.ac.uk',FALSE,7,FALSE,TRUE);

INSERT INTO person_mail_addable 
  VALUES ('fjc55','IS',TRUE,TRUE,FALSE);

INSERT INTO person_labeleduri 
  VALUES ('fjc55','IS','http://www.example.com/~fjc55/',
          'Fred''s Homepage',FALSE,8,FALSE,TRUE);

INSERT INTO person_labeleduri_addable 
  VALUES ('fjc55','IS',TRUE,TRUE,FALSE);

INSERT INTO person_phone 
  VALUES ('fjc55','IS','98765','Desk',FALSE,9,FALSE,TRUE);

INSERT INTO person_phone_addable 
  VALUES ('fjc55','IS',TRUE,TRUE,FALSE);

INSERT INTO person_postaladdress 
  VALUES ('fjc55','IS','Director'' Office',FALSE,10,FALSE,TRUE);

INSERT INTO person_postaladdress_addable 
  VALUES ('fjc55','IS',TRUE,TRUE,FALSE);

-- hm, tricky to inserte a JPEG here...

INSERT INTO person_jpegphoto_addable 
  VALUES ('fjc55','IS',TRUE,TRUE,FALSE);

--- ===Attributes of institutions===

INSERT INTO inst_ou 
  VALUES ('IS','Deaprtment of Important Studies',11,TRUE);

INSERT INTO inst_ou_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_parentinstid 
  VALUES ('IS','IS',12,TRUE);

INSERT INTO inst_parentinstid_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_groupid 
  VALUES ('IS','123456',13,TRUE);

INSERT INTO inst_groupid_addable 
  VALUES ('IS',TRUE);

-- hm, tricky to inserte a JPEG here...

INSERT INTO inst_jpegphoto_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_postaladdress 
  VALUES ('IS','East Forum',15,TRUE);

INSERT INTO inst_postaladdress_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_mail 
  VALUES ('IS','reception@inpstud.cam.ac.uk',16,TRUE);

INSERT INTO inst_mail_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_phone 
  VALUES ('IS','98000','Reception',17,TRUE);

INSERT INTO inst_phone_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_facsimiletelephonenumber 
  VALUES ('IS','98001','General',18,TRUE);

INSERT INTO inst_facsimiletelephonenumber_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_labeleduri 
  VALUES ('IS','http://impstud.cam.ac.uk/','Department Website',19,TRUE);

INSERT INTO inst_labeleduri_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_acronym 
  VALUES ('IS','DIS',20,TRUE);

INSERT INTO inst_acronym 
  VALUES ('IS','DUST',24,TRUE);

INSERT INTO inst_acronym_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_mgrgroupid 
  VALUES ('IS','123456',21,TRUE);

INSERT INTO inst_mgrgroupid_addable 
  VALUES ('IS',TRUE);

--- ===Attributes of groups===

INSERT INTO group_grouptitle
  VALUES ('123456','IS Editors',22,TRUE);

INSERT INTO group_grouptitle_addable
  VALUES ('123456',TRUE);

INSERT INTO group_description
  VALUES ('123456','Members of this group maintain the data for "Department of Important Studies"',25,TRUE);

INSERT INTO group_description_addable
  VALUES ('123456',TRUE);

INSERT INTO group_mail
  VALUES ('123456','mgrgroup@is.cam.ac.uk',26,TRUE);

INSERT INTO group_mail_addable
  VALUES ('123456',TRUE);

INSERT INTO group_expirytimestamp
  VALUES ('123456','2006-03-05 12:24:56',27,TRUE);

INSERT INTO group_expirytimestamp_addable
  VALUES ('123456',TRUE);

INSERT INTO group_warntimestamp
  VALUES ('123456','2006-03-05 12:24:56',28,TRUE);

INSERT INTO group_warntimestamp_addable
  VALUES ('123456',TRUE);



