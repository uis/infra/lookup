#!/usr/bin/perl -w

use strict;

use lib qw(t);
use TestSetup;
use Ucam::LookupDB;

use overload;

use Test::More 'no_plan';
use Test::NoWarnings;

use constant WHO => 'fjc55';

my $db = TestSetup->init;
BAIL_OUT("Can't connect to database") unless $db;
isa_ok($db,"DBIx::Class::Schema");

# Failed lookup
is($db->resultset("PersonPostaladdress")->search({uid=>'xxx99'})->single,undef);

# Basic retrieve
my $me = $db->resultset("PersonPostaladdress")->search({uid=>WHO})->single;
isa_ok($me,"Ucam::LookupDB::PersonPostaladdress");

foreach my $col (qw/uid instid postaladdress suppress rowid editable 
                    suppressible/) {
    ok(defined($me->$col),"Getting $col column");
}

is($me->postaladdress,"Director' Office","Getting address");
is($me,"Director' Office","Getting address via overload");

# Create
my $new = $db->resultset("PersonPostaladdress")->new
    ({postaladdress => '123, The Street',
      suppress=>1,
      editable=>1,
      suppressible=>1});
ok(defined($new),'Got new record');
is($new->postaladdress,'123, The Street',"Getting address");
is($new,'123, The Street',"Getting address via overload");
$new->set_columns({uid => 'fjc55'});
is($new->uid,'fjc55','UID set OK');
ok(!$new->in_storage,"New record not yet in storage");
$new->insert;
ok(defined($new->rowid),'Now have a rowid');
ok($new->in_storage,"New record now in storage");
my $result = $new->delete;
ok(defined(overload::StrVal($result)),"Object still defined");
is($new,'123, The Street',"Address still defined");
ok(!$new->in_storage,"New record no longer in storage");
