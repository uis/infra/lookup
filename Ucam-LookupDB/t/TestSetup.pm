package TestSetup;

use strict;

use FindBin;
use File::Spec;
use Test::More;

use Ucam::LookupDB;

sub init {
    my $self = shift;
    my %args = @_;

    my $dsn =    $ENV{"TESTSETUP_DSN"}    || "dbi:SQLite::memory:";
    my $dbuser = $ENV{"TESTSETUP_DBUSER"} || '';
    my $dbpass = $ENV{"TESTSETUP_DBPASS"} || '';

    my $schema = Ucam::LookupDB->connect($dsn,$dbuser,$dbpass);

    my $sqlfile = File::Spec->catfile($FindBin::Bin,'testschema.sql');
    open my $in, $sqlfile or die "Can't find 'schema.sql': $@";
    my $sql;
    { local $/ = undef; $sql = <$in>; }
    close $in;
    foreach my $statement (split(/;\n/, $sql)) {
        next unless $statement =~ /\w/;
        #print "*** '$statement'\n";
        $schema->storage->dbh->do($statement)
            || die "Error on SQL: $statement";
    }

    return $schema;
}

sub check_accessors {
    my ($self,$me,%accessors) = @_;

    foreach my $accessor (keys %accessors) {
        if (!defined($accessors{$accessor})) {
            ok(!defined($me->$accessor),"$accessor column undefined");
        }
        elsif ($accessors{$accessor} eq "scalar") {
            ok(defined($me->$accessor),"Getting $accessor column");
        }
        elsif ($accessors{$accessor} =~ /(\d+):(.*)/) {
            my $number = $1;
            my $class = $2;
            ok(my $vals = $me->$accessor,"Retrieved $accessor");
            isa_ok($vals,"DBIx::Class::ResultSet");
            is($vals->count,$number,"Number of $accessor is $number");
            if ($number > 0) {
                my $val = $vals->first;
                isa_ok($val,"Ucam::LookupDB::$class");
            }
        }
        else {
            ok(my $vals = $me->$accessor);
            isa_ok($vals,"Ucam::LookupDB::$accessors{$accessor}");
        }
    }
}

1;
