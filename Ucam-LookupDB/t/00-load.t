#!perl

use Test::More tests => 1;

BEGIN {
	use_ok( 'Ucam::LookupDB' ) || BAIL_OUT("Module won't load!");
}

diag( "Testing Ucam::LookupDB $Ucam::LookupDB::VERSION, Perl $], $^X" );
