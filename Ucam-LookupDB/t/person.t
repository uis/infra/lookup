#!/usr/bin/perl -w

use strict;

use lib qw(t);
use TestSetup;
use Ucam::LookupDB;

use Test::More 'no_plan';
use Test::NoWarnings;

use constant WHO => 'fjc55';
use constant SN  => 'Clark';

my $db = TestSetup->init;
isa_ok($db,"DBIx::Class::Schema");

# Failed lookup
is($db->resultset("Person")->find("zz99"),undef);

# Basic retrieve
my $me = $db->resultset("Person")->find(WHO);
isa_ok($me,"Ucam::LookupDB::Person");

my %accessors = 
    ( uid                     => "scalar",
      person_inst             => "1:PersonInst",
      inst                    => "1:Inst",
      inst_addable            => "1:PersonInstAddable",
      group_member            => "1:GroupMember",
      grp                     => "1:Group",
      title                   => "2:PersonTitle",
      title_addable           => "1:PersonTitleAddable",
      registeredname          => "PersonRegisteredname",
      registeredname_addable  => "1:PersonRegisterednameAddable",
      displayname             => "PersonDisplayname",
      displayname_addable     => "1:PersonDisplaynameAddable",
      mail                    => "1:PersonMail",
      mail_addable            => "1:PersonMailAddable",
      labeleduri              => "1:PersonLabeleduri",
      labeleduri_addable      => "1:PersonLabeleduriAddable",
      phone                   => "1:PersonPhone",
      phone_addable           => "1:PersonPhoneAddable",
      postaladdress           => "1:PersonPostaladdress",
      postaladdress_addable   => "1:PersonPostaladdressAddable",
      jpegphoto               => undef,
      jpegphoto_addable       => "1:PersonJpegphotoAddable",
 );

TestSetup->check_accessors($me,%accessors);

# Stringification
is($me->full_name,'Fred Clark');
is($me,'Fred Clark');

# Searching
my $result;
$result = $db->resultset("Person")->search({ uid => WHO });
isa_ok($result,"DBIx::Class::ResultSet");
is($result->count,1,"1 result for uid => '" . WHO . "'");
isa_ok($result->first,"Ucam::LookupDB::Person");

$result = $db->resultset("Person")->search
    (
     {
      'registeredname.sn' => SN
  },
     {
      join => [qw/registeredname/],
      prefetch => [qw/registeredname/],
  }
 );
isa_ok($result,"DBIx::Class::ResultSet");
is($result->count,1,"1 result for registeredname.sn => '" . SN . "'");
my $row = $result->first;
isa_ok($row,"Ucam::LookupDB::Person");
is($row->uid,WHO,"uid is '" . WHO. "'");
isa_ok($row->registeredname,"Ucam::LookupDB::PersonRegisteredname"); 
is($row->registeredname->sn,SN,"sn is '" . SN ."'");

$result = $db->resultset("Person")->search_related
    ('registeredname',{ sn => SN });
isa_ok($result,"DBIx::Class::ResultSet");
is($result->count,1,"1 result for related sn => '" . SN . "'");
isa_ok($result->first,"Ucam::LookupDB::PersonRegisteredname");

# attrdesc
$me = $db->resultset("Person")->find(WHO);
is($me->attrdesc('uid'),'CrsID',"attrdesc('uid') works");
is($me->attrdesc('uid',1),'CrsID',"attrdesc('uid',1) works");
is($me->attrdesc('uid',2),'CrsIDs',"attrdesc('uid',2) works");
is($me->attrdesc('title'),'Roles',"attrdesc('title') works");
is($me->attrdesc('title',1),'Role',"attrdesc('title',1) works");
is($me->attrdesc('title',2),'Roles',"attrdesc('title',2) works");
is($me->attrdesc('person_inst'),'person_inst',
                                         "attrdesc('person_inst') works");
is($me->attrdesc('person_inst',2),'person_insts',
                                         "attrdesc('person_insts',2) works");
is($me->attrdesc('bogus'),'bogus',"attrdesc('bogus') works");
is($me->attrdesc('bogus',2),'boguses',"attrdesc('bogus',2) works");

is(Ucam::LookupDB::Person->attrdesc('uid'),'CrsID',
                                         "class attrdesc('uid') works");
is(Ucam::LookupDB::Person->attrdesc('uid',1),'CrsID',
                                         "class attrdesc('uid',1) works");
is(Ucam::LookupDB::Person->attrdesc('uid',2),'CrsIDs',
                                         "class attrdesc('uid',2) works");

is(Ucam::LookupDB::Person->attrdesc('title'),'Role',
                                         "class attrdesc('uid') works");
is(Ucam::LookupDB::Person->attrdesc('title',1),'Role',
                                         "class attrdesc('uid',1) works");
is(Ucam::LookupDB::Person->attrdesc('title',2),'Roles',
                                         "class attrdesc('uid',2) works");

# editable
$me = $db->resultset("Person")->find(WHO);
ok($me->editable,"Me editable");
ok($me->photo_editable,"Me photo editable");

my $alter_ego = $db->resultset("Person")->create( { uid => 'xxx99' } );
ok(!$alter_ego->editable,"Alter-ego not editable");
ok(!$alter_ego->photo_editable,"Alter-ego photo not editable");

my $photo = $alter_ego->create_related('jpegphoto',
           { jpegphoto=>0, editable=>1, suppress=>0, suppressible=>0 });
$alter_ego = $db->resultset("Person")->find('xxx99');
ok(!$alter_ego->editable,"Alter-ego still not editable");
ok($alter_ego->photo_editable,"Alter-ego photo now editable");

$photo->editable(0); $photo->update;
$alter_ego = $db->resultset("Person")->find('xxx99');
ok(!$alter_ego->photo_editable,"Alter-ego photo un-editable");

$photo->suppressible(1);  $photo->update;
$alter_ego = $db->resultset("Person")->find('xxx99');
ok($alter_ego->photo_editable,"Alter-ego photo re-editable (suppressible)");

$photo->suppressible(0);  $photo->update;
$alter_ego = $db->resultset("Person")->find('xxx99');
ok(!$alter_ego->photo_editable,"Alter-ego photo no longer editable");

my $photo_addable = $alter_ego->create_related('jpegphoto_addable',
           { addable => 1, suppressible => 1, suppress=>1 });
$alter_ego = $db->resultset("Person")->find('xxx99');
ok(!$alter_ego->photo_editable,"Alter-ego photo not yet editable (addable)");

$photo->delete;
$alter_ego = $db->resultset("Person")->find('xxx99');
ok($alter_ego->photo_editable,"Alter-ego photo now editable (addable)");

my $phone = $alter_ego->create_related('phone',
           { phone=>'123', editable=>1, suppress=>0, suppressible=>0 });
ok($alter_ego->editable,"Alter-ego now editable");

$phone->editable(0); $phone->update;
ok(!$alter_ego->editable,"Alter-ego un-editable");

$phone->suppressible(1);  $phone->update;
ok($alter_ego->editable,"Alter-ego re-editable (suppressible)");

$phone->suppressible(0);  $phone->update;
ok(!$alter_ego->editable,"Alter-ego no longer editable");

# Note: can't alter addable state of existing records - no PK
$alter_ego->create_related('phone_addable',
           { addable => 0, suppressible => 1, suppress=>1 });
ok(!$alter_ego->editable,"Alter-ego phone not yet editable (addable)");
$alter_ego->create_related('mail_addable',
                           { addable => 1, suppressible => 1, suppress=>1 });
ok($alter_ego->editable,"Alter-ego phone editable (addable)");
