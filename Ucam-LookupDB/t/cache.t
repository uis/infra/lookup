#!/usr/bin/perl -w

use strict;

use lib qw(t);
use TestSetup;
use Ucam::LookupDB;
use Data::Dumper;

use Test::More 'no_plan';
use Test::NoWarnings;

use constant WHO  => 'fjc55';
use constant WHAT => 'IS';
use constant GRP  => '123456';

my $db = TestSetup->init;
BAIL_OUT("Can't connect to database") unless $db;
isa_ok($db,"DBIx::Class::Schema");

# person
my $me = $db->resultset("Person")->search
    ({'me.uid' => WHO},
     {join     => ['registeredname','displayname'],
      prefetch => ['registeredname','displayname']})->first;
isa_ok($me,"Ucam::LookupDB::Person");

is($me->uid,WHO,"Got UID");
is($me->registeredname,'Dr FJ Clark','Got registeredname');
is($me->displayname,'Fred Clark','Got displayname');

is($me->full_name,'Fred Clark','Got name 1');
is($me->full_name,'Fred Clark','Got name again');

is($me->inst->first->ou,'Deaprtment of Important Studies','Got ou 3');
is($me->grp->first->grouptitle,'IS Editors','Grouptitle');

$me = $db->resultset("Person")->std_search->search({'me.uid' => WHO})->first;
isa_ok($me,"Ucam::LookupDB::Person");

# inst
my $it = $db->resultset("Inst")->search
    ({'me.instid' => WHAT},
     {join     => ['ou'],
      prefetch => ['ou']})->first;
isa_ok($it,"Ucam::LookupDB::Inst");

is($it->instid,WHAT,"Got INSTID");
is($it->ou,'Deaprtment of Important Studies','Got ou');

is($it->full_name,'Deaprtment of Important Studies','Got name');

is($it->person->first->full_name,'Fred Clark','Got 1st member name');
is($it->parent->first->ou,'Deaprtment of Important Studies','Parent');
is($it->child->first->ou,'Deaprtment of Important Studies','Child');
is($it->grp->first->grouptitle,'IS Editors','Group');
is($it->manager->first->grouptitle,'IS Editors','Manager');

$me = $db->resultset("Inst")->std_search->search({'me.instid' => WHAT})->first;
isa_ok($me,"Ucam::LookupDB::Inst");

# group
my $grp = $db->resultset("Group")->search
    ({'me.groupid' => GRP},
     {join     => ['grouptitle'], 
      prefetch => ['grouptitle']})->first;
isa_ok($grp,"Ucam::LookupDB::Group");

is($grp->groupid,GRP,"Got groupid");
is($grp->grouptitle,'IS Editors','Got gropuptitle');

is($grp->full_name,'IS Editors','Got name');

is($grp->member->first->full_name,'Fred Clark','member');
is($grp->manager->first->full_name,'IS Editors','Manager');
is($grp->managed_group->first->full_name,'IS Editors','Managed group');
is($grp->reader->first->full_name,'IS Editors','Reader');
is($grp->readable_group->first->full_name,'IS Editors','Readable group');
is($grp->managed_inst->first->full_name,'Deaprtment of Important Studies','managed inst');
is($grp->owner->first->full_name,'Deaprtment of Important Studies','Owner');

$me = $db->resultset("Group")->std_search->search({'me.groupid' => GRP})->first;
isa_ok($me,"Ucam::LookupDB::Group");

# testing
$me = $db->resultset("Person")->search
    ({'me.uid' => WHO},
     {join     => ['registeredname'],
      prefetch => ['registeredname']})->first;
isa_ok($me,"Ucam::LookupDB::Person");

is($me->registeredname->sn,'Clark','Surname 1');
is($me->registeredname->sn,'Clark','Surname 2');
is($me->registeredname->sn,'Clark','Surname 3');

is($me->displayname,'Fred Clark','Displayname 1');
is($me->displayname,'Fred Clark','Displayname 2');
is($me->displayname,'Fred Clark','Displayname 3');

ok(!$me->jpegphoto);
ok(!$me->jpegphoto);
ok(!$me->jpegphoto);

$me->create_related('jpegphoto',
           { jpegphoto=>0, editable=>1, suppress=>0, suppressible=>0 });

ok($me->jpegphoto);
ok($me->jpegphoto);
ok($me->jpegphoto);


my $me_rs = $db->resultset("Person")->search
    ({'me.uid' => WHO},
     {join     => ['registeredname'],
      prefetch => ['registeredname']});
$me = $me_rs->search({},
                     {join     => ['displayname'],
                      prefetch => ['displayname']})->first;
isa_ok($me,"Ucam::LookupDB::Person");

