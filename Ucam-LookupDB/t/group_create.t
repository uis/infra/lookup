#!/usr/bin/perl

use warnings;
use strict;

use Ucam::LookupDB;

use Test::More 'no_plan';
use Test::NoWarnings;

use constant WHAT => 'IS';

my $db = Ucam::LookupDB->connect('dbi:Pg:dbname=jw35');
BAIL_OUT("Can't connect to database") unless $db;
isa_ok($db,"DBIx::Class::Schema");

# Basic retrieve
my $inst = $db->resultset("Inst")->find(WHAT);
isa_ok($inst,"Ucam::LookupDB::Inst");
is($inst->instid,WHAT);

# Make a group
my $group =  $inst->create_related('new_group',{});
isa_ok($group,"Ucam::LookupDB::GroupCreate");


