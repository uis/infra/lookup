#!/usr/bin/perl

use warnings;
use strict;

use lib qw(t);
use TestSetup;
use Ucam::LookupDB;

use Test::More 'no_plan';
use Test::NoWarnings;

use constant WHAT => '123456';
use constant TITLE  => 'IS Editors';

my $db = TestSetup->init;
BAIL_OUT("Can't connect to database") unless $db;
isa_ok($db,"DBIx::Class::Schema");

# Failed lookup
is($db->resultset("Group")->find("987654"),undef);

# Basic retrieve
my $me = $db->resultset("Group")->find(WHAT);
isa_ok($me,"Ucam::LookupDB::Group");

my %accessors = 
    ( groupid                 => "scalar",
      group_member            => "1:GroupMember",
      member                  => "1:Person",
      group_member_addable    => "GroupMemberAddable",
      group_mgrgroup          => "1:GroupMgrgroup",
      manager                 => "1:Group",
      mgrgroup_group          => "1:GroupMgrgroup",
      managed_group           => "1:Group",
      group_mgrgroup_addable  => "GroupMgrgroupAddable",
      group_rdrgroup          => "1:GroupRdrgroup",
      reader                  => "1:Group",
      rdrgroup_group          => "1:GroupRdrgroup",
      readable_group          => "1:Group",
      group_rdrgroup_addable  => "GroupRdrgroupAddable",
      inst_mgrgroup           => "1:InstMgrgroup",
      managed_inst            => "1:Inst",
      group_inst              => "1:InstGroup",
      owner                   => "1:Inst",
      grouptitle              => "GroupGrouptitle",
      grouptitle_addable      => "GroupGrouptitleAddable",
      description             => "GroupDescription",
      description_addable     => "GroupDescriptionAddable",
      mail                    => "1:GroupMail",
      mail_addable            => "GroupMailAddable",
      expirytimestamp         => "GroupExpirytimestamp",
      expirytimestamp_addable => "GroupExpirytimestampAddable",
      warntimestamp           => "GroupWarntimestamp",
      warntimestamp_addable   => "GroupWarntimestampAddable",
      visibility              => "GroupVisibility",
      visibility_addable      => "GroupVisibilityAddable",
 );

TestSetup->check_accessors($me,%accessors);

# Inflation
isa_ok($me->expirytimestamp->expirytimestamp,"DateTime");
isa_ok($me->warntimestamp->warntimestamp,"DateTime");

# Stringification
is($me->full_name,TITLE);
is($me,TITLE);
is($me->expirytimestamp,"2006-03-05T12:24:56");
is($me->warntimestamp,"2006-03-05T12:24:56");

# Searching
my $result;
$result = $db->resultset("Group")->search({ groupid => WHAT });
isa_ok($result,"DBIx::Class::ResultSet");
is($result->count,1,"1 result for groupid => '" . WHAT . "'");
isa_ok($result->first,"Ucam::LookupDB::Group");

$result = $db->resultset("Group")->search
    (
     {
      'grouptitle.grouptitle' => TITLE
  },
     {
      join => [qw/grouptitle/],
      prefetch => [qw/grouptitle/],
  }
 );
isa_ok($result,"DBIx::Class::ResultSet");
is($result->count,1,"1 result for title.grouptitle => '" . TITLE . "'");
my $row = $result->first;
isa_ok($row,"Ucam::LookupDB::Group");
is($row->groupid,WHAT,"groupid is '" . WHAT . "'");
isa_ok($row->grouptitle,"Ucam::LookupDB::GroupGrouptitle");
is($row->grouptitle->grouptitle,TITLE,"grouptitle is '" . TITLE ."'");
is($row->grouptitle,TITLE,"grouptitle is '" . TITLE ."'");

$result = $db->resultset("Group")->search_related
    ('grouptitle',{ grouptitle => TITLE });
isa_ok($result,"DBIx::Class::ResultSet");
is($result->count,1,"1 result for related grouptitle => '" . TITLE . "'");
isa_ok($result->first,"Ucam::LookupDB::GroupGrouptitle");

# attrdesc
$me = $db->resultset("Group")->find(WHAT);
is($me->attrdesc('groupid'),'GroupID',"attrdesc('groupid') works");
is($me->attrdesc('groupid',1),'GroupID',"attrdesc('groupid',1) works");
is($me->attrdesc('groupid',2),'GroupIDs',"attrdesc('groupid',2) works");
is($me->attrdesc('grouptitle'),'Title',"attrdesc('grouptitle') works");
is($me->attrdesc('grouptitle',1),'Title',"attrdesc('grouptitle',1) works");
is($me->attrdesc('grouptitle',2),'Titles',"attrdesc('grouptitle',2) works");
is($me->attrdesc('group_member'),'group_member',
                                         "attrdesc('group_member') works");
is($me->attrdesc('group_member',2),'group_members',
                                         "attrdesc('group_members',2) works");
is($me->attrdesc('bogus'),'bogus',"attrdesc('bogus') works");
is($me->attrdesc('bogus',2),'boguses',"attrdesc('bogus',2) works");

# editable
$me = $db->resultset("Group")->find(WHAT);
ok($me->editable,"Me editable");

my $alter_ego = $db->resultset("Group")->create( { groupid => '999999' } );
ok(!$alter_ego->editable,"Alter-ego not editable");

my $description = $alter_ego->create_related('description',
           { description => 'Test', editable => 1 });
$alter_ego = $db->resultset("Group")->find('999999');
ok($alter_ego->editable,"Alter-ego now editable");

$description->editable(0); $description->update;
$alter_ego = $db->resultset("Group")->find('999999');
ok(!$alter_ego->editable,"Alter-ego un-editable");

# Note: can't alter addable state of existing records - no PK
$alter_ego->create_related('description_addable', { addable => 0 });
ok(!$alter_ego->editable,"Alter-ego not yet editable (addable)");
$alter_ego->create_related('grouptitle_addable', { addable => 1 });
ok($alter_ego->editable,"Alter-ego editable (addable)");
