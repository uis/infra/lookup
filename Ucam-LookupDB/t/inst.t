#!/usr/bin/perl

use strict;
use warnings;

use lib qw(t);
use TestSetup;
use Ucam::LookupDB;

use Test::More 'no_plan';
use Test::NoWarnings;

use constant WHAT => 'IS';
use constant OU  => 'Deaprtment of Important Studies';

my $db = TestSetup->init;
BAIL_OUT("Can't connect to database") unless $db;
isa_ok($db,"DBIx::Class::Schema");

# Failed lookup
is($db->resultset("Inst")->find("ZZZ"),undef);

# Basic retrieve
my $me = $db->resultset("Inst")->find(WHAT);
isa_ok($me,"Ucam::LookupDB::Inst");

my %accessors = 
    ( instid                     => "scalar",
      inst_person                => "1:PersonInst",
      person                     => "1:Person",
      person_addable             => "1:PersonInstAddable",
      inst_parent                => "1:InstParentinst",
      parent                     => "1:Inst",
      inst_child                 => "1:InstParentinst",
      child                      => "1:Inst",
      parent_addable             => "InstParentinstAddable",
      inst_group                 => "1:InstGroup",
      grp                        => "1:Group",
      group_addable              => "InstGroupAddable",
      inst_mgrgroup              => "1:InstMgrgroup",
      manager                    => "1:Group",
      mgrgroup_addable           => "InstMgrgroupAddable",
      ou                         => "InstOu",
      ou_addable                 => "InstOuAddable",
      jpegphoto                  => undef,
      jpegphoto_addable          => "InstJpegphotoAddable",
      postaladdress              => "1:InstPostaladdress",
      postaladdress_addable      => "InstPostaladdressAddable",
      mail                       => "1:InstMail",
      mail_addable               => "InstMailAddable",
      phone                      => "1:InstPhone",
      phone_addable              => "InstPhoneAddable",
      facsimiletelephonenumber   => "1:InstFacsimiletelephonenumber",
      facsimiletelephonenumber_addable =>
                                    "InstFacsimiletelephonenumberAddable",
      labeleduri                 => "1:InstLabeleduri",
      labeleduri_addable         => "InstLabeleduriAddable",
      acronym                    => "2:InstAcronym",
      acronym_addable            => "InstAcronymAddable",
 );

TestSetup->check_accessors($me,%accessors);

# Stringification
is($me->full_name,OU);
is($me,OU);

# Searching
my $result;
$result = $db->resultset("Inst")->search({ instid => WHAT });
isa_ok($result,"DBIx::Class::ResultSet");
is($result->count,1,"1 result for instid => '" . WHAT . "'");
isa_ok($result->first,"Ucam::LookupDB::Inst");

$result = $db->resultset("Inst")->search
    (
     {
      'ou.ou' => OU
  },
     {
      join => [qw/ou/],
      prefetch => [qw/ou/],
  }
 );
isa_ok($result,"DBIx::Class::ResultSet");
is($result->count,1,"1 result for ou.ou => '" . OU . "'");
my $row = $result->first;
isa_ok($row,"Ucam::LookupDB::Inst");
is($row->instid,WHAT,"instid is '" . WHAT . "'");
isa_ok($row->ou,"Ucam::LookupDB::InstOu");
is($row->ou->ou,OU,"ou is '" . OU ."'");
is($row->ou,OU,"ou is '" . OU ."'");

$result = $db->resultset("Inst")->search_related
    ('ou',{ ou => OU });
isa_ok($result,"DBIx::Class::ResultSet");
is($result->count,1,"1 result for related ou => '" . OU . "'");
isa_ok($result->first,"Ucam::LookupDB::InstOu");

# attrdesc
$me = $db->resultset("Inst")->find(WHAT);
is($me->attrdesc('instid'),'InstID',"attrdesc('instid') works");
is($me->attrdesc('instid',1),'InstID',"attrdesc('instid',1) works");
is($me->attrdesc('instid',2),'InstIDs',"attrdesc('instid',2) works");
is($me->attrdesc('acronym'),'Acronyms',"attrdesc('acronym') works");
is($me->attrdesc('acronym',1),'Acronym',"attrdesc('acronym',1) works");
is($me->attrdesc('acronym',2),'Acronyms',"attrdesc('acronym',2) works");
is($me->attrdesc('inst_person'),'inst_person',
                                         "attrdesc('inst_person') works");
is($me->attrdesc('inst_person',2),'inst_persons',
                                         "attrdesc('inst-person',2) works");
is($me->attrdesc('bogus'),'bogus',"attrdesc('bogus') works");
is($me->attrdesc('bogus',2),'boguses',"attrdesc('bogus',2) works");

# editable
$me = $db->resultset("Inst")->find(WHAT);
ok($me->editable,"Me editable");
ok($me->photo_editable,"Me photo editable");

# ... create new inst
ok($db->resultset("Inst")->create( { instid => 'ZZZ' } ));
ok(my $alter_ego = $db->resultset("Inst")->find('ZZZ'));


# ... shouldn't be editable or photo_editable
ok(!$alter_ego->editable,"Alter-ego not editable");
ok(!$alter_ego->photo_editable,"Alter-ego photo not editable");

# ... add an edditable photo
my $photo = $alter_ego->create_related('jpegphoto',
           { jpegphoto => 0, editable => 1 });

# ... shouldn't be editable, but should now be photo_editable
$alter_ego = $db->resultset("Inst")->find('ZZZ');
ok(!$alter_ego->editable,"Alter-ego still not editable");
ok($alter_ego->photo_editable,"Alter-ego photo now editable");

# ... make photo un-editable
$photo->editable(0); $photo->update;
$alter_ego = $db->resultset("Inst")->find('ZZZ');

# ... now shouldn't be photo_editable
ok(!$alter_ego->photo_editable,"Alter-ego photo un-editable");

# ... make photos addable
$alter_ego->create_related('jpegphoto_addable', { addable => 1 });
$alter_ego = $db->resultset("Inst")->find('ZZZ');

# ... still shouldn't be photo_editable becasue of existing photo
ok(!$alter_ego->photo_editable,"Alter-ego photo not editable (addable)");

# ... delete photo
$photo->delete;
$alter_ego = $db->resultset("Inst")->find('ZZZ');

# ... should now be editable (becasue addable)
ok($alter_ego->photo_editable,"Alter-ego photo editable (addable)");

# ... repeat with phone
my $phone = $alter_ego->create_related('phone',
           { phone => '123', editable => 1 });

$alter_ego = $db->resultset("Inst")->find('ZZZ');
ok($alter_ego->editable,"Alter-ego now editable");

$phone->editable(0); $phone->update;
$alter_ego = $db->resultset("Inst")->find('ZZZ');
ok(!$alter_ego->editable,"Alter-ego un-editable");

# Note: can't alter addable state of existing records - no PK
$alter_ego->create_related('phone_addable', { addable => 0 });
$alter_ego = $db->resultset("Inst")->find('ZZZ');
ok(!$alter_ego->editable,"Alter-ego not yet editable (addable)");

$alter_ego->create_related('ou_addable', { addable => 1 });
$alter_ego = $db->resultset("Inst")->find('ZZZ');
ok($alter_ego->editable,"Alter-ego editable (addable)");
