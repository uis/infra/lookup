#!/usr/bin/perl -w

use strict;

use lib qw(t);
use TestSetup;
use Ucam::LookupDB;

use Test::More 'no_plan';
use Test::NoWarnings;

use constant WHO => 'fjc55';
use constant SN  => 'Clark';

my $db = TestSetup->init;
isa_ok($db,"DBIx::Class::Schema");

my $me;

# attrdesc
$me = $db->resultset("Person")->find(WHO);
is($me->attrdesc('uid'),'CrsID',"attrdesc('uid') works");
is($me->attrdesc('uid',1),'CrsID',"attrdesc('uid',1) works");
is($me->attrdesc('uid',2),'CrsIDs',"attrdesc('uid',2) works");
is($me->attrdesc('title'),'Roles',"attrdesc('title') works");
is($me->attrdesc('title',1),'Role',"attrdesc('title',1) works");
is($me->attrdesc('title',2),'Roles',"attrdesc('title',2) works");
is($me->attrdesc('person_inst'),'person_inst',
                                         "attrdesc('person_inst') works");
is($me->attrdesc('person_inst',2),'person_insts',
                                         "attrdesc('person_insts',2) works");
is($me->attrdesc('bogus'),'bogus',"attrdesc('bogus') works");
is($me->attrdesc('bogus',2),'boguses',"attrdesc('bogus',2) works");

is(Ucam::LookupDB::Person->attrdesc('uid'),'CrsID',
                                         "class attrdesc('uid') works");
is(Ucam::LookupDB::Person->attrdesc('uid',1),'CrsID',
                                         "class attrdesc('uid',1) works");
is(Ucam::LookupDB::Person->attrdesc('uid',2),'CrsIDs',
                                         "class attrdesc('uid',2) works");

is(Ucam::LookupDB::Person->attrdesc('title'),'Role',
                                         "class attrdesc('uid') works");
is(Ucam::LookupDB::Person->attrdesc('title',1),'Role',
                                         "class attrdesc('uid',1) works");
is(Ucam::LookupDB::Person->attrdesc('title',2),'Roles',
                                         "class attrdesc('uid',2) works");

# is_empty
$me = $db->resultset("Person")->new( {} );
my $name = $me->new_related('registeredname', { } );
ok(defined($name),'New name?');
ok($name->is_empty,'Empty name is empty');
$name->suppress(1);
ok($name->is_empty,'Suppressed (only) name is empty');
$name->sn('Jones');
ok(!$name->is_empty,'Not empty if it has a sn');
