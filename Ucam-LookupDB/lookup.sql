CREATE DOMAIN uid AS varchar(7);
CREATE DOMAIN instid AS varchar(7);
CREATE DOMAIN groupid AS varchar(1024);
CREATE DOMAIN generic_name AS varchar(1024);
CREATE DOMAIN uri AS varchar(1024);
CREATE DOMAIN email_addr AS varchar(1024);
CREATE DOMAIN phone_number AS varchar(1024);
CREATE DOMAIN postal_addr AS varchar(1024);
CREATE DOMAIN jpeg AS bytea;
CREATE DOMAIN visibility varchar(8)
  CONSTRAINT visibility_syntax
    CHECK (VALUE = 'cam' OR VALUE = 'members' OR VALUE = 'managers');

CREATE SEQUENCE rowid;

-- ============
-- Primary keys
-- ============

CREATE TABLE people
  (uid uid PRIMARY KEY);

CREATE TABLE insts
  (instid instid PRIMARY KEY);

CREATE TABLE groups
  (groupid groupid PRIMARY KEY);

-- =========================
-- Relations between objects
-- =========================

CREATE TABLE person_inst
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts NOT NULL,
   editable boolean NOT NULL DEFAULT TRUE,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY);

-- Note absence of "suppression" and "suppressible".

CREATE TABLE person_inst_addable
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts NOT NULL,
   addable boolean NOT NULL,
   suppressible boolean NOT NULL,
   suppress boolean NOT NULL);

CREATE TABLE group_member
  (groupid groupid REFERENCES groups NOT NULL,
   uid uid REFERENCES people NOT NULL,
   editable boolean NOT NULL DEFAULT TRUE,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY);

-- ***jw35: perhaps this table should be called group_person?

CREATE TABLE group_member_addable
 (groupid groupid REFERENCES groups NOT NULL,
  addable boolean NOT NULL);

CREATE TABLE group_mgrgroupid
  (groupid groupid REFERENCES groups NOT NULL,
   mgrgroupid groupid REFERENCES groups NOT NULL,
   editable boolean NOT NULL DEFAULT TRUE,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY);

CREATE TABLE group_mgrgroupid_addable
  (groupid groupid REFERENCES groups NOT NULL,
   addable boolean NOT NULL);

CREATE TABLE group_rdrgroupid
  (groupid groupid REFERENCES groups NOT NULL,
   rdrgroupid groupid REFERENCES groups NOT NULL,
   editable boolean NOT NULL DEFAULT TRUE,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY);

CREATE TABLE group_rdrgroupid_addable
  (groupid groupid REFERENCES groups NOT NULL,
   addable boolean NOT NULL);

CREATE TABLE group_visibility
  (groupid groupid REFERENCES groups NOT NULL,
   visibility visibility NOT NULL,
   editable boolean NOT NULL DEFAULT TRUE,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY);

CREATE TABLE group_visibility_addable
  (groupid groupid REFERENCES groups NOT NULL,
   addable boolean NOT NULL);

-- ====================
-- Attributes of people
-- ====================

CREATE TABLE person_title
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   title generic_name NOT NULL,
   suppress boolean NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE,
   suppressible boolean NOT NULL DEFAULT TRUE);

CREATE TABLE person_title_addable
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   addable boolean NOT NULL,
   suppressible boolean NOT NULL,
   suppress boolean NOT NULL);

CREATE TABLE person_registeredname
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   sn generic_name NOT NULL,
   initials generic_name NOT NULL,
   titles generic_name NOT NULL,
   suppress boolean NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE,
   suppressible boolean NOT NULL DEFAULT TRUE);

-- **jw35: it would help if we used seperate names for 'titles' as in 
-- Mr, Mrs, Sir, and 'titles' as in 'roles'. Perhaps use 'personal_title' 
-- here or 'role' there?

CREATE TABLE person_registeredname_addable
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   addable boolean NOT NULL,
   suppressible boolean NOT NULL,
   suppress boolean NOT NULL);

CREATE TABLE person_displayname
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   displayname generic_name NOT NULL,
   suppress boolean NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE,
   suppressible boolean NOT NULL DEFAULT TRUE);

CREATE TABLE person_displayname_addable
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   addable boolean NOT NULL,
   suppressible boolean NOT NULL,
   suppress boolean NOT NULL);

CREATE TABLE person_mail
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   mail email_addr NOT NULL,
   suppress boolean NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE,
   suppressible boolean NOT NULL DEFAULT TRUE);

CREATE TABLE person_mail_addable
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   addable boolean NOT NULL,
   suppressible boolean NOT NULL,
   suppress boolean NOT NULL);

CREATE TABLE person_labeleduri
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   uri uri NOT NULL,
   label generic_name,
   suppress boolean NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE,
   suppressible boolean NOT NULL DEFAULT TRUE);

CREATE TABLE person_labeleduri_addable
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   addable boolean NOT NULL,
   suppressible boolean NOT NULL,
   suppress boolean NOT NULL);

CREATE TABLE person_phone
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   phone phone_number NOT NULL,
   label generic_name,
   suppress boolean NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE,
   suppressible boolean NOT NULL DEFAULT TRUE);

CREATE TABLE person_phone_addable
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   addable boolean NOT NULL,
   suppressible boolean NOT NULL,
   suppress boolean NOT NULL);

CREATE TABLE person_postaladdress
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   postaladdress postal_addr NOT NULL,
   suppress boolean NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE,
   suppressible boolean NOT NULL DEFAULT TRUE);

CREATE TABLE person_postaladdress_addable
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   addable boolean NOT NULL,
   suppressible boolean NOT NULL,
   suppress boolean NOT NULL);

CREATE TABLE person_jpegphoto
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   jpegphoto jpeg NOT NULL,
   suppress boolean NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE,
   suppressible boolean NOT NULL DEFAULT TRUE);

CREATE TABLE person_jpegphoto_addable
  (uid uid REFERENCES people NOT NULL,
   instid instid REFERENCES insts,
   addable boolean NOT NULL,
   suppressible boolean NOT NULL,
   suppress boolean NOT NULL);

-- (most other attributes will look similar)

-- ==========================
-- Attributes of institutions
-- ==========================

CREATE TABLE inst_ou
  (instid instid REFERENCES insts NOT NULL UNIQUE,
   ou generic_name NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

-- Note absence of "suppression" and "suppressible".

-- jw35: would it be more logical to use 'name' (or similar) in place of 'ou'?

CREATE TABLE inst_ou_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_parentinstid
  (instid instid REFERENCES insts NOT NULL,
   parentinstid instid REFERENCES insts NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_parentinstid_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_groupid
  (instid instid REFERENCES insts NOT NULL,
   groupid groupid REFERENCES groups NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_groupid_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_jpegphoto
  (instid instid REFERENCES insts NOT NULL UNIQUE,
   jpegphoto jpeg NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_jpegphoto_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_postaladdress
  (instid instid REFERENCES insts NOT NULL,
   postaladdress postal_addr NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_postaladdress_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_mail
  (instid instid REFERENCES insts NOT NULL,
   mail email_addr NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_mail_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_phone
  (instid instid REFERENCES insts NOT NULL,
   phone phone_number NOT NULL,
   label generic_name,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_phone_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_facsimiletelephonenumber
  (instid instid REFERENCES insts NOT NULL,
   facsimiletelephonenumber phone_number NOT NULL,
   label generic_name,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_facsimiletelephonenumber_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_labeleduri
  (instid instid REFERENCES insts NOT NULL,
   uri uri NOT NULL,
   label generic_name,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_labeleduri_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_acronym
  (instid instid REFERENCES insts NOT NULL,
   acronym generic_name NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_acronym_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

CREATE TABLE inst_mgrgroupid
  (instid instid REFERENCES insts NOT NULL,
   mgrgroupid groupid REFERENCES groups NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE inst_mgrgroupid_addable
  (instid instid REFERENCES insts,
   addable boolean NOT NULL);

-- ====================
-- Attributes of groups
-- ====================

CREATE TABLE group_grouptitle
  (groupid groupid REFERENCES groups NOT NULL,
   grouptitle generic_name NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE group_grouptitle_addable
  (groupid groupid REFERENCES groups NOT NULL,
   addable boolean NOT NULL);

CREATE TABLE group_description
  (groupid groupid REFERENCES groups NOT NULL,
   description generic_name NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE group_description_addable
  (groupid groupid REFERENCES groups NOT NULL,
   addable boolean NOT NULL);

CREATE TABLE group_mail
  (groupid groupid REFERENCES groups NOT NULL,
   mail email_addr NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE group_mail_addable
  (groupid groupid REFERENCES groups NOT NULL,
   addable boolean NOT NULL);

CREATE TABLE group_expirytimestamp
  (groupid groupid REFERENCES groups NOT NULL,
   expirytimestamp time stamp with time zone NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE group_expirytimestamp_addable
  (groupid groupid REFERENCES groups NOT NULL,
   addable boolean NOT NULL);

CREATE TABLE group_warntimestamp
  (groupid groupid REFERENCES groups NOT NULL,
   warntimestamp time stamp with time zone NOT NULL,
   rowid bigint DEFAULT nextval('rowid') PRIMARY KEY,
   editable boolean NOT NULL DEFAULT TRUE);

CREATE TABLE group_warntimestamp_addable
  (groupid groupid REFERENCES groups NOT NULL,
   addable boolean NOT NULL);
