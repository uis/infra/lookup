package Ucam::LookupDBBase;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components(qw[InflateColumn::DateTime ResultSetManager 
                                          UTF8Columns PK::Auto Core]);

=head2 attrdesc

Return human-readable description of an attribute.

=cut

sub attrdesc {
    my $self = shift;
    my ($attr,$howmany) = @_;

    my $desc = $attr;
    $desc = $self->attrdeschash->{$attr}
        if $self->can('attrdeschash') and 
            defined($self->attrdeschash->{$attr});

    $desc = "Delete" if $desc eq "_delete";

    # Find how many if not specified
    if (!defined($howmany) ) {
        $howmany = (ref($self) and $self->can($attr) and ref($self->$attr) and
                    $self->$attr->isa('DBIx::Class::ResultSet')) ?
                        $self->$attr->count : 1;
    }
    if ($howmany > 1) {
        # Could use Lingua::EN::Inflect if things get hairy
        if ($desc !~ / by| to| for|manages/i) {
            $desc =~ /[sxz]$/ and $desc .= 'e';
            $desc .= 's';
        }
    }
    return $desc;
}

=head2 is_empty

Test if a record (at least if its from a 'attribute' table) is empty

=cut

sub is_empty {
    my ($self) = @_;
    foreach my $col ($self->result_source->columns) {
        next if $col 
            =~ /^(uid|instid|groupid|rowid|suppress|editable|suppressible)$/;
        return if defined($self->$col) and $self->$col ne '';
    }
    return 1;
}

1;
