package Ucam::LookupDB::InstGroup;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_groupid");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("groupid");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid");
__PACKAGE__->belongs_to("grp",
                        "Ucam::LookupDB::Group",
                       "groupid");

1;
