package Ucam::LookupDB::Person;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

my $keep = { cascade_delete => 0 };

__PACKAGE__->table("people");
__PACKAGE__->add_columns("uid");
__PACKAGE__->set_primary_key("uid");

# Many to many relations
__PACKAGE__->has_many    ("person_inst",
                          "Ucam::LookupDB::PersonInst",
                          "uid",
                          $keep);
__PACKAGE__->many_to_many("inst",
                          "person_inst",
                          "inst",
                          { join     => ['ou'],
                            prefetch => ['ou'],
                          }
                         );

__PACKAGE__->has_many    ("inst_addable",
                          "Ucam::LookupDB::PersonInstAddable",
                          "uid",
                          $keep);

__PACKAGE__->has_many    ("group_member",
                          "Ucam::LookupDB::GroupMember",
                          "uid",
                          $keep);
__PACKAGE__->many_to_many("grp",
                          "group_member",
                          "grp",
                          { join     => ['grouptitle'],
                            prefetch => ['grouptitle'],
                          }
                         );

# Attributes
__PACKAGE__->has_many    ("title",
                          "Ucam::LookupDB::PersonTitle",
                          "uid",
                          { %$keep,
                            join     => { 'inst' => 'ou' },
                            prefetch => { 'inst' => 'ou' },
                          }
                         );
__PACKAGE__->has_many   ("title_addable",
                          "Ucam::LookupDB::PersonTitleAddable",
                          "uid",
                          $keep);

__PACKAGE__->might_have  ("registeredname",
                          "Ucam::LookupDB::PersonRegisteredname",
                          "uid",
                          { %$keep,
                            join     => { 'inst' => 'ou' },
                            prefetch => { 'inst' => 'ou' },
                          }
                         );
__PACKAGE__->has_many    ("registeredname_addable",
                          "Ucam::LookupDB::PersonRegisterednameAddable",
                          "uid",
                          $keep);

__PACKAGE__->might_have  ("displayname",
                          "Ucam::LookupDB::PersonDisplayname",
                          "uid",
                          { %$keep,
                            join     => { 'inst' => 'ou' },
                            prefetch => { 'inst' => 'ou' },
                          }
                         );
__PACKAGE__->has_many    ("displayname_addable",
                          "Ucam::LookupDB::PersonDisplaynameAddable",
                          "uid",
                          $keep);

__PACKAGE__->has_many    ("mail",
                          "Ucam::LookupDB::PersonMail",
                          "uid",
                          { %$keep,
                            join     => { 'inst' => 'ou' },
                            prefetch => { 'inst' => 'ou' },
                          }
                         );
__PACKAGE__->has_many    ("mail_addable",
                          "Ucam::LookupDB::PersonMailAddable",
                          "uid",
                          $keep);

__PACKAGE__->has_many    ("labeleduri",
                          "Ucam::LookupDB::PersonLabeleduri",
                          "uid",
                          { %$keep,
                            join     => { 'inst' => 'ou' },
                            prefetch => { 'inst' => 'ou' },
                          }
                         );
__PACKAGE__->has_many    ("labeleduri_addable",
                          "Ucam::LookupDB::PersonLabeleduriAddable",
                          "uid",
                          $keep);

__PACKAGE__->has_many    ("phone",
                          "Ucam::LookupDB::PersonPhone",
                          "uid",
                          { %$keep,
                            join     => { 'inst' => 'ou' },
                            prefetch => { 'inst' => 'ou' },
                          }
                         );
__PACKAGE__->has_many    ("phone_addable",
                          "Ucam::LookupDB::PersonPhoneAddable",
                          "uid",
                          $keep);

__PACKAGE__->has_many    ("postaladdress",
                          "Ucam::LookupDB::PersonPostaladdress",
                          "uid",
                          { %$keep,
                            join     => { 'inst' => 'ou' },
                            prefetch => { 'inst' => 'ou' },
                          }
                         );
__PACKAGE__->has_many    ("postaladdress_addable",
                          "Ucam::LookupDB::PersonPostaladdressAddable",
                          "uid",
                          $keep);

__PACKAGE__->might_have  ("jpegphoto",
                          "Ucam::LookupDB::PersonJpegphoto",
                          "uid",
                          { %$keep,
                            join     => { 'inst' => 'ou' },
                            prefetch => { 'inst' => 'ou' },
                          }
                         );
__PACKAGE__->has_many    ("jpegphoto_addable",
                          "Ucam::LookupDB::PersonJpegphotoAddable",
                          "uid",
                          $keep);

=head2 attrdeschash

Used by attrdesc() to return a human-readable description of an
attribute.

=cut

use constant attrdeschash =>
{
 uid             => "CrsID",
 inst            => "Institution",
 grp             => "Group",
 title           => "Role",
 registeredname  => "Registered name",
 displayname     => "Display name",
 mail            => "Email address",
 labeleduri      => "Web page",
 phone           => "Phone number",
 postaladdress   => "Address",
 jpegphoto       => "Photograph",
 full_name       => "Name",
};

=head2 std_search

Standard person search

=cut

sub std_search : ResultSet {
    my ($self) = @_;
    return $self->search
        ( {},
          {join     => ['registeredname','displayname'],
           prefetch => ['registeredname','displayname'],
           order_by => 'COALESCE(registeredname.sn,me.uid)',
           cache    => 1,
          }
        );
}

=head2 full_name

Shortcut for whichever of displayname, registeredname or uid exists

=cut

use overload '""' => sub { shift->full_name }, fallback => 1;

sub full_name {
    my $self = shift;
    return $self->displayname || $self->registeredname || $self->uid;
}

=head2 editable

Test if any attributes are editable, suppressible or addable, omitting
jpegphoto and many-to-many relations (essentially "Should there be an
edit button on the profile page?")

=cut

sub editable {
    my $self = shift;

    foreach my $relationship (qw[title registeredname displayname mail
                                 labeleduri phone postaladdress]) {
        foreach my $row ($self->$relationship) {
            return 1 if defined($row) and $row->editable;
            return 1 if defined($row) and $row->suppressible;
        }
        my $addable = "${relationship}_addable";
        foreach my $row ($self->$addable) {
            return 1 if defined($row) and $row->addable;
        }
    }
    return;
}

=head2 photo_editable

Test if an existing photo is editable or suppressible, or if a photo
can be added

=cut

sub photo_editable {
    my $self = shift;

    return ($self->jpegphoto->editable or $self->jpegphoto->suppressible)
        if $self->jpegphoto;
    foreach my $row ($self->jpegphoto_addable) {
        return 1 if $row->addable;
    }
    return;
}

1;
