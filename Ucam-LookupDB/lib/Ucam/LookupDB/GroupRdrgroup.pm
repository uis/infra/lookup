package Ucam::LookupDB::GroupRdrgroup;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("group_rdrgroupid");
__PACKAGE__->add_columns("groupid");
__PACKAGE__->add_columns("rdrgroupid");
__PACKAGE__->add_columns("editable");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("read",
                        "Ucam::LookupDB::Group",
                        "groupid");

__PACKAGE__->belongs_to("reader",
                        "Ucam::LookupDB::Group",
                        "rdrgroupid");

use constant attrdeschash => {
 groupid         => "GroupID",
 rdrgroupid      => "GroupID",
};

1;
