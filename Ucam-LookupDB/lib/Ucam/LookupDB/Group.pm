package Ucam::LookupDB::Group;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

my $keep = { cascade_delete => 0 };

__PACKAGE__->table("groups");
__PACKAGE__->add_columns("groupid");
__PACKAGE__->set_primary_key("groupid");

# Many to many relations
__PACKAGE__->has_many    ("group_member",
                          "Ucam::LookupDB::GroupMember",
                          "groupid",
                          $keep);
__PACKAGE__->many_to_many("member",
                          "group_member",
                          "person",
                          { join     => ['registeredname', 'displayname'],
                            prefetch => ['registeredname', 'displayname'],
                          }
                         );

__PACKAGE__->has_one     ("group_member_addable",
                          "Ucam::LookupDB::GroupMemberAddable",
                          "groupid",
                          $keep);

__PACKAGE__->has_many    ("group_mgrgroup",
                          "Ucam::LookupDB::GroupMgrgroup",
                          "groupid",
                          $keep);
__PACKAGE__->many_to_many("manager",
                          "group_mgrgroup",
                          "manager",
                          { join     => ['grouptitle'],
                            prefetch => ['grouptitle'],
                          }
                         );
__PACKAGE__->has_many    ("mgrgroup_group",
                          "Ucam::LookupDB::GroupMgrgroup",
                          "mgrgroupid",
                          $keep);
__PACKAGE__->many_to_many("managed_group",
                          "mgrgroup_group",
                          "managed",
                          { join     => ['grouptitle'],
                            prefetch => ['grouptitle'],
                          }
                         );

__PACKAGE__->has_one     ("group_mgrgroup_addable",
                          "Ucam::LookupDB::GroupMgrgroupAddable",
                          "groupid",
                          $keep);

__PACKAGE__->has_many    ("group_rdrgroup",
                          "Ucam::LookupDB::GroupRdrgroup",
                          "groupid",
                          $keep);
__PACKAGE__->many_to_many("reader",
                          "group_rdrgroup",
                          "reader",
                          { join     => ['grouptitle'],
                            prefetch => ['grouptitle'],
                          }
                         );
__PACKAGE__->has_many    ("rdrgroup_group",
                          "Ucam::LookupDB::GroupRdrgroup",
                          "rdrgroupid",
                          $keep);
__PACKAGE__->many_to_many("readable_group",
                          "rdrgroup_group",
                          "read",
                          { join     => ['grouptitle'],
                            prefetch => ['grouptitle'],
                          },
                         );

__PACKAGE__->has_one     ("group_rdrgroup_addable",
                          "Ucam::LookupDB::GroupRdrgroupAddable",
                          "groupid",
                          $keep);

__PACKAGE__->has_many    ("inst_mgrgroup",
                          "Ucam::LookupDB::InstMgrgroup",
                          "mgrgroupid",
                          $keep);
__PACKAGE__->many_to_many("managed_inst",
                          "inst_mgrgroup",
                          "inst",
                          { join     => ['ou'],
                            prefetch => ['ou'],
                          }
                         );

__PACKAGE__->has_many    ("group_inst",
                          "Ucam::LookupDB::InstGroup",
                          "groupid",
                          $keep);
__PACKAGE__->many_to_many("owner",
                          "group_inst",
                          "inst",
                          { join     => ['ou'],
                            prefetch => ['ou'],
                          }
                         );

# Other attributes
__PACKAGE__->might_have  ("grouptitle",
                          "Ucam::LookupDB::GroupGrouptitle",
                          "groupid",
                          $keep);

__PACKAGE__->has_one     ("grouptitle_addable",
                          "Ucam::LookupDB::GroupGrouptitleAddable",
                          "groupid",
                          $keep);

__PACKAGE__->might_have  ("description",
                          "Ucam::LookupDB::GroupDescription",
                          "groupid",
                          $keep);

__PACKAGE__->has_one     ("description_addable",
                          "Ucam::LookupDB::GroupDescriptionAddable",
                          "groupid",
                          $keep);

__PACKAGE__->has_many    ("mail",
                          "Ucam::LookupDB::GroupMail",
                          "groupid",
                          $keep);

__PACKAGE__->has_one     ("mail_addable",
                          "Ucam::LookupDB::GroupMailAddable",
                          "groupid",
                          $keep);

__PACKAGE__->might_have  ("expirytimestamp",
                          "Ucam::LookupDB::GroupExpirytimestamp",
                          "groupid",
                          $keep);

__PACKAGE__->has_one     ("expirytimestamp_addable",
                          "Ucam::LookupDB::GroupExpirytimestampAddable",
                          "groupid",
                          $keep);

__PACKAGE__->might_have  ("warntimestamp",
                          "Ucam::LookupDB::GroupWarntimestamp",
                          "groupid",
                          $keep);

__PACKAGE__->has_one     ("warntimestamp_addable",
                          "Ucam::LookupDB::GroupWarntimestampAddable",
                          "groupid",
                          $keep);

__PACKAGE__->might_have  ("visibility",
                          "Ucam::LookupDB::GroupVisibility",
                          "groupid",
                          $keep);

__PACKAGE__->has_one     ("visibility_addable",
                          "Ucam::LookupDB::GroupVisibilityAddable",
                          "groupid",
                          $keep);

=head2 attrdeschash

Used by attrdesc() to return a human-readable description of an
attribute.

=cut

use constant attrdeschash =>
{
 groupid         => "GroupID",
 member          => "Members",
 group_mgrgroup  => "Managed by",
 manager         => "Managed by",
 managed_group   => "Manages",
 group_rdrgroup  => "Privileged access by",
 reader          => "Privileged access by",
 readable_group  => "Privileged access to",
 managed_inst    => "Manages lookup data for",
 owner           => "Belongs to",
 grouptitle      => "Title",
 description     => "Description",
 mail            => "Email address",
 expirytimestamp => "Expiry date",
 warntimestamp   => "Last expiry warning date",
 visibility      => "Visibility",
 full_name      => "Name",
};

=head2 std_search

Standard inst search

=cut

sub std_search : ResultSet {
    my ($self) = @_;
    return $self->search
        ( {},
          {join     => ['grouptitle'],
           prefetch => ['grouptitle'],
           order_by => 'COALESCE(grouptitle.grouptitle,me.groupid)',
           cache    => 1,
          }
        );
}

=head2 name

Shortcut for whichever of ou or instid exists

=cut

use overload '""' => sub { shift->full_name }, fallback => 1;

sub full_name {
    my $self = shift;

    return $self->grouptitle || $self->groupid;

}

=head2 editable

Test if any attributes are editable, suppressible or addable, omitting
jpegphoto and many-to-many relations (essentially "Should there be an
edit button on the profile page?")

=cut

sub editable {
    my $self = shift;

    foreach my $relationship (qw[grouptitle description mail
                                 expirytimestamp warntimestamp]) {
        foreach my $row ($self->$relationship) {
            return 1 if defined($row) and $row->editable;
        }
        my $addable = "${relationship}_addable";
        foreach my $row ($self->$addable) {
            return 1 if defined($row) and $row->addable;
        }
    }
    return;
}

1;
