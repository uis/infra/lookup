package Ucam::LookupDB::GroupMember;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("group_member");
__PACKAGE__->add_columns("groupid");
__PACKAGE__->add_columns("uid");
__PACKAGE__->add_columns("editable");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("grp",
                        "Ucam::LookupDB::Group",
                        "groupid");

__PACKAGE__->belongs_to("person",
                        "Ucam::LookupDB::Person",
                        "uid");

1;

