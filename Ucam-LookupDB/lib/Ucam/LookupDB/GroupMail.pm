package Ucam::LookupDB::GroupMail;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("group_mail");
__PACKAGE__->add_columns("groupid");
__PACKAGE__->add_columns("mail");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("grp",
                        "Ucam::LookupDB::Group",
                        "groupid");

use overload '""' => sub { shift->mail }, fallback => 1;

use constant attrdeschash =>
{ mail         => 'Address',
  editable     => 'Editable',
};

1;

