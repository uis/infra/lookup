package Ucam::LookupDB::PersonRegisteredname;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("person_registeredname");
__PACKAGE__->add_columns("uid");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("titles");
__PACKAGE__->add_columns("initials");
__PACKAGE__->add_columns("sn");
__PACKAGE__->add_columns("uid");
__PACKAGE__->add_columns("suppress");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");
__PACKAGE__->add_columns("suppressible");
__PACKAGE__->set_primary_key("rowid");
__PACKAGE__->utf8_columns("titles","initials","sn");

__PACKAGE__->belongs_to("person",
                        "Ucam::LookupDB::Person",
                        "uid");
__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid",
                        { join_type => 'left' });

=head2 attrdeschash

Used by attrdesc() to return a human-readable description of an
attribute.

=cut

use constant attrdeschash =>
{ instid       => 'Institution',
  sn           => 'Surname',
  initials     => 'Initials',
  titles       => 'Titles',
  suppress     => 'Suppress',
  editable     => 'Editable',
  suppressible => 'Suppressible',
};

=head2 registeredname

Re-joins titles, initials, sn into text-form registeredname

=cut

__PACKAGE__->mk_classaccessor(qw/ _registeredname /);

use overload '""' => sub { shift->registeredname }, fallback => 1;

sub registeredname {
    my ($self) = @_;

    unless ($self->_registeredname) {
        $self->_registeredname(join(" ", grep { defined($_) and $_ ne '' }
                              $self->titles, $self->initials, $self->sn));
    }
    return $self->_registeredname;
}

1;

