package Ucam::LookupDB::InstPostaladdress;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_postaladdress");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("postaladdress");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");
__PACKAGE__->utf8_columns("postaladdress");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid");

use overload '""' => sub { shift->postaladdress }, fallback => 1;

use constant attrdeschash =>
{ postaladdress  => 'Address',
  editable       => 'Editable',
};

1;

