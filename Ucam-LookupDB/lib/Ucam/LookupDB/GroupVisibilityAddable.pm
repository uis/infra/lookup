package Ucam::LookupDB::GroupVisibilityAddable;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("group_visibility_addable");
__PACKAGE__->add_columns("groupid","addable");
__PACKAGE__->set_primary_key("groupid");

__PACKAGE__->belongs_to("grp",
                        "Ucam::LookupDB::Group",
                        "groupid");

1;

