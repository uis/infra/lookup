package Ucam::LookupDB::InstOu;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_ou");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("ou");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");
__PACKAGE__->utf8_columns("ou");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid");

use overload '""' => sub { shift->ou }, fallback => 1;

use constant attrdeschash =>
{ ou       => "Name",
  editable => 'Editable',
};

1;

