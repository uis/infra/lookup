package Ucam::LookupDB::InstLabeleduri;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_labeleduri");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("uri");
__PACKAGE__->add_columns("label");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");
__PACKAGE__->utf8_columns("uri");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid");

use overload '""' => sub { shift->uri }, fallback => 1;

use constant attrdeschash =>
{ uri          => 'URL',
  label        => 'Comment',
  editable     => 'Editable',
};

1;

