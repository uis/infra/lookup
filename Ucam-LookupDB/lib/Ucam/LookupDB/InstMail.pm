package Ucam::LookupDB::InstMail;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_mail");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("mail");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid");

use overload '""' => sub { shift->mail }, fallback => 1;

use constant attrdeschash =>
{ mail         => 'Address',
  editable     => 'Editable',
};

1;

