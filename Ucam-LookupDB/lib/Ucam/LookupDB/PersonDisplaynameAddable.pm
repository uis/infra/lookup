package Ucam::LookupDB::PersonDisplaynameAddable;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("person_displayname_addable");
__PACKAGE__->add_columns("uid");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("addable",);
__PACKAGE__->add_columns("suppressible");
__PACKAGE__->add_columns("suppress");

__PACKAGE__->belongs_to("person",
                        "Ucam::LookupDB::Person",
                        "uid");

=head2 are_adable

Return rows for which addable is true

=cut

sub are_addable : ResultSet {
    my ($self) = @_;
    return $self->search
        ( {addable  => \'=TRUE'},
          {order_by => 'instid'}
        );
}

1;

