package Ucam::LookupDB::PersonLabeleduri;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("person_labeleduri");
__PACKAGE__->add_columns("uid");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("uri");
__PACKAGE__->add_columns("label");
__PACKAGE__->add_columns("suppress");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");
__PACKAGE__->add_columns("suppressible");

__PACKAGE__->set_primary_key("rowid");
__PACKAGE__->utf8_columns("label");

__PACKAGE__->belongs_to("person",
                        "Ucam::LookupDB::Person",
                        "uid");
__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid",
                        { join_type => 'left' });

=head2 attrdeschash

Used by attrdesc() to return a human-readable description of an
attribute.

=cut

use constant attrdeschash =>
{ instid       => 'Institution',
  uri          => 'URL',
  label        => 'Comment',
  suppress     => 'Suppress',
  editable     => 'Editable',
  suppressible => 'Suppressible',
};

use overload '""' => sub { shift->uri }, fallback => 1;

1;
