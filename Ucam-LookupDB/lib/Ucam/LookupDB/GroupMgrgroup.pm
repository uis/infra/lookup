package Ucam::LookupDB::GroupMgrgroup;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("group_mgrgroupid");
__PACKAGE__->add_columns("groupid");
__PACKAGE__->add_columns("mgrgroupid");
__PACKAGE__->add_columns("editable");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("managed",
                        "Ucam::LookupDB::Group",
                        "groupid");

__PACKAGE__->belongs_to("manager",
                        "Ucam::LookupDB::Group",
                        "mgrgroupid");

use constant attrdeschash => {
 groupid         => "GroupID",
 mgrgroupid      => "GroupID",
};

1;
