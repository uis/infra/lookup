package Ucam::LookupDB::InstAcronym;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_acronym");

__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("acronym");
__PACKAGE__->add_columns("rowid", { sequence => 'rowid' });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");
__PACKAGE__->utf8_columns("acronym");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid");

use overload '""' => sub { shift->acronym }, fallback => 1;

use constant attrdeschash =>
{ acronym  => "Acronym",
  editable => 'Editable',
};

1;

