package Ucam::LookupDB::InstFacsimiletelephonenumberAddable;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_facsimiletelephonenumber_addable");
__PACKAGE__->add_columns("instid","addable");
__PACKAGE__->set_primary_key("instid");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid");

1;

