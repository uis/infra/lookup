package Ucam::LookupDB::GroupWarntimestamp;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("group_warntimestamp");

__PACKAGE__->add_columns("groupid");
__PACKAGE__->add_columns("warntimestamp", { data_type => "timestamp" });
__PACKAGE__->add_columns("rowid",         { sequence  => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("grp",
                        "Ucam::LookupDB::Group",
                        "groupid");

use constant attrdeschash =>
{ warntimestamp => "Last expiry warning",
  editable      => 'Editable',
};

use overload '""' => sub { shift->warntimestamp->iso8601 }, fallback => 1;

1;

