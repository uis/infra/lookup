package Ucam::LookupDB::GroupGrouptitle;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("group_grouptitle");
__PACKAGE__->add_columns("groupid");
__PACKAGE__->add_columns("grouptitle");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");
__PACKAGE__->utf8_columns("grouptitle");

__PACKAGE__->belongs_to("grp",
                        "Ucam::LookupDB::Group",
                        "groupid");

use overload '""' => sub { shift->grouptitle }, fallback => 1;

use constant attrdeschash =>
{ grouptitle => "Title",
  editable   => 'Editable',
};


1;

