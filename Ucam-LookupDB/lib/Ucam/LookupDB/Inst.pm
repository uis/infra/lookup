package Ucam::LookupDB::Inst;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

my $keep = { cascade_delete => 0 };

__PACKAGE__->table("insts");
__PACKAGE__->add_columns("instid");
__PACKAGE__->set_primary_key("instid");


# Many to many relations
__PACKAGE__->has_many    ("inst_person",
                          "Ucam::LookupDB::PersonInst",
                          "instid",
                          $keep);
__PACKAGE__->many_to_many("person",
                          "inst_person",
                          "person",
                          { join     => ['registeredname', 'displayname'],
                            prefetch => ['registeredname', 'displayname'],
                          }
                         );

__PACKAGE__->has_many    ("person_addable",
                          "Ucam::LookupDB::PersonInstAddable",
                          "instid",
                          $keep);

__PACKAGE__->has_many    ("inst_parent",
                          "Ucam::LookupDB::InstParentinst",
                          "instid",
                          $keep);
__PACKAGE__->many_to_many("parent",
                          "inst_parent",
                          "parent",
                          { join     => ['ou'],
                            prefetch => ['ou'],
                          }
                         );
__PACKAGE__->has_many    ("inst_child",
                          "Ucam::LookupDB::InstParentinst",
                          "parentinstid",
                         $keep);
__PACKAGE__->many_to_many("child",
                          "inst_child",
                          "child",
                          { join     => ['ou'],
                            prefetch => ['ou'],
                          }
                         );

__PACKAGE__->has_one     ("parent_addable",
                          "Ucam::LookupDB::InstParentinstAddable",
                          "instid",
                          $keep);

__PACKAGE__->has_many    ("inst_group",
                          "Ucam::LookupDB::InstGroup",
                          "instid",
                          $keep);
__PACKAGE__->many_to_many("grp",
                          "inst_group",
                          "grp",
                          { join     => ['grouptitle'],
                            prefetch => ['grouptitle'],
                          }
                         );

__PACKAGE__->has_one     ("group_addable",
                          "Ucam::LookupDB::InstGroupAddable",
                          "instid",
                          $keep);

__PACKAGE__->has_many    ("inst_mgrgroup",
                          "Ucam::LookupDB::InstMgrgroup",
                          "instid",
                          $keep);
__PACKAGE__->many_to_many("manager",
                          "inst_mgrgroup",
                          "grp",
                          { join     => ['grouptitle'],
                            prefetch => ['grouptitle'],
                          }
                         );

__PACKAGE__->has_one     ("mgrgroup_addable",
                          "Ucam::LookupDB::InstMgrgroupAddable",
                          "instid",
                          $keep);

# Group creation interface
__PACKAGE__->has_many    ("new_group",
                          "Ucam::LookupDB::GroupCreate",
                          "instid",
                          $keep);

# Other attributes
__PACKAGE__->might_have  ("ou",
                          "Ucam::LookupDB::InstOu",
                          "instid",
                          $keep);
__PACKAGE__->has_one     ("ou_addable",
                          "Ucam::LookupDB::InstOuAddable",
                          "instid",
                          $keep);

__PACKAGE__->might_have  ("jpegphoto",
                          "Ucam::LookupDB::InstJpegphoto",
                          "instid",
                          $keep);
__PACKAGE__->has_one     ("jpegphoto_addable",
                          "Ucam::LookupDB::InstJpegphotoAddable",
                          "instid",
                          $keep);

__PACKAGE__->has_many    ("postaladdress",
                          "Ucam::LookupDB::InstPostaladdress",
                          "instid",
                          $keep);
__PACKAGE__->has_one     ("postaladdress_addable",
                          "Ucam::LookupDB::InstPostaladdressAddable",
                          "instid",
                          $keep);

__PACKAGE__->has_many    ("mail",
                          "Ucam::LookupDB::InstMail",
                          "instid",
                          $keep);
__PACKAGE__->has_one     ("mail_addable",
                          "Ucam::LookupDB::InstMailAddable",
                          "instid",
                          $keep);

__PACKAGE__->has_many    ("phone",
                          "Ucam::LookupDB::InstPhone",
                          "instid",
                          $keep);
__PACKAGE__->has_one     ("phone_addable",
                          "Ucam::LookupDB::InstPhoneAddable",
                          "instid",
                          $keep);

__PACKAGE__->has_many    ("facsimiletelephonenumber",
                          "Ucam::LookupDB::InstFacsimiletelephonenumber",
                          "instid",
                          $keep);
__PACKAGE__->has_one     ("facsimiletelephonenumber_addable",
                          "Ucam::LookupDB::InstFacsimiletelephonenumberAddable",
                          "instid",
                          $keep);

__PACKAGE__->has_many    ("labeleduri",
                          "Ucam::LookupDB::InstLabeleduri",
                          "instid",
                          $keep);
__PACKAGE__->has_one     ("labeleduri_addable",
                          "Ucam::LookupDB::InstLabeleduriAddable",
                          "instid",
                          $keep);

__PACKAGE__->has_many    ("acronym",
                          "Ucam::LookupDB::InstAcronym",
                          "instid",
                          $keep);
__PACKAGE__->has_one     ("acronym_addable",
                          "Ucam::LookupDB::InstAcronymAddable",
                          "instid",
                          $keep);

=head2 attrdeschash

Used by attrdesc() to return a human-readable description of an
attribute.

=cut

use constant attrdeschash =>
{
 instid                   => "InstID",
 person                   => "Member",
 parent                   => "Parent institution",
 child                    => "Child institution",
 grp                      => "Group",
 manager                  => "Data manager",
 ou                       => "Name",
 jpegphoto                => "Image",
 postaladdress            => "Address",
 mail                     => "Email address",
 phone                    => "Phone number",
 facsimiletelephonenumber => "Fax number",
 labeleduri               => "Web page",
 acronym                  => "Acronym",
 full_name                => "Name",
};

=head2 std_search

Standard inst search

=cut

sub std_search : ResultSet {
    my ($self) = @_;
    return $self->search
        ( {},
          {join     => ['ou'],
           prefetch => ['ou'],
           order_by => 'COALESCE(ou.ou,me.instid)',
           cache    => 1,
          }
        );
}

=head2 full_name

Shortcut for whichever of ou or instid exists

=cut

use overload '""' => sub { shift->full_name }, fallback => 1;

sub full_name {
    my $self = shift;

    return $self->ou || $self->instid;

}

=head2 editable

Test if any attributes are editable, suppressible or addable, omitting
jpegphoto and many-to-many relations (essentially "Should there be an
edit button on the profile page?")

=cut

sub editable {
    my $self = shift;

    foreach my $relationship (qw[ou postaladdress mail phone
                                 facsimiletelephonenumber labeleduri
                                 acronym]) {
        foreach my $row ($self->$relationship) {
            return 1 if defined($row) and $row->editable;
        }
        my $addable = "${relationship}_addable";
        foreach my $row ($self->$addable) {
            return 1 if defined($row) and $row->addable;
        }
    }
    return;
}

=head2 photo_editable

Test if an existing photo is editable or suppressible, or if a photo
can be added

=cut

sub photo_editable {
    my $self = shift;

    return $self->jpegphoto->editable if $self->jpegphoto;
    return $self->jpegphoto_addable->addable if $self->jpegphoto_addable;
    return;
}

1;
