package Ucam::LookupDB::GroupCreate;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

my $keep = { cascade_delete => 0 };

__PACKAGE__->table("group_create");
__PACKAGE__->add_columns("groupid");
__PACKAGE__->add_columns("instid");

sub insert {
    my $self = shift;

    my $dbh = $self->result_source->storage->dbh;
    my $nextval = $dbh->selectrow_array(qq[select new_groupid()]);
    die "Failed to get new groupid" unless defined($nextval);
    $self->groupid($nextval);

    return $self->next::method( @_ );
}

1;
