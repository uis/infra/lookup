package Ucam::LookupDB::InstParentinst;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_parentinstid");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("parentinstid");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("child",
                        "Ucam::LookupDB::Inst",
                        "instid");
__PACKAGE__->belongs_to("parent",
                        "Ucam::LookupDB::Inst",
                        "parentinstid");

1;

