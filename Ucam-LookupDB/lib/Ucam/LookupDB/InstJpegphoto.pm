package Ucam::LookupDB::InstJpegphoto;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_jpegphoto");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("jpegphoto");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid");

use constant attrdeschash =>
{ jpegphoto    => 'Image',
  suppress     => 'Suppress',
  editable     => 'Editable',
  suppressible => 'Suppressible',
};

1;

