package Ucam::LookupDB::PersonInst;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("person_inst");
__PACKAGE__->add_columns("uid");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("editable");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("person",
                        "Ucam::LookupDB::Person",
                        "uid");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid",
                        { join_type => 'left' });

1;

