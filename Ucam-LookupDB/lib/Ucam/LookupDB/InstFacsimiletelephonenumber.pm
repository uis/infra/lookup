package Ucam::LookupDB::InstFacsimiletelephonenumber;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("inst_facsimiletelephonenumber");
__PACKAGE__->add_columns("instid");
__PACKAGE__->add_columns("facsimiletelephonenumber");
__PACKAGE__->add_columns("label");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");

__PACKAGE__->belongs_to("inst",
                        "Ucam::LookupDB::Inst",
                        "instid");

use overload '""' => sub { shift->facsimiletelephonenumber }, fallback => 1;

use constant attrdeschash =>
{ facsimiletelephonenumber => 'Number',
  label                    => 'Comment',
  editable                 => 'Editable',
};

1;

