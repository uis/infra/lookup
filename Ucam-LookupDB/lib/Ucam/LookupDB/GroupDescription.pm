package Ucam::LookupDB::GroupDescription;

use strict;
use warnings;

use base 'Ucam::LookupDBBase';

__PACKAGE__->table("group_description");
__PACKAGE__->add_columns("groupid");
__PACKAGE__->add_columns("description");
__PACKAGE__->add_columns("rowid", { sequence => "rowid" });
__PACKAGE__->add_columns("editable");

__PACKAGE__->set_primary_key("rowid");
__PACKAGE__->utf8_columns("description");

__PACKAGE__->belongs_to("grp",
                        "Ucam::LookupDB::Group",
                        "groupid");

use overload '""' => sub { shift->description }, fallback => 1;

use constant attrdeschash =>
{ description => "Description",
  editable    => 'Editable',
};


1;

