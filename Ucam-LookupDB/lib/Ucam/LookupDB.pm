package Ucam::LookupDB;

use strict;
use warnings;

use base 'DBIx::Class::Schema';

=head1 NAME

Ucam::LookupDB - DBIX::Class interface to lookup

=head1 VERSION

Version 0.100

=cut

our $VERSION = '0.100';

=head1 SYNOPSIS

  use Ucam::LookupDB;
  my $schema = Ucam::LookupDB->connect($dbi_dsn, $user, $pass, \%dbi_params);

  # Query for all people and put them in an array,
  my @all_people = $schema->resultset('People')->all;

=head1 DESCRIPTION

A DBIX::Class interface to the lookup SQL database.

=head1 FUNCTIONS

See individual classes for details.

=cut

=head1 AUTHOR

University of Cambridge Computing Service,
C<< <lookup-support@ucs.cam.ac.uk> >>

=head1 COPYRIGHT & LICENSE

Copyright 2006 University of Cambridge Computing Service, all rights reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut

__PACKAGE__->load_classes;

1;

