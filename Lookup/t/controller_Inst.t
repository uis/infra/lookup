use strict;
use warnings;
use Test::More tests => 3;

BEGIN { use_ok 'Catalyst::Test', 'Lookup' }
BEGIN { use_ok 'Lookup::Controller::Inst' }

ok( request('/inst')->is_success, 'Request should succeed' );


