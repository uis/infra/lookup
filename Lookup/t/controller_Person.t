use strict;
use warnings;
#use Test::More tests => 3;
use Test::More qw/no_plan/;
use Test::NoWarnings;

BEGIN { use_ok 'Catalyst::Test', 'Lookup' }
BEGIN { use_ok 'Lookup::Controller::Person' }

my $response;

ok( $response = request('/person') );
ok( $response->is_redirect, 'Un-auth request should redirect' );
like( $response->header('Location'), qr!^https://raven.cam! ); 

$ENV{LOOKUP_USER} = 'test001';

ok( $response = request('/person') );
ok( $response->is_error, "Auth'd request should 'Not Found'" );

ok( $response = request('/person/fjc55') );
ok( $response->is_success, "Auth'd request should succeed" );
