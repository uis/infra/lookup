use strict;
use warnings;
use Test::More tests => 3;

BEGIN { use_ok 'Catalyst::Test', 'Lookup' }

ok( request('/static/home.gif')->is_success, 'Request should succeed');
ok( request('/')->is_redirect, 'Request should redirect' );

