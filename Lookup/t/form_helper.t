#!/usr/bin/perl -w

use strict;

use FindBin;
use File::Spec;
use Ucam::LookupDB;

# Use eq_or_diff from Test::Differences if available
use Test::More 'no_plan', import => ['!is_deeply'];
eval "use Test::Differences";  # In case it isn't installed
sub is_deeply {
    goto &eq_or_diff if defined &eq_or_diff;
    goto &Test::More::is_deeply;
}

use Test::NoWarnings;

use constant WHO => 'fjc55';
my $database = File::Spec->catfile($FindBin::Bin,'test.db');
BAIL_OUT("Can't find database") unless -e $database;

my $db = Ucam::LookupDB->connect("dbi:SQLite:dbname=$database","", "");
BAIL_OUT("Can't connect to database") unless $db;
my $row = $db->resultset("PersonRegisteredname")->search({uid => WHO})->single;
BAIL_OUT("Failed to get test database row") unless $row;

BEGIN {
    use_ok( 'Ucam::DBICFormHelper' ) || BAIL_OUT("Module won't load!");
}
diag( "Testing Ucam::DBICFormHelper $Ucam::DBICFormHelper::VERSION, Perl $], $^X" );

my $form;

# Basic creation
$form = Ucam::DBICFormHelper->new;
isa_ok($form,"Ucam::DBICFormHelper");

# Multi accessor
$form = Ucam::DBICFormHelper->new;
ok(!$form->multi);
ok(!$form->multi(1));
ok($form->multi);
ok($form->multi(0));
ok(!$form->multi);


# errors_for
$form = Ucam::DBICFormHelper->new;
foreach (0,1) {
    $form->multi($_);
    is($form->errors_for($row,'sn'),undef,"Multi is $_");
    is($form->errors_for($row,'sn','What?'),undef,"Multi is $_");
    is($form->errors_for($row,'sn'),'What?',"Multi is $_");
    $form->clear_errors;
    is($form->errors_for($row,'sn'),undef,"Multi is $_");
}

# name_for
$form = Ucam::DBICFormHelper->new;
is($form->name_for($row,'sn'),'PersonRegisteredname.sn');
$form->multi(1);
is($form->name_for($row,'sn'),'PersonRegisteredname.5.sn');

# Extract
$form = Ucam::DBICFormHelper->new;
my $args1 = 
    { "Table1.foo" => 'A',
      "Table1.bar" => 'B',
      "Table2.baz" => 'C',
 };
is_deeply($form->extract($args1),
          { Table1 =>
            { foo => 'A', bar => 'B'
         },
            Table2 =>
            { baz => 'C'
         },
       });

my $args2 = 
    { "Table1.1.foo" => 'A',
      "Table1.1.bar" => 'B',
      "Table2.2.baz" => 'C',
};
is_deeply($form->extract($args2),
          { Table1 =>
            { '1' =>
              { foo => 'A', bar => 'B'
              },
            },
            Table2 =>
            { '2' =>
              { baz => 'C'
              },
            },
          });

# import_params/default_for
$form = Ucam::DBICFormHelper->new;
is($form->default_for($row,'sn'),'Clark');
$form->import_params({"PersonRegisteredname.sn" => ['Smith','Jones'],
                      "PersonRegisteredname.5.sn" => 'Wu'});
is($form->default_for($row,'sn'),'Smith');
$form->multi(1);
is($form->default_for($row,'sn'),'Wu');

$form->multi(0);
$form->import_params;
is($form->default_for($row,'sn'),'Clark');
$form->import_params({});
is($form->default_for($row,'sn'),'Clark');

# Form widgits
$form = Ucam::DBICFormHelper->new;

is($form->text_field($row,'sn',id=>'test'),<<'EOT');
<input id="test" name="PersonRegisteredname.sn" type="text" value="Clark" />
EOT

is($form->text_area($row,'sn',id=>'test'),<<'EOT');
<textarea id="test" name="PersonRegisteredname.sn">Clark</textarea>
EOT

is($form->hidden_field($row,'sn',id=>'test'),<<'EOT');
<input id="test" name="PersonRegisteredname.sn" type="hidden" value="Clark" />
EOT

is($form->check_box($row,'sn',"Clark","Jones",id=>'test'),<<'EOT',"Checked");
<input checked="checked" id="test" name="PersonRegisteredname.sn" type="checkbox" value="Clark" />
<input name="PersonRegisteredname.sn" type="hidden" value="Jones" />
EOT

is($form->check_box($row,'sn',"Jones","Clark",id=>'test'),<<'EOT',"Unchecked");
<input id="test" name="PersonRegisteredname.sn" type="checkbox" value="Jones" />
<input name="PersonRegisteredname.sn" type="hidden" value="Clark" />
EOT

my $defaults1 = ['Clark','Smith','Jones'];
is($form->select($row,'sn',$defaults1,id=>'test'),<<'EOT');
<select id="test" name="PersonRegisteredname.sn"><option selected="selected" value="Clark">Clark</option><option value="Smith">Smith</option><option value="Jones">Jones</option></select>
EOT

my $defaults2 = [ ['Clark','Dr Clark'],
                  ['Smith','Mr Smith'],
                  ['Jones','The Butcher'] ];
is($form->select($row,'sn',$defaults2,id=>'test'),<<'EOT');
<select id="test" name="PersonRegisteredname.sn"><option selected="selected" value="Clark">Dr Clark</option><option value="Smith">Mr Smith</option><option value="Jones">The Butcher</option></select>
EOT

is($form->file_field($row,'sn',id=>'test'),<<'EOT');
<input id="test" name="PersonRegisteredname.sn" type="file" />
EOT
