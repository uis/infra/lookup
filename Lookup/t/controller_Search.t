use strict;
use warnings;
use Test::More tests => 3;

BEGIN { use_ok 'Catalyst::Test', 'Lookup' }
BEGIN { use_ok 'Lookup::Controller::Search' }

ok( request('/search')->is_success, 'Request should succeed' );


