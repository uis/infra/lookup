package Lookup;

use strict;
use warnings;

use Catalyst::Runtime '5.70';

# Set flags and add plugins for the application
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a YAML file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root 
#                 directory

use Catalyst qw/
  -Debug 
  ConfigLoader
  Static::Simple
  StackTrace

  Authentication
  Authentication::Credential::Raven

  Session
  Session::Store::FastMmap
  Session::State::Cookie

  Unicode

  Dumper
  /;

our $VERSION = '0.01';

# Configure the application. 
#
# Note that settings in Lookup.yml (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with a external configuration file acting as an override for
# local deployment.

__PACKAGE__->config( name => 'Lookup' );

# Start the application
__PACKAGE__->setup;

# Strip from a parameter hash all new records with no efective content
sub strip_blank_attr {
    my ($c,$params) = @_;

    foreach my $table (keys %$params) {
        foreach my $key (keys %{$params->{$table}}) {
            next if $key !~ /^NEW/;
            my $values = $params->{$table}->{$key};
            my $content = 0;
            foreach my $column (keys %{$values}) {
                next if $column =~ 
                    /^(uid|instid|rowid|suppress|editable|suppressible)$/;
                $content |= $values->{$column} ne '';
            }
            delete $params->{$table}->{$key} unless $content;
        }
    }
}

# Expand common UofC telephone abbriviations
sub expand_phone {
    my ($c,$val) = @_;

    if ($val =~ /^3\d{4}$/) {
        return "+44 1223 3$val";
    }
    elsif ($val =~ /^[4,6]\d{4}$/) {
        return "+44 1223 7$val";
    }
    elsif ($val =~ /^\d{6}$/) {
        return "+44 1223 $val";
    }
    elsif ($val =~ /^0(.+)/) {
        return "+44 $1";
    }
    return $val;
}


=head1 NAME

Lookup - Catalyst based application

=head1 SYNOPSIS

    script/lookup_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<Lookup::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Jon Warbrick

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
