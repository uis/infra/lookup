package Catalyst::Plugin::FormHelper;

use warnings;
use strict;

our $VERSION = '0.001';

=head1 NAME

Catalyst::Plugin::FormHelper - Matching form helper for Ucam::FormHelper

=head1 SYNOPSIS

=head1 DESCRIPTION

Tools to access and use the data from form submissions created with
the Ucam::FormHelperDBIx::Class helper

=head1 METHODS

=head2 $values=$c->form_helper_extract;

Extract form values to a hashref

=cut

sub form_helper_extract {
    my ($c) = @_;

    my $result = {};
    my $params = $c->req->params;

    foreach my $param (keys %$params) {
        next unless $param =~ /^UFH\./;
        my ($ufh,$source,$key,$col) = split('\.',$param);
        my $value = ref($params->{$param}) ? 
            $params->{$param}->[0] : $params->{$param};
        $result->{$source}->{$key}->{$col} = $value;

    }

    return $result;

}

=head2 $c->form_helper_update($params);

Update/create database records from hashref

=cut

sub form_helper_update {
    my ($c,$schema,$params) = @_;

    # foreach source, foreach key
    foreach my $source (keys %$params) {
        foreach my $key (keys %{$params->{$source}}) {
            my $values = $params->{$source}->{$key};

            # insert new, non_empty records
            if ($key =~ /^NEW/) {
                my $record = $schema->resultset($source)->new($values);
                $record->insert unless $record->is_empty;
            }
            # for existing records...
            else {
                my $record = $schema->resultset($source)->find($key);
                # ... delete some
                if ($values->{_delete}) {
                    $record->delete;
                }
                # ... and update the rest
                else {
                    delete($values->{_delete});
                    $record->set_columns($values);
                    $record->update;
                }
            }
        }
    }
}



