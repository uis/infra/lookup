#!/usr/bin/perl

package Catalyst::Plugin::Params::Nested::Expander;
use base qw/CGI::Expand/;

use strict;
use warnings;

sub split_name {
    my ( $class, $name ) = @_;

    my @elements;

    if ( $name =~ /^ [\w:]+ \[/x ) {
        @elements = grep { defined } ( $name =~ /
            ^  ([\w:]+)      # root param
          | \[ ([\w:]+) \] # nested
        /gx );
    } else {
        @elements = $class->SUPER::split_name( $name );
    }

    return map { /^(0|[1-9]\d*)$/ ? "\\$_" : $_ } @elements;
}


__PACKAGE__;

__END__

=pod

=head1 NAME

Catalyst::Plugin::Params::Nested::Expander - CGI::Expand subclass with rails
like tokenization.

Modified from the version supplied in the
Catalyst-Plugin-Params-Nested-0.02 distribytion to:

1) Allow ':' in segments sepearted by []

2) Escape any segment that looks like a number so it's treated as a
   hash key and not an array index.

=cut


