package Lookup::Controller::Root;

use strict;
use warnings;
use base 'Catalyst::Controller';

# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
__PACKAGE__->config->{namespace} = '';

=head1 NAME

Lookup::Controller::Root - Root Controller for Lookup

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=cut

=head2 self

/self redirects to the profile page for the logegd-in user

=cut

sub self : Local {
     my ( $self, $c ) = @_;

     $c->response->redirect($c->uri_for('/person',$c->user));
}

=head2 default

Default behaviour returns 'Not Found'

=cut

sub default : Private {
    my ( $self, $c ) = @_;

    $c->forward('not_found');
}

=head2 index

Requests for the root 'index' page do searching

=cut

sub index : Path() {
    my ( $self, $c ) = @_;

    $c->forward("/search/index");
}

=head2 not_foud

=cut

sub not_found : Private {
    my ( $self, $c ) = @_;

    $c->response->status(404);
    $c->stash->{template} = '404';
    $c->detach;
}

=head2 logout

=cut

sub logout : Local {
    my ( $self, $c ) = @_;

    if ($c->user_exists) {
        $c->logout;
        $c->stash->{message} = "You have been logged-out";
    }
    else {
        $c->stash->{message} = "You were not logegd-in";
    }
}

=head2 auto

=cut

sub auto : Private {
    my ( $self, $c ) = @_;

    return 0 unless $c->forward('authenticate');
    return 0 unless $c->forward('set_auth',[$c->user]);

    return 1;
}

=head2 set_auth

=cut

sub set_auth : Private {
    my ( $self, $c, $who ) = @_;

    my $dbh = $c->model('LookupDB')->storage->dbh;
    if ($who) {
        $c->log->debug("Setting session auth to $who");
        return 0 unless $dbh->do('SET SESSION AUTHORIZATION ?',{},"$who");
    }
    else {
        $c->log->debug("Setting session auth to default");
        return 0 unless $dbh->do('SET SESSION AUTHORIZATION DEFAULT');
    }

    return 1;
}

=head2 authenticate

=cut

sub authenticate : Private {
    my ( $self, $c ) = @_;

    # Allow the user to be set from the environment for testing
    if ($ENV{LOOKUP_USER}) {
        $c->user($ENV{LOOKUP_USER});
        return 1;
    }

    # OK if we know the user. Fix test0nnn -> testnnn
    if ($c->user_exists) {
        my $who = $c->user;
        if ($who =~ /^test\d(\d\d\d)$/) {
            $c->user("test$1");
            $c->log->debug("User reset to $who");
        }
        return 1;
    }

    # OK if it's for something that doesn't need authentication
    if ($c->request->path =~ m|^logout$| or
        $c->request->path =~ m|^static/|) {
        return 1;
    }


    # Otherwise if we require authentication, get it
    if ($c->authenticate_raven()) {
        $c->response->redirect($c->stash->{raven_ticket}->{url});
    }
    $c->response->body('Raven login failed: "' . 
                       $c->stash->{raven_ticket}->{msg} . '"')
        if $c->stash->{raven_ticket}->{msg};
    return 0;

}

=head2 render

Attempt to render a view. Display a custom error page if needed

=cut

sub render : ActionClass('RenderView') {
    my ( $self, $c ) = @_;

    # Display our own error page after an error (in production)
    if ($c->error and !$c->debug) {
        $c->log->error($_) foreach @{$c->error};
        $c->clear_errors; # To suppress default screen
        $c->response->status(500);
        $c->stash->{template} = '500';
    }

}

=head2 end

Invoke render to display any page, then reset session authorisation to
default. Need to do it in this order so that view processing happens
as the right user. See Catalyst::Action::RenderView manpage for
details.

=cut

sub end : Private {
    my ( $self, $c ) = @_;

    $c->forward('render');
    $c->forward('set_auth',[]);

}

=head1 AUTHOR

Jon Warbrick

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
