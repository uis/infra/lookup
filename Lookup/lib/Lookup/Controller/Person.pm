package Lookup::Controller::Person;

use strict;
use warnings;
use base 'Catalyst::Controller';

use English qw/-no_match_vars/;
use Ucam::DBICFormHelper;
use DBD::Pg qw(:pg_types);

=head1 NAME

Lookup::Controller::Person - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 profile

Action for URLs /person/<crsid>

=cut

sub profile : PathPart('person') Chained('/') Args(1) {
    my ( $self, $c, $uid ) = @_;
    $c->forward('load_person',$uid);
}

=head2 load_person

Chained action for URLs starting /person/<crsid>/...

=cut

sub load_person : PathPart('person') Chained('/') CaptureArgs(1) {
    my ( $self, $c, $uid ) = @_;
    my $person =  $c->model('LookupDB::Person')->
        std_search->search('me.uid' => $uid)->single;
    $c->detach("/not_found") unless $person;
    $c->stash->{person} = $person;
    $c->stash->{title} = "$person";
}

=head2 identity

Chained action for URLs ending /identity

=cut

sub identity : PathPart('identity') Chained('load_person') Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{title} .= ' / Identity';
}

=head2 groups

Chained action for URLs ending /groups

=cut

sub groups : PathPart('groups') Chained('load_person') Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{title} .= ' / Groups';
}

=head2 photo

Chained action for URLs ending /photo

=cut

sub photo : PathPart('photo') Chained('load_person') Args(0) {
    my ( $self, $c ) = @_;

    my $photo;

    if ($c->stash->{person}->jpegphoto) {
        $photo = $c->stash->{person}->jpegphoto->jpegphoto;
    }
    else {
        $c->error("No default photo file name")
            unless $c->config->{nophoto_file};
        my $fn = $c->path_to($c->config->{nophoto_file});
        open(my $img, "<", $fn) or
            $c->error("Failed to open default photo '$fn': $!");
        {
            local ($INPUT_RECORD_SEPARATOR);
            $photo = <$img>;
        };
        close ($img);
    }

    $c->log->debug("Got photo, size" . length($photo));

    $c->error("Zero-length photograph") unless $photo;
    $c->response->body($photo);
    $c->response->content_type("image/jpeg");
}

=head2 vcard

Return person object as a vcard

=cut

sub vcard : PathPart('vcard') Chained('load_person') Args(0) {
    my ( $self, $c ) = @_;
    my $person = $c->stash->{person};

    $c->stash->{vcards} = [$person];
    $c->stash->{filename} = $person->uid;
    $c->forward($c->view('PersonVCard'));

}

=head2 edit

Edit action responds to URLs ending /edit.

=cut

sub edit : PathPart('edit') Chained('load_person') Args(0) {
    my ( $self, $c ) = @_;
    my $person = $c->stash->{person};
    my $form = $c->stash->{form} = Ucam::DBICFormHelper->new();
    $c->stash->{title} .= ' / Edit';

    my $error = '';
    my $message = '';

    if ($c->req->params->{_update}) {
        eval {
            my $params = $form->extract($c->request->params);
            $c->strip_blank_attr($params);
            $form->update_multi($c->model('LookupDB'),$params);
        };
        if ($@) {
            $error = $@;
        }
        else {
            $message = "Update complete";
        }
    }

    if ($c->req->params->{_cancel}) {
        $message = 'Edit cancelled';
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($person->uid));
    }

}

=head2 photoedit

Edit action responds to URLs ending .../photoedit

=cut

sub photoedit : PathPart('photoedit') Chained('load_person') Args(0) {
    my ( $self, $c ) = @_;
    my $person = $c->stash->{person};
    my $form = $c->stash->{form} = Ucam::DBICFormHelper->new();
    $c->stash->{title} .= ' / Edit Photo';

    my $error = '';
    my $message = '';

    my $params = $form->extract($c->request->params)->{PersonJpegphoto};
    $c->log->dumper($params,'params');

    # Delete?
    if ($params->{_delete}) {
        my $photo = $person->search_related('jpegphoto',{});
        $photo->delete if $photo;
        $message = "Photograph deleted";
    }
    # If we are doing an update...
    elsif ($c->req->params->{_update}) {
        my $dbh = $person->result_source->schema->storage->dbh;
        $dbh->do('BEGIN WORK');
        eval {
            my $suppress = $params->{suppress};
            my $instid   = $params->{instid};
            my $data = undef;
            my $upload = $c->request->upload('PersonJpegphoto.jpegphoto');
            if ($upload) {
                $c->log->debug("Got photo, type " . $upload->type . ", size " .
                             $upload->size . " filename " . $upload->filename);
                $data = $upload->slurp;
            }
            my $sth;
            my $photo = $person->search_related('jpegphoto',{});
            if ($photo) {
                $sth = $dbh->prepare(<<"SQL");
UPDATE person_jpegphoto SET suppress = ? where rowid = ?
SQL
                $sth->execute($suppress,$photo->rowid);
                if ($data) {
                    $sth = $dbh->prepare(<<"SQL");
UPDATE person_jpegphoto SET jpegphoto = ? where rowid = ?
SQL
                    $sth->bind_param(1,$data, {pg_type => DBD::Pg::PG_BYTEA});
                    $sth->bind_param(2,$photo->rowid);
                    $sth->execute();
                }
            }
            elsif ($data) {
                $sth = $dbh->prepare(<<"SQL");
INSERT INTO person_jpegphoto (uid,instid,jpegphoto,suppress) VALUES (?,?,?,?)
SQL
                $sth->bind_param(1,$person->uid);
                $sth->bind_param(2,$instid);
                $sth->bind_param(3,$data, {pg_type => DBD::Pg::PG_BYTEA});
                $sth->bind_param(4,$suppress);
                $sth->execute();
            }
            else {
                $message = "Cancelled - nothing to do";
            }
        };
        if ($@) {
            $dbh->do('ROLLBACK');
            $error = $@;
        }
        else {
            $dbh->do('COMMIT');
            $message ||= "Update complete";
        }

    }

    if ($c->req->params->{_cancel}) {
        $message = 'Edit cancelled';
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($person->uid));
    }

}

=head1 AUTHOR

Jon Warbrick

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
