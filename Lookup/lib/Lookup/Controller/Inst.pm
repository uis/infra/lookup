package Lookup::Controller::Inst;

use strict;
use warnings;
use base 'Catalyst::Controller';

use English qw/-no_match_vars/;
use DBD::Pg qw(:pg_types);
use Ucam::DBICFormHelper;

=head1 NAME

Lookup::Controller::Inst - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 institution

Action for URLs /inst/<instid>

=cut

sub institution : PathPart('inst') Chained('/') Args(1) {
    my ( $self, $c, $instid ) = @_;
    $c->forward('load_inst',$instid);
}

=head2 load_inst

Chained action for URLs starting /inst/<instid>/...

=cut

sub load_inst : PathPart('inst') Chained('/') CaptureArgs(1) {
    my ( $self, $c, $instid ) = @_;
    my $inst =  $c->model('LookupDB::Inst')->
        std_search->search({'me.instid' => $instid})->first;
    $c->detach("/not_found") unless $inst;
    $c->stash->{inst} = $inst;
    $c->stash->{title} = "$inst";
}

=head2 contacts

Chained action for URLs ending /contacts

=cut

sub contacts : PathPart('contacts') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{title} .= ' / Contacts';
}

=head2 members

Chained action for URLs ending /members

=cut

sub members : PathPart('members') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{title} .= ' / Members';
}

=head2 groups

Chained action for URLs ending /groups

=cut

sub groups : PathPart('groups') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{title} .= ' / Groups';
}

=head2 photo

Chained action for URLs ending /photo

=cut

sub photo : PathPart('photo') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;

    my $photo;

    if ($c->stash->{inst}->jpegphoto) {
        $photo = $c->stash->{inst}->jpegphoto->jpegphoto;
    }
    else {
        $c->error("No default image file name")
            unless $c->config->{noimg_file};
        my $fn = $c->path_to($c->config->{noimg_file});
        $c->log->debug("Image file name: $fn");
        open(my $img, "<", $fn) or
            $c->error("Failed to open default image '$fn': $!");
        {
            local ($INPUT_RECORD_SEPARATOR);
            $photo = <$img>;
        };
        close ($img);
    }

    $c->error("Zero-length photograph") unless $photo;
    $c->response->body($photo);
    $c->response->content_type("image/jpeg");
}

=head2 vcard

Return inst object as a vcard

=cut

sub vcard : PathPart('vcard') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;
    my $inst = $c->stash->{inst};

    $c->stash->{vcards} = [$inst];
    $c->stash->{filename} = lc($inst->instid);
    $c->forward($c->view('InstVCard'));

}

=head2 edit

Edit action responds to URLs ending /edit.

=cut

sub edit : PathPart('edit') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;
    my $inst = $c->stash->{inst};
    my $form = $c->stash->{form} = Ucam::DBICFormHelper->new();
    $c->stash->{title} .= ' / Edit';

    my $error = '';
    my $message = '';

    if ($c->req->params->{_update}) {
        eval {
            my $params = $form->extract($c->request->params);
            $c->strip_blank_attr($params);
            $form->update_multi($c->model('LookupDB'),$params);
        };
        if ($@) {
            $error = $@;
        }
        else {
            $message = "Update complete";
        }
    }

    if ($c->req->params->{_cancel}) {
        $message = 'Edit cancelled';
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($inst->instid));
    }

}

=head2 photoedit

Edit action responds to URLs ending .../photoedit

=cut

sub photoedit : PathPart('photoedit') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;
    my $inst = $c->stash->{inst};
    my $form = $c->stash->{form} = Ucam::DBICFormHelper->new();
    $c->stash->{title} .= ' / Edit Image';

    my $error = '';
    my $message = '';

    my $params = $form->extract($c->request->params)->{InstJpegphoto};
    $c->log->dumper($params,'params');

    # Delete?
    if ($params->{_delete}) {
        my $photo = $inst->search_related('jpegphoto',{});
        $photo->delete if $photo;
        $message = "Photograph deleted";
    }
    # If we are doing an update...
    elsif ($c->req->params->{_update}) {
        my $dbh = $inst->result_source->schema->storage->dbh;
        $dbh->do('BEGIN WORK');
        eval {
            my $data = undef;
            my $upload = $c->request->upload('InstJpegphoto.jpegphoto');
            if ($upload) {
                $c->log->debug("Got photo, type " . $upload->type . ", size " .
                             $upload->size . " filename " . $upload->filename);
                $data = $upload->slurp;
            }
            if ($data) {
                my $sth;
                my $photo = $inst->search_related('jpegphoto',{});
                if ($photo) {
                    $sth = $dbh->prepare(<<"SQL");
UPDATE inst_jpegphoto SET jpegphoto = ? where rowid = ?
SQL
                    $sth->bind_param(1,$data, {pg_type => DBD::Pg::PG_BYTEA});
                    $sth->bind_param(2,$photo->rowid);
                    $sth->execute();
                }
                else {
                    $sth = $dbh->prepare(<<"SQL");
INSERT INTO inst_jpegphoto (instid,jpegphoto) VALUES (?,?)
SQL
                    $sth->bind_param(1,$inst->instid);
                    $sth->bind_param(2,$data, {pg_type => DBD::Pg::PG_BYTEA});
                    $sth->execute();
                }
            }
            else {
                $message = "Cancelled - nothing to do";
            }
        };
        if ($@) {
            $dbh->do('ROLLBACK');
            $error = $@;
        }
        else {
            $dbh->do('COMMIT');
            $message ||= "Update complete";
        }

    }

    if ($c->req->params->{_cancel}) {
        $message = 'Edit cancelled';
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($inst->instid));
    }

}

=head2 add-members

Chained action for URLs ending /add-members

=cut

sub add_members : PathPart('add-members') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;
    my $inst = $c->stash->{inst};
    $c->stash->{title} .= ' / Add members';

    my $error = '';
    my $message = '';

    if ($c->request->params->{_update}) {
        my $uids = $c->request->params->{ids};
        my @uids = split(/[^a-zA-Z0-9]+/,$uids);
        eval {
            $c->model('LookupDB')->schema->txn_do( sub {
                foreach my $uid (@uids) {
                    $inst->create_related('inst_person', { uid => $uid });
                }
            });
        };
        if ($@) {
            $error = $@;
        }
        else {
            $message = @uids == 1 ? "Person added" : "People added";
        }

    }

    if ($c->req->params->{_cancel}) {
        $message = 'Edit cancelled';
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($inst->instid,'members'));
    }

}

=head2 drop-member

Chained action for URLs ending /drop-member

=cut

sub drop_member : PathPart('drop-member') Chained('load_inst') Args(1) {
    my ( $self, $c, $uid ) = @_;
    my $inst = $c->stash->{inst};
    $c->stash->{title} .= ' / Remove member';

    my $error = '';
    my $message = '';

    if ($c->req->params->{_cancel}) {
        $message = 'Removal cancelled';
    }
    else {
        $c->model('LookupDB')->schema->txn_do( sub {
            my $person = $c->model('LookupDB::Person')->find($uid);
            $c->stash->{person} = $person;
            if (!$person) {
                $message = "Person removed (doesn't exist)";
            }
            elsif ($c->request->params->{_confirm}) {
                eval {
                    $inst->remove_from_person($person);
                };
                if ($@) {
                    $error = $@;
                }
                else {
                    $message = "Person removed";
                }
            }
        });

    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($inst->instid,'members'));
    }

}

=head2 new-group

Chained action for URLs ending /new-group

=cut

sub new_group : PathPart('new-group') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{title} .= ' / New group';
    my $inst = $c->stash->{inst};
    my $form = $c->stash->{form} = Ucam::DBICFormHelper->new();
    $c->stash->{template} = 'group/edit';

    my $groupid = $c->req->params->{groupid};

    my $error = '';
    my $message = '';

    # If we are creating/updating...
    if ($c->req->params->{_update}) {
        eval {
            my $params = $form->extract($c->request->params);
            $c->strip_blank_attr($params);
            $form->update_multi($c->model('LookupDB'),$params);
        };
        if ($@) {
            $error = $@;
        }
        else {
            $message = "Group created";
        }
    }
    # Else if we are canceling
    elsif ($c->req->params->{_cancel}) {
        $message = 'Create cancelled';
    }
    # Else it's first time through
    else {
        # Create a new, empty group and then retrieve it
        my $new_group = $inst->create_related('new_group',{});
        $groupid = $new_group->groupid;
    }

    $c->stash->{group} = $c->model('LookupDB::Group')->find($groupid);

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for('/inst',$inst->instid,'groups'));
    }

}

=head2 add-groups

Chained action for URLs ending /add-groups

=cut

sub add_groups : PathPart('add-groups') Chained('load_inst') Args(0) {
    my ( $self, $c ) = @_;
    my $inst = $c->stash->{inst};
    $c->stash->{title} .= ' / Add groups';

    my $error = '';
    my $message = '';

    if ($c->request->params->{_update}) {
        my $groupids = $c->request->params->{ids};
        my @groupids = split(/[^0-9]+/,$groupids);
        eval {
            $c->model('LookupDB')->schema->txn_do( sub {
                foreach my $groupid (@groupids) {
                    $inst->create_related('inst_group',{groupid => $groupid});
                }
            });
        };
        if ($@) {
            $error = $@;
        }
        else {
            $message = @groupids == 1 ? "Group added" : "Groups added";
        }

    }

    if ($c->req->params->{_cancel}) {
        $message = 'Edit cancelled';
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($inst->instid,'groups'));
    }

}

=head2 drop-group

Chained action for URLs ending /drop-group

=cut

sub drop_group : PathPart('drop-group') Chained('load_inst') Args(1) {
    my ( $self, $c, $groupid ) = @_;
    my $inst = $c->stash->{inst};
    $c->stash->{title} .= ' / Remove group';

    my $error = '';
    my $message = '';

    if ($c->req->params->{_cancel}) {
        $message = 'Removal cancelled';
    }
    else {
        $c->model('LookupDB')->schema->txn_do( sub {
            my $group = $c->model('LookupDB::Group')->find($groupid);
            $c->stash->{group} = $group;
            if (!$group) {
                $message = "Group removed (doesn't exist)";
            }
            elsif ($c->request->params->{_confirm}) {
                eval {
                    $inst->remove_from_grp($group);
                };
                if ($@) {
                    $error = $@;
                }
                else {
                    $message = "Group removed";
                }
            }
        });
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($inst->instid,'groups'));
    }

}

=head1 AUTHOR

Jon Warbrick

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
