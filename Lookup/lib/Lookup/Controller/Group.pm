package Lookup::Controller::Group;

use strict;
use warnings;
use base 'Catalyst::Controller';
use Ucam::DBICFormHelper;

use English qw/-no_match_vars/;

=head1 NAME

Lookup::Controller::Group - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 group

Action for URLs /group/<groupid>

=cut

sub group : PathPart('group') Chained('/') Args(1) {
    my ( $self, $c, $groupid ) = @_;
    $c->forward('load_group',$groupid);
}

=head2 load_group

Chained action for URLs starting /group/<groupid>/...

=cut

sub load_group : PathPart('group') Chained('/') CaptureArgs(1) {
    my ( $self, $c, $groupid ) = @_;
    my $group =  $c->model('LookupDB::Group')->
        std_search->search({'me.groupid' => $groupid})->first;
    $c->detach("/not_found") unless $group;
    $c->stash->{group} = $group;
    $c->stash->{title} = "$group";
}

=head2 memberss

Chained action for URLs ending /details

=cut

sub details : PathPart('details') Chained('load_group') Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{title} .= ' / Details';
}

=head2 memberss

Chained action for URLs ending /members

=cut

sub members : PathPart('members') Chained('load_group') Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{title} .= ' / Members';
}

=head2 edit

Edit action responds to URLs ending /edit.

=cut

sub edit : PathPart('edit') Chained('load_group') Args(0) {
    my ( $self, $c ) = @_;
    my $group = $c->stash->{group};
    my $form = $c->stash->{form} = Ucam::DBICFormHelper->new();
    $c->stash->{title} .= ' / Edit';

    my $error = '';
    my $message = '';

    if ($c->req->params->{_update}) {
        eval {
            my $params = $form->extract($c->request->params);
            $c->strip_blank_attr($params);
            $form->update_multi($c->model('LookupDB'),$params);
        };
        if ($@) {
            $error = $@;
        }
        else {
            $message = "Update complete";
        }
    }

    if ($c->req->params->{_cancel}) {
        $message = 'Edit cancelled';
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($group->groupid));
    }

}

=head2 add-members

Chained action for URLs ending /add-members

=cut

sub add_members : PathPart('add-members') Chained('load_group') Args(0) {
    my ( $self, $c ) = @_;
    my $group = $c->stash->{group};
    $c->stash->{title} .= ' / Add members';

    my $error = '';
    my $message = '';

    if ($c->request->params->{_update}) {
        my $uids = $c->request->params->{ids};
        my @uids = split(/[^a-zA-Z0-9]+/,$uids);
        eval {
            $c->model('LookupDB')->schema->txn_do( sub {
                foreach my $uid (@uids) {
                    $group->create_related('group_member', { uid => $uid });
                }
            });
        };
        if ($@) {
            $error = $@;
        }
        else {
            $message = @uids == 1 ? "Person added" : "People added";
        }

    }

    if ($c->req->params->{_cancel}) {
        $message = 'Edit cancelled';
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($group->groupid,'members'));
    }

}

=head2 drop-member

Chained action for URLs ending /drop-member

=cut

sub drop_member : PathPart('drop-member') Chained('load_group') Args(1) {
    my ( $self, $c, $uid ) = @_;
    my $group = $c->stash->{group};
    $c->stash->{title} .= ' / Remove member';

    my $error = '';
    my $message = '';

    if ($c->req->params->{_cancel}) {
        $message = 'Removal cancelled';
    }
    else {
        $c->model('LookupDB')->schema->txn_do( sub {
            my $person = $c->model('LookupDB::Person')->find($uid);
            $c->stash->{person} = $person;
            if (!$person) {
                $message = "Person removed (doesn't exist)";
            }
            elsif ($c->request->params->{_confirm}) {
                eval {
                    $group->remove_from_member($person);
                };
                if ($@) {
                    $error = $@;
                }
                else {
                    $message = "Person removed";
                }
            }
        });
    }

    if ($error) {
        $c->stash->{error_msg} = $error;
    }
    elsif ($message) {
        $c->flash->{status_msg} = $message;
        $c->response->redirect($c->uri_for($group->groupid,'members'));
    }

}

=head1 AUTHOR

Jon Warbrick

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
