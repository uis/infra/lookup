package Lookup::Controller::Search;

use strict;
use warnings;
use base 'Catalyst::Controller';

=head1 NAME

Lookup::Controller::Search - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index : Private {
    my ( $self, $c ) = @_;

    my $type = $c->req->param('type');
    $c->detach('person') if !defined($type);
    $c->detach($type) if $type =~ /^(person|inst|group)$/;
    $c->detach('/not_found');
}

=head2 person

=cut

sub person : Private {
    my ( $self, $c ) = @_;
    # FIXME: Everyone, for testing
    $c->stash->{people} = $c->model('LookupDB::Person')->std_search
        ->search(undef,{join => ['phone'], prefetch=>['phone']});
    $c->stash->{template} = 'search/person';
}

=head2 inst

=cut

sub inst : Private {
    my ( $self, $c ) = @_;
    # FIXME: Everyone, for testing
    $c->stash->{insts} = $c->model('LookupDB::Inst')->std_search;
    $c->stash->{template} = 'search/inst';
}

=head2 group

=cut

sub group : Private {
    my ( $self, $c ) = @_;
    # FIXME: Everyone, for testing
    $c->stash->{groups} = $c->model('LookupDB::Group')->std_search;
    $c->stash->{template} = 'search/group';
}

=head1 AUTHOR

Jon Warbrick

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
