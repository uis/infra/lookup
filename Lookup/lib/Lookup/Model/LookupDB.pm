package Lookup::Model::LookupDB;

use strict;
use base 'Catalyst::Model::DBIC::Schema';

__PACKAGE__->config(
    schema_class => 'Ucam::LookupDB',
    connect_info => [
                     Lookup->config->{database},
                     Lookup->config->{dbuser} || '',
                     Lookup->config->{dbpasswd} || '',
                     { RaiseError => 1 },
    ],
);

=head1 NAME

Lookup::Model::LookupDB - Catalyst DBIC Schema Model
=head1 SYNOPSIS

See L<Lookup>

=head1 DESCRIPTION

L<Catalyst::Model::DBIC::Schema> Model using schema L<LookupDB>

=head1 AUTHOR

Jon Warbrick

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
