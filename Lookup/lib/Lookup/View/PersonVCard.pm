package Lookup::View::PersonVCard;

use strict;
use Catalyst::View::vCard;
use base 'Catalyst::View::vCard';

sub convert_to_vcard {
    my($self,$c,$person,$vcard) = @_;

    $vcard->version("3.0");
    $vcard->fn($person->full_name);

    # Attempt to extract given and middle names from display
    # name. Only try this if display name ends with whatever we have
    # in registeredname->sn.

    my $display = $person->displayname;
    my $family  = $person->registeredname->sn;
    my $given   = $person->registeredname->initials;
    my $titles  = $person->registeredname->titles;
    my $middle  = '';

    if ($display and $display =~ /^(.+)\s+$family$/) {
        my @rest   = split(' ',$1);
        $given  = shift(@rest);
        $middle = join(' ',@rest);
    }

    my $name = $vcard->add_node({node_type=>'name'});
    $name->family($family);
    $name->given($given);
    $name->middle($middle);
    $name->prefixes($titles);

    foreach ($person->postaladdress->all) {
        my $label = $vcard->add_node({node_type => 'LABELS'});
        my $addr = $_;
        $addr =~ s/\r*\n/\\n/g;
        $label->value($addr);
    }

    foreach ($person->phone->all) {
        my $phone = $vcard->add_node({node_type => 'PHONES'});
        $phone->value($c->expand_phone($_));
    }

    foreach ($person->mail->all) {
        my $mail = $vcard->add_node({node_type => 'EMAIL'});
        $mail->value($_);
        $mail->add_types('INTERNET');
    }

    foreach ($person->title->all) {
        my $title = $vcard->add_node({node_type => 'TITLE'});
        $c->log->debug("Setting title = " . $title);
        $title->value($_);
    }

    my @orgs = map { $_->ou } $person->inst->all;
    my $org = $vcard->add_node({node_type => 'ORG'});
    $org->name('University of Cambridge');
    $org->unit(\@orgs);

    foreach ($person->labeleduri->all) {
        my $url = $vcard->add_node({node_type => 'URL'});
        $url->value($_);
    }

}

1;
