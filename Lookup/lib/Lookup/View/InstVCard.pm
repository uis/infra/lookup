package Lookup::View::InstVCard;

use strict;
use Catalyst::View::vCard;
use base 'Catalyst::View::vCard';

sub convert_to_vcard {
    my($self,$c,$inst,$vcard) = @_;

    $vcard->version("3.0");
    $vcard->fn($inst->full_name);
    my $name = $vcard->add_node({node_type=>'name'});
    $name->given($inst->full_name);

    foreach ($inst->postaladdress->all) {
        my $label = $vcard->add_node({node_type => 'LABELS'});
        my $addr = $_;
        $addr =~ s/\r*\n/\\n/g;
        $label->value($addr);
    }

    foreach ($inst->phone->all) {
        my $phone = $vcard->add_node({node_type => 'PHONES'});
        $phone->value($c->expand_phone($_));
    }
    foreach ($inst->facsimiletelephonenumber->all) {
        my $fax = $vcard->add_node({node_type => 'PHONES'});
        $fax->value($c->expand_phone($_));
        $fax->add_types('FAX');
    }

    foreach ($inst->mail->all) {
        my $mail = $vcard->add_node({node_type => 'EMAIL'});
        $mail->value($_);
        $mail->add_types('INTERNET');
    }

    foreach ($inst->labeleduri->all) {
        my $url = $vcard->add_node({node_type => 'URL'});
        $url->value($_);
    }

}

1;
