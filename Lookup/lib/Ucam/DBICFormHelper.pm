package Ucam::DBICFormHelper;

use warnings;
use strict;

our $VERSION = '0.001';

use HTML::Element;

=head1 NAME

Ucam::DBICFormHelper - a form processing toolkit for Catalyst and DBIx::Class

=head1 SYNOPSIS

=head1 DESCRIPTION

Tools to create form widgits and then to access and use the data from
such forms on submission

=head1 METHODS

=head2 my $form = Ucam::DBICFormHelper->new();

Return a new form helper object

=cut

sub new {
    my ($class) = @_;

    my $helper = bless {}, $class;
    $helper->multi(0);
    $helper->{errors} = {};
    return $helper;

}

=head2 my $multi = $form->multi; $form->multi(1);

Read/set the form's multi flag

=cut

sub multi {
    my ($self,$new) = @_;
    my $result = $self->{multi};
    $self->{multi} = $new if defined($new);
    return $result;
}


=head2 my $message = $obj->errors_for($row,$column,$new)

Mutator for error messages associated with $column from $row

=cut

sub errors_for {
    my ($self,$row,$column,$new) = @_;

    my $name  = $self->name_for($row,$column);
    my $result = $self->{errors}->{name};
    $self->{errors}->{name} = $new if @_ == 4;
    return $result;
}

=head2 $form->clear_errors

Clear all errors established by errors_for

=cut

sub clear_errors {
    my ($self) = @_;

    $self->{errors} = {};
}

=head2 my $name = $form->name_for($row,$column)

Return the correct name for an editing widget working on $column from
the CDBIx::Row object $row

=cut

sub name_for {
    my ($self,$row,$column) = @_;

    my $class = $row->source_name;
    if ($self->multi) {
        # currently this only works for single-column primary keys
        my $key = $row->id || 'NEW';
        return "$class.$key.$column";
    }
    else {
        return "$class.$column";
    }

}

=head2 my $params = $form->extract($c->request->params)

Extract nested form values.  Parameters like Person.123.name end up in
$argshashref->{Person}->{123}->{name} and parameters like Person.name
end up in $argshashref->{Person}->{name}

=cut

sub extract {
    my ($self,$params) = @_;

    my $result = {};

    foreach my $param (keys %$params) {
        next unless $param =~ /\./;
        my $value = ref($params->{$param}) ?
            $params->{$param}->[0] : $params->{$param};
        my ($source,$p1,$p2) = split('\.',$param,3);
        if (defined($p2)) {
            $result->{$source}->{$p1}->{$p2} = $value;
        }
        else {
            $result->{$source}->{$p1} = $value;
        }
    }

    return $result;

}

=head2 $form->update_multi($schema,$params);

Update/create database records from an extract'ed hashref in
multi-mode. Note that if there's a '_delete' key with a true value for
any record it will be deleted!

=cut

sub update_multi {
    my ($self,$model,$params) = @_;

    my $code = sub {
        # foreach source, foreach key
        foreach my $source (keys %$params) {
            foreach my $key (keys %{$params->{$source}}) {
                my $values = $params->{$source}->{$key};

                # insert new, non_empty records
                if ($key =~ /^NEW/) {
                    my $record = $model->resultset($source)->new($values);
                    $record->insert unless 
                        $record->can('is_empty') and $record->is_empty;
                }
                # for existing records...
                else {
                    my $record = $model->resultset($source)->find($key);
                    # ... delete some
                    if ($values->{_delete}) {
                        $record->delete;
                    }
                    # ... and update the rest
                    else {
                        delete($values->{_delete});
                        $record->set_columns($values);
                        $record->update;
                    }
                }

            }
        }
    };

    $model->schema->txn_do($code);
}

=head2 $form->import_params([$params])

Establish default values for all columns in $row from request
parameters or column values. Call this before using form control
widgits if you want the default vaues to be sticky. Clears all
existing error.

=cut

sub import_params {
    my ($self,$params) = @_;

    $self->{defaults} = {};
    return unless $params;

    foreach my $param (keys %$params) {
        next unless $param =~ /\./;
        my $val = $params->{$param};
        $val = $val->[0] if ref($val);
        $self->{defaults}->{$param} = $val;
    }

    return;
}

=head2 $value = $form->default_for($row,$column)

Return a default value for $column in $row from imported parameters or column value

=cut

sub default_for {
    my ($self,$row,$column) = @_;

    my $name = $self->name_for($row,$column);
    return $self->{defaults}->{$name} if defined($self->{defaults}->{$name});
    return $row->$column if $row->can($column);
    return;
}

=head2 print $form->text_field($row,$column,[@params])

Return a textfield

=cut

sub text_field {
    my ($self,$row,$column,@p) = @_;

    my $name    = $self->name_for($row,$column);
    my $default = $self->default_for($row,$column);

    my $a = HTML::Element->new("input", type => "text", name => $name, @p);
    $a->attr(class => "error") if $self->errors_for($row,$column);
    $a->attr(value => $default) if defined($default);

    my $result = $a->as_XML;
    $a->delete;
    return $result;
}

=head2 $form->text_area($row,$column,[@params])

Return a textarea

=cut

sub text_area {
    my ($self,$row,$column,@p) = @_;

    my $name    = $self->name_for($row,$column);
    my $default = $self->default_for($row,$column);

    my $a = HTML::Element->new("textarea", name => $name,@p);
    $a->attr(class => "error") if $self->errors_for($row,$column);
    $a->push_content($default) if defined($default);

    my $result = $a->as_XML;
    $a->delete;
    return $result;
}

=head2 print $form->hidden_field($row,$column,[@params])

Return a hidden value

=cut

sub hidden_field {
    my ($self,$row,$column,@p) = @_;

    my $name    = $self->name_for($row,$column);
    my $default = $self->default_for($row,$column);

    my $a = HTML::Element->new("input", type => "hidden", name => $name, @p);
    $a->attr(value => $default) if defined($default);

    my $result = $a->as_XML;
    $a->delete;
    return $result;
}

=head2 $form->check_box($row,$column,[$yes,[$no,[@params]]])

Return a checkbox, $yes and $no set the values for a checked and
unchecked box.

=cut

sub check_box {
    my ($self,$row,$column,$yes,$no,@p) = @_;
    $yes ||= 1;
    $no  ||= 0;

    my $name    = $self->name_for($row,$column);
    my $default = $self->default_for($row,$column);

    my $a = HTML::Element->new("input", type => "checkbox",
                               name => $name, value => $yes, @p);
    $a->attr(class => "error") if $self->errors_for($row,$column);
    $a->attr(checked => "checked") if defined($default) and $default eq $yes;

    my $b = HTML::Element->new("input", type => "hidden", class => "hidden",
                               name => $name, value => $no);

    my $result = $a->as_XML . $b->as_XML;
    $a->delete;
    $b->delete;
    return $result;
}

=head2 $form->select($row,$column,$values,@p)

Return a select list. $values should be a reference to an array of
values for the list - if an element is a scalar it's used both for the
visible text and the returned value; if itself an array reference then
the first element provides the returned value and the second the
visible text label.

=cut

sub select {
    my ($self,$row,$column,$values,@p) = @_;

    my $name    = $self->name_for($row,$column);
    my $default = $self->default_for($row,$column);

    my $a = HTML::Element->new("select", name => $name, @p);
    foreach my $v (@$values) {
        my ($value,$label) = ref($v) ? @$v : ($v,$v);
        my $sel = HTML::Element->new("option", value => $value);
        $sel->attr(selected => "selected") 
            if defined($default) and $default eq $value;
        $sel->push_content($label);
        $a->push_content($sel);
    }

    my $result = $a->as_XML;
    $a->delete;
    return $result;
}

=head2 print $form->file_field($row,$column,[@params])

Return a file upload field. Doesnt attempt to do any sort of stickeyness

=cut

sub file_field {
    my ($self,$row,$column,@p) = @_;

    my $name    = $self->name_for($row,$column);

    my $a = HTML::Element->new("input", type => "file", name => $name, @p);

    my $result = $a->as_XML;
    $a->delete;
    return $result;
}

1;

__END__

cgi.mason implimentation, for comparison

<%method textfield>

<%args>
$name
$default    => ''
$override   => ''
$size       => ''
$maxlength  => ''
$title      => ''
</%args>

<%init>
my $value  = $override ? $default : 
                     defined($args{$name}) ? $args{$name} : $default;
$value     = ' value="'     . encode_entities($value)     . '"' if $value;
$size      = ' size="'      . encode_entities($size)      . '"' if $size;
$maxlength = ' maxlength="' . encode_entities($maxlength) . '"' if $maxlength;
$title     = ' title="'     . encode_entities($title)     . '"' if $title;
</%init>

<input type="text" id="<% $name |h %>" 
       name="<% $name |h %>"<% "$value$size$maxlength$title" |n %> />

</%method>

%# ===================================================================

<%method text>

<%args>
$name
$default    => ''
$override   => ''
</%args>

<%init>
my $value  = $override ? $default : 
                     defined($args{$name}) ? $args{$name} : $default;
</%init>

<% $value |h %>

</%method>

%# ===================================================================

<%method password_field>

<%args>
$name
$default    => ''
$override   => ''
$size       => ''
$maxlength  => ''
$title      => ''
</%args>

<%init>
my $value  = $override ? $default : 
                     defined($args{$name}) ? $args{$name} : $default;
$value     = ' value="'     . encode_entities($value)     . '"' if $value;
$size      = ' size="'      . encode_entities($size)      . '"' if $size;
$maxlength = ' maxlength="' . encode_entities($maxlength) . '"' if $maxlength;
$title     = ' title="'     . encode_entities($title)     . '"' if $title;
</%init>

<input type="password" id="<% $name |h %>" 
       name="<% $name |h %>"<% "$value$size$maxlength$title" |n %> />

</%method>

%# ===================================================================

<%method textarea>

<%args>
$name
$default    => ''
$override   => ''
$rows       => ''
$cols       => ''
$title      => ''
</%args>

<%init>
my $value  = $override ? $default : 
                     defined($args{$name}) ? $args{$name} : $default;
$rows      = ' rows="'  . encode_entities($rows)  . '"' if $rows;
$cols      = ' cols="'  . encode_entities($cols)  . '"' if $cols;
$title     = ' title="' . encode_entities($title) . '"' if $title;
</%init>

<textarea id="<% $name |h %>" name="<% $name |h %>"
          <% "$rows$cols" |n %>><% $value |h %><% $title |n %></textarea>

</%method> 
 
%# ===================================================================

<%method popup_menu>

<%args>
$name
@values
$default    => ''
$override   => ''
%labels     => ()
$title      => ''
</%args>

<%init>
my $select = $override ? $default :
                    defined($args{$name}) ? $args{$name} : $default;
$title     = ' title="' . encode_entities($title) . '"' if $title;
</%init>

<select id="<% $name |h %>" name="<% $name |h %><% $title |n %>">
% foreach (@values) {
%   my $selected = $_ eq $select ? qq/ selected="selected"/ : '';
%   my $label    = $labels{$_} ? $labels{$_} : $_;  
  <option value="<% $_ |h %>"<% $selected |n %>><% $label |h %></option>
% } 
</select>

</%method>

%# ===================================================================

<%method scrolling_list>

<%args>
$name
@values
@default    => ()
$override   => ''
$size       => ''
$multiple   => ''
%labels     => ()
$title      => ''
</%args>

<%init>
my @last   = ref($args{$name}) eq 'ARRAY' ? @{$args{$name}} : 
                                     $args{$name} ? ($args{$name}) : ();
my %select = map { $_ => 1 } ((!@last or $override) ? @default : @last);
$size      ||= scalar(@values);
$size      = ' size="'     . encode_entities($size)     . '"';
$multiple  = $multiple ? ' multiple="multiple"' : '';
$title     = ' title="'    . encode_entities($title)    . '"' if $title;
</%init>

<select id="<% $name |h %>" name="<% $name |h %>"<% "$size$multiple$title" |n %>>
% foreach (@values) {
%   my $selected = $select{$_} ? qq/ selected="selected"/ : '';
%   my $label    = $labels{$_} ? $labels{$_} : $_;  
  <option value="<% $_ |h %>"<% $selected |n %>><% $label |h %></option>
% } 
</select>

</%method>

%# ===================================================================

<%method checkbox_group>

<%args>
$name
@values
@default    => ()
$override   => ''
$linebreak  => ''
%labels     => ()
$title      => ''
</%args>

<%init>
my @last   = ref($args{$name}) eq 'ARRAY' ? @{$args{$name}} : 
                                     $args{$name} ? ($args{$name}) : ();
my %select = map { $_ => 1 } ((!@last or $override) ? @default : @last);
$title     = ' title="' . encode_entities($title) . '"' if $title;
</%init>

%foreach (@values) {
%   my $select = $select{$_} ? qq/ checked="checked"/ : '';
%   my $label  = $labels{$_} ? $labels{$_} : $_;  
  <input type="checkbox" id="<% "$name-$_" |h %>" name="<% $name |h %>" 
         value="<% $_ |h %>"<% "$select$title" |n %> />
  <label for="<% "$name-$_" |h %>"><% $label |h %></label>
%   if ($linebreak) {
  <br />
%   }
% }

</%method>

%# ===================================================================

<%method checkbox>

<%args>
$name
$value
$default    => ''
$override   => ''
$title      => ''
</%args>

<%init>
my $select = $override ? $default :
                    defined($args{$name}) ? $args{$name} : $default;
$select = $select ? qq/ checked="checked"/ : '';
$title =  ' title="' . encode_entities($title) . '"' if $title;
</%init>

<input type="checkbox" id="<% "$name-$value" |h %>" name="<% $name |h %>" 
       value="<% $value |h %>"<% "$select$title" |n %> />

</%method>

%# ===================================================================

<%method radio_group>

<%args>
$name
@values
$default    => ''
$override   => ''
$linebreak  => ''
%labels     => ()
$title      => ''
</%args>

<%init>
my $select = $override ? $default :
                    defined($args{$name}) ? $args{$name} : $default;
$title     = ' title="' . encode_entities($title) . '"' if $title;
</%init>

%foreach (@values) {
%   my $checked  = $_ eq $select ? qq/ checked="checked"/ : '';
%   my $label    = $labels{$_} ? $labels{$_} : $_;  
  <input type="radio" id="<% "$name-$_" |h %>" name="<% $name |h %>" 
         value="<% $_ |h %>"<% "$checked$title" |n %> />
  <label for="<% "$name-$_" |h %>"><% $label |h %></label>
%   if ($linebreak) {
  <br />
%   }
% }

</%method>
%# ===================================================================

%# Note - all radio buttons with the same name (and so part of the
%# same group) must have the same default otherwise chaos will
%# ensue. You have been warned!

<%method radio>

<%args>
$name
$value
$default    => ''
$override   => ''
$title      => ''
</%args>

<%init>
my $select = $override ? $default :
                    defined($args{$name}) ? $args{$name} : $default;
$select = $value eq $select ? qq/ checked="checked"/ : '';
$title = ' title="' . encode_entities($title) . '"' if $title;
</%init>

<input type="radio" id="<% "$name-$value" |h %>" name="<% $name |h %>" 
       value="<% $value |h %>"<% "$select$title" |n %> />

</%method>

%# ===================================================================

<%method submit>

<%args>
$name
$label
$title => ''
</%args>

<%init>
$title = ' title="' . encode_entities($title) . '"' if $title;
</%init>

<input type="submit" id="<% $name |h %>" name="<% $name |h %>" 
       value="<% $label |h %>"<% $title |n %> />

</%method>

%# ===================================================================

<%method hidden>

<%args>
$name
$default    => ''
$override   => ''
$title      => ''
</%args>

<%init>
my $value = $override ? $default : 
                     defined($args{$name}) ? $args{$name} : $default;
$title = ' title="' . encode_entities($title) . '"' if $title;
</%init>

<input type="hidden" id="<% $name |h %>" name="<% $name |h %>" 
       value="<% $value |h %>"<% $title |n %> />

</%method>

%# ===================================================================




