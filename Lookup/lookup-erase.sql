--- ===Attributes of people===

DROP TABLE person_title;

DROP TABLE person_title_addable;

DROP TABLE person_registeredname;

DROP TABLE person_registeredname_addable;

DROP TABLE person_displayname;

DROP TABLE person_displayname_addable;

DROP TABLE person_mail;

DROP TABLE person_mail_addable;

DROP TABLE person_labeleduri;

DROP TABLE person_labeleduri_addable;

DROP TABLE person_phone;

DROP TABLE person_phone_addable;

DROP TABLE person_postaladdress;

DROP TABLE person_postaladdress_addable;

DROP TABLE person_jpegphoto;

DROP TABLE person_jpegphoto_addable;


--- ===Attributes of institutions===

DROP TABLE inst_ou;

--- Note absence of "suppression" and "suppressible".

DROP TABLE inst_ou_addable;

DROP TABLE inst_parentinstid;

DROP TABLE inst_parentinstid_addable;

DROP TABLE inst_groupid;

DROP TABLE inst_groupid_addable;

DROP TABLE inst_jpegphoto;

DROP TABLE inst_jpegphoto_addable;

DROP TABLE inst_postaladdress;

DROP TABLE inst_postaladdress_addable;

DROP TABLE inst_mail;

DROP TABLE inst_mail_addable;

DROP TABLE inst_phone;

DROP TABLE inst_phone_addable;

DROP TABLE inst_facsimiletelephonenumber;

DROP TABLE inst_facsimiletelephonenumber_addable;

DROP TABLE inst_labeleduri;

DROP TABLE inst_labeleduri_addable;

DROP TABLE inst_acronym;

DROP TABLE inst_acronym_addable;

DROP TABLE inst_mgrgroupid;

DROP TABLE inst_mgrgroupid_addable;

--- ===Attributes of groups===

DROP TABLE group_grouptitle;

DROP TABLE group_grouptitle_addable;

DROP TABLE group_description;

DROP TABLE group_description_addable;

DROP TABLE group_mail;

DROP TABLE group_mail_addable;

DROP TABLE group_expirytimestamp;

DROP TABLE group_expirytimestamp_addable;

DROP TABLE group_warntimestamp;

DROP TABLE group_warntimestamp_addable;

--- ===Relations between objects===

DROP TABLE person_inst;

DROP TABLE person_inst_addable;

--- Note absence of "suppression" and "suppressible".

DROP TABLE group_member;

DROP TABLE group_member_addable;

DROP TABLE group_mgrgroupid;

DROP TABLE group_mgrgroupid_addable;

DROP TABLE group_rdrgroupid;

DROP TABLE group_rdrgroupid_addable;

--- ===Primary keys===

DROP TABLE people;

DROP TABLE insts;

DROP TABLE groups;

--- Sequence

DROP SEQUENCE rowid_seq;

--- Domains

DROP DOMAIN uid;
DROP DOMAIN instid;
DROP DOMAIN groupid;
DROP DOMAIN generic_name;
DROP DOMAIN uri;
DROP DOMAIN email_addr;
DROP DOMAIN phone_number;
DROP DOMAIN postal_addr;
DROP DOMAIN jpeg;
