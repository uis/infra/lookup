
INSERT INTO people 
  VALUES ('fjc55');

INSERT INTO insts 
  VALUES ('IS');
INSERT INTO insts
  VALUES ('BOT');

INSERT INTO groups 
  VALUES ('123456');

--- ===Relations between objects===

INSERT INTO person_inst 
  VALUES ('fjc55','IS',TRUE,DEFAULT);

INSERT INTO person_inst_addable
  VALUES ('fjc55','IS',TRUE);

INSERT INTO group_member 
  VALUES ('123456','fjc55',TRUE,DEFAULT);

INSERT INTO group_member_addable
  VALUES ('123456',TRUE);

INSERT INTO group_mgrgroupid 
  VALUES ('123456','123456',TRUE,DEFAULT);

INSERT INTO group_mgrgroupid_addable
  VALUES ('123456',TRUE);

INSERT INTO group_rdrgroupid 
  VALUES ('123456','123456',TRUE,DEFAULT);

INSERT INTO group_rdrgroupid_addable
  VALUES ('123456',TRUE);

--- ===Attributes of people===

INSERT INTO person_title 
  VALUES ('fjc55','IS','Director',FALSE,DEFAULT,TRUE,TRUE);
INSERT INTO person_title
  VALUES ('fjc55','BOT','Dogsbody',FALSE,DEFAULT,TRUE,TRUE);

INSERT INTO person_title_addable 
  VALUES ('fjc55','IS',TRUE);

INSERT INTO person_registeredname 
  VALUES ('fjc55','IS','Clark','FJ','Dr',FALSE,DEFAULT,FALSE,TRUE);

-- INSERT INTO person_registeredname_addable 
--   VALUES ('fjc55','IS',TRUE);

INSERT INTO person_displayname 
  VALUES ('fjc55','IS','Fred Clark',FALSE,DEFAULT,TRUE,TRUE);

-- INSERT INTO person_displayname_addable 
--  VALUES ('fjc55','IS',TRUE);

INSERT INTO person_mail 
  VALUES ('fjc55','IS','fjc55@cam.ac.uk',FALSE,DEFAULT,TRUE,TRUE);

INSERT INTO person_mail_addable 
  VALUES ('fjc55','IS',TRUE);

INSERT INTO person_labeleduri 
  VALUES ('fjc55','IS','http://www.example.com/~fjc55/',
          'Fred''s Homepage',FALSE,DEFAULT,TRUE,TRUE);

INSERT INTO person_labeleduri_addable 
  VALUES ('fjc55','IS',TRUE);

INSERT INTO person_phone 
  VALUES ('fjc55','IS','98765','Desk',FALSE,DEFAULT,TRUE,TRUE);

INSERT INTO person_phone_addable 
  VALUES ('fjc55','IS',TRUE);

INSERT INTO person_postaladdress 
  VALUES ('fjc55','IS','Director'' Office',FALSE,DEFAULT,FALSE,TRUE);

INSERT INTO person_postaladdress_addable 
  VALUES ('fjc55','IS',TRUE);

--- ===Attributes of institutions===

INSERT INTO inst_ou 
  VALUES ('IS','Deaprtment of Important Studies',DEFAULT,TRUE);

INSERT INTO inst_ou_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_parentinstid 
  VALUES ('IS','IS',DEFAULT,TRUE);

INSERT INTO inst_parentinstid_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_groupid 
  VALUES ('IS','123456',DEFAULT,TRUE);

INSERT INTO inst_groupid_addable 
  VALUES ('IS',TRUE);

-- hm, tricky to inserte a JPEG here...

INSERT INTO inst_jpegphoto_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_postaladdress 
  VALUES ('IS','East Forum',DEFAULT,TRUE);

INSERT INTO inst_postaladdress_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_mail 
  VALUES ('IS','reception@inpstud.cam.ac.uk',DEFAULT,TRUE);

INSERT INTO inst_mail_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_phone 
  VALUES ('IS','98000','Reception',DEFAULT,TRUE);

INSERT INTO inst_phone_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_facsimiletelephonenumber 
  VALUES ('IS','98001','General',DEFAULT,TRUE);

INSERT INTO inst_facsimiletelephonenumber_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_labeleduri 
  VALUES ('IS','http://impstud.cam.ac.uk/','Department Website',DEFAULT,TRUE);

INSERT INTO inst_labeleduri_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_acronym 
  VALUES ('IS','DIS',DEFAULT,TRUE);

INSERT INTO inst_acronym 
  VALUES ('IS','DUST',DEFAULT,TRUE);

INSERT INTO inst_acronym_addable 
  VALUES ('IS',TRUE);

INSERT INTO inst_mgrgroupid 
  VALUES ('IS','123456',DEFAULT,TRUE);

INSERT INTO inst_mgrgroupid_addable 
  VALUES ('IS',TRUE);

--- ===Attributes of groups===

INSERT INTO group_grouptitle
  VALUES ('123456','IS Editors',DEFAULT,TRUE);

INSERT INTO group_grouptitle_addable
  VALUES ('123456',TRUE);
