person_attrs = [
    'uid', 'title', 'cn', 'sn', 'instid', 'ou', 'displayname', 'mail',
    'labeleduri', 'phone', 'postaladdress', 'jpegphoto'
    ]

inst_attrs = [
    'instid', 'ou', 'parentinstid', 'groupid', 'jpegphoto', 'postaladdress',
    'mail', 'phone', 'facsimiletelephonenumber', 'labeleduri', 'mgrgroupid',
    'acronym'
    ]

group_attrs = [
    'groupid', 'grouptitle', 'description', 'mgrgroupid',
    'rdrgroupid', 'uid', 'alluid', 'visibility', 'mail', 'expirytimestamp',
    'warntimestamp'
    ]

print '''
START TRANSACTION;

DROP SCHEMA publdap CASCADE;
CREATE SCHEMA publdap;

CREATE AGGREGATE publdap.cat(
  BASETYPE = text,
  SFUNC = textcat,
  STYPE = text,
  INITCOND = ''
);

CREATE FUNCTION publdap.base64(text) RETURNS text AS $$
  import base64
  return base64.b64encode(args[0])
$$ LANGUAGE plpythonu IMMUTABLE STRICT;

CREATE FUNCTION publdap.base64(bytea) RETURNS text AS $$
  import base64
  return base64.b64encode(args[0])
$$ LANGUAGE plpythonu IMMUTABLE STRICT;

CREATE FUNCTION publdap.ldif_is_safe(text) RETURNS boolean AS $$
  SELECT $1 ~ '^[\\x01-\\x09\\x0b\\x0c\\x0e-\\x1f\\x21-\\x39\\x3b\\x3d-\\x7f]'
              '[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f]*$';
$$ LANGUAGE sql IMMUTABLE STRICT;

CREATE FUNCTION publdap.ldifify(text, text) RETURNS text AS $$
  SELECT CASE WHEN publdap.ldif_is_safe($2) THEN $1 || ':' || $2 || E'\\n'
    ELSE $1 || '::' || publdap.base64($2) || E'\\n'
    END;
$$ LANGUAGE sql IMMUTABLE STRICT;

CREATE FUNCTION publdap.ldifify(text, bytea) RETURNS text AS $$
  SELECT $1 || '::' || publdap.base64($2) || E'\\n';
$$ LANGUAGE sql IMMUTABLE STRICT;

CREATE FUNCTION publdap.ldifify(text, timestamp with time zone)
    RETURNS text AS $$
  SELECT $1 || ':' ||
    to_char($2 AT TIME ZONE 'UTC', 'YYYYMMDDHH24MISS"."US"Z"');
$$ LANGUAGE sql IMMUTABLE STRICT;

CREATE FUNCTION publdap.makecn(text, text, text) RETURNS text AS $$
  return " ".join(filter(lambda x: x not in [None, ''], args))
$$ LANGUAGE plpythonu IMMUTABLE;

CREATE FUNCTION publdap.makephone(text, text) RETURNS text AS $$
  SELECT coalesce($1 || ' (' || nullif($2, '') || ')', $1);
$$ LANGUAGE sql IMMUTABLE;
    
CREATE FUNCTION publdap.makepostal(text) RETURNS text AS $$
  SELECT replace(replace(replace($1, E'\\\\', E'\\\\5c'), '$', E'\\\\24'),
      E'\n', ' $ ');
$$ LANGUAGE sql IMMUTABLE STRICT;



CREATE VIEW publdap.person_dn_ldif AS
  SELECT uid,
    'dn:uid=' || uid || ',ou=people,o=University of Cambridge,dc=cam,dc=ac,dc=uk' || E'\\n' AS ldif FROM allpeople;

CREATE VIEW publdap.person_uid_ldif AS
  SELECT uid, 'uid' AS attr, publdap.ldifify('uid', uid) AS ldif
    FROM people;

CREATE VIEW publdap.person_title_ldif AS
  SELECT uid, 'title' AS attr, publdap.ldifify('title', title) AS ldif
    FROM person_title;

CREATE VIEW publdap.person_cn_ldif AS
  SELECT uid, 'cn' AS attr,
         publdap.ldifify('cn', publdap.makecn(titles, initials, sn)) AS ldif
    FROM person_registeredname;

CREATE VIEW publdap.person_sn_ldif AS
  SELECT uid, 'sn' AS attr, publdap.ldifify('sn', sn) AS ldif
    FROM person_registeredname;

CREATE VIEW publdap.person_instid_ldif AS
  SELECT uid, 'instid' AS attr, publdap.ldifify('instid', instid) AS ldif
    FROM person_inst;

CREATE VIEW publdap.person_ou_ldif AS
  SELECT uid, 'ou' AS attr, publdap.ldifify('ou', ou) AS ldif
    FROM person_inst JOIN inst_ou USING (instid);

CREATE VIEW publdap.person_displayname_ldif AS
  SELECT uid, 'displayname' AS attr,
         publdap.ldifify('displayname', displayname) AS ldif
    FROM person_displayname;

-- XXX Handle preferred --
CREATE VIEW publdap.person_mail_ldif AS
  SELECT uid, 'mail' AS attr, publdap.ldifify('mail', mail) AS ldif
    FROM person_mail;

CREATE VIEW publdap.person_labeleduri_ldif AS
  SELECT uid, 'labeleduri' AS attr, publdap.ldifify('labeleduri',
      CASE WHEN label IS NULL THEN uri ELSE uri || ' ' || label END) AS ldif
    FROM person_labeleduri;

CREATE VIEW publdap.person_phone_ldif AS
  SELECT uid, 'phone' AS attr,
         publdap.ldifify('phone', publdap.makephone(phone, label)) AS ldif
    FROM person_phone;

CREATE VIEW publdap.person_postaladdress_ldif AS
  SELECT uid, 'postaladdress' AS attr, publdap.ldifify('postaladdress',
         publdap.makepostal(postaladdress)) AS ldif
    FROM person_postaladdress;

CREATE VIEW publdap.person_jpegphoto_ldif AS
  SELECT uid, 'jpegphoto' AS attr,
         publdap.ldifify('jpegphoto', jpegphoto) AS ldif
    FROM person_jpegphoto;
'''

print 'CREATE VIEW publdap.person_attrs_ldif AS '
print " UNION ALL\n".join(map(lambda(x):
                             "SELECT * FROM publdap.person_%s_ldif"%x,
                             person_attrs))+";"

print "CREATE TABLE publdap.person_attrs (attr text);"
for x in person_attrs:
    print "INSERT INTO publdap.person_attrs VALUES ('%s');" % x

print '''
CREATE VIEW publdap.person_catattrs_ldif AS
  SELECT uid, publdap.cat(ldif) AS ldif
    FROM publdap.person_attrs_ldif GROUP BY uid;

CREATE VIEW publdap.person_ldif AS
  SELECT uid,
    publdap.person_dn_ldif.ldif ||
      publdap.person_catattrs_ldif.ldif || E'\\n' AS ldif
  FROM publdap.person_dn_ldif JOIN publdap.person_catattrs_ldif USING (uid)
    JOIN people USING (uid);

CREATE VIEW publdap.person_ldif_add AS
  SELECT uid,
    publdap.person_dn_ldif.ldif || E'changetype:add\\n' ||
      publdap.person_catattrs_ldif.ldif || E'\\n' AS ldif
  FROM publdap.person_dn_ldif JOIN publdap.person_catattrs_ldif USING (uid);

CREATE VIEW publdap.person_modattrs_ldif AS
  SELECT uid,
         'replace:' || attr || E'\\n' || coalesce(ldif, '') || E'-\\n' AS ldif
  FROM publdap.person_attrs CROSS JOIN people LEFT JOIN (
    SELECT uid, attr, publdap.cat(ldif) AS ldif 
      FROM publdap.person_attrs_ldif GROUP BY uid, attr
  ) AS values USING (uid, attr);

CREATE VIEW publdap.person_catmodattrs_ldif AS
  SELECT uid, publdap.cat(ldif) AS ldif
  FROM publdap.person_modattrs_ldif GROUP BY uid;

CREATE VIEW publdap.person_ldif_modify AS
  SELECT uid,
    publdap.person_dn_ldif.ldif || E'changetype:modify\\n' ||
    publdap.person_catmodattrs_ldif.ldif AS ldif
  FROM publdap.person_dn_ldif JOIN publdap.person_catmodattrs_ldif USING (uid);

CREATE VIEW publdap.person_ldif_delete AS
  SELECT uid, ldif || E'changetype:delete\\n\\n' AS ldif
  FROM publdap.person_dn_ldif;


CREATE VIEW publdap.inst_dn_ldif AS
  SELECT instid,
    'dn:instid=' || instid || ',ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk' || E'\\n' AS ldif FROM allinsts;

CREATE VIEW publdap.inst_instid_ldif AS
  SELECT instid, 'instid' AS attr, publdap.ldifify('instid', instid) AS ldif
    FROM insts;

CREATE VIEW publdap.inst_ou_ldif AS
  SELECT instid, 'ou' AS attr, publdap.ldifify('ou', ou) AS ldif
    FROM inst_ou;

CREATE VIEW publdap.inst_parentinstid_ldif AS
  SELECT instid, 'parentinstid' AS attr,
         publdap.ldifify('parentinstid', parentinstid) AS ldif
    FROM inst_parentinstid;

CREATE VIEW publdap.inst_groupid_ldif AS
  SELECT instid, 'groupid' AS attr, publdap.ldifify('groupid', groupid) AS ldif
    FROM inst_groupid;

CREATE VIEW publdap.inst_jpegphoto_ldif AS
  SELECT instid, 'jpegphoto' AS attr, 
         publdap.ldifify('jpegphoto', jpegphoto) AS ldif
    FROM inst_jpegphoto;

CREATE VIEW publdap.inst_postaladdress_ldif AS
  SELECT instid, 'postaladdress' AS attr,
         publdap.ldifify('postaladdress', publdap.makepostal(postaladdress))
           AS ldif
    FROM inst_postaladdress;

-- XXX Need to split up mail and mailalternative properly --
CREATE VIEW publdap.inst_mail_ldif AS
  SELECT instid, 'mail' AS attr, publdap.ldifify('mail', mail) AS ldif
    FROM inst_mail;

CREATE VIEW publdap.inst_phone_ldif AS
  SELECT instid, 'phone' AS attr,
      publdap.ldifify('phone', publdap.makephone(phone, label)) AS ldif
    FROM inst_phone;

CREATE VIEW publdap.inst_facsimiletelephonenumber_ldif AS
  SELECT instid, 'facsimiletelephonenumber' AS attr,
         publdap.ldifify('facsimiletelephonenumber',
           publdap.makephone(facsimiletelephonenumber, label)) AS ldif
    FROM inst_facsimiletelephonenumber;

CREATE VIEW publdap.inst_labeleduri_ldif AS
  SELECT instid, 'labeleduri' AS attr, publdap.ldifify('labeleduri',
      CASE WHEN label IS NULL THEN uri ELSE uri || ' ' || label END) AS ldif
    FROM inst_labeleduri;

CREATE VIEW publdap.inst_mgrgroupid_ldif AS
  SELECT instid, 'mgrgroupid' AS attr,
         publdap.ldifify('mgrgroupid', mgrgroupid) AS ldif
    FROM inst_mgrgroupid;

CREATE VIEW publdap.inst_acronym_ldif AS
  SELECT instid, 'acronym' AS attr, publdap.ldifify('acronym', acronym) AS ldif
    FROM inst_acronym;

-- XXX lastupdated is not yet supported --
-- XXX cancelled is never emitted since it's not public --
-- XXX jdfather and jdname are not yet supported --
'''

print 'CREATE VIEW publdap.inst_attrs_ldif AS '
print " UNION ALL\n".join(map(lambda(x):
                             "SELECT * FROM publdap.inst_%s_ldif"%x,
                             inst_attrs))+";"

print "CREATE TABLE publdap.inst_attrs (attr text);"
for x in inst_attrs:
    print "INSERT INTO publdap.inst_attrs VALUES ('%s');" % x

print '''
CREATE VIEW publdap.inst_catattrs_ldif AS
  SELECT instid, publdap.cat(ldif) AS ldif
    FROM publdap.inst_attrs_ldif GROUP BY instid;

CREATE VIEW publdap.inst_ldif AS
  SELECT instid,
    publdap.inst_dn_ldif.ldif || publdap.inst_catattrs_ldif.ldif || E'\\n'
      AS ldif
  FROM publdap.inst_dn_ldif JOIN publdap.inst_catattrs_ldif USING (instid)
    JOIN insts USING (instid);

CREATE VIEW publdap.inst_ldif_add AS
  SELECT instid,
    publdap.inst_dn_ldif.ldif || E'changetype:add\\n' ||
      publdap.inst_catattrs_ldif.ldif || E'\\n' AS ldif
  FROM publdap.inst_dn_ldif JOIN publdap.inst_catattrs_ldif USING (instid);

CREATE VIEW publdap.inst_modattrs_ldif AS
  SELECT instid,
         'replace:' || attr || E'\\n' || coalesce(ldif, '') || E'-\\n' AS ldif
  FROM publdap.inst_attrs CROSS JOIN insts LEFT JOIN (
    SELECT instid, attr, publdap.cat(ldif) AS ldif 
      FROM publdap.inst_attrs_ldif GROUP BY instid, attr
  ) AS values USING (instid, attr);

CREATE VIEW publdap.inst_catmodattrs_ldif AS
  SELECT instid, publdap.cat(ldif) AS ldif
  FROM publdap.inst_modattrs_ldif GROUP BY instid;

CREATE VIEW publdap.inst_ldif_modify AS
  SELECT instid,
    publdap.inst_dn_ldif.ldif || E'changetype:modify\\n' ||
    publdap.inst_catmodattrs_ldif.ldif AS ldif
  FROM publdap.inst_dn_ldif JOIN publdap.inst_catmodattrs_ldif USING (instid);

CREATE VIEW publdap.inst_ldif_delete AS
  SELECT instid, ldif || E'changetype:delete\\n\\n' AS ldif
  FROM publdap.inst_dn_ldif;



CREATE VIEW publdap.group_dn_ldif AS
  SELECT groupid,
    'dn:groupid=' || groupid || ',ou=groups,o=University of Cambridge,dc=cam,dc=ac,dc=uk' || E'\\n' AS ldif FROM groups;

CREATE VIEW publdap.group_groupid_ldif AS
  SELECT groupid, 'groupid' AS attr,
         publdap.ldifify('groupid', groupid) AS ldif
    FROM groups;

CREATE VIEW publdap.group_grouptitle_ldif AS
  SELECT groupid, 'grouptitle' AS attr,
         publdap.ldifify('grouptitle', grouptitle) AS ldif
    FROM group_grouptitle;

CREATE VIEW publdap.group_description_ldif AS
  SELECT groupid, 'description' AS attr,
         publdap.ldifify('description', description) AS ldif
    FROM group_description;

CREATE VIEW publdap.group_mgrgroupid_ldif AS
  SELECT groupid, 'mgrgroupid' AS attr,
         publdap.ldifify('mgrgroupid', mgrgroupid) AS ldif
    FROM group_mgrgroupid;

CREATE VIEW publdap.group_rdrgroupid_ldif AS
  SELECT groupid, 'rdrgroupid' AS attr,
         publdap.ldifify('rdrgroupid', rdrgroupid) AS ldif
    FROM group_rdrgroupid;

CREATE VIEW publdap.group_uid_ldif AS
  SELECT groupid, 'uid' AS attr, publdap.ldifify('uid', uid) AS ldif
    FROM group_member;

CREATE VIEW publdap.group_alluid_ldif AS
  SELECT groupid, 'alluid' AS attr, publdap.ldifify('alluid', uid) AS ldif
    FROM group_local_member;

CREATE VIEW publdap.group_visibility_ldif AS
  SELECT groupid, 'visibility' AS attr,
         publdap.ldifify('visibility', visibility) AS ldif
    FROM group_visibility;

-- XXX userpassword not yet implemented.

CREATE VIEW publdap.group_mail_ldif AS
  SELECT groupid, 'mail' AS attr, publdap.ldifify('mail', mail) AS ldif
    FROM group_mail;

-- XXX cancelled not populated, but that's OK.
-- XXX lastupdate not yet implemented.

CREATE VIEW publdap.group_expirytimestamp_ldif AS
  SELECT groupid, 'expirytimestamp' AS attr,
         publdap.ldifify('expirytimestamp', expirytimestamp) AS ldif
    FROM group_expirytimestamp;

CREATE VIEW publdap.group_warntimestamp_ldif AS
  SELECT groupid, 'warntimestamp' AS attr,
         publdap.ldifify('warntimestamp', warntimestamp) AS ldif
    FROM group_warntimestamp;
'''

print 'CREATE VIEW publdap.group_attrs_ldif AS '
print " UNION ALL\n".join(map(lambda(x):
                             "SELECT * FROM publdap.group_%s_ldif"%x,
                             group_attrs))+";"

print "CREATE TABLE publdap.group_attrs (attr text);"
for x in group_attrs:
    print "INSERT INTO publdap.group_attrs VALUES ('%s');" % x

print '''
CREATE VIEW publdap.group_catattrs_ldif AS
  SELECT groupid, publdap.cat(ldif) AS ldif
    FROM publdap.group_attrs_ldif GROUP BY groupid;

CREATE VIEW publdap.group_ldif AS
  SELECT groupid,
    publdap.group_dn_ldif.ldif || publdap.group_catattrs_ldif.ldif || E'\\n'
      AS ldif
  FROM publdap.group_dn_ldif JOIN publdap.group_catattrs_ldif USING (groupid);

CREATE VIEW publdap.group_ldif_add AS
  SELECT groupid,
    publdap.group_dn_ldif.ldif || E'changetype:add\\n' ||
      publdap.group_catattrs_ldif.ldif || E'\\n' AS ldif
  FROM publdap.group_dn_ldif JOIN publdap.group_catattrs_ldif USING (groupid);

CREATE VIEW publdap.group_modattrs_ldif AS
  SELECT groupid,
         'replace:' || attr || E'\\n' || coalesce(ldif, '') || E'-\\n' AS ldif
  FROM publdap.group_attrs CROSS JOIN groups LEFT JOIN (
    SELECT groupid, attr, publdap.cat(ldif) AS ldif 
      FROM publdap.group_attrs_ldif GROUP BY groupid, attr
  ) AS values USING (groupid, attr);

CREATE VIEW publdap.group_catmodattrs_ldif AS
  SELECT groupid, publdap.cat(ldif) AS ldif
  FROM publdap.group_modattrs_ldif GROUP BY groupid;

CREATE VIEW publdap.group_ldif_modify AS
  SELECT groupid,
    publdap.group_dn_ldif.ldif || E'changetype:modify\\n' ||
    publdap.group_catmodattrs_ldif.ldif AS ldif
  FROM publdap.group_dn_ldif JOIN 
    publdap.group_catmodattrs_ldif USING (groupid);

CREATE VIEW publdap.group_ldif_delete AS
  SELECT groupid, ldif || E'changetype:delete\\n\\n' AS ldif
  FROM publdap.group_dn_ldif;



CREATE DOMAIN publdap.op varchar(3)
  CONSTRAINT op CHECK (VALUE='add' OR VALUE='del' OR VALUE='mod');

CREATE TABLE publdap.person_pending (
  uid uid REFERENCES allpeople PRIMARY KEY,
  op publdap.op NOT NULL
);

CREATE TABLE publdap.inst_pending (
  instid instid REFERENCES allinsts PRIMARY KEY,
  op publdap.op NOT NULL
);

CREATE TABLE publdap.group_pending (
  groupid groupid REFERENCES groups PRIMARY KEY,
  op publdap.op NOT NULL
);

CREATE FUNCTION publdap.add_person(uid) RETURNS void AS $$
  -- If there's already a record for this person it must be 'del'
  -- (since otherwise they can't be added), and we convert
  -- 'del' + 'add' into 'mod'.
  UPDATE publdap.person_pending SET op = 'mod' WHERE uid = $1;
  INSERT INTO publdap.person_pending SELECT $1, 'add'
    WHERE NOT EXISTS
      (SELECT 1 FROM publdap.person_pending WHERE uid = $1);
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION publdap.modify_person(uid) RETURNS void AS $$
  INSERT INTO publdap.person_pending SELECT $1, 'mod'
    WHERE NOT EXISTS
      (SELECT 1 FROM publdap.person_pending WHERE uid = $1);
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION publdap.delete_person(uid) RETURNS void AS $$
  UPDATE publdap.person_pending SET op = 'del' WHERE uid = $1;
  INSERT INTO publdap.person_pending SELECT $1, 'del'
    WHERE NOT EXISTS
      (SELECT 1 FROM publdap.person_pending WHERE uid = $1);
$$ LANGUAGE sql SECURITY DEFINER;


CREATE FUNCTION publdap.person_modattr() RETURNS trigger AS $$
  BEGIN
    PERFORM publdap.modify_person(NEW.uid);
    RETURN NULL;
  END
$$ LANGUAGE plpgsql SECURITY DEFINER;
'''

for table in ['person_local_title', 'person_local_inst',
              'person_local_displayname', 'person_local_mail',
              'person_local_labeleduri', 'person_local_phone',
              'person_local_postaladdress', 'person_local_jpegphoto',
              'jackdaw_person_inst', 'jackdaw_users',
              'jackdaw_registeredname_suppress', 'jackdaw_mail_suppress'
              ]:
    print '''
    CREATE TRIGGER publdap_person
      AFTER INSERT OR DELETE OR UPDATE ON %(table)s
      FOR EACH ROW EXECUTE PROCEDURE publdap.person_modattr();
    ''' % { 'table': table }

print '''
CREATE FUNCTION publdap.person_jdchange() RETURNS trigger AS $$
  BEGIN
    IF TG_OP = 'DELETE' THEN
      PERFORM publdap.delete_person(OLD.uid);
    ELSIF TG_OP = 'INSERT' THEN
      PERFORM publdap.add_person(NEW.uid);
    ELSIF TG_OP = 'UPDATE' THEN
      IF NEW.cancelled AND NOT OLD.cancelled THEN
        PERFORM publdap.delete_person(OLD.uid);
      ELSIF OLD.cancelled AND NOT NEW.cancelled THEN
        PERFORM publdap.add_person(NEW.uid);
      END IF;
    END IF;
    RETURN NULL;
  END
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE TRIGGER publdap_jdchange
  AFTER INSERT OR UPDATE OR DELETE ON jackdaw_users
  FOR EACH ROW EXECUTE PROCEDURE publdap.person_jdchange();


CREATE FUNCTION publdap.add_inst(instid) RETURNS void AS $$
  -- If there's already a record for this inst it must be 'del'
  -- (since otherwise they can't be added), and we convert
  -- 'del' + 'add' into 'mod'.
  UPDATE publdap.inst_pending SET op = 'mod' WHERE instid = $1;
  INSERT INTO publdap.inst_pending SELECT $1, 'add'
    WHERE NOT EXISTS
      (SELECT 1 FROM publdap.inst_pending WHERE instid = $1);
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION publdap.modify_inst(instid) RETURNS void AS $$
  INSERT INTO publdap.inst_pending SELECT $1, 'mod'
    WHERE NOT EXISTS
      (SELECT 1 FROM publdap.inst_pending WHERE instid = $1);
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION publdap.delete_inst(instid) RETURNS void AS $$
  UPDATE publdap.inst_pending SET op = 'del' WHERE instid = $1;
  INSERT INTO publdap.inst_pending SELECT $1, 'del'
    WHERE NOT EXISTS
      (SELECT 1 FROM publdap.inst_pending WHERE instid = $1);
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION publdap.rename_inst(instid) RETURNS void AS $$
  INSERT INTO publdap.person_pending SELECT uid, 'mod' FROM person_inst
    WHERE instid = $1 AND NOT EXISTS
      (SELECT 1 FROM publdap.person_pending WHERE uid = person_inst.uid)
$$ LANGUAGE sql SECURITY DEFINER;  

CREATE FUNCTION publdap.inst_modattr() RETURNS trigger AS $$
  BEGIN
    PERFORM publdap.modify_inst(NEW.instid);
    RETURN NULL;
  END
$$ LANGUAGE plpgsql SECURITY DEFINER;
'''

for table in ['inst_local_mgrgroupid', 'inst_local_groupid',
              'inst_local_jpegphoto', 'inst_local_postaladdress',
              'inst_local_mail', 'inst_local_phone',
              'inst_local_facsimiletelephonenumber', 'inst_local_labeleduri',
              'inst_local_acronym']:
    print '''
    CREATE TRIGGER publdap_inst
      AFTER INSERT OR DELETE OR UPDATE ON %(table)s
      FOR EACH ROW EXECUTE PROCEDURE publdap.inst_modattr();
    ''' % { 'table': table }

# XXX need something similar for changes to inst_remap.

print '''
CREATE FUNCTION publdap.inst_rename() RETURNS trigger AS $$
  BEGIN
    IF NEW.name != OLD.name THEN
      PERFORM publdap.rename_inst(NEW.instid);
    END IF;
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE FUNCTION publdap.inst_jdchange() RETURNS trigger AS $$
  BEGIN
    IF TG_OP = 'DELETE' THEN
      PERFORM publdap.delete_inst(OLD.instid);
      PERFORM publdap.rename_inst(OLD.instid);
    ELSIF TG_OP = 'INSERT' THEN
      PERFORM publdap.add_inst(NEW.instid);
      PERFORM publdap.rename_inst(NEW.instid);
    ELSIF TG_OP = 'UPDATE' THEN
      IF NEW.cancelled AND NOT OLD.cancelled THEN
        PERFORM publdap.delete_inst(OLD.instid);
        PERFORM publdap.rename_inst(OLD.instid);
      ELSIF OLD.cancelled AND NOT NEW.cancelled THEN
        PERFORM publdap.add_inst(NEW.instid);
        PERFORM publdap.rename_inst(NEW.instid);
      ELSIF OLD.name != NEW.name THEN
        PERFORM publdap.rename_inst(NEW.instid);
      END IF;
    END IF;
    RETURN NULL;
  END
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE TRIGGER publdap_jdchange
  AFTER INSERT OR UPDATE OR DELETE ON jackdaw_inst
  FOR EACH ROW EXECUTE PROCEDURE publdap.inst_jdchange();


CREATE FUNCTION publdap.add_group(groupid) RETURNS void AS $$
  -- If there's already a record for this group it must be 'del'
  -- (since otherwise they can't be added), and we convert
  -- 'del' + 'add' into 'mod'.
  UPDATE publdap.group_pending SET op = 'mod' WHERE groupid = $1;
  INSERT INTO publdap.group_pending SELECT $1, 'add'
    WHERE NOT EXISTS
      (SELECT 1 FROM publdap.group_pending WHERE groupid = $1);
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION publdap.modify_group(groupid) RETURNS void AS $$
  INSERT INTO publdap.group_pending SELECT $1, 'mod'
    WHERE NOT EXISTS
      (SELECT 1 FROM publdap.group_pending WHERE groupid = $1);
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION publdap.delete_group(groupid) RETURNS void AS $$
  UPDATE publdap.group_pending SET op = 'del' WHERE groupid = $1;
  INSERT INTO publdap.group_pending SELECT $1, 'del'
    WHERE NOT EXISTS
      (SELECT 1 FROM publdap.group_pending WHERE groupid = $1);
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION publdap.group_modattr() RETURNS trigger AS $$
  BEGIN
    PERFORM publdap.modify_group(NEW.groupid);
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql SECURITY DEFINER;
'''

for table in ['group_local_grouptitle', 'group_local_description',
              'group_local_mgrgroupid', 'group_local_rdrgroupid',
              'group_local_member', 'group_local_visibility',
              'group_local_mail', 'group_local_expirytimestamp',
              'group_local_warntimestamp' ]:
    print '''
    CREATE TRIGGER publdap_group
      AFTER INSERT OR DELETE OR UPDATE ON %(table)s
      FOR EACH ROW EXECUTE PROCEDURE publdap.group_modattr();
    ''' % { 'table': table }

print '''
CREATE FUNCTION publdap.group_create() RETURNS trigger AS $$
  BEGIN
    PERFORM publdap.add_group(NEW.groupid);
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE TRIGGER publdap_group
  AFTER INSERT ON groups
  FOR EACH ROW EXECUTE PROCEDURE publdap.group_create();


CREATE VIEW publdap.pending_ldif AS
  SELECT ldif FROM publdap.person_pending JOIN
    publdap.person_ldif_add USING (uid) WHERE op = 'add' UNION
  SELECT ldif FROM publdap.person_pending JOIN
    publdap.person_ldif_modify USING (uid) where op = 'mod' UNION
  SELECT ldif FROM publdap.person_pending JOIN
    publdap.person_ldif_delete USING (uid) WHERE op = 'del' UNION
  SELECT ldif FROM publdap.inst_pending JOIN
    publdap.inst_ldif_add USING (instid) WHERE op = 'add' UNION
  SELECT ldif FROM publdap.inst_pending JOIN
    publdap.inst_ldif_modify USING (instid) where op = 'mod' UNION
  SELECT ldif FROM publdap.inst_pending JOIN
    publdap.inst_ldif_delete USING (instid) WHERE op = 'del' UNION
  SELECT ldif FROM publdap.group_pending JOIN
    publdap.group_ldif_add USING (groupid) WHERE op = 'add' UNION
  SELECT ldif FROM publdap.group_pending JOIN
    publdap.group_ldif_modify USING (groupid) where op = 'mod' UNION
  SELECT ldif FROM publdap.group_pending JOIN
    publdap.group_ldif_delete USING (groupid) WHERE op = 'del';
'''

print 'COMMIT;'

