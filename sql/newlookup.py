class sqlfield:
    '''
    Abstract view of a SQL field.  Has two attributes, name and type.
    '''
    def __init__(self, name, type=None):
        (self.name, self.type) = (name, type)

def formlist(format, list):
    def gendict(elem):
        return {'name': elem.name, 'type': elem.type}
    return map(lambda(elem): format % gendict(elem), list)
def fullfields(fields):
    return ", ".join(formlist("%(name)s %(type)s", fields))
def fieldnames(fields):
    return ", ".join(formlist("%(name)s", fields))
def newfields(fields):
    return ", ".join(formlist("NEW.%(name)s", fields))
def updfields(fields):
    return ", ".join(formlist("%(name)s = NEW.%(name)s", fields))
def selfields(fields):
    return 'rowid = OLD.rowid'
def selnewfields(fields):
    return " AND ".join(formlist("%(name)s = NEW.%(name)s", fields))

# Standard fields
suppress = [sqlfield('suppress', 'boolean NOT NULL')]
suppressible = [sqlfield('suppressible', 'boolean NOT NULL')]
editable = [sqlfield('editable', 'boolean NOT NULL')]
addable = [sqlfield('addable', 'boolean NOT NULL')]
mailpref = [sqlfield('mail', 'email_addr NOT NULL'),
            sqlfield('preferred', 'boolean NOT NULL')]
groupid = [sqlfield('groupid', 'groupid REFERENCES groups NOT NULL')]
mgrgroupid = [sqlfield('mgrgroupid', 'groupid REFERENCES groups NOT NULL')]
uid = [sqlfield('uid', 'uid REFERENCES allpeople NOT NULL')]
instid = [sqlfield('instid', 'instid REFERENCES allinsts NOT NULL')]

class sqlrelation:
    '''
    Abstract view of a SQL relation.  Has a name and some fields.
    '''
    def __init__(self, name, fields=[]):
        (self.name, self.fields) = (name, fields)

print '''
CREATE LANGUAGE plpgsql;

START TRANSACTION;

-- We probably want a SQL domain for each type of data we store.

-- crsids
CREATE DOMAIN uid    varchar(7)
  CONSTRAINT uid_syntax CHECK (VALUE ~ '^[[:lower:][:digit:]]+$');
CREATE DOMAIN instid varchar(7)
  CONSTRAINT instid_syntax CHECK (VALUE = '-none-' OR
                                  VALUE ~ '^[[:upper:][:digit:]]+$');
CREATE DOMAIN groupid varchar(1024)
  CONSTRAINT groupid_syntax CHECK (VALUE = '000000' OR
                                   VALUE ~ '[1-9][[:digit:]]*');

-- name (personal, role, etc)
CREATE DOMAIN generic_name varchar(1024);

-- telephone number (and description)
CREATE DOMAIN phone_number varchar(1024)
  CONSTRAINT phone_number_syntax
  CHECK (VALUE ~ '^[-A-Za-z0-9\(\)+,\.\/:\? ]*$');

-- (labelled) URI
CREATE DOMAIN uri varchar(1024)
  CONSTRAINT uri_syntax CHECK (VALUE ~ '^(ftp|http|gopher|https)://');

-- email address
CREATE DOMAIN email_addr varchar(1024)
  CONSTRAINT email_addr_syntax
  CHECK (VALUE ~ '^[-A-Za-z0-9!#$%&''*+/;=?^_`{|}~]+'
                 '(\\.[-A-Za-z0-9!#$%&''*+/;=?^_`{|}~]+)*\\@'
                 '[-A-Za-z0-9!#$%&''*+/;=?^_`{|}~]+'
                 '(\\.[-A-Za-z0-9!#$%&''*+/;=?^_`{|}~]+)*$');

-- postal address
CREATE DOMAIN postal_addr varchar(1024);

-- JPEG photo
CREATE DOMAIN jpeg bytea;

-- Group visibility
CREATE DOMAIN visibility varchar(8)
  CONSTRAINT visibility_syntax
    CHECK (VALUE = 'cam' OR VALUE = 'members' OR VALUE = 'managers');

CREATE SEQUENCE rowid;
GRANT SELECT, UPDATE ON rowid TO PUBLIC;

CREATE TABLE allpeople (uid uid PRIMARY KEY);

CREATE FUNCTION people_insert() RETURNS trigger AS $$
  BEGIN
    EXECUTE 'CREATE USER ' || quote_ident(NEW.uid);
    RETURN NEW;
  EXCEPTION
    WHEN duplicate_object THEN
      RETURN NEW;
  END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER people_insert BEFORE INSERT ON allpeople
  FOR EACH ROW EXECUTE PROCEDURE people_insert();

CREATE FUNCTION people_delete() RETURNS trigger AS $$
  BEGIN
    EXECUTE 'DROP USER '|| quote_ident(OLD.uid);
    RETURN OLD;
  END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER people_delete BEFORE DELETE ON allpeople
  FOR EACH ROW EXECUTE PROCEDURE people_delete();

CREATE TABLE jackdaw_users (
  uid uid REFERENCES allpeople NOT NULL,
  sn generic_name NOT NULL,
  initials generic_name NOT NULL,
  titles generic_name NOT NULL,
  cn generic_name NOT NULL,
  atcam boolean,
  exdir boolean,
  cancelled boolean,
  rowid bigint DEFAULT nextval('rowid') PRIMARY KEY
  );

CREATE VIEW people AS
  SELECT uid FROM allpeople JOIN jackdaw_users USING (uid) WHERE NOT cancelled;

CREATE TABLE allinsts (instid instid PRIMARY KEY);

INSERT INTO allinsts (instid) VALUES ('-none-');

CREATE TABLE jackdaw_inst (
  instid instid REFERENCES allinsts NOT NULL,
  parentinstid instid REFERENCES allinsts NOT NULL,
  name generic_name NOT NULL,
  cancelled boolean NOT NULL,
  rowid bigint DEFAULT nextval('rowid') PRIMARY KEY);

CREATE VIEW insts AS
  SELECT instid FROM allinsts JOIN jackdaw_inst USING (instid)
    WHERE NOT cancelled UNION
  SELECT '-none-' AS instid;

CREATE SEQUENCE groupid_gen START 102000;

CREATE FUNCTION new_groupid() RETURNS groupid AS $$
  SELECT CAST(nextval('groupid_gen') AS groupid);
$$ LANGUAGE sql SECURITY DEFINER;

CREATE TABLE groups (groupid groupid PRIMARY KEY
                     DEFAULT new_groupid());

CREATE TABLE noaccess (dummy int);
INSERT INTO noaccess VALUES (0);
CREATE FUNCTION noaccess() RETURNS trigger AS $$
  BEGIN
    RAISE EXCEPTION 'noaccess: permission denied';
  END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER noaccess BEFORE INSERT OR DELETE OR UPDATE ON noaccess
  FOR EACH ROW EXECUTE PROCEDURE noaccess();
'''

def dodeleterule(table, what):
    print '''
    CREATE RULE delete_dummy AS ON DELETE TO %(table)s DO INSTEAD NOTHING;
    CREATE RULE delete_ok AS ON DELETE TO %(table)s WHERE OLD.editable
      DO ALSO %(what)s;
    CREATE RULE delete_bad AS ON DELETE TO %(table)s WHERE NOT OLD.editable
      DO ALSO DELETE FROM noaccess;
    ''' % { 'table': table, 'what': what }

def doupdaterule(table, what, extrawhere = 'TRUE'):
    where = 'OLD.editable AND (' + extrawhere + ')'
    print '''
    CREATE RULE update_dummy AS ON UPDATE TO %(table)s DO INSTEAD NOTHING;
    CREATE RULE update_ok AS ON UPDATE TO %(table)s WHERE %(where)s
      DO ALSO %(what)s;
    CREATE RULE update_bad AS ON UPDATE TO %(table)s WHERE NOT (%(where)s)
      DO ALSO UPDATE noaccess SET dummy = 0;
    ''' % { 'table': table, 'what': what, 'where': where }

def doinsertrule(table, keys, what, extrawhere = 'TRUE'):
    where = ( 'EXISTS(SELECT 1 FROM ' + table + '_addable WHERE ' +
              selnewfields(keys) + ' AND addable AND (' + extrawhere + '))' )
    print '''
    CREATE RULE insert_dummy AS ON INSERT TO %(table)s DO INSTEAD NOTHING;
    CREATE RULE insert_ok AS ON INSERT TO %(table)s WHERE %(where)s
      DO ALSO %(what)s;
    CREATE RULE insert_bad AS ON INSERT TO %(table)s WHERE NOT (%(where)s)
      DO ALSO INSERT INTO noaccess VALUES (0);
    ''' % { 'table': table, 'what': what, 'where': where }

def oldgenlogtbl(table, fields):
    print '''
    CREATE TABLE %(table)s_log (
      %(fullfields)s,
      created timestamp with time zone NOT NULL,
      deleted timestamp with time zone,
      rowid bigint DEFAULT nextval('rowid') PRIMARY KEY);
    CREATE VIEW %(table)s AS
      SELECT %(fieldnames)s, rowid FROM %(table)s_log WHERE deleted IS NULL;
    CREATE RULE oninsert AS
      ON INSERT TO %(table)s
      DO INSTEAD INSERT INTO %(table)s_log
        VALUES ( %(newfields)s, current_timestamp );
    CREATE RULE ondelete AS
      ON DELETE TO %(table)s
      DO INSTEAD UPDATE %(table)s_log
        SET deleted = current_timestamp
        WHERE %(selfields)s;
    CREATE RULE onupdate AS
      ON UPDATE TO %(table)s
      DO INSTEAD (
        UPDATE %(table)s_log
          SET deleted = current_timestamp
          WHERE %(selfields)s;
        INSERT INTO %(table)s_log
          VALUES ( %(newfields)s, current_timestamp );
      );
    ''' % {
        'table': table,
        'fullfields': fullfields(fields),
        'fieldnames': fieldnames(fields),
        'newfields': newfields(fields),
        'selfields': selfields(fields)
    }
    return sqlrelation(table, fields)

def genlogtbl(table, fields):
    print '''
    CREATE TABLE %(table)s (
      %(fullfields)s,
      rowid bigint DEFAULT nextval('rowid') PRIMARY KEY);
    ''' % {
        'table': table,
        'fullfields': fullfields(fields),
    }
    return sqlrelation(table, fields)

def genindex(table, fields):
    for field in fields:
        print '''
          CREATE INDEX %(table)s_by%(field)s ON %(table)s (%(field)s);
        ''' % { 'table': table.name, 'field': field.name }

def pubview(view):
    print '''
    GRANT SELECT, INSERT, DELETE, UPDATE ON %(view)s TO PUBLIC;
    ''' % { 'view': view }

# These tables have to be split from their standard views because the views
# depend on values generated from the tables themselves.
group_local_member = genlogtbl('group_local_member', groupid + uid + suppress)
genindex(group_local_member, groupid + uid);
group_local_mgrgroupid = genlogtbl('group_local_mgrgroupid',
                                   groupid + mgrgroupid)
genindex(group_local_mgrgroupid, groupid + mgrgroupid)

print '''
CREATE VIEW group_management AS
  SELECT group_local_mgrgroupid.groupid, uid
    FROM group_local_member JOIN group_local_mgrgroupid ON
      group_local_member.groupid = group_local_mgrgroupid.mgrgroupid UNION ALL
  SELECT groups.groupid, uid
    FROM group_local_member CROSS JOIN groups
      WHERE group_local_member.groupid = '000000';

CREATE VIEW group_generic_addable AS
  SELECT groupid, EXISTS (SELECT 1 FROM group_management WHERE
      group_management.groupid = groups.groupid AND
      group_management.uid = session_user) AS addable
    FROM groups;
CREATE VIEW group_generic_editable AS
  SELECT groupid, addable AS editable FROM group_generic_addable;
'''

def stdgroupviews(attr, fields, condition = ''):
    bits = { 'attr': attr,
             'condition': condition }
    print '''
    CREATE VIEW group_%(attr)s AS
      SELECT *
        FROM group_local_%(attr)s NATURAL JOIN group_generic_editable
        %(condition)s;
    CREATE VIEW group_%(attr)s_addable AS
      SELECT * FROM group_generic_addable;
    ''' % bits
    pubview('group_' + attr)
    pubview('group_' + attr + '_addable')

def svgroupviews(attr, fields):
    bits = { 'attr': attr, 'selfields': selfields(fields) }
    print '''
    CREATE VIEW group_%(attr)s AS
      SELECT *
        FROM group_local_%(attr)s NATURAL JOIN group_generic_editable;
    CREATE VIEW group_%(attr)s_addable AS
      SELECT groupid,
          addable AND
            NOT EXISTS (
              SELECT 1 FROM group_%(attr)s
                WHERE groupid = group_generic_addable.groupid
            ) AS addable
        FROM group_generic_addable;
    ''' % bits
    pubview('group_' + attr)
    pubview('group_' + attr + '_addable')

def rogroupviews(attr, fields):
    bits = { 'attr': attr, 'fieldnames': fieldnames(fields) }
    print '''
    CREATE VIEW group_%(attr)s AS
      SELECT %(fieldnames)s, rowid, FALSE AS editable
        FROM group_local_%(attr)s;
    CREATE VIEW group_%(attr)s_addable AS
      SELECT groupid, FALSE AS addable FROM groups;
    ''' % bits
    pubview('group_' + attr)
    pubview('group_' + attr + '_addable')

def stdgrouprules(attr, fields):
    bits = { 'attr': attr,
             'newfields': newfields(fields),
             'selfields': selfields(fields),
             'updfields': updfields(fields) }
    doinsertrule('group_' + attr, [sqlfield('groupid')], '''
      INSERT INTO group_local_%(attr)s VALUES ( %(newfields)s )
      ''' % bits)
    doupdaterule('group_' + attr, '''
      UPDATE group_local_%(attr)s SET %(updfields)s WHERE %(selfields)s
      ''' % bits)
    dodeleterule('group_' + attr, '''
      DELETE FROM group_local_%(attr)s WHERE %(selfields)s
      ''' % bits)

def stdgroupattr(attr, fields):
    fields = (groupid + fields)
    tbl = genlogtbl('group_local_' + attr, fields)
    genindex(tbl, groupid);
    stdgroupviews(attr, fields)
    stdgrouprules(attr, fields)

def svgroupattr(attr, fields):
    fields = (groupid + fields)
    tbl = genlogtbl('group_local_' + attr, fields)
    genindex(tbl, groupid)
    svgroupviews(attr, fields)
    stdgrouprules(attr, fields)

def rogroupattr(attr, fields):
    fields = (groupid + fields)
    tbl = genlogtbl('group_local_' + attr, fields)
    genindex(tbl, groupid)
    rogroupviews(attr, fields)
    stdgrouprules(attr, fields)

stdgroupattr('rdrgroupid',
             [sqlfield('rdrgroupid', 'groupid REFERENCES groups NOT NULL')])

print '''
CREATE VIEW group_readership AS
  SELECT group_local_rdrgroupid.groupid, uid
    FROM group_local_member JOIN group_local_rdrgroupid ON
      group_local_member.groupid = group_local_rdrgroupid.rdrgroupid;
'''

svgroupattr('visibility', [sqlfield('visibility', 'visibility NOT NULL')])

print '''
CREATE VIEW group_member AS
  SELECT groupid, uid, group_local_member.rowid, editable, suppress,
    uid = session_user AS suppressible
    FROM group_local_member JOIN group_generic_editable USING (groupid)
      LEFT JOIN group_local_visibility USING (groupid)
      WHERE editable OR uid = session_user OR
        (NOT suppress AND
         (visibility = 'cam' OR
          (visibility = 'members' AND
           EXISTS (SELECT 1 FROM group_local_member WHERE
                     group_local_member.groupid = groupid AND
                     group_local_member.uid = session_user)) OR
          (visibility = 'managers' AND
           EXISTS (SELECT 1 FROM group_management WHERE
                     group_management.groupid = groupid AND
                     group_management.uid = session_user)) OR
          EXISTS (SELECT 1 FROM group_readership WHERE
                    group_readership.groupid = groupid AND
                    group_readership.uid = session_user)));

CREATE VIEW group_member_addable AS
  SELECT groupid, uid, addable, uid = session_user AS suppressible,
    FALSE AS suppress FROM group_generic_addable CROSS JOIN people;
'''
pubview('group_member')
pubview('group_member_addable')

stdgrouprules('member', group_local_member.fields)
stdgroupviews('mgrgroupid', group_local_mgrgroupid.fields)
stdgrouprules('mgrgroupid', group_local_mgrgroupid.fields)

svgroupattr('grouptitle', [sqlfield('grouptitle', 'generic_name NOT NULL')])
svgroupattr('description', [sqlfield('description', 'generic_name NOT NULL')])
svgroupattr('mail', [sqlfield('mail', 'email_addr NOT NULL')])
rogroupattr('expirytimestamp',
            [sqlfield('expirytimestamp', 'timestamp with time zone NOT NULL')])
rogroupattr('warntimestamp',
             [sqlfield('warntimestamp', 'timestamp with time zone NOT NULL')])

print '''
INSERT INTO groups (groupid) VALUES ('000000');
INSERT INTO allpeople (uid) VALUES ('root');
INSERT INTO group_local_member (groupid, uid, suppress)
  VALUES ('000000', 'root', FALSE);
'''

#
# Institution management goo
#

inst_local_mgrgroupid = genlogtbl('inst_local_mgrgroupid', instid + mgrgroupid)
genindex(inst_local_mgrgroupid, instid + mgrgroupid)

print '''
CREATE VIEW inst_management AS
  SELECT instid, uid
    FROM group_local_member JOIN inst_local_mgrgroupid ON
      groupid = mgrgroupid UNION ALL
  SELECT instid, uid
    FROM group_local_member CROSS JOIN insts WHERE groupid = '000000';
CREATE VIEW inst_generic_addable AS
  SELECT instid, EXISTS (SELECT 1 FROM inst_management WHERE
      inst_management.instid = insts.instid AND
      inst_management.uid = session_user) AS addable
    FROM insts;
CREATE VIEW inst_generic_editable AS
  SELECT instid, addable AS editable FROM inst_generic_addable;
'''

print '''
CREATE VIEW inst_parentinstid AS
  SELECT instid, parentinstid, rowid, FALSE as editable FROM jackdaw_inst;

CREATE VIEW inst_parentinstid_addable AS
  SELECT instid, FALSE as addable FROM insts;
'''
pubview('inst_parentinstid')

def stdinstviews(attr, fields):
    bits = { 'attr': attr,
             'newfields': newfields(fields),
             'selfields': selfields(fields),
             'updfields': updfields(fields) }
    print '''
    CREATE VIEW inst_%(attr)s AS
      SELECT *
        FROM inst_local_%(attr)s NATURAL JOIN inst_generic_editable;
    CREATE VIEW inst_%(attr)s_addable AS
      SELECT * FROM inst_generic_addable;
    ''' % bits
    doinsertrule('inst_' + attr, [sqlfield('instid')], '''
      INSERT INTO inst_local_%(attr)s VALUES ( %(newfields)s )
      ''' % bits)
    doupdaterule('inst_' + attr, '''
      UPDATE inst_local_%(attr)s SET %(updfields)s WHERE %(selfields)s
      ''' % bits)
    dodeleterule('inst_' + attr, '''
      DELETE FROM inst_local_%(attr)s WHERE %(selfields)s
      ''' % bits)
    pubview('inst_' + attr)
    pubview('inst_' + attr + '_addable')

stdinstviews('mgrgroupid', instid +
             [sqlfield('mgrgroupid', 'groupid REFERENCES groups NOT NULL')])

def stdinstattr(attr, fields):
    fields = instid + fields
    tbl = genlogtbl('inst_local_' + attr, fields)
    genindex(tbl, instid)
    stdinstviews(attr, fields)

print '''
  CREATE VIEW inst_ou AS
    SELECT instid, name as ou, rowid, FALSE AS editable FROM jackdaw_inst;
  CREATE VIEW inst_ou_addable AS
    SELECT instid, FALSE AS addable FROM insts;
'''
pubview('inst_ou')
pubview('inst_ou_addable')
stdinstattr('groupid',
            [sqlfield('groupid', 'groupid REFERENCES groups NOT NULL')])
stdinstattr('jpegphoto',
            [sqlfield('jpegphoto', 'jpeg NOT NULL')])
stdinstattr('postaladdress',
            [sqlfield('postaladdress', 'postal_addr NOT NULL')])
stdinstattr('mail', [sqlfield('mail', 'email_addr NOT NULL')])
stdinstattr('phone',
            [sqlfield('phone', 'phone_number NOT NULL'),
             sqlfield('label', 'generic_name')])
stdinstattr('facsimiletelephonenumber',
            [sqlfield('facsimiletelephonenumber', 'phone_number NOT NULL'),
             sqlfield('label', 'generic_name')])
stdinstattr('labeleduri',
            [sqlfield('uri', 'uri NOT NULL'),
             sqlfield('label', 'generic_name')])
stdinstattr('acronym', [sqlfield('acronym', 'generic_name NOT NULL')])


print '''
CREATE TABLE jackdaw_person_inst (
  uid uid REFERENCES allpeople NOT NULL,
  instid instid REFERENCES allinsts NOT NULL,
  rowid bigint DEFAULT nextval('rowid') PRIMARY KEY
);
CREATE INDEX jackdaw_person_inst_byuid ON jackdaw_person_inst ( uid );
CREATE INDEX jackdaw_person_inst_byinstid ON jackdaw_person_inst ( instid );
'''

person_local_inst = genlogtbl('person_local_inst', uid + instid)
genindex(person_local_inst, uid + instid)

print '''
CREATE TABLE inst_remap (
  mapfrom instid REFERENCES allinsts NOT NULL,
  mapto instid REFERENCES allinsts NOT NULL
);

CREATE VIEW person_inst AS
  SELECT uid, COALESCE(mapto, instid) AS instid, FALSE AS editable, rowid
    FROM jackdaw_person_inst LEFT JOIN inst_remap
      ON mapfrom = instid
  UNION
  SELECT uid, instid, editable, rowid FROM
    person_local_inst NATURAL JOIN inst_generic_editable;
CREATE RULE person_inst_insert AS
  ON INSERT TO person_inst DO INSTEAD
    INSERT INTO person_local_inst VALUES ( NEW.uid, NEW.instid );
CREATE RULE person_inst_delete AS
  ON DELETE TO person_inst DO INSTEAD
    DELETE FROM person_local_inst WHERE rowid = OLD.rowid;
'''
pubview('person_inst')

print '''
CREATE VIEW person_inst_addable AS
  SELECT * FROM inst_generic_addable;
'''
pubview('person_inst_addable')

#
# Group creation mechanism
#
print '''
CREATE VIEW group_create AS
  SELECT groupid, instid FROM inst_groupid;
CREATE VIEW group_create_addable AS
  SELECT instid, addable FROM inst_groupid_addable;
'''
doinsertrule('group_create', [sqlfield('instid')],
             '''(
             INSERT INTO groups VALUES (NEW.groupid);
             INSERT INTO group_local_mgrgroupid (groupid, mgrgroupid)
               SELECT NEW.groupid, mgrgroupid FROM inst_mgrgroupid
                 WHERE inst_mgrgroupid.instid = NEW.instid;
             INSERT INTO inst_local_groupid (instid, groupid)
               VALUES (NEW.instid, NEW.groupid);
             INSERT INTO group_local_expirytimestamp (groupid, expirytimestamp)
               VALUES (NEW.groupid, current_timestamp + '1 year');
             )''')
pubview('group_create');
pubview('group_create_addable');

print '''
CREATE VIEW person_generic_addable AS
  SELECT uid, instid,
    (uid = session_user AND instid = '-none-') OR
      inst_generic_addable.addable AS addable,
    uid = session_user AS suppressible FROM
    people CROSS JOIN inst_generic_addable;
CREATE VIEW person_generic_editable AS
  SELECT uid, instid, addable AS editable FROM person_generic_addable;
CREATE VIEW person_generic_suppressible AS
  SELECT uid, instid, suppressible FROM person_generic_addable;
'''

def person_attr_suppress(attr):
    tbl = genlogtbl('person_' + attr + '_local_suppress', uid + suppress)
    genindex(tbl, uid)
    print '''
    CREATE VIEW person_%(attr)s_suppress AS
      SELECT uid, suppress, rowid, COALESCE(suppress, FALSE) AS suppress FROM
      users NATURAL LEFT JOIN person_%(attr)s_local_suppress;
    CREATE RULE update_dummy AS ON UPDATE TO person_%(attr)s_suppress
      DO INSTEAD NOTHING;
    CREATE RULE update_ok AS ON UPDATE TO person_%(attr)s_suppress
      WHERE OLD.suppressible DO ALSO
      UPDATE person_%(attr)s_local_suppress SET suppress = NEW.suppress
        WHERE rowid = OLD.rowid;
    CREATE RULE update_bad AS ON UPDATE TO person_%(attr)s_suppress
      WHERE NOT OLD.suppressible DO ALSO
      UPDATE noaccess SET dummy = 0;
    ''' % { 'attr' : attr }

def person_sv_addable(attr):
    print '''
    CREATE VIEW person_%(attr)s_addable AS
      SELECT uid, instid,
          addable AND
            NOT EXISTS (
              SELECT 1 FROM person_%(attr)s
                WHERE uid = person_generic_addable.uid
            ) AND
            instid = '-none-' AS addable, suppressible, suppress
        FROM person_generic_addable JOIN person_%(attr)s_suppress USING (uid);
    ''' % { 'attr': attr }
    pubview('person_' + attr + "_addable")

def person_mv_addable(attr):
    print '''
    CREATE VIEW person_%(attr)s_addable AS
      SELECT uid, instid, addable, suppressible, suppress
        FROM person_generic_addable JOIN person_%(attr)s_suppress USING (uid);
    ''' % { 'attr': attr }
    pubview('person_' + attr + "_addable")

def dopersoninsertrule(attr, what):
    doinsertrule(attr, [sqlfield('uid'), sqlfield('instid')] , what,
                 "suppressible OR suppress = NEW.suppress")
def dopersonupdaterule(attr, what):
    doupdaterule(attr, what, "OLD.suppressible OR OLD.suppress = NEW.suppress")

notsuppressed = "(NOT suppress OR uid = session_user)"

jackdaw_registeredname_suppress = genlogtbl('jackdaw_registeredname_suppress',
                                            uid + suppress)
genindex(jackdaw_registeredname_suppress, uid)

print '''
CREATE VIEW person_registeredname_unsuppressed AS
  SELECT uid, CAST('-none-' as instid) as instid,
      sn, initials, titles, suppress IS NOT NULL AND suppress AS suppress,
      jackdaw_users.rowid, FALSE as editable, suppressible
    FROM jackdaw_users NATURAL LEFT JOIN
      jackdaw_registeredname_suppress NATURAL JOIN
      person_generic_suppressible
    WHERE person_generic_suppressible.instid = '-none-';
CREATE VIEW person_registeredname AS
  SELECT * FROM person_registeredname_unsuppressed
    WHERE %(notsuppressed)s;
''' % { 'notsuppressed': notsuppressed }
print '''
CREATE VIEW person_registeredname_addable AS
  SELECT uid, instid, FALSE AS addable, FALSE AS suppressible,
    FALSE AS suppress
    FROM people CROSS JOIN insts;
'''
dopersoninsertrule('person_registeredname', 'NOTHING')
dodeleterule('person_registeredname', 'NOTHING')
pubview('person_registeredname')
pubview('person_registeredname_addable')

jackdaw_mail_suppress = genlogtbl('jackdaw_mail_suppress', uid + suppress)
genindex(jackdaw_mail_suppress, uid)

print '''
CREATE VIEW jackdaw_mail AS
  SELECT uid, instid, uid || '@cam.ac.uk' AS mail,
      exdir OR exdir IS NULL OR (suppress IS NOT NULL AND suppress) AS
        suppress,
      jackdaw_users.rowid, FALSE AS editable, suppressible
    FROM jackdaw_users NATURAL LEFT JOIN jackdaw_mail_suppress NATURAL JOIN
      person_generic_suppressible
    WHERE atcam AND instid = '-none-';
CREATE RULE update AS
  ON UPDATE TO jackdaw_mail DO INSTEAD (
    DELETE FROM jackdaw_mail_suppress
      WHERE uid = OLD.uid AND suppress = OLD.suppress;
    INSERT INTO jackdaw_mail_suppress
      VALUES ( NEW.uid, NEW.suppress );
  );
'''

person_local_mail = genlogtbl('person_local_mail', uid + instid +
           [sqlfield('mail',
                     "email_addr NOT NULL CHECK(mail !~ '@cam\\.ac\\.uk$')")] +
          suppress)
genindex(person_local_mail, uid)

print '''
CREATE VIEW person_mail AS
  SELECT * FROM jackdaw_mail UNION ALL
  SELECT * FROM
    person_local_mail NATURAL JOIN
    person_generic_editable NATURAL JOIN
    person_generic_suppressible WHERE %(notsuppressed)s;
''' % { 'notsuppressed': notsuppressed }
genindex(genlogtbl('person_mail_suppress', uid + suppress), uid)
person_mv_addable('mail')
dopersoninsertrule('person_mail', '''
  INSERT INTO person_local_mail
    VALUES ( NEW.uid, NEW.instid, NEW.mail, NEW.suppress )
  ''')
dodeleterule('person_mail', '''
  DELETE FROM person_local_mail
    WHERE uid = OLD.uid AND instid = OLD.instid AND mail = OLD.mail;
  ''')
pubview('person_mail')

def stdpersonattr(attr, fields):
    fields = (uid + instid + fields + suppress)
    tbl = genlogtbl('person_local_' + attr, fields)
    stbl = genlogtbl('person_' + attr + '_suppress', uid + suppress)
    genindex(tbl, uid)
    genindex(stbl, uid)
    bits = { 'attr': attr,
             'newfields': newfields(fields),
             'selfields': selfields(fields),
             'notsuppressed': notsuppressed }
    print '''
    CREATE VIEW person_%(attr)s AS
      SELECT * FROM
        person_local_%(attr)s NATURAL JOIN
        person_generic_editable NATURAL JOIN
        person_generic_suppressible WHERE %(notsuppressed)s;
    ''' % bits
    pubview('person_' + attr)
    return sqlrelation('person_' + attr, fields + editable + suppressible)

def stdpersonrules(attr, fields):
    fields = (uid + instid + fields + suppress)
    bits = { 'attr': attr,
             'newfields': newfields(fields),
             'selfields': selfields(fields),
             'updfields': updfields(fields),
             'notsuppressed': notsuppressed }
    dopersoninsertrule('person_' + attr, '''
      INSERT INTO person_local_%(attr)s VALUES (%(newfields)s)
      ''' % bits)
    dopersonupdaterule('person_' + attr, '''
      UPDATE person_local_%(attr)s SET %(updfields)s WHERE %(selfields)s
      ''' % bits)
    dodeleterule('person_' +attr, '''
      DELETE FROM person_local_%(attr)s WHERE %(selfields)s
      ''' % bits)

def mvpersonattr(attr, fields):
    stdpersonattr(attr, fields)
    person_mv_addable(attr)
    stdpersonrules(attr, fields)

def svpersonattr(attr, fields):
    stdpersonattr(attr, fields)
    person_sv_addable(attr)
    stdpersonrules(attr, fields)

svpersonattr('displayname', [sqlfield('displayname', 'generic_name NOT NULL')])
mvpersonattr('title', [sqlfield('title', 'generic_name NOT NULL')])
mvpersonattr('labeleduri',
             [sqlfield('uri', 'uri NOT NULL'),
              sqlfield('label', 'generic_name')])
mvpersonattr('phone',
             [sqlfield('phone', 'phone_number NOT NULL'),
              sqlfield('label', 'generic_name')])
mvpersonattr('postaladdress',
             [sqlfield('postaladdress', 'postal_addr NOT NULL')])
svpersonattr('jpegphoto', [sqlfield('jpegphoto', 'jpeg NOT NULL')])

# XXX This is probably wrong
pubview('groups')
pubview('people')
pubview('insts')

print "COMMIT;"
