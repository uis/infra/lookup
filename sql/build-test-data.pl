#!/usr/bin/perl

use warnings;
use strict;

#use Ucam::Lookup::JackdawUtils qw/sortout_name/;

use constant HOW_MANY => 200;

my %user_insts = map { $_ => 1 } (qw[BOT BOTT06 IMPSTUD CS]);

# Titles
my @titles = (qw[Prof. Prof Dr Dr.]);
push @titles,"Lord Sir";
my $n_titles = scalar(@titles);

# Surname and forename data
my @surnames;
my $n_surnames = 0;
open my $surnames, 'random-data/surnames.txt'
    or die 'Failed to open surnames.txt';
while (my $line = <$surnames>) {
    chomp $line;
    push @surnames,$line;
    ++$n_surnames;
}
my @firstnames;
my $n_firstnames = 0;
open my $firstnames, 'random-data/firstnames.txt'
    or die 'Failed to open firstnames.txt';
while (my $line = <$firstnames>) {
    chomp $line;
    push @firstnames,$line;
    ++$n_firstnames;
}

print "START TRANSACTION;\n\n";

use constant INSTID => 0;
use constant PARENT => 1;
use constant DESC   => 2;

my @base_insts = 
( ["ROOT","ROOT","Root"],
  ["COLL","ROOT","Colleges"],
  ["BOT","COLL","St Botolph''s College"],
  ["BOTT06","BOT","St Botolph''s College - 2006 entry"],
  ["UOC","ROOT","University of Cambridge"],
  ["INGB","UOC","Other General Board Institutions"],
  ["INGBS","INGB","Other General Board Institutions (Service and Admin.)"],
  ["IMPSTUD","INGBS","Department of Important Studies"],
  ["CS","INGBS","University Computing Service"],
  ["BAR06","CS","Dip. Bar Sci. 2006 entry"],
);

use constant UID     => 0;
use constant NAME    => 1;
use constant INST    => 2;
use constant COLL    => 3;
use constant ATCAM   => 4;
use constant EXDIR   => 5;
use constant DISPLAY => 6;
use constant INSTS   => 7;

my @base_people =

( ["test001","Smith A.A.","CS","","Y","N","Andy Smith",[]],
  ["test002","Mcivor A.D.","CS","","Y","N","",[]],
  ["test003","Lever A.M.","CS","","Y","N","",[]],
  ["test004","Donnelly Dr A.S.","CS","","Y","N","Albert Donnelly",[]],
  ["test005","Maloney C.D.","CS","","Y","N","Clarles (Fred) Maloney",[]],
  ["test006","Clarkson C.P.","CS","","Y","N","",[]],
  ["test007","Banham J.","CS","","Y","N","Jane Banham",[]],
  ["test008","Bristow Prof. D.P.","CS","BOT","Y","N","Donald Bristow",["IMPSTUD"]],
  ["test009","Brown E.J.","CS","","Y","N","Edwina Brown",["IMPSTUD"]],
  ["test010","Altavista A.","CS","BOT","Y","N","Anton Altavista",["BAR06"]],
  ["test011","Calaresu R.G.","BOT","","Y","N","",["BOTT06"]],
  ["test012","Carrington P.J.","BAR06","BOT","Y","N","",["BOTT06"]],
  ["test013","Fellows P.C.","BOTT06","BOT","Y","N","",[]],
);

use constant GROUPID    => 0;
use constant GTITLE     => 1;
use constant GDESC      => 2;
use constant OWNERS     => 3;
use constant MANAGES    => 4;
use constant MANAGERS   => 5;
use constant READERS    => 6;
use constant MEMBERS    => 7;
use constant VISIBILITY => 8;

my @groups = 
    ( ["000000",
       qq[Top-level group],
       qq[The root of all evil],
       ["ROOT"],
       [],
       ["000000"],
       [],
       [qw[test001]],
       "managers",
   ],
      ["100655",
       qq[Management group for "University Computing Service (UCS)".],
       qq[The members of this group have control over the group of editors for the directory page for "University Computing Service (UCS)". They may well manage other groups too.],
       ["CS"],
       [],
       ["000000"],
       [],
       [qw[test003 test010 test011]],
       "members",
   ],
      ["100656",
       qq[Editors group for "University Computing Service (UCS)".],
       qq[The members of this group may edit the directory data for "University Computing Service (UCS)".],
       ["CS"],
       ["CS"],
       ["100655"],
       ["100657"],
       [qw[test002 test003 test004]],
       "managers",
   ],
      ["100657",
       qq[Privileged access group for "University Computing Service (UCS)"],
       qq[The members of this group have priviledged access to the membership list of the group of editors for the directory page for "University Computing Service (UCS)".],
       ["CS"],
       [],
       ["000000"],
       [],
       [qw[test003 test005 test012 test013]],
       "cam",
   ],
      ["100664",
       qq[Editors group for "Department of Important Studies"],
       qq[The members of this group may edit the directory data for "Deaprtment of Important Studies"],
       ["IMPSTUD"],
       ["IMPSTUD"],
       ["000000"],
       [],
       [],
       "managers",
   ],
      ["100662",
       qq[Editors group for "St Botolphs'' College".],
       qq[The members of this group may edit the directory data for "St Botolphs'' College".],
       ["BOT"],
       ["BOT"],
       ["000000"],
       [],
       [qw[test005]],
       "members",
   ],
 );

print "-- insts\n\n";

my @insts;
foreach my $inst (@base_insts) {
    push @insts,$inst->[INSTID] if $user_insts{$inst->[INSTID]};
    print "INSERT INTO insts VALUES ('$inst->[INSTID]');\n";
    print "INSERT INTO jackdaw_inst (instid,parentinstid,name)\n";
    print "          VALUES ('$inst->[INSTID]', '$inst->[PARENT]',\n";
    print "          '$inst->[DESC]');\n";
    print "\n";
}
my $n_ints = scalar(@insts);

print "-- base people\n\n";

foreach my $person (@base_people) {
    print "INSERT INTO people VALUES ('$person->[UID]');\n";
    my ($cn,$title,$initials,$surname) = sortout_name($person->[NAME]);
    print "INSERT INTO jackdaw_users\n";
    print "      VALUES ('$person->[UID]','$surname','$initials','$title',";
    print "'$cn',TRUE,FALSE);\n";
    print "INSERT INTO jackdaw_person_inst (uid, instid) \n";
    print "      VALUES ('$person->[UID]','$person->[INST]');\n";
    print "\n";
}

print "-- random people\n\n";

my %display;
for my $person (100..99+HOW_MANY) {

    my $uid = "test$person";

    my $first = $firstnames[rand($n_firstnames)];
    $first =~ s/'/''/g;
    my $initials = $first;
    $initials =~ s/\b([A-Z]).*?\b/$1./g;
    my $last  = $surnames[rand($n_surnames)];
    $last =~ s/'/''/g;

    $display{$uid} = (rand>0.7) ? "$first $last" : "$initials $last";

    my $title = '';
    $title = $titles[rand($n_titles)] if rand>0.5;

    my $inst = $insts[rand($n_ints)];
    my $col = ($inst eq 'BOTT06' or rand>0.9) ? 'BOT' : "";

    my $cn = "$title $initials $last";

    my $atcam = (rand>0.1) ? 'TRUE' : 'FALSE';

    my $exdir = (rand>0.8 and $atcam eq 'TRUE') ? 'TRUE' : 'FALSE';

    print "INSERT INTO people VALUES ('$uid');\n";

    print "INSERT INTO jackdaw_users VALUES \n";
    print "      ('$uid','$last','$initials','$title','$cn',$atcam,$exdir);\n";

    print "INSERT INTO jackdaw_person_inst (uid, instid) \n";
    print "      VALUES ('$uid','$inst');\n";

    print "\n";

}

print "-- groups\n\n";

foreach my $group (@groups) {
    print "INSERT INTO groups VALUES ('$group->[GROUPID]');\n"
	unless $group->[GROUPID] eq '000000';
}
print "\n";

print "-- switch identity\n\n";

# Become 'root' to initialise 'user' data
print "SET SESSION AUTHORIZATION 'root';\n";
print "\n";

print "-- base people non-Jackdaw data\n\n";

foreach my $person (@base_people) {
    my $stuff = 0;
    if ($person->[DISPLAY]) {
        my $suppress = suppress();
    	print "INSERT INTO person_displayname (uid,instid,displayname,suppress) \n";
    	printf "    VALUES ('%s','-none-','%s',(select suppress from person_displayname_addable where uid = '%1\$s' and instid = '-none-'));\n", $person->[UID], $person->[DISPLAY];
        $stuff = 1;
    }
    foreach my $inst (@{$person->[INSTS]}) {
        print "INSERT INTO person_inst (uid,instid)\n";
        print "       VALUES ('$person->[UID]','$inst');\n";
        $stuff = 1;
    }
    print "\n" if $stuff;
}

print "-- random people non-Jackdaw data\n\n";

for my $person (100..99+HOW_MANY) {

    my $uid = "test$person";
    my $suppress;

    if ($display{$uid}) {
      print "INSERT INTO person_displayname (uid,instid,displayname,suppress)\n";
      printf "    VALUES ('%s','-none-','%s',(select suppress from person_displayname_addable where uid = '%1\$s' and instid = '-none-'));\n", $uid, $display{$uid};
    }

    my (@i);
    push @i,$insts[rand($n_ints)] if rand>0.8;
    push @i,$insts[rand($n_ints)] if rand>0.9;
    foreach my $inst (@i) {
	print "INSERT INTO person_inst (uid,instid)\n";
        print "       VALUES ('$uid','$inst');\n";
    }

    (my $domain =lc($display{$uid})) =~ s/[ .]//g;

    my (@m);
    push @m, "$domain\@nonsutch.invalid" if rand>0.6;
    push @m, rand(100000) . "\@x.invalid" if rand>0.6;
    foreach my $mail (@m) {
        print "INSERT INTO person_mail (uid,instid,mail,suppress)\n";
        printf "    VALUES ('%s','-none-','%s',(select suppress from person_mail_addable where uid = '%1\$s' and instid = '-none-'));\n", $uid, $mail;
    }

    my (@t);
    push @t, sprintf("%5.5i",int(rand(100000))) if rand>0.4;
    push @t, sprintf("01223 %6.6i",int(rand(1000000))) if rand>0.7;
    push @t, sprintf("+44 1223 %6.6i",int(rand(1000000))) if rand>0.7;
    foreach my $nmbr (@t) {
	print "INSERT INTO person_phone (uid,instid,phone,suppress)\n";
        printf "    VALUES ('%s','-none-','%s',(select suppress from person_phone_addable where uid = '%1\$s' and instid = '-none-'));\n", $uid, $nmbr;
    }

    my (@u);
    push @u, ["http://www.pwf.cam.ac.uk.invalid/$uid"] if rand>0.4;
    push @u, ["http://www.$domain.invalid/",
            "$display{$uid}''s home page"] if rand>0.2;
    foreach my $uri (@u) {
        my $label = $uri->[1] ? "'$uri->[1]'" : "NULL";
	print "INSERT INTO person_labeleduri (uid,instid,uri,label,suppress)\n";
        printf "    VALUES ('%s','-none-','%s',%s,(select suppress from person_labeleduri_addable where uid = '%1\$s' and instid = '-none-'));\n", $uid, $uri->[0], $label;
    }
    print "\n";
}

print "-- group data\n\n";

foreach my $group (@groups) {
    foreach my $owner (@{$group->[OWNERS]}) {
        print "INSERT INTO inst_groupid (instid,groupid)\n";
        print "       VALUES ('$owner','$group->[GROUPID]');\n";
    }
    foreach my $mgd_inst (@{$group->[MANAGES]}) {
        print "INSERT INTO inst_mgrgroupid (instid,mgrgroupid)\n";
        print "       VALUES ('$mgd_inst','$group->[GROUPID]');\n";
    }
    foreach my $mgr (@{$group->[MANAGERS]}) {
        print "INSERT INTO group_mgrgroupid (groupid,mgrgroupid)\n";
        print "       VALUES ('$group->[GROUPID]','$mgr');\n";
    }
    foreach my $rdr (@{$group->[READERS]}) {
        print "INSERT INTO group_rdrgroupid (groupid,rdrgroupid)\n";
        print "       VALUES ('$group->[GROUPID]','$rdr');\n";
    }
    foreach my $member (@{$group->[MEMBERS]}) {
	my $suppress = suppress();
        print "INSERT INTO group_member (groupid,uid,suppress)\n";
        print "       VALUES ('$group->[GROUPID]','$member',$suppress);\n";
    }

    print "INSERT INTO group_grouptitle (groupid,grouptitle)\n";
    print "       VALUES ('$group->[GROUPID]','$group->[GTITLE]');\n";

    print "INSERT INTO group_description (groupid,description)\n";
    print "       VALUES ('$group->[GROUPID]','$group->[GDESC]');\n";

    print "INSERT INTO group_visibility (groupid,visibility)\n";
    print "       VALUES ('$group->[GROUPID]','$group->[VISIBILITY]');\n";

    print "\n";
}

print "COMMIT;\n";

# Possible surname prefixes - omitting al and el which _seem_ to be
# used more commonly with a hyphen than not (probably Arabic
# vs. Spanish)
my %possible_prefix = map { $_ => 1 }
    qw/da de del den der di do du la las le les los ten van von zu/;

# Exception table for known 'broken' names in Jackdaw
my %exception = ();

sub sortout_name {

    my ($raw_name) = @_;

    return ('','','','') unless defined $raw_name;

    # Deal with known exceptions
    if ($exception{$raw_name}) {
        return @{$exception{$raw_name}};
    }

    # split raw_name into proto-surname, and everything else
    my ($proto_surname,@rest) = split(' ',$raw_name);

    # the 'rest' contains optional title(s), one or more blocks of initials,
    # and occasionally one or more surname prefixes
    my (@title,@initials,@prefix,@surname);

    # Grab all the recognised titles from the front
    while (@rest and map_title(_strip_dot($rest[0]))) {
        push @title, map_title(_strip_dot(shift @rest));
    }

    # Then, it's either a recognised prefix or a clump of initials.
    # This may re-order recognised and unrecognised prefixes
    # w.r.t. initials but there's no option, Both unrecognised and
    # non-leading titles also end up looking like initials.
    foreach my $bit (@rest) {
        if ($possible_prefix{lc($bit)}) {
            push @prefix, $bit;
        }
        else {
            push @initials, $bit;
        }
    }

    # proto-surname contains the real surname, possibly with
    # hyphen-separated prefixes on the front. Collect the prefixes and
    # whatever is left, however improbable, has to be the surname (the
    # Sherlock Holmes algorithm)
    my @bits;
    @bits = (split(/-+/,$proto_surname)) if defined($proto_surname);
    while (@bits and $possible_prefix{lc($bits[0])}) {
        push @prefix, shift @bits;
    }
    my $surname = join(' ', @prefix, join('-',@bits));

    # join title(s) and initial(s) with space
    my $title = join (' ',@title);
    my $initials = join (' ',@initials);

    # make cn out of title, initials and surname again
    my $cn = join (' ', grep { $_ ne '' } $title, $initials, $surname);

    return ($cn,$title,$initials,$surname);

}


sub _strip_dot {

    my ($text) = @_;

    $text =~ s/\.$//;

    return $text;

}

use constant title_map => {
    'Dr'    => 'Dr',
    'Prof'  => 'Prof.',

    'Canon' => 'Canon',
    'Rabbi' => 'Rabbi',
    'Rev'   => 'Rev.',
    'Revd'  => 'Revd',

    'Dame'  => 'Dame',
    'Sir'   => 'Sir',
    'Lady'  => 'Lady',
    'Lord'  => 'Lord',
    'Judge' => 'Judge',
  };


sub map_title {
    return title_map->{$_[0]};
}

sub suppress {
    return (rand > 0.5) ? 'TRUE' : 'FALSE';
}
