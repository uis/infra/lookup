#!/usr/bin/perl -w

use strict;

use constant HOST => "mnementh.csi.cam.ac.uk";

use Net::LDAP;

my %lookup =
    ('2.16.840.1.113730.3.4.18' => "Proxied Authorization draft-weltman-ldapv3-proxy-xx.txt",
     '2.16.840.1.113730.3.4.2'  => "Manage DSA IT LDAPv3 RFC 3296",
     '1.3.6.1.4.1.4203.1.10.1'  => "Subentries RFC 3672",
     '1.2.840.113556.1.4.1340'  => "Search options control Microsoft",
     '1.2.840.113556.1.4.1413'  => "Permissive modify Microsoft",
     '1.2.840.113556.1.4.1339'  => "Domain scope control Microsoft",
     '1.2.840.113556.1.4.319'   => "Paged Results RFC2696",
     '1.2.826.0.1.334810.2.3'   => "Attribute Value Filter RFC3876",
     '1.3.6.1.1.13.2'           => "Post-read Control draft-zeilenga-ldap-readentry-xx.txt",
     '1.3.6.1.1.13.1'           => "Pre-read Control draft-zeilenga-ldap-readentry-xx.txt",
     '1.3.6.1.1.12'             => "Assertion Control draft-zeilenga-ldap-assert-xx.txt",

     '1.3.6.1.4.1.1466.20037'   => "Start TLS RFC 2830",
     '1.3.6.1.4.1.4203.1.11.1'  => "Modify passwod RFC 3062",
     '1.3.6.1.4.1.4203.1.11.3'  => "Who am I? draft-zeilenga-ldap-authzid-xx.txt",

     '1.3.6.1.1.14'             => "Modify-Increment draft-zeilenga-ldap-incr-xx.txt",
     '1.3.6.1.4.1.4203.1.5.1'   => "All Operational Attributes RFC 3673",
     '1.3.6.1.4.1.4203.1.5.2'   => "All attributes of an object class draft-zeilenga-ldap-adlist-xx.txt",
     '1.3.6.1.4.1.4203.1.5.3'   => "True/False filters draft-zeilenga-ldap-t-f-xx.txt",
     '1.3.6.1.4.1.4203.1.5.4'   => "Language Tag Options RFC 3866",
     '1.3.6.1.4.1.4203.1.5.5'   => "Language Range Options RFC 3866",

 );

my $ldap = Net::LDAP->new(HOST) or die $@;

my $mesg = $ldap->bind;
die $mesg->error if $mesg->is_error;

my $root = $ldap->root_dse;

foreach my $feature (qw/namingContexts
                        supportedControl 
                        supportedExtension
                        supportedFeatures
                        supportedLDAPVersion
                        supportedSASLMechanisms/) {


    print "\n$feature:\n\n";

    foreach my $it ($root->get_value($feature)) {

        print $it;
        print $lookup{$it} ? " ($lookup{$it})" : '';
        print "\n";

    }

}
