# $Id$

package Ucam::Lookup::JackdawUtils;

use strict;
use warnings;

use Ucam::Directory::Person qw/map_title/;
use List::Util qw/max/;

# must be all on one line or MakeMaker will get confused.
our $VERSION = do { my @r = (q$Revision$ =~ /\d+/g); sprintf "%d."."%03d" x $#r, @r };

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT    = qw();
our @EXPORT_OK = qw(create_inst update_inst create_person update_person 
                    cancel_entry remake_ou sortout_name fix_insts 
                    get_prefix get_college);

# Possible surname prefixes - omitting al and el which _seem_ to be
# used more commonly with a hyphen than not (probably Arabic
# vs. Spanish)
my %possible_prefix = map { $_ => 1 }
    qw/da de del den der di do du la las le les los ten van von zu/;

# Exception table for known 'broken' names in Jackdaw
my %exception =
    ( 'identifier ECDL course' =>
      ['ECDL course identifier','','','ECDL course identifier'],
      'Students Linguistics' =>
      ['Linguistics Students','','','Linguistics Students'],
      'Identifier Course' =>
      ['Course Identifier','','','Course Identifier'],
      'Temporary Identifier' =>
      ['Temporary Identifier','','','Temporary Identifier'],
      'Identifier - PWF' =>
      ['PWF Identifier','','','PWF Identifier'],
      'Course CUS' =>
      ['CUS Course','','','CUS Course'],
      'R.D.Hardind - Temporary Userid' =>
      ['R.D. Hardind - Temporary Userid','','R.D.','R.D.Hardind'],
      'R.D.Harding - Temporary Userid' =>
      ['R.D. Harding - Temporary Userid','','R.D.','R.D.Harding'],
      'R.D.Harding - Temporary userid' =>
      ['R.D. Harding - Temporary userid','','R.D.','R.D.Harding'],
      'Dowling R. - Test Account' =>
      ['R. Dowling - Test Account','','R.','Dowling'],
      'testing id PWF' =>
      ['PWF testing id','','','PWF testing id'],
      'Testing Software' =>
      ['Software Testing','','','Software Testing'],
      'Testing-Thirty-Two-Bit Uid' =>
      ['Testing-Thirty-Two-Bit Uid','','','Testing-Thirty-Two-Bit Uid'],
      'Testing-Six-Digit Uid' =>
      ['Testing-Six-Digit Uid','','','Testing-Six-Digit Uid'],
      'MISD Operations CamCORS/CamGrad account' =>
      ['MISD Operations CamCORS/CamGrad account','','',
                             'MISD Operations CamCORS/CamGrad account'],
      'Temporary Identifier - Conference use' =>
      ['Temporary Identifier - Conference use','','',
                             'Temporary Identifier - Conference use'],
      'Temporary Identifier - Summer School use' =>
      ['Temporary Identifier - Summer School use','','',
                             'Temporary Identifier - Summer School use'],
);

# College instID => annual group prefix mapping (and back again)

my %coll2prefix = 
    ( CAIUS => 'CAIUS',
      CATH => 'CATH',
      CHRISTS => 'CHRST',
      CHURCH => 'CHUR',
      CLARE => 'CLARE',
      CLAREH => 'CLARH',
      CORPUS => 'CORP',
      DARWIN => 'DAR',
      DOWN => 'DOWN',
      EDMUND => 'EDM',
      EMM => 'EMM',
      FITZ => 'FITZ',
      GIRTON => 'GIRT',
      HOM => 'HOM',
      HUGHES => 'HUGH',
      JESUS => 'JESUS',
      JOHNS => 'JOHNS',
      KINGS => 'KINGS',
      LCC => 'LCC',
      MAGD => 'MAGD',
      NEWH => 'NEWH',
      NEWN => 'NEWN',
      PEMB => 'PEMB',
      PET => 'PET',
      QUEENS => 'QUEN',
      ROBIN => 'ROBIN',
      SEL => 'SEL',
      SID => 'SID',
      TRIN => 'TRIN',
      TRINH => 'TRINH',
      WOLFC => 'WOLFC',
 );

my %prefix2coll = map { $coll2prefix{$_} => $_ } keys %coll2prefix;

# ---

# Creating institutions
sub create_inst {

    my ($dir,$do_update,$id,$name,$father) = @_;

    print "Inst $id: create UG/PG groups if this is really a new college\n"
        if $father eq 'COLL';

    if (get_college($id) and $id =~ /\d\d$/) {
        print "Inst $id: annual college inst, skipping\n";
        return undef;
    }

    my $inst = $dir->create_inst(uc($id));
    my @groups = _get_manager_group($dir,$do_update,$id,$name,$father);
    if ($do_update) {
        $inst->replace (attr => 'mgrGroupID', val  => $groups[0]);
        $inst->replace (attr => 'groupID',    val  => \@groups);
    }

    my $what = $do_update ? 'created' : 'would be created';
    print "Inst $id: $what\n";

    return $inst;

}

# ---

# Updating/populating institutions
sub update_inst {

    my ($dir,$inst,$do_update,$name,$father) = @_;
    my $done_something = 0;
    my $remake_ou_needed = 0;

    return unless $inst;

    my $instid = $inst->instID;

    # Has the parent ID changed?
    if (!defined($inst->jdFather) or $inst->jdFather ne $father) {
        print "Inst $instid: parentInstID/jdFather needs update\n";

        my @new = fix_insts(scalar($inst->jdFather),
                            $father,
                            $inst->parentInstID);
        print "Inst $instid:   jdFather: '",
            $inst->jdFather,
                "' --> '",
                    $father, "'\n";
        print "Inst $instid:   parentInstID: '",
            join(",",$inst->parentInstID),
                "' --> '";
        print @new ? join(",",@new) : "[unchanged]";
        print "'\n";
        if ($do_update) {
            $inst->replace(attr=>'parentInstID',val=>\@new) if @new;
            $inst->replace(attr => 'jdFather', val=>$father);
            $done_something = 1;
        }

    }

    # Does it need renaming?
    if (!defined($inst->jdName) or $inst->jdName ne $name) {
        print "Inst $instid: ou/jdName needs update\n";
        print "Inst $instid:   jdName: '",$inst->jdName,"' --> '$name'\n";
        print "Inst $instid:   ou: '",$inst->ou,"' --> '$name'\n";
        if ($do_update) {
            $inst->replace(attr=>'ou', val=>$name);
            $inst->replace(attr=>'jdName', val=>$name);
            $done_something = 1;
            $remake_ou_needed = 1;
        }
    }

    if ($done_something and $do_update) {
        $inst->commit;
        print "Inst ", $inst->instID, ": update committed\n";
    }

    remake_ou($dir,$inst->instID,$do_update) if $remake_ou_needed;

}

# ---

sub create_person {

    my ($dir,$do_update,$id,$name) = @_;

    my $person = $dir->create_person(lc($id));
    my ($cn) = sortout_name($name);
    $person->replace(attr => 'displayName', value => $cn);

    my $what = $do_update ? 'created' : 'would be created';
    print "Person $id: $what, displayName = $cn\n";

    return $person;

}

# Updating/populating people
sub update_person {

    my ($dir,$person,$do_update,$name,$inst,
        $cammail,$ex_dir,$coll,$staff,$student) = @_;
    my $done_something = 0;

    my $uid = $person->uid;

    # Pretend $inst is the coresponsing COLs UG inst if $inst is an
    # undergrad 'annual intake' group
    my $prefix = get_prefix(get_college($inst));
    $inst = $prefix . 'UG' if $prefix;

    # Name changed?
    if (!defined($person->jdName) or $person->jdName ne $name) {
        my ($cn,$title,$initials,$surname) = sortout_name($name);
        print "Person $uid: sn/cn/jdName needs update\n";
        print "Person $uid:   jdName: '",$person->jdName,"' --> '$name'\n";
        print "Person $uid:   sn: '",$person->sn,"' --> '$surname'\n";
        print "Person $uid:   cn: '",$person->cn,"' --> '$cn'\n";
        if ($do_update) {
            $person->replace(attr=>'sn',     val=>$surname);
            $person->replace(attr=>'cn',     val=>$cn);
            $person->replace(attr=>'jdName', val=>$name);
            $done_something = 1;
        }
    }

    # Change of affiliation or college?
    if (!defined($person->jdInst) or $person->jdInst ne $inst or
        ($coll and (!defined($person->jdColl) or $person->jdColl ne $coll))) {

        print "Person $uid: instID/jdInst/jdColl needs update\n";

        my @instids = $person->instID;
        my $updated = 0;

        # Update based on any inst change
        my @new1 = fix_insts(scalar($person->jdInst),$inst,@instids);
        if (@new1) {
            @instids = @new1;
            $updated = 1;
        }

        # Update based on coll change for graduates/postgraduates...
        my $old = get_prefix(scalar($person->jdColl));
        $old .= 'PG' if $old;
        my $col_pref = get_prefix($coll);
        my $new;
        $new = $col_pref . 'PG' if $inst =~ /\d\d$/ and $col_pref and
                $inst !~ /^$col_pref\d\d$/;
        if (defined($old) and grep { $old eq $_ } @instids) {
            @instids = grep { $old ne $_ } @instids;
            $updated = 1;
        }
        if (defined($new) and !grep { $new eq $_ } @instids) {
            push @instids, $new;
            $updated = 1;
        }

        if (!defined($person->jdInst) or $person->jdInst ne $inst) {
            print "Person $uid:   jdInst: '",$person->jdInst,"' --> '$inst'\n";
            $person->replace(attr =>'jdInst', val=>$inst) if $do_update;
            $done_something = 1;
        }
        if ($coll and 
            (!defined($person->jdColl) or $person->jdColl ne $coll)) {
            print "Person $uid:   jdColl: '",$person->jdColl,"' --> '$coll'\n";
            $person->replace(attr =>'jdColl', val=>$coll) if $do_update;
            $done_something = 1;
        }
        if ($updated) {
            print "Person $uid:   instID: '",join(",",$person->instID),
                "' --> '", join(",",@instids), "'\n";
            $person->replace(attr=>'instID', val=>\@instids) if $do_update;
            $done_something = 1;
        }

    }

    # Change in @cam email status and or ex-directory flag?
    $ex_dir = '-' if $ex_dir eq '';
    if (!defined($person->jdCamMail) or !defined($person->jdExDir) or
        $person->jdCamMail ne $cammail or $person->jdExDir ne $ex_dir) {
        print "Person ", $person->uid, 
                ": allmail/jdCamMail/jdExDir needs update\n";
        print "Person $uid:   jdCamMail: '",$person->jdCamMail,
            "' --> '$cammail'\n";
        print "Person $uid:   jdExDir: '",$person->jdExDir,
            "' --> '$ex_dir'\n";

        my $cam_email = $person->uid . '@cam.ac.uk';
        # Include @cam address if it exists and isn't ex-directory
        if ($cammail eq 'Y' and $ex_dir eq 'N') {
            if (!grep { $_ eq $cam_email } $person->allmail) {
                my @new = ($person->allmail, $cam_email);
                print "Person $uid:   allmail: '",
                    join(",",$person->allmail),
                        "' --> '",
                            join(",",@new),"'\n";
                $person->replace(attr=>'allmail',val=>\@new) if $do_update;
            }
        }
        # ... otherwise remove it
        else {
            if (grep { $_ eq $cam_email } $person->allmail) {
                my @new = grep { $_ ne $cam_email } $person->allmail;
                print "Person $uid:   allmail: '",
                    join(",",$person->allmail),
                        "' --> '",
                            join(",",@new),"'\n";
                $person->replace(attr=>'allmail', val=>\@new) if $do_update;
            }
        }

        if ($do_update) {
            $person->replace(attr =>'jdCamMail', val=>$cammail);
            $person->replace(attr =>'jdExDir', val=>$ex_dir);
            $done_something = 1;
        }


    }

    # Change in staff/student flag?
    my @affil = $person->misAffiliation;
    my $cur_staff   = (grep { $_ eq 'staff'   } @affil) ? 1 : 0;
    my $cur_student = (grep { $_ eq 'student' } @affil) ? 1 : 0;
    my $new_staff   = ($staff   eq 'Y') ? 1 : 0;
    my $new_student = ($student eq 'Y') ? 1 : 0;
    if ($new_staff ne $cur_staff or $new_student ne $cur_student) {
        print "Person ", $person->uid,
                ": misAffiliation needs update\n";
        my @new;
        push @new,'staff'   if $new_staff;
        push @new,'student' if $new_student;
        print "Person $uid:   misAffiliation: '",
            join(",",$person->misAffiliation),
                "' --> '",
                    join(",",@new),"'\n";
        $person->replace(attr=>'misAffiliation',val=>\@new) if $do_update;
        $done_something = 1;
    }

    if ($done_something and $do_update) {
        $person->commit;
        print "Person ", $person->uid, ": update committed\n";
    }

}

# ---

# This puts the cancelled attribute into the object.
sub cancel_entry
{
    my ($dir,$entry,$cancel,$do_update) = @_;

    return unless $entry;

    my $done_something = 0;

    my $what = "Unknown entry";
    $what = "Person " . $entry->uid
        if $entry->isa('Ucam::Directory::Person');
    $what = "Inst " . $entry->instID
        if $entry->isa('Ucam::Directory::Institution');
    $what = "Group " . $entry->groupID
        if $entry->isa('Ucam::Directory::Group');

    # Don't cancel recognised college UG and PG insts, don't un-cancel
    # recognised annual insts
    if ($entry->isa('Ucam::Directory::Institution') and
        get_college($entry->instID)) {
        if ($entry->instID=~ /(UG|PG)$/ and $cancel and !$entry->cancelled) {
            print "$what: college UG/PG inst, not cancelling\n";
            return
        }
        elsif ($entry->instID=~ /\d\d$/ and !$cancel and $entry->cancelled) {
            print "$what: college anual inst, not un-cancelling\n";
            return
        }
    }

    if ($cancel and !$entry->cancelled) {
        print "$what: needs cancelling\n";
        if ($do_update) {
            $entry->cancel;
            print "$what: cancel status committed\n";
            $done_something = 1;
        }
    }
    elsif (!$cancel and $entry->cancelled) {
        print "$what: needs un-cancelling\n";
        if ($do_update) {
            $entry->replace(attr=>'cancelled',val=>undef);
            $entry->commit;
            print "$what: cancel status committed\n";
            $done_something = 1;
        }
    }

    # Remake ous if necessary
    if ($done_something and $entry->isa('Ucam::Directory::Institution')) {
        remake_ou($dir,$entry->instID,$do_update);
    }

}

# ---

# Remake the ou attributes for everyone associated with $inst_id
sub remake_ou
{
    my ($dir,$inst_id,$do_update) = @_;

    my $filter = '(instID=' . $inst_id . ')';
    my @people = $dir->search_for_people($filter);
    foreach my $person (@people) {
        print "Person ", $person->uid, ": ou(s) need rebuilding\n";
        # This bit of magic works because when instid is updated the
        # Ucam::Directory::Person object updates its list of ou
        # entries, omitting the ones that correspond to cancelled or
        # absent insts.
        if ($do_update) {
            my @inst_ids = $person->instID;
            my @ous = $person->ou;
            $person->replace(attr=>'instID', val=> \@inst_ids);
            $person->commit();
            print "Person ", $person->uid, ":   ou: '",
                    join(",",@ous),
                        "' --> '",
                            join(",",$person->ou),"'\n";
            print "Person ", $person->uid, ": ou(s) rebuilt\n";
        }
    }

}


# ---

sub sortout_name {

    my ($raw_name) = @_;

    return ('','','','') unless defined $raw_name;

    # Deal with known exceptions
    if ($exception{$raw_name}) {
        return @{$exception{$raw_name}};
    }

    # split raw_name into proto-surname, and everything else
    my ($proto_surname,@rest) = split(' ',$raw_name);

    # the 'rest' contains optional title(s), one or more blocks of initials,
    # and occasionally one or more surname prefixes
    my (@title,@initials,@prefix,@surname);

    # Grab all the recognised titles from the front
    while (@rest and map_title(_strip_dot($rest[0]))) {
        push @title, map_title(_strip_dot(shift @rest));
    }

    # Then, it's either a recognised prefix or a clump of initials.
    # This may re-order recognised and unrecognised prefixes
    # w.r.t. initials but there's no option, Both unrecognised and
    # non-leading titles also end up looking like initials.
    foreach my $bit (@rest) {
        if ($possible_prefix{lc($bit)}) {
            push @prefix, $bit;
        }
        else {
            push @initials, $bit;
        }
    }

    # proto-surname contains the real surname, possibly with
    # hyphen-separated prefixes on the front. Collect the prefixes and
    # whatever is left, however improbable, has to be the surname (the
    # Sherlock Holmes algorithm)
    my @bits;
    @bits = (split(/-+/,$proto_surname)) if defined($proto_surname);
    while (@bits and $possible_prefix{lc($bits[0])}) {
        push @prefix, shift @bits;
    }
    my $surname = join(' ', @prefix, join('-',@bits));

    # join title(s) and initial(s) with space
    my $title = join (' ',@title);
    my $initials = join (' ',@initials);

    # make cn out of title, initials and surname again
    my $cn = join (' ', grep { $_ ne '' } $title, $initials, $surname);

    return ($cn,$title,$initials,$surname);

}

# ---

sub fix_insts {
    my ($old,$new,@list) = @_;

    my $done_something = 0;

    # If old is in the list but new isn't then remove old
    if (@list and defined($old) and defined($new) and
        grep { $_ eq $old } @list and !grep { $_ eq $new } @list) {
        @list = grep { $_ ne $old } @list;
        $done_something = 1;
    }

    # Add new if it wasn't there already
    if (defined($new) and (!@list or !grep { $_ eq $new } @list)) {
        push @list,$new;
        $done_something = 1;
    }

    # Return the list if we did something

    return $done_something ? @list : ();

}

# ---
# Return the 'annual group' prefix for an instID if it exists, undef otherwise
sub get_prefix {

    my ($instid) = @_;

    return undef unless defined($instid);
    return $coll2prefix{$instid};

}

# ---
# return the instID coresponding to an 'annual group' or UG or PG
# instID, undef otherwise
sub get_college {

    my ($instid) = @_;

    return $prefix2coll{$1} if $instid =~ /^(.*)(UG|PG|\d\d)$/;
    return undef;

}

# ---
# Select or create a managing group for a new institution
sub _get_manager_group {

    my ($dir,$do_update,$id,$ou,$father) = @_;

    # Is this an annual intake 'institution' (instID ending with two digits)?
    if ($id =~ /(.+)(\d\d)$/) {
        my ($stem,$this_year) = ($1,$2);

        # Does the corresponding previous year's institution exits?
        my $last_year = $this_year eq "00"
                        ? "99"
                        : sprintf "%02d", ($this_year - 1);
        my $previous_inst = eval { $dir->lookup_inst("$stem$last_year") };

        # If so, we'll use its editors group as our editors group
        if (defined($previous_inst)) {
            my ($result) = $previous_inst->mgrGroupID; # Could be m/valued
            return ($result);
        }

        # If not, try to get the group that manages the parent
        # institution, or (better) the group that manages that
        my $p_inst = eval { $dir->lookup_inst($father) };
        if (defined($p_inst)) {
            my ($p_manager) = $p_inst->mgrgroups; # Could be m/valued
            if (defined($p_manager)) {
                my ($p_manager_2) = $p_manager->mgrgroups; # dito
                return ($p_manager_2->groupID) if defined($p_manager_2);
                return ($p_manager->groupID);
            }
        }

    }

    # if the above failed, or it's simply not an annual group then it
    # _may_ represent a new college undergraduate or graduate inst (if
    # it ends UG or PG), but we can't be sure. Warn about it amd move
    # on

    if ($id =~ /(UG|PG)$/) {
        print "Inst $id: may be college UG/PG pseudo-inst -\n";
        print "Inst $id: management groups may need to be corrected\n";
    }

    # Otherwise, create new, empty editor and manager groups and
    # return them

    # Make 'manager' group
    my $m_group = $dir->create_group;
    $m_group->replace(attr => 'mgrGroupID',
                      value => '000000');
    $m_group->replace(attr => 'visibility',
                      value => 'members');
    $m_group->replace(attr => 'groupTitle',
                      value => "Managers group for \"$ou\"");
    $m_group->replace(attr => 'description', 
                      value => "The members of this group control " .
                               "the group of editors for the directory " .
                               "entry for \"$ou\".  They may manage other " .
                               "groups.");
    my $m_group_id = '000000'; # Dummy in case do_update is false
    if ($do_update) {
        $m_group->commit;
        $m_group_id = $m_group->groupID;
        $m_group->replace(attr => 'mgrGroupID', 
                          value => ['000000', $m_group_id]);
        $m_group->commit;
        print "Group $m_group_id (", $m_group->groupTitle, "): created\n";
    }
    else {
        print "Need to create new manager group\n";
    }

    # Make 'editor' group
    my $e_group = $dir->create_group;
    $e_group->replace(attr => 'mgrGroupID', 
                      value => [$m_group_id]);
    $e_group->replace(attr => 'visibility', 
                      value => 'members');
    $e_group->replace(attr => 'groupTitle',
                      value => "Editors group for \"$ou\"");
    $e_group->replace(attr => 'description', 
                      value => "The members of this group may edit the " .
                               "directory data for \"$ou\"");
    if ($do_update) {
        $e_group->commit;
        print "Group ", $e_group->groupID, " (", $e_group->groupTitle, 
            "): created\n";
    }
    else {
        print "Need to create new editor group\n";
    }

    return ($e_group->groupID,$m_group_id);

}

# ---

sub _strip_dot {

    my ($text) = @_;

    $text =~ s/\.$//;

    return $text;

}

1;

__END__

=head1 NAME

Ucam::Lookup::JackdawUtils - Jackdaw -> lookup interface utilities

=head1 SYNOPSIS

  use Ucam::Lookup::JackdawUtils qw(create_inst   update_inst 
                                    create_person update_person 
                                    cancel_entry remake_ou 
                                    sortout_name fix_insts);

  my ($cn,$title,$initials,$surname) = sortout_name('Warbrick Mr J.');

  my @new = fix_insts($old,$new,@existing);

  $inst = create_inst($dir,1,'UES','Emulsion Service','FOO');
  update_inst($dir,$inst,1,'University Emulsion Service','BAR');

  $person = create_person($dir,1,'JW99','Warbrick Mr J.');    
  update_person($dir,$person,1,'von-Smith Sir A.B.',
                'UES','$cammail','$ex_dir');

  cancel_entry($dir,$inst,1,$do_update);

=head1 DESCRIPTION

This is a set of utility routines for updating lookup from data
retrieved from Jackdaw. If you don't know what this means then you
probably don't need to use them.

=head1 EXPORT

Nothing by default; any of C<create_inst>, C<update_inst>, 
C<create_person>, C<update_person>, C<cancel_entry>, C<remake_ou>, 
C<sortout_name> and C<fix_insts> on request.

=head1 FUNCTIONS

For all the routines below, the L<Ucam::Directory> object C<$dir> will
need to have write access to the directory, to be bound to a DN that
can see hidden objects, and to be created with C<godmode =E<gt> 1>.

Most of these routines can throw exceptions - most generated by the
underlying L<Ucam::Directory> objects.

=head2 create_inst

  $inst = create_inst($dir,$do_update,$id,$name,$father);

Create a new L<Ucam::Directory::Institution> object C<$inst>. Unless
C<$do_update> is true, no objects will actually be created or
changed. C<$id> will become the new inst's 'instID'. C<$name> is the
name of this this inst. C<$father> is the instID of this inst's
parent. The new inst will have it's 'mgrGroupID' attribute initialised
(see below) but no others, and will not have been committed.

For an inst representing an annual pseudo-inst (C<$id> matches
C</\d\d$/>), 'mgrGroupID' will be set to the 'mgrGroupID' of the
previous year's version of the inst if that can be found, otherwise to
the groupID of the group managing the group managing the inst
identified by C<$father>, and otherwise to the groupID of the group
managing the inst identified by C<$father>. For non-annual insts, new
managers and editors groups will be created using the information in
C<$name>.

=head2 update_inst 

  update_inst($dir,$inst,$do_update,$name,$father);

Update or initialise the L<Ucam::Direcrory::Institution> object
C<$inst>. Unless C<$do_update> is true, no objects will be created or
changed. C<$name> is a (possibly new) name for the inst. C<$father>
is the instID of a (possibly new) parent institution.

C<$name> will replace the object's 'ou' attribute if C<$name> has
changed since a previous update or if there has never been such an
update.

C<$father> will be used to update the inst's 'parentInstID'
attributes using C<fix_insts>.

=head2 create_person

  $person = create_person($dir,$do_update,$id,$name);

Create a new L<Ucam::Directory::Person> object C<$person>. Unless
C<$do_update> is true, no objects will actually be created or
changed. C<$id> will become the new person's 'uid'. C<$name> is the
Jackdaw-formatted name of this person. The new person will have it's
'displayName' attribute initialised (see below) but no others, and
will not have been committed.

=head2 update_person

   update_person($dir,$person,$do_update,$name,$inst,$cammail,$ex_dir);

Update or initialise the L<Ucam::Direcrory::Person> object
C<$person>. Unless C<$do_update> is true, no objects will be created
or changed. C<$name> is the Jackdaw-formatted name of this
person. C<$inst> is their Jackdaw institution affiliation. C<$cammail>
reflects the existence of an @cam mail address for this user ('Y' or
'N'). C<$ex_dir> is their Jackdaw ex-directory flag ('Y', 'N', or
blank if the user has not yet expressed a preference.

If C<$name> has changed since a previous update it will be used to set
the person's 'cn' and 'sn' attributes via a call to C<fix_insts>.

C<$inst> will be used to update the person's 'instID' attributes using
C<fix_insts>.

If C<$cammail> or C<$ex_dir> have changed since a previous update then
the person's @cam e-mail address will be added to their 'mail' or
'mailAlternative' attributes if C<$cammail> is 'Y' and C<$ex_dir> is
'N'. Otherwise it will be removed.

=head2 cancel_entry

  cancel_entry($dir,$entry,$do_cancel,$do_update);

C<$entry> represents a L<Ucam::Direcrory::Entry> object, normally a
L<Ucam::Direcrory::Institution> or a
L<Ucam::Direcrory::Person>. Unless C<$do_update> is true, no objects
will actually be changed. If C<$do_cancel> is true, the object will be
marked as cancelled if not already cancelled; otherwise it will be
marked uncancelled if currently cancelled.

=head2 remake_ou

    remake_ou($dir,$inst_id,$do_update);

Unless C<$do_update> is true, no objects will actually be
changed. Otherwise, every L<Ucam::Direcrory::Person> in the directory
that includes C<$inst_id> as one of the values of its 'instID'
attribute will have its 'ou' attribute recalculated. This will cause
'ou' to contain only the 'ou's from non-cancelled institutions.

=head2 sortout_name

    my ($cn,$title,$initials,$sn) = sortout_name($name);

Attempts to parse C<$name> according to the rules used within Jackdaw
for storing names, titles and initials. C<$cn> is suitable for use as
a 'cn' attribute and will contain title(s), initial(s) and surname
space-separated in that order. C<$title> will contain title(s), if
any. C<$initials> will contain all initials. C<$sn> will contain the
surname part of C<$name>. The Jackdaw format is ill-defined, poorly
enforced, and hard to parse. Some names will produce stupid results.

=head2 fix_insts

  my @new = fix_insts($old,$new,@existing);

Attempt to deal with a change in institution affiliation (e.g a
person's institution or an institution's parent). C<@existing>
contains a current list of affiliations, C<$old> is a previous value
obtained from Jackdaw, C<$new> is a new value. 

If C<$old> appears in C<@existing> and C<$new> does not then C<$old>
is removed. Independently of this, if C<$new> does not appear in
C<@existing> then it is appended. If any changes have been made than
the new list is returned, otherwise an empty list is returned. Becasue
if this, it is important to check for an empty list and not to blindly
replace current data with the C<fix_inst>s return value.

=head2 get_prefix

  $prefix = get_prefix($instid);

For a recognised college, returns the prefix of $instid used to form
annual intake groups, undergraduate and postgraduate groups,
etc. Otherwise returns undef.

=head2 get_college

  $collegeid = get_college($instid);

For an instid representing a recognised college annual intake group or
undergraduate or postgraduate group, return the corresponding college
instID. Otherwise returns undef.

=head1 SEE ALSO

L<Ucam::Direcrory>, L<Ucam::Direcrory::Person>,
L<Ucam::Direcrory::Institution>, L<Ucam::Direcrory::Entry>

=head1 AUTHOR

Jon Warbrick, jw35@cam.ac.uk

=cut
