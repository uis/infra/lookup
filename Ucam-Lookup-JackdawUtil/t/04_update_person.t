# $Id$   -*- cperl -*-

use warnings;
use strict;

#use Test::More 'no_plan';
use Test::More tests => 142, import => ['!is_deeply'];
eval "use Test::Differences"; # In case it isn't installed
use Test::Exception;
use Test::Output;
use Test::NoWarnings;

use Ucam::Directory;
use Ucam::Directory::TestServer;

# Upgrade to Test::Differences::eq_or_diff if available
sub is_deeply {
    goto &eq_or_diff if defined &eq_or_diff;
    goto &Test::More::is_deeply;
}

BEGIN {
    use_ok('Ucam::Lookup::JackdawUtils','update_person');
}

my $server;
ok($server = new Ucam::Directory::TestServer("t/fakedir.ldif"));

my $dir;
ok($dir = new Ucam::Directory(ldapserver => $server->url,
                              godmode => 1,
                              viewer => 'root', 
                              service => "test",
                              binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk",
                              password => "ossifrage"));

use constant DOUPDATE => 1;
use constant DRYRUN => 0;

my ($inst,$person);

# Create an person and populate it using update
ok($person = $dir->create_person('zz99'),'created zz99');
stdout_is { update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
                          'CS','N','N','CAIUS','Y','') } <<'EOT';
Person zz99: sn/cn/jdName needs update
Person zz99:   jdName: '' --> 'von-Smith Sir A.B.'
Person zz99:   sn: '' --> 'von Smith'
Person zz99:   cn: '' --> 'Sir A.B. von Smith'
Person zz99: instID/jdInst/jdColl needs update
Person zz99:   jdInst: '' --> 'CS'
Person zz99:   jdColl: '' --> 'CAIUS'
Person zz99:   instID: '' --> 'CS'
Person zz99: allmail/jdCamMail/jdExDir needs update
Person zz99:   jdCamMail: '' --> 'N'
Person zz99:   jdExDir: '' --> 'N'
Person zz99: misAffiliation needs update
Person zz99:   misAffiliation: '' --> 'staff'
Person zz99: update committed
EOT
is($person->uid,'zz99');
is($person->cn,'Sir A.B. von Smith');
is($person->sn,'von Smith');
is($person->jdName,'von-Smith Sir A.B.');
is($person->instID,'CS');
is($person->jdInst,'CS');
is($person->allmail,undef);
is($person->jdCamMail,'N');
is($person->jdExDir,'N');
is($person->misAffiliation,'staff');

# Null update
stdout_is { update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
                          'CS','N','N','CAIUS','Y','') } "";
is($person->uid,'zz99');
is($person->cn,'Sir A.B. von Smith');
is($person->sn,'von Smith');
is($person->jdName,'von-Smith Sir A.B.');
is($person->instID,'CS');
is($person->jdInst,'CS');
is($person->allmail,undef);
is($person->jdCamMail,'N');
is($person->jdExDir,'N');
is($person->misAffiliation,'staff');

# Change everything
update_person($dir,$person,DOUPDATE,'Jones Z.',
              'INGBS','Y','N','CAIUS','','Y');
is($person->uid,'zz99');
is($person->cn,'Z. Jones');
is($person->sn,'Jones');
is($person->jdName,'Jones Z.');
is($person->instID,'INGBS');
is($person->jdInst,'INGBS');
is_deeply([$person->allmail],['zz99@cam.ac.uk']);
is($person->jdCamMail,'Y');
is($person->jdExDir,'N');
is($person->misAffiliation,'student');

# Dry-run a change
stdout_is { update_person($dir,$person,DRYRUN,'Sprog','CS','N','Y',
                          'CAIUS','','') } <<'EOT';
Person zz99: sn/cn/jdName needs update
Person zz99:   jdName: 'Jones Z.' --> 'Sprog'
Person zz99:   sn: 'Jones' --> 'Sprog'
Person zz99:   cn: 'Z. Jones' --> 'Sprog'
Person zz99: instID/jdInst/jdColl needs update
Person zz99:   jdInst: 'INGBS' --> 'CS'
Person zz99:   instID: 'INGBS' --> 'CS'
Person zz99: allmail/jdCamMail/jdExDir needs update
Person zz99:   jdCamMail: 'Y' --> 'N'
Person zz99:   jdExDir: 'N' --> 'Y'
Person zz99:   allmail: 'zz99@cam.ac.uk' --> ''
Person zz99: misAffiliation needs update
Person zz99:   misAffiliation: 'student' --> ''
EOT
is($person->uid,'zz99');
is($person->cn,'Z. Jones');
is($person->sn,'Jones');
is($person->jdName,'Jones Z.');
is($person->instID,'INGBS');
is($person->jdInst,'INGBS');
is_deeply([$person->allmail],['zz99@cam.ac.uk']);
is($person->jdCamMail,'Y');
is($person->jdExDir,'N');
is($person->misAffiliation,'student');

$person=$dir->lookup_person('zz99'); # force reload
is($person->uid,'zz99');
is($person->cn,'Z. Jones');
is($person->sn,'Jones');
is($person->jdName,'Jones Z.');
is($person->instID,'INGBS');
is($person->jdInst,'INGBS');
is_deeply([$person->allmail],['zz99@cam.ac.uk']);
is($person->jdCamMail,'Y');
is($person->jdExDir,'N');
is($person->misAffiliation,'student');


# Set EX_DIRECTORY
update_person($dir,$person,DOUPDATE,'Jones Z.','INGBS','Y','Y','CAIUS','Y','');
is_deeply([$person->allmail],[]);

# Add CSTEACH as a second instID then replace INGBS by CS
$person->replace(attr=>'instID',val=>['INGBS','CSTEACH']);
$person->replace(attr=>'jdInst',val=>'INGBS');
is_deeply([$person->instID],['INGBS','CSTEACH']);
is($person->jdInst,'INGBS');
update_person($dir,$person,DOUPDATE,'Jones Z.','CS','Y','Y','CAIUS','Y','');
is_deeply([$person->instID],['CSTEACH','CS']);
is($person->jdInst,'CS');

# Add CSTEACH as a second instID then re-add INGBS
$person->replace(attr=>'instID',val=>['INGBS','CSTEACH']);
$person->replace(attr=>'jdInst',val=>'INGBS');
is_deeply([$person->instID],['INGBS','CSTEACH']);
is($person->jdInst,'INGBS');
update_person($dir,$person,DOUPDATE,'Jones Z.','INGBS','Y','Y','CAIUS','Y','');
is_deeply([$person->instID],['INGBS','CSTEACH']);
is($person->jdInst,'INGBS');

# Set CSTEACH and INGBS as parent, then add CS
$person->replace(attr=>'instID',val=>['INGBS','CSTEACH']);
$person->replace(attr=>'jdInst',val=>'ROOT');
is_deeply([$person->instID],['INGBS','CSTEACH']);
is($person->jdInst,'ROOT');
update_person($dir,$person,DOUPDATE,'Jones Z.','CS','Y','Y','CAIUS','Y','');
is_deeply([$person->instID],['INGBS','CSTEACH','CS']);
is($person->jdInst,'CS');

# Initialize test manipulations of cammail and ex_dir
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','N','CAIUS','Y','');
is($person->allmail,undef);
is($person->jdCamMail,'N');
is($person->jdExDir,'N');

# Add cammail
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','N','CAIUS','Y','');
is_deeply([$person->allmail],['zz99@cam.ac.uk']);
is($person->jdCamMail,'Y');
is($person->jdExDir,'N');

# Suppress
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','Y','CAIUS','Y','');
is_deeply([$person->allmail],[]);
is($person->jdCamMail,'Y');
is($person->jdExDir,'Y');

# Unsuppress
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','N','CAIUS','Y','');
is_deeply([$person->allmail],['zz99@cam.ac.uk']);
is($person->jdCamMail,'Y');
is($person->jdExDir,'N');

# Remove
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','N','CAIUS','Y','');
is_deeply([$person->allmail],[]);
is($person->jdCamMail,'N');
is($person->jdExDir,'N');


# add/suppress/unsuppress with an additional address
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','N','CAIUS','Y','');
$person->replace(attr=>'allmail',val=>['fred@fred.org']);
is_deeply([$person->allmail],['fred@fred.org']);

# Add @cam
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','N','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org','zz99@cam.ac.uk']);

# Go ex dir
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','Y','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org']);

# Cancel ex dir
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','N','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org','zz99@cam.ac.uk']);

# Remove @cam
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','N','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org']);

# Add @cam
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','N','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org','zz99@cam.ac.uk']);

# Change mail list order
$person->replace(attr=>'allmail',val=>['zz99@cam.ac.uk','fred@fred.org']);
is_deeply([$person->allmail],['zz99@cam.ac.uk','fred@fred.org']);

# Remove @cam
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','N','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org']);

# Go ex dir, then add @cam
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','Y','CAIUS','Y','');
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','Y','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org']);

# More cammail/ex_dir

$person->replace(attr=>'allmail',val=>undef);
is($person->allmail,undef);
$person->replace(attr=>'jdCamMail',val=>undef);
is($person->jdCamMail,undef);
$person->replace(attr=>'jdExDir',val=>undef);
is($person->jdExDir,undef);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','','CAIUS','Y','');
is_deeply([$person->allmail],[]);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','Y','CAIUS','Y','');
is_deeply([$person->allmail],[]);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','N','CAIUS','Y','');
is_deeply([$person->allmail],[]);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','','CAIUS','Y','');
is_deeply([$person->allmail],[]);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','Y','CAIUS','Y','');
is_deeply([$person->allmail],[]);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','N','CAIUS','Y','');
is_deeply([$person->allmail],['zz99@cam.ac.uk']);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','N','CAIUS','Y','');
is_deeply([$person->allmail],[]);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','N','CAIUS','Y','');
is_deeply([$person->allmail],['zz99@cam.ac.uk']);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','Y','CAIUS','Y','');
is_deeply([$person->allmail],[]);

$person->replace(attr=>'allmail',val=>'fred@fred.org');
is_deeply([$person->allmail],['fred@fred.org']);
$person->replace(attr=>'jdCamMail',val=>undef);
is($person->jdCamMail,undef);
$person->replace(attr=>'jdExDir',val=>undef);
is($person->jdExDir,undef);

update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','Y','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org']);
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','Y','N','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org','zz99@cam.ac.uk']);
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.','CS','N','N','CAIUS','Y','');
is_deeply([$person->allmail],['fred@fred.org']);

# Undergraduate/graduate/postgraduate overrides
$person->replace(attr=>'instID',val=>undef);
$person->replace(attr=>'jdInst',val=>undef);
$person->replace(attr=>'jdColl',val=>undef);
# Create as an undergraduste
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'DAR99','N','N','DARWIN','Y','');
is_deeply([$person->instID],['DARUG']);
# Update to following year
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'DAR00','N','N','DARWIN','Y','');
is_deeply([$person->instID],['DARUG']);
# Change college
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CAIUS00','N','N','CAIUS','Y','');
is_deeply([$person->instID],['CAIUSUG']);
# Turn into a postgraduate
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'FOO01','N','N','CAIUS','Y','');
is_deeply([$person->instID],['FOO01','CAIUSPG']);
# Change college
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'FOO01','N','N','DARWIN','Y','');
is_deeply([$person->instID],['FOO01','DARPG']);
# Move to non-student
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CS','N','N','DARWIN','Y','');
is_deeply([$person->instID],['CS']);
# Chaneg college
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CS','N','N','CAIUS','Y','');
is_deeply([$person->instID],['CS']);
# Re-initialise
$person->replace(attr=>'instID',val=>undef);
$person->replace(attr=>'jdInst',val=>undef);
$person->replace(attr=>'jdColl',val=>undef);
# Create as a postgrad
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'FOO01','N','N','CAIUS','Y','');
is_deeply([$person->instID],['FOO01','CAIUSPG']);
# Make into undergraduate
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CAIUS99','N','N','CAIUS','Y','');
is_deeply([$person->instID],['CAIUSUG']);
# Make into college staff
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CAIUS','N','N','CAIUS','Y','');
is_deeply([$person->instID],['CAIUS']);
# Make into staff elsewhere
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CS','N','N','CAIUS','Y','');
is_deeply([$person->instID],['CS']);
# Re-initialise
$person->replace(attr=>'instID',val=>undef);
$person->replace(attr=>'jdInst',val=>undef);
$person->replace(attr=>'jdColl',val=>undef);
# Make into undergraduate
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CAIUS99','N','N','CAIUS','Y','');
is_deeply([$person->instID],['CAIUSUG']);
# Make into staff elsewhere
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CS','N','N','CAIUS','Y','');
is_deeply([$person->instID],['CS']);
# Make into a postgrad
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'FOO01','N','N','CAIUS','Y','');
is_deeply([$person->instID],['FOO01','CAIUSPG']);
# Back to staff
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CS','N','N','CAIUS','Y','');
is_deeply([$person->instID],['CS']);
# Make into an undergrad
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.C.',
              'CAIUS01','N','N','CAIUS','Y','');
is_deeply([$person->instID],['CAIUSUG']);
# Re-initialise with other instID
$person->replace(attr=>'instID',val=>'INGBS');
$person->replace(attr=>'jdInst',val=>undef);
$person->replace(attr=>'jdColl',val=>undef);
# Make into undergraduate
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CAIUS99','N','N','CAIUS','Y','');
is_deeply([$person->instID],['INGBS','CAIUSUG']);
# Make into staff elsewhere
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CS','N','N','CAIUS','Y','');
is_deeply([$person->instID],['INGBS','CS']);
# Make into a postgrad
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'FOO01','N','N','CAIUS','Y','');
is_deeply([$person->instID],['INGBS','FOO01','CAIUSPG']);
# Back to staff
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.',
              'CS','N','N','CAIUS','Y','');
is_deeply([$person->instID],['INGBS','CS']);
# Make into an undergrad
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.C.',
              'CAIUS01','N','N','CAIUS','Y','');
is_deeply([$person->instID],['INGBS','CAIUSUG']);

# Change college (so INST and COLL don't agree)
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.C.',
              'CAIUS01','N','N','DARWIN','Y','');
is_deeply([$person->instID],['INGBS','CAIUSUG']);

# Non-existent institution
ok($person = $dir->create_person('zz01'),'created zz01');
dies_ok { update_person($dir,$person,DOUPDATE,'Smith J.',
                        'NONEXISTENT','N','N','NONEXISTENT','Y','') };

# Check misAffiliation tracks staff/student OK
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.C.',
              'CAIUS01','N','N','DARWIN','','');
is_deeply([$person->misAffiliation],[]);
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.C.',
              'CAIUS01','Y','N','DARWIN','Y','');
is_deeply([$person->misAffiliation],['staff']);
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.C.',
              'CAIUS01','N','N','DARWIN','','Y');
is_deeply([$person->misAffiliation],['student']);
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.C.',
              'CAIUS01','N','N','DARWIN','Y','Y');
is_deeply([$person->misAffiliation],['staff','student']);
update_person($dir,$person,DOUPDATE,'von-Smith Sir A.B.C.',
              'CAIUS01','N','N','DARWIN','','');
is_deeply([$person->misAffiliation],[]);

