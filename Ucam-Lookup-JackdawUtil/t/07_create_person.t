# $Id$   -*- cperl -*-

use warnings;
use strict;

#use Test::More 'no_plan', import => ['!is_deeply'];
use Test::More tests => 11, import => ['!is_deeply'];
use Test::Exception;
use Test::NoWarnings;
use Test::Output;

use Ucam::Directory;
use Ucam::Directory::TestServer;

BEGIN {
    use_ok('Ucam::Lookup::JackdawUtils','create_person');
}

my $server;
ok($server = new Ucam::Directory::TestServer("t/fakedir.ldif"));

my $dir;
ok($dir = new Ucam::Directory(ldapserver => $server->url,
                              godmode => 1,
                              viewer => 'root', 
                              service => "test",
                              binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk",
                              password => "ossifrage"));

use constant DOUPDATE => 1;
use constant DRYRUN => 0;

my ($inst,$person,$group,$group2,@result);

# Dry run create

stdout_is { $person = create_person($dir,DRYRUN,'ZZ99','Warbrick Mr J.') }
    <<'EOT';
Person ZZ99: would be created, displayName = Mr J. Warbrick
EOT
is($person->uid,'zz99');
is($person->displayName,'Mr J. Warbrick');
dies_ok { $dir->lookup_person('zz99') } 'Person not commited';

# Creaing a person

stdout_is { $person = create_person($dir,DOUPDATE,'ZZ99','Warbrick Mr J.') }
    <<'EOT';
Person ZZ99: created, displayName = Mr J. Warbrick
EOT
is($person->uid,'zz99');
is($person->displayName,'Mr J. Warbrick');


