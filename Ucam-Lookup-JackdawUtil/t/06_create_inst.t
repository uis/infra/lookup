# $Id$   -*- cperl -*-

$| = 1;

use warnings;
use strict;

#use Test::More 'no_plan', import => ['!is_deeply'];;
use Test::More tests => 47, import => ['!is_deeply'];
eval "use Test::Differences"; # In case it isn't installed
use Test::Exception;
use Test::NoWarnings;
use Test::Output;

use Ucam::Directory;
use Ucam::Directory::TestServer;

# Upgrade to Test::Differences::eq_or_diff if available
sub is_deeply {
    goto &eq_or_diff if defined &eq_or_diff;
    goto &Test::More::is_deeply;
}

BEGIN {
    use_ok('Ucam::Lookup::JackdawUtils','create_inst');
}

my $server;
ok($server = new Ucam::Directory::TestServer("t/fakedir.ldif"),'Server OK');

my $dir;
ok($dir = new Ucam::Directory(ldapserver => $server->url,
                              godmode => 1,
                              viewer => 'root', 
                              service => "test",
                              binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk",
                              password => "ossifrage"),'directory OK');

use constant DOUPDATE => 1;
use constant DRYRUN => 0;

my ($inst,$person,$group,$group2);

# Dry run create, shouldn't create groups

stdout_is {
$inst = create_inst($dir,DRYRUN,'ZZZ','ZZZ Inst','CS');
} <<'EOT';
Need to create new manager group
Need to create new editor group
Inst ZZZ: would be created
EOT
is($inst->instID,'ZZZ','InstID is ZZZ');
is($inst->mgrGroupID,undef);
ok(!$inst->mgrgroups);
dies_ok { $dir->lookup_inst('ZZZ') } 'Inst not actually commited';

# Creaing an 'ordinary' inst, should provoke group creation

stdout_is { $inst = create_inst($dir,DOUPDATE,'ZZZ','ZZZ Inst','CS') } <<'EOT';
Group 200000 (Managers group for "ZZZ Inst"): created
Group 200001 (Editors group for "ZZZ Inst"): created
Inst ZZZ: created
EOT
is($inst->instID,'ZZZ');
isnt($inst->mgrGroupID,undef);
ok($inst->mgrgroups);

is_deeply([$inst->groupID],['200001','200000']);

($group) = $inst->mgrgroups;
is($group->groupTitle,'Editors group for "ZZZ Inst"');
isnt($group->mgrGroupID,undef);
ok($group->mgrgroups);
is($group->visibility,'members');

($group2) = $group->mgrgroups;
is($group2->groupTitle,'Managers group for "ZZZ Inst"');
is_deeply([$group2->mgrGroupID],
          ['000000',$group2->groupID]); # Should be group '0 or self'
is($group2->visibility,'members');

# Create a non-College 'annual' inst for a group we haven't seen in
# previous years

use constant CS_MANAGERS => '100655';
use constant CS_EDITORS  => '100656';

stdout_is { $inst = create_inst($dir,DOUPDATE,'XXX99','MSc in XXX','CS') } 
    <<'EOT';
Inst XXX99: created
EOT
is($inst->instID,'XXX99');
is($inst->mgrGroupID,CS_MANAGERS); # The CS management group
ok($inst->mgrgroups);

# Swap management of XXX99 to CS editors group (200656), give it an ou
# and commit the result

$inst->replace(attr=>'ou',val=>'MSc in XXX');
$inst->replace(attr=>'mgrGroupID',val=>CS_EDITORS);
is($inst->mgrGroupID,CS_EDITORS);
lives_ok { $inst->commit; };

# Create XXX00 which should get the management group for XXX99
# (checkes year 99 -> 00 special case)
stdout_is { $inst = create_inst($dir,DOUPDATE,'XXX00','MSc in XXX','CS') }
    <<'EOT';
Inst XXX00: created
EOT
is($inst->instID,'XXX00');
is($inst->mgrGroupID,CS_EDITORS);
ok($inst->mgrgroups);

$inst->replace(attr=>'ou',val=>'MSc in XXX');
lives_ok { $inst->commit; };

# Dito XXX01 for non-special case years
stdout_is { $inst = create_inst($dir,DOUPDATE,'XXX01','MSc in XXX','CS') }
    <<'EOT';
Inst XXX01: created
EOT
is($inst->instID,'XXX01');
is($inst->mgrGroupID,CS_EDITORS);
ok($inst->mgrgroups);

# Create an annual inst with a non-existent parent inst
stdout_is { $inst = create_inst($dir,DOUPDATE,'ZZZ01','ZZZ01 inst','NOTEXIST') 
   } <<'EOT';
Group 200002 (Managers group for "ZZZ01 inst"): created
Group 200003 (Editors group for "ZZZ01 inst"): created
Inst ZZZ01: created
EOT

# Create BASE with no mgrGroupID, create inst with this as parent -
# should get new groups created
ok(my $inst2 = $dir->create_inst('BASE'));
$inst2->replace(attr=>'ou',val=>'Base');
lives_ok { $inst2->commit; };
stdout_is { $inst = create_inst($dir,DOUPDATE,'ZZZ02','ZZZ02 inst','BASE') }
    <<'EOT';
Group 200004 (Managers group for "ZZZ02 inst"): created
Group 200005 (Editors group for "ZZZ02 inst"): created
Inst ZZZ02: created
EOT

# Create a new College
stdout_is { $inst = create_inst($dir,DRYRUN,'BOT','St Botolphs','COLL') }
    <<'EOT';
Inst BOT: create UG/PG groups if this is really a new college
Need to create new manager group
Need to create new editor group
Inst BOT: would be created
EOT

# Don't create College annual institution groups, do create 'UG' and 'PG' ones
stdout_is { $inst = create_inst($dir,DRYRUN,'CAIUS99','1999 intake','CAIUS') }
    <<'EOF';
Inst CAIUS99: annual college inst, skipping
EOF
is($inst,undef);

stdout_is { $inst = create_inst($dir,DRYRUN,'CAIUSUG','Undergrad','CAIUS') }
    <<'EOF';
Inst CAIUSUG: may be college UG/PG pseudo-inst -
Inst CAIUSUG: management groups may need to be corrected
Need to create new manager group
Need to create new editor group
Inst CAIUSUG: would be created
EOF
isnt($inst,undef);

stdout_is { $inst = create_inst($dir,DRYRUN,'CAIUSPG','Grad','CAIUS') }
    <<'EOF';
Inst CAIUSPG: may be college UG/PG pseudo-inst -
Inst CAIUSPG: management groups may need to be corrected
Need to create new manager group
Need to create new editor group
Inst CAIUSPG: would be created
EOF
isnt($inst,undef);
