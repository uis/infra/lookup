# $Id$   -*- cperl -*-

use strict;
use warnings;

use Test::More tests => 36;
eval "use Test::Differences"; # In case it's not installed
use Test::NoWarnings;

BEGIN {
    use_ok('Ucam::Lookup::JackdawUtils', 'sortout_name');
}

# basic stuff
do_test('Warbrick J.',       ['J. Warbrick','','J.','Warbrick']);

# Inanity
do_test('0',                ['0','','','0']);
do_test(0,                  ['0','','','0']);
do_test('',                 ['','','','']);
do_test(undef,              ['','','','']);
do_test('Warbrick 0',       ['0 Warbrick','','0','Warbrick']);

# various sorts of initials
do_test('Warbrick J.X.Y.',   ['J.X.Y. Warbrick',  '','J.X.Y.',  'Warbrick']);
do_test('Warbrick J. X. Y.', ['J. X. Y. Warbrick','','J. X. Y.','Warbrick']);
do_test('Warbrick J.X-Y.',   ['J.X-Y. Warbrick',  '','J.X-Y.',  'Warbrick']);
do_test('Warbrick J.XY',     ['J.XY Warbrick',    '','J.XY',    'Warbrick']);

# Titles with and without trailing '.', and invalid titles
do_test('Warbrick Dr J.',    ['Dr J. Warbrick',   'Dr',   'J.','Warbrick']);
do_test('Warbrick Dr. J.',   ['Dr J. Warbrick',   'Dr',   'J.','Warbrick']);
do_test('Warbrick Prof J.',  ['Prof. J. Warbrick','Prof.','J.','Warbrick']);
do_test('Warbrick Prof. J.', ['Prof. J. Warbrick','Prof.','J.','Warbrick']);
do_test('Warbrick Prof. Dr. J.',
                       ['Prof. Dr J. Warbrick','Prof. Dr','J.','Warbrick']);
do_test('Warbrick Dr Prof. J.',
                       ['Dr Prof. J. Warbrick','Dr Prof.','J.','Warbrick']);
do_test('Warbrick Zz J.',    ['Zz J. Warbrick','','Zz J.','Warbrick']);

# surname prefixes at the end, including things that could be titles
do_test('Warbrick J. De',    ['J. De Warbrick',   '','J.',   'De Warbrick']);
do_test('Warbrick J. de',    ['J. de Warbrick',   '','J.',   'de Warbrick']);
do_test('Warbrick J.Z. de',  ['J.Z. de Warbrick', '','J.Z.', 'de Warbrick']);
do_test('Warbrick J. Z. de', ['J. Z. de Warbrick','','J. Z.','de Warbrick']);
do_test('Warbrick J. zz',    ['J. zz Warbrick',   '','J. zz','Warbrick']);
do_test('Warbrick J. Dr',    ['J. Dr Warbrick',   '','J. Dr','Warbrick']);

# surname prefixes at the start, including unrecognised prefixes and
# excessive numbers of hyphens
do_test('de-Warbrick J.',   ['J. de Warbrick','','J.','de Warbrick']);
do_test('De-Warbrick J.',   ['J. De Warbrick','','J.','De Warbrick']);
do_test('Zz-Warbrick J.',   ['J. Zz-Warbrick','','J.','Zz-Warbrick']);
do_test('De-La-von-Warbrick J.',
                     ['J. De La von Warbrick','','J.','De La von Warbrick']);
do_test('De-Zz-La-Warbrick J.',
                       ['J. De Zz-La-Warbrick','','J.','De Zz-La-Warbrick']);
do_test('de--Warbrick J.',  ['J. de Warbrick',   '','J.','de Warbrick']);
do_test('de--la-Warbrick J.', ['J. de la Warbrick','','J.','de la Warbrick']);
do_test('de-la--Warbrick J.', ['J. de la Warbrick','','J.','de la Warbrick']);

# the entire kit and caboodle, with and without gratutious space
do_test('De-le-Zz-La-Warbrick Dr Prof. J.Z. X. von du',
               ['Dr Prof. J.Z. X. von du De le Zz-La-Warbrick',
                'Dr Prof.',
                'J.Z. X.',
                'von du De le Zz-La-Warbrick']);
do_test('De-le-Zz-La-Warbrick  Dr    Prof.   J.Z.   X.       von du',
               ['Dr Prof. J.Z. X. von du De le Zz-La-Warbrick',
                'Dr Prof.',
                'J.Z. X.',
                'von du De le Zz-La-Warbrick']);

# Exception processing
do_test('Dowling R. - Test Account',
                              ['R. Dowling - Test Account','','R.','Dowling']);

sub do_test {
    my ($name,$result) = @_;

    # Use Test::Differences if available, else Test::More's is_deeply
    if (defined &eq_or_diff) {
        eq_or_diff([sortout_name($name)],$result,
                   (defined($name) ? $name : '(undef)') . " -> $result->[0]");
    }
    else {
       is_deeply([sortout_name($name)],$result,
                   (defined($name) ? $name : '(undef)') . " -> $result->[0]"); 
   }

}
