# $Id$   -*- cperl -*-

use warnings;
use strict;

#use Test::More 'no_plan';
use Test::More tests => 46, import => ['!is_deeply'];
eval "use Test::Differences"; # In case it isn't installed
use Test::Exception;
use Test::Output;
use Test::NoWarnings;

use Ucam::Directory;
use Ucam::Directory::TestServer;

# Upgrade to Test::Differences::eq_or_diff if available
sub is_deeply {
    goto &eq_or_diff if defined &eq_or_diff;
    goto &Test::More::is_deeply;
}

BEGIN {
    use_ok('Ucam::Lookup::JackdawUtils','update_inst');
}

my $server;
ok($server = new Ucam::Directory::TestServer("t/fakedir.ldif"));

my $dir;
ok($dir = new Ucam::Directory(ldapserver => $server->url,
                              godmode => 1,
                              viewer => 'root', 
                              service => "test",
                              binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk",
                              password => "ossifrage"));

use constant DOUPDATE => 1;
use constant DRYRUN => 0;

my ($inst,$person);

# Don't worry about $inst == undef
lives_ok { update_inst($dir,undef(),DOUPDATE,'ZZZ Inst','CS') };

# Create an inst and populate it using update
ok($inst = $dir->create_inst('ZZZ'),'created ZZZ');
stdout_is { update_inst($dir,$inst,DOUPDATE,'ZZZ Inst','CS') } <<'EOT';
Inst ZZZ: parentInstID/jdFather needs update
Inst ZZZ:   jdFather: '' --> 'CS'
Inst ZZZ:   parentInstID: '' --> 'CS'
Inst ZZZ: ou/jdName needs update
Inst ZZZ:   jdName: '' --> 'ZZZ Inst'
Inst ZZZ:   ou: '' --> 'ZZZ Inst'
Inst ZZZ: update committed
EOT
is($inst->instID,'ZZZ');
is($inst->ou,'ZZZ Inst');
is($inst->parentInstID,'CS');
is($inst->jdFather,'CS');
is($inst->jdName,'ZZZ Inst');

# Re-do the (now null) update
stdout_is { update_inst($dir,$inst,DOUPDATE,'ZZZ Inst','CS') } '';
is($inst->instID,'ZZZ');
is($inst->ou,'ZZZ Inst');
is($inst->parentInstID,'CS');
is($inst->jdFather,'CS');
is($inst->jdName,'ZZZ Inst');

# Change everything
update_inst($dir,$inst,DOUPDATE,'ZZZ Inst2','INGBS');
is($inst->instID,'ZZZ');
is($inst->ou,'ZZZ Inst2');
is($inst->parentInstID,'INGBS');
is($inst->jdFather,'INGBS');
is($inst->jdName,'ZZZ Inst2');

# Dry-run a change
stdout_is { update_inst($dir,$inst,DRYRUN,'ZZZ Inst3','INGB') } <<'EOT';
Inst ZZZ: parentInstID/jdFather needs update
Inst ZZZ:   jdFather: 'INGBS' --> 'INGB'
Inst ZZZ:   parentInstID: 'INGBS' --> 'INGB'
Inst ZZZ: ou/jdName needs update
Inst ZZZ:   jdName: 'ZZZ Inst2' --> 'ZZZ Inst3'
Inst ZZZ:   ou: 'ZZZ Inst2' --> 'ZZZ Inst3'
EOT
is($inst->instID,'ZZZ');
is($inst->ou,'ZZZ Inst2');
is($inst->parentInstID,'INGBS');
is($inst->jdFather,'INGBS');
is($inst->jdName,'ZZZ Inst2');
$inst = $dir->lookup_inst('ZZZ'); # Roll-back by refreshing from disk
is($inst->instID,'ZZZ');
is($inst->ou,'ZZZ Inst2');
is($inst->parentInstID,'INGBS');
is($inst->jdFather,'INGBS');
is($inst->jdName,'ZZZ Inst2');

# Add INGB as a second parent then replace INGBS by CS
$inst->replace(attr=>'parentInstID',val=>['INGBS','INGB']);
$inst->replace(attr=>'jdFather',val=>'INGBS');
is_deeply([$inst->parentInstID],['INGBS','INGB']);
is($inst->jdFather,'INGBS');
update_inst($dir,$inst,DOUPDATE,'ZZZ Inst2','CS');
is_deeply([$inst->parentInstID],['INGB','CS']);
is($inst->jdFather,'CS');

# Set INGB and INGBS as parent, then add CS
$inst->replace(attr=>'parentInstID',val=>['INGBS','INGB']);
$inst->replace(attr=>'jdFather',val=>'ROOT');
is_deeply([$inst->parentInstID],['INGBS','INGB']);
is($inst->jdFather,'ROOT');
update_inst($dir,$inst,DOUPDATE,'ZZZ Inst2','CS');
is_deeply([$inst->parentInstID],['INGBS','INGB','CS']);
is($inst->jdFather,'CS');

# Update CS's name - this should propogate to the inst of fjc55
$inst = $dir->lookup_inst('CS');
is($inst->ou,'University Computing Service (UCS)');
$person = $dir->lookup_person('fjc55');
is($person->ou,'University Computing Service (UCS)');
update_inst($dir,$inst,DOUPDATE,'Commuting Service','INGBS');
is($inst->ou,'Commuting Service');
$person = $dir->lookup_person('fjc55'); # Need to reload
is($person->ou,'Commuting Service');

