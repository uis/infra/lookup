# $Id$   -*- cperl -*-

use strict;
use warnings;

#use Test::More 'no_plan';
use Test::More tests => 24;
eval "use Test::Differences"; # In case it's not installed
use Test::NoWarnings;

BEGIN {
    use_ok('Ucam::Lookup::JackdawUtils', 'fix_insts');
}

#              old  new  list-------    result---------
fix_inst_test(['a', 'b', 'a','b','c'], [               ]); # unchanged a,b,c
fix_inst_test(['a', 'b', 'b','c'    ], [               ]); # unchanged b,c
fix_inst_test(['a', 'b', 'a','c'    ], ['c','b'        ]);
fix_inst_test(['a', 'b', 'c'        ], ['c','b'        ]);
fix_inst_test(['a', 'b', 'a'        ], ['b'            ]);
fix_inst_test(['a', 'b', 'b'        ], [               ]); # unchanged b
fix_inst_test(['a', 'b'             ], ['b'            ]);
fix_inst_test(['a', 'a'             ], ['a'            ]); # unchanged a
fix_inst_test(['a', 'a', 'a'        ], [               ]); # unchanged a
fix_inst_test(['a', 'b', 'a','c','d'], ['c','d','b'    ]);
fix_inst_test(['a', 'b', 'b','c','d'], [               ]); # unchanged b,c,d
fix_inst_test(['a', 'b', 'c','d','e'], ['c','d','e','b']);

fix_inst_test(['0', 'b', '0'        ], ['b'            ]);
fix_inst_test([0,   'b', 0          ], ['b'            ]);
fix_inst_test([undef,'b',           ], ['b'            ]);
fix_inst_test([undef,'b','b'        ], [               ]); # unchanged b
fix_inst_test([undef,'b','b','c'    ], [               ]); # unchanged b,c
fix_inst_test([undef,'b','c','d'    ], ['c','d','b'    ]);

fix_inst_test(['a', '0', 'a'        ], ['0'            ]);
fix_inst_test(['a',  0,  'a'        ], ['0'            ]);
fix_inst_test(['a',  undef, 'a'     ], [               ]);

fix_inst_test(['a',  'b', 0         ], ['0','b'        ]);

sub fix_inst_test {

    my ($args,$result) = @_;

    my @args = @$args;
    my $desc = "old: " .
        def($args[0]) .
        "; new: " .
        def($args[1]) .
        "; list: " .
        join(",",map { def($_) } @args[2..$#args]) .
        "; result: " .
            join(',',map { def($_) } @$result);

    # Use Test::Differences if available, else Test::More's is_deeply
    if (defined(&eq_or_diff)) {
        eq_or_diff([fix_insts(@args)],$result,$desc);
    }
    else {
        is_deeply([fix_insts(@args)],$result,$desc);
    }
}

sub def {
    my ($what) = @_;
    return defined($what) ? $what : '(undef)';
}

