# $Id$   -*- cperl -*-

use strict;
use warnings;

use Test::More tests => 12;
use Test::NoWarnings;

BEGIN {
    use_ok('Ucam::Lookup::JackdawUtils', 'get_prefix', 'get_college');
}

is(get_prefix('QUEENS'),'QUEN');
is(get_prefix('CS'),undef);

is (get_college('QUEN01'),'QUEENS');
is (get_college('QUENUG'),'QUEENS');
is (get_college('QUENPG'),'QUEENS');
is (get_college('QUEENS'),undef);
is (get_college('CS01'),undef);
is (get_college('CSUG'),undef);
is (get_college('CSPG'),undef);
is (get_college('CS'),undef);
