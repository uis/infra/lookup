# $Id$   -*- cperl -*-

use warnings;
use strict;

#use Test::More 'no_plan';
use Test::More tests => 50, import => ['!is_deeply'];
eval "use Test::Differences";  # In case it isn't installed
use Test::Output;
use Test::NoWarnings;
use Test::Exception;

use Ucam::Directory;
use Ucam::Directory::TestServer;

# Upgrade to Test::Differences::eq_or_diff if available
sub is_deeply {
    goto &eq_or_diff if defined &eq_or_diff;
    goto &Test::More::is_deeply;
}

BEGIN {
    use_ok('Ucam::Lookup::JackdawUtils','cancel_entry');
}

my $server;
ok($server = new Ucam::Directory::TestServer("t/fakedir.ldif"));

my $dir;
ok($dir = new Ucam::Directory(ldapserver => $server->url,
                              godmode => 1,
                              viewer => 'root', 
                              service => "test",
                              binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk",
                              password => "ossifrage"));

my ($inst,$person);

use constant CANCEL => 1;
use constant UNCANCEL => 0;
use constant DOUPDATE => 1;
use constant DRYRUN => 0;

# Check cancelling with $instid == undef OK
lives_ok { cancel_entry($dir,undef(),UNCANCEL,DOUPDATE) };

# CS exists in test data, fjc55 belongs to CS and CAKE
$inst = $dir->lookup_inst('CS');
$person = $dir->lookup_person('fjc55');

ok(!$inst->cancelled,'Inst not yet cancelled');
ok(!$person->cancelled,'Person not yet cancelled');
is_deeply([$person->instID],['CS','CAKE']);
is_deeply([$person->ou],['University Computing Service (UCS)']);

# Insts

# Un-cancel un-canceled CS
stdout_is { cancel_entry($dir,$inst,UNCANCEL,DOUPDATE) } "";
ok(!$inst->cancelled,'Inst remains un-cancelled');
$person = $dir->lookup_person('fjc55');
is_deeply([$person->ou],['University Computing Service (UCS)']);

# Dry-run cancel CS
stdout_is { cancel_entry($dir,$inst,CANCEL,DRYRUN) } <<'EOT';
Inst CS: needs cancelling
EOT
ok(!$inst->cancelled,'Inst remains un-cancelled');
$person = $dir->lookup_person('fjc55');
is_deeply([$person->ou],['University Computing Service (UCS)']);

# Really cancel CS
stdout_is { cancel_entry($dir,$inst,CANCEL,DOUPDATE) } <<'EOT';
Inst CS: needs cancelling
Inst CS: cancel status committed
Person fjc55: ou(s) need rebuilding
Person fjc55:   ou: 'University Computing Service (UCS)' --> ''
Person fjc55: ou(s) rebuilt
EOT
ok($inst->cancelled,'Inst now cancelled');
$person = $dir->lookup_person('fjc55');
is_deeply([$person->ou],[]);

# Cancel already canceled inst
stdout_is { cancel_entry($dir,$inst,CANCEL,DOUPDATE) } "";
ok($inst->cancelled,'Inst remains cancelled');
$person = $dir->lookup_person('fjc55');
is_deeply([$person->ou],[]);

# Dry-run un-cancel CS
stdout_is { cancel_entry($dir,$inst,UNCANCEL,DRYRUN) } <<'EOT';
Inst CS: needs un-cancelling
EOT
ok($inst->cancelled,'Inst still cancelled');
$person = $dir->lookup_person('fjc55');
is_deeply([$person->ou],[]);

# Really un-cancel CS
stdout_is { cancel_entry($dir,$inst,UNCANCEL,DOUPDATE) } <<'EOT';
Inst CS: needs un-cancelling
Inst CS: cancel status committed
Person fjc55: ou(s) need rebuilding
Person fjc55:   ou: '' --> 'University Computing Service (UCS)'
Person fjc55: ou(s) rebuilt
EOT
ok(!$inst->cancelled,'Inst no-longer cancelled');
$person = $dir->lookup_person('fjc55');
is_deeply([$person->ou],['University Computing Service (UCS)']);

# People

# Un-cancel un-canceled person
stdout_is { cancel_entry($dir,$person,UNCANCEL,DOUPDATE) } "";
ok(!$person->cancelled,'Person remains un-cancelled');

# Dry-run cancel fjc55
stdout_is { cancel_entry($dir,$person,CANCEL,DRYRUN) } <<'EOT';
Person fjc55: needs cancelling
EOT
ok(!$person->cancelled,'Person still not cancelled');

# Really cancel fjc99
stdout_is { cancel_entry($dir,$person,CANCEL,DOUPDATE) } <<'EOT';
Person fjc55: needs cancelling
Person fjc55: cancel status committed
EOT
ok($person->cancelled,'Person now cancelled');

# Cancel cancelled fjc99
stdout_is { cancel_entry($dir,$person,CANCEL,DOUPDATE) } "";
ok($person->cancelled,'Person remains cancelled');

# Dry-run un-cancel fjc99
stdout_is { cancel_entry($dir,$person,UNCANCEL,DRYRUN) } <<'EOT';
Person fjc55: needs un-cancelling
EOT
ok($person->cancelled,'Person still cancelled');

# Really un-cancel fjc99
stdout_is { cancel_entry($dir,$person,UNCANCEL,DOUPDATE) } <<'EOT';
Person fjc55: needs un-cancelling
Person fjc55: cancel status committed
EOT
ok(!$person->cancelled,'Person no-longer cancelled');

# CAIUSUG should be imune to cancelation
$inst = $dir->lookup_inst('CAIUSUG');
stdout_is { cancel_entry($dir,$inst,CANCEL,DOUPDATE) } <<'EOT';
Inst CAIUSUG: college UG/PG inst, not cancelling
EOT
ok(!$inst->cancelled,'CAIUSUG should not be cancelled');
# ...but should be un-cancelable
$inst->replace(attr=>'cancelled',val=>'YES');
ok($inst->cancelled);
stdout_is { cancel_entry($dir,$inst,UNCANCEL,DOUPDATE) } <<'EOT';
Inst CAIUSUG: needs un-cancelling
Inst CAIUSUG: cancel status committed
EOT
ok(!$inst->cancelled,'CAIUSUG should be un-cancelled');

# CAIUS99 should not be un-cancelable
$inst = $dir->lookup_inst('CAIUS99');
$inst->replace(attr=>'cancelled',val=>'YES');
ok($inst->cancelled);
stdout_is { cancel_entry($dir,$inst,UNCANCEL,DOUPDATE) } <<'EOT';
Inst CAIUS99: college anual inst, not un-cancelling
EOT
ok($inst->cancelled,'CAIUS99 should remain cancelled');
# ...but should be cancelable
$inst->replace(attr=>'cancelled',val=>undef);
ok(!$inst->cancelled);
stdout_is { cancel_entry($dir,$inst,CANCEL,DOUPDATE) } <<'EOT';
Inst CAIUS99: needs cancelling
Inst CAIUS99: cancel status committed
EOT
ok($inst->cancelled,'CAIUS99 should be cancellable');
