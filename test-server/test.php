<?php
// basic sequence with LDAP is connect, bind, search, interpret search
// result, close connection

echo "LDAP query test\n";
echo "Connecting ...\n";
$ds=ldap_connect("ldap://mnementh.csi.cam.ac.uk/");  // must be a valid LDAP server!
echo "connect result is " . $ds . "\n";

if ($ds) {

  echo "Starting TLS...\n";
  //if (!ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3)) {
  //   fatal_error("Failed to set LDAP Protocol version to 3, TLS not supported."); 
  // }
   if (!ldap_start_tls($ds)) {
     fatal_error("Ldap_start_tls failed");
   } 

   echo "Binding ...\n";
   $r=ldap_bind($ds);    // this is an "anonymous" bind, typically
                           // read-only access
   echo "Bind result is " . $r . "\n";

   echo "Searching for (uid=test0001) ...\n";
   // Search surname entry
   $sr=ldap_search($ds, "o=University of Cambridge,dc=cam,dc=ac,dc=uk", 
                        "uid=test0001"); 
   echo "Search result is " . $sr . "\n";

   echo "Number of entires returned is " . ldap_count_entries($ds, $sr) . "\n";

   echo "Getting entries ...\n";
   $info = ldap_get_entries($ds, $sr);
   echo "Data for " . $info["count"] . " items returned:\n";

   for ($i=0; $i<$info["count"]; $i++) {
       echo "dn is: " . $info[$i]["dn"] . "\n";
       //echo "first cn entry is: " . $info[$i]["cn"][0] . "\n";
       //echo "first email entry is: " . $info[$i]["mail"][0] . "\n";
   }

   echo "Closing connection\n";
   ldap_close($ds);

} else {
   echo "Unable to connect to LDAP server\n";
}
?> 