#!/usr/bin/perl

use warnings;
use strict;

use List::MoreUtils qw/uniq/;

use constant HOW_MANY => 200;

# Suppressible attributes
my @suppressible = qw/mailAlternative displayName jpegPhoto labeledURI
                      mail postalAddress telephoneNumber sn cn
                      unreal title/;

    
# 'Institution' data
my @insts = ('CS','BOT','IMPSTUD');
my $n_ints = scalar(@insts);
my %ou = ('CS' => 'Computing Service',
          'BOT' => "St Botolphs' College",
          'IMPSTUD' => 'Deaprtment of Important Studies');

# Surname and forename data
my @surnames;
my $n_surnames = 0;
open my $surnames, 'surnames.txt' or die 'Failed to open surnames.txt';
while (my $line = <$surnames>) {
    chomp $line;
    push @surnames,$line;
    ++$n_surnames;
}
my @firstnames;
my $n_firstnames = 0;
open my $firstnames, 'firstnames.txt' or die 'Failed to open firstnames.txt';
while (my $line = <$firstnames>) {
    chomp $line;
    push @firstnames,$line;
    ++$n_firstnames;
}


for my $person (100..99+HOW_MANY) {

    my $uid = "test0$person";
    
    my $first = $firstnames[rand($n_firstnames)];
    my $initials = $first;
    $initials =~ s/\b([A-Z]).*?\b/$1./g;
    my $last  = $surnames[rand($n_surnames)];

    my $display = (rand>0.7) ? "$first $last" : "$initials $last";

    my (@i,@o);
    $i[0] = $insts[rand($n_ints)] if rand>0.2;
    push @i,$insts[rand($n_ints)] if rand>0.6;
    push @i,$insts[rand($n_ints)] if rand>0.9;
    @i = uniq @i;
    push @o, $ou{$_} foreach (@i);

    my (@m);
    push @m, "$uid\@cam.ac.uk.invalid" if rand>0.4;
    push @m, "$first.$last\@nonsutch.invalid" if rand>0.6;
    push @m, "${last}_$initials\@cam.ac.uk.invalid" if rand>0.6;
    push @m, rand(100000) . "\@x.invalid" if rand>0.6;

    my (@t);
    push @t, sprintf("%5.5i",int(rand(100000))) if rand>0.7;
    push @t, sprintf("01223 %6.6i",int(rand(1000000))) if rand>0.7;
    push @t, sprintf("+44 1223 %6.6i",int(rand(1000000))) if rand>0.7;

    my (@u);
    push @u, "http://www.pwf.cam.ac.uk.invalid/$uid" if rand>0.4;
    push @u, "http://www." . lc($last) . ".invalid/ ${first}'s home page"
        if rand>0.2;

    my (@s);
    if ((rand>0.5)) {
        foreach (@suppressible) {
            push @s, $_ if rand>0.5;
        }
    }

    my (@a);
    push @a, "student" if rand>0.4;
    push @a, "staff"   if rand>0.8;

print <<"EOT";
dn: uid=$uid,ou=people,o=University of Cambridge,dc=cam,dc=ac,dc=uk
uid: $uid
objectClass: camAcUkPerson
objectClass: inetOrgPerson
cn: $initials $last
sn: $last
displayName: $display
EOT

    print "instID: $_\n" foreach (@i);
    print "ou: $_\n" foreach (@o);
    print "mail: $m[0]\n" if $m[0];
    foreach my $c (1..scalar(@m)-1) {
        print "mailAlternative: $m[$c]\n";
    }
    print "telephoneNumber: $_\n" foreach (@t);
    print "labeledURI: $_\n" foreach (@u);
    print "suppression: yes\n" if rand>0.9;
    print "suppressAttribute: $_\n" foreach (@s);
    print "misAffiliation: $_\n" foreach (@a);
    print "\n";

}


