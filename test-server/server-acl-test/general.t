#!/usr/bin/perl -w

# A set of general tests that should all pass whan run on a
# non-lapwing CUDN-connected machine. See outside.t and lapwing.t for
# coresponding tests for other environments. These tests assume an
# LDAP serrver freshly loaded with testdb.ldif and will probably
# false-positive if used against anything else.

use strict;

#use Test::More tests => 1;
use Test::More 'no_plan';

#syncrepl/jackdaw can
#  authenticate
#  read/write cancelled objects
#  read/write ou=ldap_admin
#  read/write suppressAttribute,suppression,jdCamMail,jdExDir,jdColl
#  read/write jdName for ou=people
#  read/write suppressed attributes
#  read/write from outside the CUDN
#  read/write when conencted from Lapwing without SSL
#  read/write uid and allUid
#  read/write everything else
#
#lookup can
#  authenticate
#  NOT read/write cancelled objects
#  read/write everything else
#  read/write ou=ldap_admin
#  read/write suppressAttribute,suppression,jdCamMail,jdExDir,jdColl
#  read/write jdName for ou=people
#  read/write suppressed attributes
#  read/write from outside the CUDN
#  read/write when conencted from Lapwing without SSL
#  read/write uid and allUid
#  read everything else
#
#jdawread can
#  authenticate
#  NOT read/write cancelled objects
#  NOT read/write everything else
#  NOT read/write ou=ldap_admin
#  NOT read/write suppressAttribute,suppression,jdCamMail,jdExDir,jdColl
#  NOT read/write jdName for ou=people
#  NOT read/write suppressed attributes
#  NOT read/write from outside the CUDN
#  NOT read/write when conencted from Lapwing without SSL
#  read allUid
#  read everything else
#
#a group can
#  authenticate
#  NOT read/write cancelled objects
#  NOT read/write everything else
#  NOT read/write ou=ldap_admin
#  NOT read/write suppressAttribute,suppression,jdCamMail,jdExDir,jdColl
#  NOT read/write jdName for ou=people
#  NOT read/write suppressed attributes
#  NOT read/write from outside the CUDN
#  NOT read/write when conencted from Lapwing without SSL
#  read uid and allUid for self even if visibility != cam
#  read uid and allUid for groups it manages even if visibility != cam
#  read uid and allUid for groups it reads even if visibility != cam
#  read everything else
#
#anon can
#  [NOT authenticate]
#  NOT read/write cancelled objects
#  NOT read/write everything else
#  NOT read/write ou=ldap_admin
#  NOT read/write suppressAttribute,suppression,jdCamMail,jdExDir,jdColl
#  NOT read/write jdName for ou=people
#  NOT read/write suppressed attributes
#  NOT read/write anything from outside the CUDN
#  NOT read/write anything whe conencted from Lapwing without SSL
#  NOT read/write allUid
#  read everything else

use Net::LDAP qw(LDAP_STRONG_AUTH_REQUIRED LDAP_INSUFFICIENT_ACCESS);

use constant DEBUG => 0;   # Net::LDAP debugging

use constant TARGET    => 'jodrell.csx.cam.ac.uk';
use constant BASE_DN   => 'o=University of Cambridge,dc=cam,dc=ac,dc=uk';
use constant PEOPLE_DN => 'ou=people,'     . BASE_DN;
use constant GROUPS_DN => 'ou=groups,'     . BASE_DN;
use constant ADMIN_DN  => 'ou=ldap_admin,' . BASE_DN;

use constant CAFILE    => '/etc/ssl/certs/ca-certificates.crt';

use constant SYNCREPL => 0;
use constant JACKDAW  => 1;
use constant LOOKUP   => 2;
use constant JDAWREAD => 3;
use constant GROUP1   => 4;
use constant GROUP2   => 5;
use constant ANON_TLS => 6;
use constant ANON     => 7;

use constant LASTCON  => 7;

use constant DESCR    => ["syncrepl","jackdaw","lookup",
                          "jdawread","group1 (mgr)","group2 (rdr)",
                          "anon(tls)","anon"];

my @con;

sub get_obj {
    my ($ldap,$what) = @_;
    my $msg = $ldap->search(base => $what . ',' . BASE_DN,
                            scope => 'base',
                            filter => '(objectClass=*)',
                            attrs => ['1.1']);
    diag("$what: " . $msg->error) && return if $msg->is_error;
    return $msg->count;
}

sub get_attr {
    my ($ldap,$what,$attr) = @_;
    my $msg = $ldap->search(base => $what . ',' . BASE_DN,
                            scope => 'base',
                            filter => '(objectClass=*)',
                            attrs => [$attr]);
    diag("$what: " . $msg->error) || return if $msg->is_error;
    return 0 unless $msg->count;
    return $msg->pop_entry->exists($attr) ? 1 : 0;
}

sub test_all {
    my ($sub,$msg,@exp) = @_;
    BAIL_OUT("Not enough arguements to test_all") unless $#exp == LASTCON;
    foreach (0..LASTCON) {
        is(&$sub($con[$_]), $exp[$_],
           "$msg by ".DESCR->[$_].", want $exp[$_]");
    }
}

sub bind_as {

    my ($tls,$dn,$pwd) = @_;

    my $ldap = Net::LDAP->new(TARGET, debug=>DEBUG);
    return unless $ldap;

    my $msg;
    $msg = $ldap->start_tls(verify => 'require', cafile => CAFILE);
    return if $msg->is_error;

    if ($dn) {
        $msg = $ldap->bind($dn . ',' . BASE_DN, password => $pwd);
        return if $msg->is_error;
    }

    return $ldap;

}

# an object that has been canceleld
sub cancelled {
    my ($ldap) = @_;
    return get_obj($ldap,'uid=cance1,ou=people');
}

# an object in ou=ldap_admin
sub ldap_admin {
    my ($ldap) = @_;
    return get_obj($ldap,'cn=lookup,ou=ldap_admin');
}

# a person's 'suppression' attribute
sub suppression {
    my ($ldap) = @_;
    return get_attr($ldap,'uid=fjc55,ou=people','suppression');
}

# 'jdName' attribute - person and inst
sub jdname_person {
    my ($ldap) = @_;
    return get_attr($ldap,'uid=fjc55,ou=people','jdName');
}
sub jdname_inst {
    my ($ldap) = @_;
    return get_attr($ldap,'instid=CS,ou=insts','jdName');
}

# a suppressed atribute
sub suppressed {
    my ($ldap) = @_;
    return get_attr($ldap,'uid=fjc55,ou=people','cn');
}

# a group's uid attribute - group 3 has visibility cam, group 4 has
# visibility managers
sub uid_cam {
    my ($ldap) = @_;
    return get_attr($ldap,'groupid=3,ou=groups','uid');
}
sub uid_mgr {
    my ($ldap) = @_;
    return get_attr($ldap,'groupid=4,ou=groups','uid');
}

# a group's allUid attribute
sub alluid_cam {
    my ($ldap) = @_;
    return get_attr($ldap,'groupid=3,ou=groups','allUid');
}
sub alluid_mgr {
    my ($ldap) = @_;
    return get_attr($ldap,'groupid=3,ou=groups','allUid');
}

# a 'normal' attrubute
sub normal {
    my ($ldap) = @_;
    return get_attr($ldap,'uid=fjc55,ou=people','sn');
}

sub genesis {
    my ($ldap) = @_;
    my $msg = $ldap->add('uid=new1,' . PEOPLE_DN,
                         attr => [
                             'objectclass' => ['camAcUkPerson','inetOrgPerson'],
                             'uid' => 'new1',
                             'cn' => 'S. New',
                             'sn' => 'New',
                             'ou' => 'University Computing Service',
                             'instID' => 'CS',
                             'jdName' => 'New S.'
                         ]);
    return 0 if $msg->code == LDAP_STRONG_AUTH_REQUIRED or
        $msg->code == LDAP_INSUFFICIENT_ACCESS;
    diag("creating new1: " . $msg->error) || return if $msg->is_error;
    $msg = $ldap->modify('uid=new1,' . PEOPLE_DN,
                         add => { 'displayName' => 'Someone New' },
                         replace => {'cn' => 'S.W. New' },
                         delete => ['jdName']);
    diag("modifying new1: " . $msg->error) || return if $msg->is_error;
    $msg = $ldap->delete('uid=new1,' . PEOPLE_DN);
    diag("modifying new1: " . $msg->error) || return if $msg->is_error;
    return 1;
}

ok($con[SYNCREPL] = bind_as(1,'cn=syncrepl,ou=ldap_admin','syncrepl'));
ok($con[JACKDAW]  = bind_as(1,'cn=jackdaw,ou=ldap_admin','jackdaw'));
ok($con[LOOKUP]   = bind_as(1,'cn=lookup,ou=ldap_admin','lookup'));
ok($con[JDAWREAD] = bind_as(1,'cn=jdawread,ou=ldap_admin','jdawread'));
ok($con[GROUP1]   = bind_as(1,'groupID=1,ou=groups','1'));
ok($con[GROUP2]   = bind_as(1,'groupID=2,ou=groups','2'));
ok($con[ANON_TLS] = bind_as(1));
ok($con[ANON]     = bind_as());

foreach (0..LASTCON) {
    BAIL_OUT("Failed to conenct as " . DESCR->[$_]) unless $con[$_];
}

#                                                          S J L R 1 2 T A
# an object that has been canceleld
test_all(\&cancelled,     "Reading cancelled person",      1,1,0,0,0,0,0,0);

# an object in ou=ldap_admin
test_all(\&ldap_admin,    "Reading ldap_admin object",     1,1,1,0,0,0,0,0);

# a person's 'suppression' attribute
test_all(\&suppression,   "Reading suppression attr",      1,1,1,0,0,0,0,0);

# 'jdName' attribute - person and inst
test_all(\&jdname_person,"Reading jdname for person",      1,1,1,0,0,0,0,0);
test_all(\&jdname_inst,  "Reading jdname for inst",        1,1,1,1,1,1,1,1);

# a suppressed atribute
test_all(\&suppression,  "Reading a suppressed attr",      1,1,1,0,0,0,0,0);

# a group's uid attribute
test_all(\&uid_cam,      "Reading group uid, vis=cam",     1,1,1,1,1,1,1,1);
test_all(\&uid_mgr,      "Reading group uid, vis=manager", 1,1,1,0,1,1,0,0);

# a group's allUid attribute
test_all(\&alluid_cam,   "Reading allUid, vis=cam",        1,1,1,1,1,1,0,0);
test_all(\&alluid_mgr,   "Reading allUid, vis=manager",    1,1,1,1,1,1,0,0);

# a 'normal' attrubute
test_all(\&normal,       "Reading a normal attr",          1,1,1,1,1,1,1,1);

# create/modify/delete
test_all(\&genesis,      "Creating a person",              1,1,1,0,0,0,0,0);


# Disconnect cleanly
foreach (0..LASTCON) {
    $con[$_]->unbind;
    $con[$_]->disconnect;
}

