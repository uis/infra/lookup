#!/usr/bin/perl -w

use strict;

# $Id$

use Ucam::Directory::TestServer;

my $server = new Ucam::Directory::TestServer("Ucam-Directory/t/fakedir.ldif");

print "OK, running as ", $server->url, "\n";
print "Type 'quit<return>' to quit\n";
while (<>) {
  last if /quit/;
}
