#!/usr/bin/perl

use strict;
use warnings;

use Net::LDAP;

my $ldap = Net::LDAP->new ( "ldap://mnementh.csi.cam.ac.uk/",
                             onerror => 'die')
    or die "$@";

$ldap->start_tls(verify => 'require',
          cafile => './ca-bundle.crt');

print "Connected, using ", $ldap->cipher, "\n";

$ldap->bind;

my $result = $ldap->search 
    ( base    => "o=University of Cambridge,dc=cam,dc=ac,dc=uk",
      scope   => "sub",
      filter  => "uid=test0001",
 );

my @entries = $result->entries;
print "Got ", scalar(@entries), " results:\n";
foreach my $entry (@entries) {
    print "  dn is " . $entry->dn, "\n";
}

$ldap->unbind;

