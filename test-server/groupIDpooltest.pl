#!/usr/bin/perl

use strict;
use warnings;

use Net::LDAP;
use Net::LDAP::Constant qw(LDAP_NO_SUCH_ATTRIBUTE);
use Net::LDAP::Util;
use Time::HiRes qw(tv_interval gettimeofday);

my $ldap = Net::LDAP->new ( "ldap://mnementh.csi.cam.ac.uk/",
                             )
    or die "$@";

$ldap->start_tls(verify => 'require',
          cafile => './ca-bundle.crt');

$ldap->bind
    ("cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk",
     password => "FOObar");

# -----------------------------

my $t0 = [gettimeofday];

my $groupid;
my $ineration_counter = 0;
LOOP:
{
    my $result = $ldap->search
        ( base    => "cn=groupIDPool,ou=groups,o=University of Cambridge," .
                     "dc=cam,dc=ac,dc=uk",
          scope   => "base",
          filter  => "(objectClass=*)",
     );

    my $entry = $result->pop_entry;
    die "Can't find groupIDPool object" unless $entry;

    my @groupids = $entry->get_value('groupID');
    die "Wrong number of next groupID values" unless scalar(@groupids) == 1;
    ($groupid) = @groupids;

    my $msg = $ldap->modify
        ( $entry->dn,
          changes =>
          [ delete => [ groupID => $groupid   ],
            add    => [ groupID => $groupid+1 ],
       ]
     );

    if ($msg->is_error) {
        print "\n";
        print "Code:         ", $msg->code, "\n";
        print "Error:        ", $msg->error, "\n";
        print "Error name:   ", $msg->error_name, "\n";
        print "Error text:   ", $msg->error_text, "\n";
        print "Error desc:   ", $msg->error_desc, "\n";
        print "Servers error:", $msg->server_error, "\n\n";
        die $msg->error unless $msg->code == LDAP_NO_SUCH_ATTRIBUTE;
        die "Failed to get next groupID - too many iterations"
            if $ineration_counter++ > 10;
        redo LOOP;
    }

};

print tv_interval( $t0,[gettimeofday]), "\n";

# ------------------------------

$ldap->unbind;

print "Next groupID is $groupid\n";
