#!/usr/bin/perl -w

use strict;

#use Test::More tests => 1;
use Test::More 'no_plan';

use URI;
use URI::QueryParam;
use Test::MockObject;

# Data::FormValidator calls UNIVERSAL::can() as a function, preventing
# Test::MockObject from doing it's stuff. Loading UNIVERSAL::can fixes
# this.

use UNIVERSAL::can;
no warnings 'UNIVERSAL::can';
 
our $DEBUG = 0;
our %CONFIG;

# Mock-up an Apache::Request and a Mason Request object
my $r = make_mock_request();
my $m = make_mock_mason();

# Establish the environment
$ENV{REMOTE_USER} = 'jw99';
$CONFIG{'LOOKUPldapserver'} = "ldap.lookup.cam.ac.uk";

# Basic 'can we use it' test
BEGIN { use_ok('Ucam::Lookup::Controller') };

# Test the dispatch process

my %targets =
  ( 'http://mock.invalid/'                    => 'person-search',
    'http://mock.invalid/?type=person'        => 'person-search',
    'http://mock.invalid/?type=person&query=' => 'person-search',
    'http://mock.invalid/?type=inst'          => 'inst-search',

    'http://mock.invalid/person/jw99'          => 'person-profile',
    'http://mock.invalid/person/jw99/profile'  => 'person-profile',
    'http://mock.invalid/person/jw99/identity' => 'person-identity',
    'http://mock.invalid/person/jw99/groups'   => 'person-groups',

    'http://mock.invalid/inst/CS'          => 'inst-profile',
    'http://mock.invalid/inst/CS/profile'  => 'inst-profile',
    'http://mock.invalid/inst/CS/contacts' => 'inst-contacts',
    'http://mock.invalid/inst/CS/members'  => 'inst-members',
  );

SKIP: {

  skip "Tests not yet fixed for new template calling code";
    
  foreach my $target (keys %targets) {
    test_dispatch($target,$targets{$target});
  }

}


sub make_mock_request {
 
  my $r = Test::MockObject->new();
  $r->set_always('log', $r);        # We'll masquarade as our own log object
  $r->set_true('content_type');
  $r->mock('debug', sub { shift; diag("Debug: ",  join('', @_)) if $DEBUG } );
  $r->mock('info',  sub { shift; diag("Info: ",   join('', @_)) if $DEBUG } );
  $r->mock('notice',sub { shift; diag("Notice: ", join('', @_)) if $DEBUG } );
  $r->mock('error', sub { shift; diag("Error: ",  join('', @_)) if $DEBUG } );
  $r->mock('dir_config',sub { return $CONFIG{$_[1]} } );
  
  return $r;
}

sub test_dispatch {
  my ($url, $comp) = @_;
  setup_query($url);
  Ucam::Lookup::Controller::run($m,0);
  is ($m->call_pos(-1), 
      "comp", 
      "'$url': should call comp");
  is (($m->call_args(-1))[1], 
      "$comp.mason", 
      "component for '$url'");
}


sub make_mock_mason {

  my $m = Test::MockObject->new();
  $m->set_true('comp_exists','comp','clear_buffer','abort',);
  $m->set_always('apache_req', $r);

  return $m;
}

sub setup_query {
  my ($url,$prefix) = @_;

  my $uri = URI->new($url);

  my $dhandler_arg = $uri->path;
  $dhandler_arg =~ s{^$prefix}{};
  $dhandler_arg =~ s{^/}{};

  $r->set_always('construct_url',$url);
  $r->mock('param', sub { shift; return $uri->query_param(@_) } );
  $m->set_always('dhandler_arg',$dhandler_arg);

}

