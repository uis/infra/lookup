#!/usr/bin/perl -w

use strict;

use Test::More tests => 6;
#use Test::More 'no_plan';

use constant MAC_IE => "Mozilla/4.0 (compatible; MSIE 5.23; Mac_PowerPC)";
use constant LINUX_FIREFOX => "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) Gecko/20050922 Fedora/1.0.7-1.1.fc3 Firefox/1.0.7";
use constant LINUX_NETSCAPE_4_78 => "Mozilla/4.78 [en] (X11; U; Linux 2.0.27 i586)";
use constant LINUX_NETSCAPE_4_77 => "Mozilla/4.77 [en] (X11; U; Linux 2.0.27 i586)";
use constant PPC_NETSCAPE => "Mozilla/4.08 (Macintosh; U; PPC, Nav)";


BEGIN { use_ok('Ucam::Lookup::Controller') };

ok(Ucam::Lookup::Controller::is_unreliable(MAC_IE),"Mac IE unreliable");
ok(Ucam::Lookup::Controller::is_unreliable(LINUX_NETSCAPE_4_78),"Some Netscape unreliable");
ok(!Ucam::Lookup::Controller::is_unreliable(LINUX_FIREFOX),"Linux Firefox OK");
ok(!Ucam::Lookup::Controller::is_unreliable(LINUX_NETSCAPE_4_77),"Most Linux NEtscape OK");
ok(!Ucam::Lookup::Controller::is_unreliable(PPC_NETSCAPE),"PPC Netscape OK");
