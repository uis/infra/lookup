#!/usr/bin/perl -w

#use Test::More tests => 7;
use Test::More 'no_plan';

use strict;
use charnames ':full';
use Test::MockObject;

BEGIN { use_ok('Ucam::Lookup::Helpers',qw/InReadable priv_name/) };

# InReadable

# There seems to be a problem with using \p{InReadable} with Test::More's 
# like/unlike finctions - use ok instead

ok("X" =~ qr/\p{InReadable}/, "'X' is readable");
ok("\N{LATIN CAPITAL LETTER A WITH GRAVE}" =~ /^\p{InReadable}*$/, 
                         "'{LATIN CAPITAL LETTER A WITH GRAVE}' is readable");
ok("\N{CYRILLIC CAPITAL LETTER IE WITH GRAVE}" !~ /^\p{InReadable}*$/, 
                 "'{CYRILLIC CAPITAL LETTER IE WITH GRAVE}' is not readable");
ok("Foobar" =~ /^\p{InReadable}*$/, "'Foobar' is readable");
ok("Foobar\N{LATIN CAPITAL LETTER A WITH GRAVE}" =~ /^\p{InReadable}*$/, 
                    "Foobar{LATIN CAPITAL LETTER A WITH GRAVE}' is readable");
ok("\N{CYRILLIC CAPITAL LETTER IE WITH GRAVE}" !~ /^\p{InReadable}*$/, 
           "'Foobar{CYRILLIC CAPITAL LETTER IE WITH GRAVE}' is not readable");

# priv_name

my $person = Test::MockObject->new();
$person->set_always('displayName','Fred Clark');
$person->set_always('cn','F.J. Clark');

my ($displayname_suppressed, $cn_suppressed);
$person->mock('is_suppressed', 
	      sub { $_[1] eq 'displayname' ? $displayname_suppressed :
                    $_[1] eq 'cn'          ? $cn_suppressed :
                    "0 but confused" } );

my ($name);
$person->mock('name',sub { $name } );

# neither
($displayname_suppressed, $cn_suppressed) = (0,0);
$name = 'Fred Clark';    ok(!priv_name($person));
$name = 'F.J. Clark';    ok(!priv_name($person));
$name = 'fjc55';         ok(!priv_name($person));

# displayName
($displayname_suppressed, $cn_suppressed) = (1,0);
$name = 'Fred Clark';    ok(priv_name($person));
$name = 'F.J. Clark';    ok(!priv_name($person));
$name = 'fjc55';         ok(!priv_name($person));

# cn
($displayname_suppressed, $cn_suppressed) = (0,1);
$name = 'Fred Clark';    ok(!priv_name($person));
$name = 'F.J. Clark';    ok(priv_name($person));
$name = 'fjc55';         ok(!priv_name($person));

# both
($displayname_suppressed, $cn_suppressed) = (1,1);
$name = 'Fred Clark';    ok(priv_name($person));
$name = 'F.J. Clark';    ok(priv_name($person));
$name = 'fjc55';         ok(!priv_name($person));

