package Ucam::Lookup::Helpers;

use strict;

use vars qw/$VERSION/;
$VERSION = 1.01;

use vars qw(@ISA @EXPORT @EXPORT_OK $VERSION);

require Exporter;
@ISA = qw(Exporter);
@EXPORT    = qw();
@EXPORT_OK = qw(InReadable priv_name);

# InReadable defines a character property representing all Unicode
# characters and symbols whic hare considered 'readable' by the
# directory's audiance. Where names, for example, include characters
# outside this set then other representations of the name mught be
# displayed.

sub InReadable {
  return <<'END';
+utf8::IsASCII
+utf8::InLatin1Supplement
+utf8::Latin
+utf8::Mark
+utf8::Punctuation
+utf8::Symbol
+utf8::Separator
END
}

# priv_name works out if the value we are seeing for $person->name is
# special becasue we have privaledge

sub priv_name {
  my ($person) = @_;

  return (($person->name eq $person->displayName and
	   $person->is_suppressed('displayname')) or 
	  ($person->name eq $person->cn          and 
	   $person->is_suppressed('cn')));
}

1;

