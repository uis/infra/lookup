# $Id$

package Ucam::Lookup::Controller;

# Master controller for the directory lookup service.
#
# The controller's job is to parse an incoming request, perform any
# required actions, and dispatch to an appropriate Mason component to
# render the 'next' page. The Controller populates a number of data
# structures which it passes to the Mason components.

use 5.008005;
use strict;
use warnings;
use English qw/-no_match_vars/;

# must be all on one line or MakeMaker will get confused.
our $VERSION = do { my @r = (q$Revision$ =~ /\d+/g); sprintf "%d."."%03d" x $#r, @r };

use Data::FormValidator;

use Ucam::Directory qw/escape_filter_value/;
use Ucam::Directory::Person;    # For access to supportedattrs
use Ucam::Directory::LabeledPhone;
use Ucam::Directory::LabeledURI;
use Ucam::Directory::Exception qw/general_exception
                                  notfound_exception 
                                  argument_exception 
                                  permission_exception/;

use Data::Dumper;
use Encode;
use List::MoreUtils qw(uniq);
use Net::LDAP::Constant qw (LDAP_SIZELIMIT_EXCEEDED);
use Time::HiRes qw(gettimeofday tv_interval);
use DateTime;
use Apache2::URI;
use Apache2::Log;
use Apache2::Util;
use Apache2::Upload;

use constant DEBUG => 0;

# We should do 'use Apache::Const' or 'use Apache::Constants', but they
# are such a pain across the Apache1/2 divide that I'll just define
# some constants locally.

use constant HTTP_BAD_REQUEST => 400;
use constant HTTP_NOT_FOUND => 404;
use constant HTTP_INTERNAL_SERVER_ERROR => 500;
use constant HTTP_SEE_OTHER => 303;

# Symbolic constants for CGI argument names

use constant ARG_CHARSET => '_charset_';

# Password generation parameters

use constant PWCHARS => 
    'AaBbCcDdEeFfGgHhJjKkMmNPpQqRrSsTtUuVvWwXxYyZz23456789';
use constant PWLEN => 32;

# If logging is enabled (by "PerlSetVar LOOKUPlogPath" in the Apache
# config) then we open that file the first time we are called and
# store it here. Subsequent invocations just use the stored
# value. Restarting/reconfiguring Apache should be sufficient to cause
# subsequent writes to go to a new file

our $fh;

# We save the 'nophoto' and 'noimage' jpegs to save having to keep
# re-reading them

our ($nophoto, $noimage);

# Start time

our $start_time;

# Initialise Perl's prng. This will use /dev/urandom if available and
# time and process-id otherwise on Perl 5.004 and above. Assume that
# theis is good enough for initial password generation

srand();

###

# run() is the entry point for the controller. It basically sets up
# various data items that may be useful in the future and then
# dispatches to an appropriate handle_* routine to actually process the
# result

sub run {

    my ($m) = @_;

    my $r = $m->apache_req;
    my $log = $r->log;

    $start_time = [gettimeofday];
    timestamp($log,$start_time,"Start");

    eval {

        # We expect and require authentication 

        my $viewer = $ENV{REMOTE_USER};
        permission_exception("REMOTE_USER not defined but required")
            unless $viewer;

        # Configure the directory object using info from Apache PerlSetVar
        # directives

        my %dir_conf;
        $dir_conf{viewer} = $viewer;
        $dir_conf{service} = 'lookup';
        $dir_conf{ldapserver} = $r->dir_config('LOOKUPldapserver')
            if defined($r->dir_config('LOOKUPldapserver'));
        $dir_conf{binddn} = $r->dir_config('LOOKUPbinddn')
            if defined($r->dir_config('LOOKUPbinddn'));
        $dir_conf{password} = $r->dir_config('LOOKUPpassword')
            if defined($r->dir_config('LOOKUPpassword'));
        $dir_conf{cafile} = $r->dir_config('LOOKUPcafile')
            if defined($r->dir_config('LOOKUPcafile'));
        $dir_conf{capath} = $r->dir_config('LOOKUPcapath')
            if defined($r->dir_config('LOOKUPcapath'));
        $dir_conf{timeout} = $r->dir_config('LOOKUPtimeout')
            if defined($r->dir_config('LOOKUPtimeout'));

        my $logpath = $r->dir_config('LOOKUPlogFile');
        if (defined($logpath) and !$fh) {
            open($fh,'>>',$logpath) 
                or general_exception("Unable to open logfile '$logpath': $!");
        }
        $dir_conf{log} = $fh if $fh;

        my $dir = new Ucam::Directory(%dir_conf);

        # dhandler_arg is the remaining path after the app's prefix has been
        # removed and omitting the first '/'.
        my $path = $m->dhandler_arg;
        $log->notice("Lookup controller: processing '$path'");

        # $ctx gets used to pass round useful information, both inside this
        # module and from this module to the views. Everything in ctx gets 
        # passed to the views as arguments

        my $ctx = 
            { dir         => $dir,
              log         => $log,
              path        => $path,
              prefix      => get_prefix($m),
              full_prefix => get_fullprefix($m),
              viewer      => $viewer,
              args        => {},
         };

        $log->info("Prefix is: ", get_prefix($m)) if DEBUG;

        # Main dispatch process. See
        #   https://intranet.csi.cam.ac.uk/index.php/Directory_URL_structure
        # for URL structure details

        timestamp($log,$start_time,"Initialised");

        my ($type,$key,$action,$row,$handler);

        # Empty path implies a search
        if ($path eq '') {
            $handler = \&search_HANDLER;
        }

        # 'self' redirects to logged-in person
        elsif ($path =~ m!^self$!) {
            $handler = \&self_redirect_HANDLER;
        }

        # '<type>/<key>/photo' is a photo request (as is
        # 'photo/<type>/<key>' for histerical, backwards-compatibility
        # reasons
        elsif (($path =~ m!^([^/]+)/([^/]+)/photo$!) or
               ($path =~ m!^photo/([^/]+)/([^/]+)$!)) {
            $type = $1;
            $key = $2;
            $handler = \&photo_HANDLER;
        }

        # '<type>/<key>/vcard' is a vcard request 
        elsif ($path =~ m!^([^/]+)/([^/]+)/vcard$!) {
            $type = $1;
            $key = $2;
            $handler = \&vcard_HANDLER;
        }

        # 'person/<key>/suppress' applies or revokes blanket suppression
        # to a person
        elsif ($path =~ m!^person/([^/]+)/suppress$!) {
            $type = 'person';
            $key = $1;
            $handler = \&suppress_HANDLER;
        }

        # 'person/<key>/suppress-group' applies of revokes group
        # membership suppression
        elsif ($path =~ m!^person/([^/]+)/suppress-group$!) {
            $type = 'person';
            $key = $1;
            $handler = \&suppress_group_HANDLER;
        }

        # '<type>/<key>/add-people' adds people to something
        elsif ($path =~ m!^([^/]+)/([^/]+)/add-people$!) {
            $type = $1;
            $key = $2;
            $handler = \&add_people_HANDLER;
        }

        # '<type>/<key>/add-groups' adds groups to something
        elsif ($path =~ m!^([^/]+)/([^/]+)/add-groups$!) {
            $type = $1;
            $key = $2;
            $handler = \&add_groups_HANDLER;
        }

        # '<type>/<key>/new-group' creates a group
        elsif ($path =~ m!^([^/]+)/([^/]+)/new-group$!) {
            $type = $1;
            $key = $2;
            $handler = \&new_group_HANDLER;
        }

        # '<type>/<key>/remove-person' removes a person from something
        elsif ($path =~ m!^([^/]+)/([^/]+)/remove-person$!) {
            $type = $1;
            $key = $2;
            $handler = \&remove_person_HANDLER;
        }

        # '<type>/<key>/remove-group' removes a group from something
        elsif ($path =~ m!^([^/]+)/([^/]+)/remove-group$!) {
            $type = $1;
            $key = $2;
            $handler = \&remove_group_HANDLER;
        }

        # 'inst/<key>/edit-contact/[<row>]' edits or adds a contact row
        elsif ($path =~ m!^inst/([^/]+)/edit-contact(/([^/]+))?$!) {
            $type = 'contact';
            $key = $1;
            $row = $3; 
            $handler = \&edit_contact_HANDLER;
        }

        # 'inst/<key>/delete-contact/<row>' deletes a contact row
        elsif ($path =~ m!^inst/([^/]+)/delete-contact/([^/]+)$!) {
            $type = 'contact';
            $key = $1;
            $row = $2;
            $handler = \&delete_contact_HANDLER;
        }

        # '<type>/<key>/cancel' cancels something
         elsif ($path =~ m!^([^/]+)/([^/]+)/cancel$!) {
            $type = $1;
            $key = $2;
            $handler = \&cancel_HANDLER;
        }

        # '<type>/<key>/edit' is a general edit submission 
        elsif ($path =~ m!^([^/]+)/([^/]+)/edit$!) {
            $type = $1;
            $key = $2;
            $handler = \&edit_HANDLER;
        }

        # '<type>/<key>[/<action>]' is a lookup of some sort. 
        elsif ($path =~ m!^([^/]+)/([^/]+)(/([^/]+))?$!) {
            $type = $1;
            $key = $2;
            $action = $4;
            $handler = \&lookup_HANDLER;
        }

        # Anything else is an error
        unless ($handler) {
            notfound_exception("No handler for $path");
        }

        timestamp($log,$start_time,"Dispatch done");

        # If we have a type and key, go get the data
        my $data;
        if (defined($type) and defined($key)) {

            if ($type eq 'person') {
                notfound_exception("Person keys can't contain upper-case " .
                                   "letters: $key")
                    if $key =~ /[A-Z]/;
                $data = $dir->lookup_person($key);
            } elsif ($type eq 'inst') {
                notfound_exception("InstIDs can't contain lower-case " .
                                   "letters: $key")
                    if $key =~ /[a-z]/;
                $data = $dir->lookup_inst($key);
            } elsif ($type eq 'group') {
                $data = $dir->lookup_group($key);
            } elsif ($type eq 'contact') {
                notfound_exception("InstIDs can't contain lower-case " .
                                   "letters: $key")
                    if $key =~ /[a-z]/;
                my $inst = $dir->lookup_inst($key);

                if (defined($row)) {
                    $data = $inst->contactRow($row);
                } else {
                    $data = $inst->create_contactrow;
                    $ctx->{newcontactrow} = 1;
                }
            } else {
                notfound_exception("Don't know how to find object '$type'");
            }

        }

        timestamp($log,$start_time,"Data retrieved");

        # Further populate $ctx and call the handler

        $ctx->{type}   = $type;
        $ctx->{key}    = $key;
        $ctx->{row}    = $row;
        $ctx->{action} = $action || 'profile';
        $ctx->{data}   = $data;

        &$handler($m,$ctx);

        timestamp($log,$start_time,"Handler run");

        # Someone has to set the content type
        $r->content_type($ctx->{content_type} || "text/html; charset=utf-8");

        # Discourage storage in shared caches
        $r->headers_out->{"Cache-Control"} = "private";

        # If the handler didn't select a view component then assume it has
        # done whatever was necessary itself. Otherwise attempt to
        # retrieve a custom title string from the selected component and
        # then render the master template (the template will insert the
        # selected component at the appropriate place)

        if (defined($ctx->{component})) { 
            my $component = $m->fetch_comp($ctx->{component});
            notfound_exception("Component " . $ctx->{component}. " not found")
                if !$component;
            $ctx->{title} = $component->scall_method('title',%{$ctx})
                if $component->method_exists('title');
            $m->comp('TEMPLATE.mason',%{$ctx});
        }

        timestamp($log,$start_time,"Component processed");

    };

    # Master exception handler

    if ($EVAL_ERROR) {

        timestamp($log,$start_time,"Exception caught");

        # Just propagate the error if it's not one of ours
        die $EVAL_ERROR unless Ucam::Directory::Exception->caught();

        my $e;
        if ( $e = Ucam::Directory::Exception::notfound->caught() ) {
            $log->error("Not found: " . $e->as_string);
            $m->clear_buffer;
            $m->abort(HTTP_NOT_FOUND);
        }
        elsif ( $e = Ucam::Directory::Exception::argument->caught() ) {
            $log->error("CGI argument error: " . $e->as_string);
            $m->clear_buffer;
            $m->abort(HTTP_BAD_REQUEST);
        }
        elsif ( $e = Ucam::Directory::Exception::permission->caught() ) {
            $log->error("Permission error: " . $e->as_string);
            $m->clear_buffer;
            $m->abort(HTTP_INTERNAL_SERVER_ERROR);
        }
        elsif ( $e = Ucam::Directory::Exception::ldap->caught() ) {
            $log->error("LDAP error ", $e->ldap_message->code, ": ", 
                        $e->as_string);
            if ($e->ldap_message and 
                $e->ldap_message->code == LDAP_SIZELIMIT_EXCEEDED) {
                $r->content_type("text/html; charset=utf-8");
                $m->comp('size_limit.mason');
            }
            else {
                $m->clear_buffer;
                $m->abort(HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        else {
            $e = $EVAL_ERROR;
            $log->error("General directory exception: " . $e->as_string);
            $m->clear_buffer;
            $m->abort(HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    timestamp($log,$start_time,"Complete");

}

###

# Handlers are required to set $ctx->{component} to the component that
# they want invoked, and $ctx->{data} to a reference to whatever they
# want to pass to the component as the 'data' argument. They should
# throw an exception, rather than return, if they can't do this.

sub search_HANDLER {

    my ($m,$ctx) = @_;
    my $log  = $ctx->{log};

    # Support 'query' as alias for any of 'pquery','iquery','gquery'
    # for backward compatibility
    my $args =
        args_validate($m,
                      { filters  => ['trim'],
                        optional => ['type','query','pquery','iquery','gquery',
                                     'approx','submit',ARG_CHARSET]
                   } );
    my $charset = ($args->valid(ARG_CHARSET))[0] || 'utf-8';
    my $type = ($args->valid('type'))[0] || 'person';
    $type = fix_charenc($type,$charset,$log);
    my $query = ($args->valid('query'))[0] ||
        ($args->valid('pquery'))[0] ||
            ($args->valid('iquery'))[0] ||
                ($args->valid('gquery'))[0];
    $query = fix_charenc($query,$charset,$log);
    my $approx = ($args->valid('approx'))[0];

    my @data;

    if (defined($query)) {
        if ($type eq 'person') {
            @data = person_search($m,$ctx,$query,$approx);
        } elsif ($type eq 'inst') {
            @data = inst_search($m,$ctx,$query,$approx);
        } elsif ($type eq 'group') {
            @data = group_search($m,$ctx,$query,$approx);
        } else {
            argument_exception("Unrecognised search type: $type");
        }
    }

    my %args;
    foreach my $key ($m->apache_req->param()) {
        $args{$key} = $m->apache_req->param($key);
    }
    $args{pquery} ||= $args{query};
    $args{iquery} ||= $args{query};
    $args{gquery} ||= $args{query};
    $ctx->{args} = \%args;
    $ctx->{data} = \@data;
    $ctx->{component} = "$type-search.mason";

}

###

sub self_redirect_HANDLER {

    my ($m,$ctx) = @_;
    my $viewer  = $ctx->{viewer};

    redirect($m,$ctx,"person/$viewer");

}

###

sub photo_HANDLER {

    my ($m,$ctx) = @_;
    my $log  = $ctx->{log};
    my $data = $ctx->{data};
    my $type = $ctx->{type};

    my $photo = $data->jpegPhoto;

    # Use a default if no photo data is available.

    if (!defined($photo)) {

        if ($type eq 'person') {
            my $fn = $m->apache_req->dir_config('LOOKUPnoPhoto');
            $nophoto = read_photo($fn) unless $nophoto;
            $photo = $nophoto;
        } else {
            my $fn = $m->apache_req->dir_config('LOOKUPnoImage');
            $noimage = read_photo($fn) unless $noimage;
            $photo = $noimage;
        }

    }

    $ctx->{content_type} = "image/jpeg";
    $m->print($photo);

}

###

sub vcard_HANDLER {

    my ($m,$ctx) = @_;
    my $log  = $ctx->{log};
    my $data = $ctx->{data};
    my $type = $ctx->{type};

    # Can only do vcards for objects that can return them
    notfound_exception("Can't create vCard for $type")
        unless $data->can('as_vcard');

    $ctx->{content_type} = "text/x-vcard; charset=utf-8";
    $m->apache_req->headers_out->{"Content-Disposition"} =
        "atachment; filename=" . $data->rdn . ".vcf";

    $m->print($data->as_vcard);
    $m->print("\n");

}

###

sub suppress_HANDLER {

    my ($m,$ctx) = @_;
    my $log    = $ctx->{log};
    my $data   = $ctx->{data};
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};
    my $viewer = $ctx->{viewer};

    # Suppression only exists for people
    notfound_exception("Don't know how to do suppression for $type")
        unless $type eq "person";

    # Check we got either suppress or unsuppress - if we didn't it's
    # most likely that the user got sent to Raven to re-authenticate
    unless ($m->apache_req->param('suppress') or
            $m->apache_req->param('unsuppress')) {
        $log->warn('suppress_HANDLER called without suppress ' .
                   'or unsuppress arg');
        $ctx->{message} = "Suppression or un-suppression failed, probably " .
            "because you had to re-authenticate first. Please try again.";
        $ctx->{component} = 'person-profile.mason';
        return;
    }

    my $suppressing = defined($m->apache_req->param('suppress')); # Shortcut

    # Something is wrong if we can't suppress the right stuff
    if (!$data->is_suppressible) {
        permission_exception("Can't honour request from $viewer to suppress " .
                             "all attributes for $key: nothing suppressible");
    } elsif (!$data->is_editable('suppression')) {
        permission_exception("Can't honour request from $viewer to suppress " .
                             "all attributes for $key: can't edit " .
                             "suppression attribute");
    }

    # Set or unset per-attribute suppression
    foreach my $attr ($data->supportedattrs) {
        next unless $data->is_suppressible(attr => $attr);
        if ($suppressing) {
            $data->suppress(attr => $attr);
        } else {
            $data->unsuppress(attr => $attr);
        }
    }

    # Set or unset the 'suppression' attribute which remembers what the
    # user did last time
    $data->replace(attr => 'suppression',
                   val => ($suppressing ? 'on' : undef));

    # Commit the result - this is where it blows up if we've done
    # something wrong
    $data->commit;

    # ... and then, do much the same to group membership
    foreach my $group ($data->groups) {
        if ($suppressing) {
            $group->suppress_membership($data->uid);
        } else {
            $group->unsuppress_membership($data->uid);
        }
        $group->commit;
    }

    # ... and redirect to the profile
    redirect($m,$ctx,"$type/$key");

}

###

sub suppress_group_HANDLER {

    my ($m,$ctx) = @_;
    my $log    = $ctx->{log};
    my $data   = $ctx->{data};
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};
    my $viewer = $ctx->{viewer};
    my $dir    = $ctx->{dir};

    # Group visibility only be manipulated for groups
    notfound_exception("Can't manipulate group suppression for $type")
        unless $type eq "person";

    # Check we got a groupID and either suppress or unsuppress - if we
    # didn't it's most likely that the user got sent to Raven to
    # re-authenticate
    unless (defined($m->apache_req->param('groupid'))) {
        $log->warn('visibility_HANDLER called without required args');
        $ctx->{message} = "Group visibility change failed, probably " .
            "because you had to re-authenticate first. Please try again.";
        $ctx->{component} = 'person-groups.mason';
        return;
    }

    my $group = $dir->lookup_group($m->apache_req->param('groupid'));
    my $groupid = $group->groupID;
    if (defined($m->apache_req->param("suppress-$groupid"))) {
        $group->suppress_membership($data->uid);
    }
    else {
        $group->unsuppress_membership($data->uid);
    }
    $group->commit;

    # ... and redirect to the groups page
    redirect($m,$ctx,"person/$key/groups");

}

###

sub add_people_HANDLER {

    my ($m,$ctx) = @_;
    my $log    = $ctx->{log};
    my $data   = $ctx->{data};
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};
    my $viewer = $ctx->{viewer};
    my $dir    = $ctx->{dir};

    # Honour the 'cancel' button
    if (defined($m->apache_req->param('cancel'))) {
        redirect($m,$ctx,"$type/$key/members");
    }

    # If there is a 'ids' CGI argument then there is data to process
    if (defined($m->apache_req->param('ids'))) {

        my %errors;

        eval {

            # extract things that might be CRSids from the ids parameter and
            # check that they are valid
            my $ids = $m->apache_req->param('ids');
            my @ids = split(/[^a-zA-Z0-9]+/,$ids);
            @ids = map { lc($ARG) } @ids;
            $log->info("ids: '", join('|',@ids), "'") if DEBUG;
            my @invalid = $dir->check_uids(@ids);

            if (@invalid) {
                $log->info("invalid: '", join('|',@invalid), "'") if DEBUG;
                $errors{'uid'} = "The following IDs don't exist " .
                    "in the directory: " . join(', ',@invalid);
            }

            # If it's a group, life is easy...
            if ($type eq 'group') {
                $data->add_members(@ids);
                $data->commit;
            }

            # Otherwise it's an inst, in which case we find all the people in
            # @ids and add this institution to them. Blame this nutters who
            # designed the standard LDAP objects
            elsif ($type eq 'inst') {
                my $instid = $data->instID;
                foreach my $id (@ids) {
                    my $person = $dir->lookup_person($id);
                    my @replacement = uniq $person->instID, $instid;
                    $log->info("replacement is '",
                               join('|',@replacement), "'") if DEBUG;
                    $log->info("instID  was '",
                               join('|',$person->instID), "'") if DEBUG;
                    $person->replace(attr => 'instID', val => \@replacement);
                    $log->info("instID  is '", join('|',$person->instID), "'")
                        if DEBUG;
                    $log->info(Dumper(\$person)) if DEBUG;
                    $person->commit;
                }
            }

            # Otherwise it's something we don't understand
            else {
                notfound_exception
                    ("Don't know how to do 'add_person' for $type")
            }

        };
        if ($EVAL_ERROR) {
            if ( my $e = Ucam::Directory::Exception->caught() ) {
                $log->error($e->as_string . "\n" . $e->trace);
                $errors{'uid'} = $EVAL_ERROR->as_string;
                delete($errors{'alluid'});
            } else {
                die $EVAL_ERROR;
            }
        }

        # If there were validation errors, populate %args and redisplay
        # the edit form
        if (%errors) {
            $log->info("There were errors") if DEBUG;
            my %args;
            foreach my $key ($m->apache_req->param()) {
                $args{$key} = $m->apache_req->param($key);
            }
            ### pro tem log what we got
            if (DEBUG) {
                $log->info("Regenerated values:");
                foreach my $key (sort keys %args) {
                    $log->info("  $key: " . $args{$key});
                }
            }
            $ctx->{args}      = \%args;
            $ctx->{errors}    = \%errors;
        }
        # Otherwise redirect to the relevant members page
        else {
            redirect($m,$ctx,"$type/$key/members");
        }

    }

    # However we got here (if we do and are not redirected), display the
    # appropriate form
    $ctx->{component} = "$type-add-people.mason";

}

###

sub add_groups_HANDLER {

    my ($m,$ctx) = @_;
    my $log    = $ctx->{log};
    my $data   = $ctx->{data};
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};
    my $viewer = $ctx->{viewer};
    my $dir    = $ctx->{dir};

    # Honour the 'cancel' button
    if (defined($m->apache_req->param('cancel'))) {
        redirect($m,$ctx,"$type/$key/groups");
    }

    # If there is a 'ids' CGI argument then there is data to process
    if (defined($m->apache_req->param('ids'))) {

        my %errors;

        eval {

            # extract things that might be groupids from the ids parameter and
            # check that they are valid
            my $ids = $m->apache_req->param('ids');
            $log->info("ids: '$ids'") if DEBUG;
            my @ids = split(/[^0-9]+/,$ids);
            $log->info("ids: '", join('|',@ids), "'") if DEBUG;
            my @invalid = $dir->check_groupids(@ids);

            if (@invalid) {
                $log->info("invalid: '", join('|',@invalid), "'") if DEBUG;
                $errors{'uid'} = "The following group IDs don't exist " .
                    "in the directory: " . join(', ',@invalid);
            }

            # If type is inst then we append the new values to groupID
            if ($type eq 'inst') {
                my @replacement = uniq $data->groupID, @ids;
                $data->replace(attr => 'groupID', val => \@replacement);
                $data->commit;
            }

            # Otherwise it's something we don't understand
            else {
                notfound_exception
                    ("Don't know how to do 'add_group' for $type")
            }

        };
        if ($EVAL_ERROR) {
            if ( my $e = Ucam::Directory::Exception->caught() ) {
                $log->error($e->as_string . "\n" . $e->trace);
                $errors{'ids'} = $EVAL_ERROR->as_string;
            } else {
                die $EVAL_ERROR;
            }
        }

        # If there were validation errors, populate %args and redisplay
        # the edit form
        if (%errors) {
            $log->info("There were errors") if DEBUG;
            my %args;
            foreach my $key ($m->apache_req->param()) {
                $args{$key} = $m->apache_req->param($key);
            }
            ### pro tem log what we got
            if (DEBUG) {
                $log->info("Regenerated values:");
                foreach my $key (sort keys %args) {
                    $log->info("  $key: " . $args{$key});
                }
            }
            $ctx->{args}      = \%args;
            $ctx->{errors}    = \%errors;
        }
        # Otherwise redirect to the relevant groups page
        else {
            redirect($m,$ctx,"$type/$key/groups");
        }

    }

    # However we got here (if we do and are not redirected), display the
    # appropriate form
    $ctx->{component} = "$type-add-groups.mason";

}

###

sub new_group_HANDLER {

    my ($m,$ctx) = @_;
    my $log    = $ctx->{log};
    my $data   = $ctx->{data};
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};
    my $viewer = $ctx->{viewer};
    my $dir    = $ctx->{dir};

    # We only do insts
    notfound_exception("Don't know how to do 'add_group' for '$type'")
        unless $type eq 'inst';

    # ... and we only do it for people who can edit that inst
    permission_exception("$viewer trying to create a group for "
                         . $data->instID) unless $data->is_editable;

    # If not redirected, we'll invoke group-edit.mason and come back here
    $ctx->{submitto} = "$type/$key/new-group";
    $ctx->{component} = "group-edit.mason";

    # Initial request?
    if (defined($m->apache_req->param('new'))) {

        # setup editing data
        my %args;
        my $ctr = 0;
        foreach my $val ($data->get(attr => 'mgrgroupid')) {
            $args{"mgrgroupid_${ctr}"} = $val;
            ++$ctr;
        }
        $args{mgrgroupid_count} = $ctr;
        $args{visibility_0} = 'managers';
        $args{visibility_count} = 1;
        $args{expirytimestamp_0} = DateTime->now->add(years=>1);
        $args{expirytimestamp_count} = 1;

        # Create a temporary group to make the form work - need at
        # least to assign some relavent managers
        my $group = $dir->create_group;
        my @mgrs = $data->get(attr => 'mgrgroupid');
        $group->replace(attr=>'mgrgroupid',val=>@mgrs, internal=>1);

        $ctx->{args}   = \%args;
        $ctx->{data}   = $group;
        $ctx->{type}   = 'group';
        $ctx->{errors} = [];

    }

    # Otherwise it's a submition
    else {

        my $group = $dir->create_group;
        # Bootstrap access with a group we must be in....
        my @mgrs = $data->get(attr => 'mgrgroupid');
        $group->replace(attr=>'mgrgroupid',val=>@mgrs, internal=>1);
        $ctx->{data} = $group;
        if (generic_edit_complete($m,$ctx)) {
            if ($group->groupID) {
                # If it worked, add group to inst and redirect to group
                my @groups = uniq $data->groupID, $group->groupID;
                $data->replace(attr=>'groupid', val=>\@groups);
                $data->commit;
                redirect($m,$ctx,"group/" . $group->groupID);
            }
            else {
                # Else redirect to inst groups page
                redirect($m,$ctx,"inst/$key/groups");
            }
        }
        $ctx->{type}   = 'group';

    }

}

###

sub remove_person_HANDLER {

    my ($m,$ctx) = @_;
    my $log    = $ctx->{log};
    my $data   = $ctx->{data};
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};
    my $viewer = $ctx->{viewer};
    my $dir    = $ctx->{dir};

    # There must be a 'uid' argument
    my $uid = $m->apache_req->param('uid');
    argument_exception("remove_person called with no 'uid' argument")
        unless $uid;

    # If it's a group it's easy...
    if ($type eq 'group') {
        $data->remove_members($uid);
        $data->commit;
    }

    # Otherwise it's an inst, in which case we find the person
    # represented by $uid and remove this instID from them. Blame this
    # nutters... (etc., see above). If $uid doesn't exist we die ('cos
    # that 'shouldn't happen')
    elsif ($type eq 'inst') {
        my $instid = $data->instID;
        my $person = $dir->lookup_person($uid);
        my @replacement = grep { $ARG ne $instid } $person->instID;
        $log->info("replacement is '", join('|',@replacement), "'") if DEBUG;
        $person->replace(attr => 'instID', val => \@replacement);
        $person->commit;
    }

    # Otherwise it's something we don't understand
    else { 
        notfound_exception("Don't know how to do 'remove_person' for $type")
    }

    # Otherwise commit and redirect to the relevant members page
    $log->info("Committing...") if DEBUG;
    $data->commit;
    redirect($m,$ctx,"$type/$key/members");

}

###

sub remove_group_HANDLER {

    my ($m,$ctx) = @_;
    my $log    = $ctx->{log};
    my $data   = $ctx->{data};
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};
    my $viewer = $ctx->{viewer};
    my $dir    = $ctx->{dir};

    # There must be a 'groupID' argument
    my $groupid = $m->apache_req->param('groupID');
    argument_exception("remove_group called with no 'groupID' argument")
        unless $groupid;

    # Currently we only do insts, in which case we just drop the
    # groupid from the existing list
    if ($type eq 'inst') {
        my @replacement = grep { $ARG ne $groupid } $data->groupID;
        $log->info("replacement is '", join('|',@replacement), "'") if DEBUG;
        $data->replace(attr => 'groupID', val => \@replacement);
        $data->commit;
    }

    # Otherwise it's something we don't understand
    else {
        notfound_exception("Don't know how to do 'remove_group' for $type")
    }

    # Otherwise redirect to the relevant groups page
    redirect($m,$ctx,"$type/$key/groups");

}

###

sub edit_contact_HANDLER {

    my ($m,$ctx) = @_;
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};

    # We only do contacts
    notfound_exception("Don't know how to do 'edit_contact' for '$type'")
        unless $type eq 'contact';

    $ctx->{component} = "inst-edit-contact.mason";
    if (generic_edit_complete($m,$ctx)) {
        redirect($m,$ctx,"inst/$key/contacts");
    }

}

###

sub delete_contact_HANDLER {

    my ($m,$ctx) = @_;
    my $data   = $ctx->{data};
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};

    # We only do contacts
    notfound_exception("Don't know how to do 'edit_contact' for '$type'")
        unless $type eq 'contact';

    # Delete it
    $data->delete;

    # And go somewhere useful
    redirect($m,$ctx,"inst/$key/contacts");

}

###

sub cancel_HANDLER {

    my ($m,$ctx) = @_;
    my $data   = $ctx->{data};
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};

    # We only do groups
    notfound_exception("Don't know how to do 'cancel' for '$type'")
        unless $type eq 'group';

    if ($m->apache_req->param('abort')) {
        redirect ($m,$ctx,"group/$key");
    }

    if ($m->apache_req->param('confirm')) {
        $data->cancel;
        redirect($m,$ctx,"?type=group");
    }

    $ctx->{component} = "group-confirm-cancel.mason";

}

###

sub edit_HANDLER {

    my ($m,$ctx) = @_;
    my $type   = $ctx->{type};
    my $key    = $ctx->{key};

    $ctx->{component} = "$type-edit.mason";
    if (generic_edit_complete($m,$ctx)) {
        redirect($m,$ctx,"$type/$key");
    }

}

###

sub lookup_HANDLER {

    my ($m,$ctx) = @_;
    my $log    = $ctx->{log};
    my $type   = $ctx->{type};
    my $action = $ctx->{action};
    my $data   = $ctx->{data};

    $log->info("In handle_lookup, data = $data = ", $ctx->{data}) if DEBUG;

    $ctx->{component} = "$type-$action.mason";

}

###

sub person_search {

    my ($m,$ctx,$query,$approx) = @_;

    my $log    = $ctx->{log};

    $log->notice("person_search - looking for '$query'");

    my @query = map { lc } split (/\s+/,$query);
    my $n_words = scalar(@query);
    my %s;
    my @r = ();

    if ($n_words == 1) {
        if ($query[0] =~ /([a-z]+)(\d+)/) {
            $log->notice("One word with digits - treat as CRS ID") if DEBUG;
            # look for uid eq term and (if approx) uid is alpha part of term
            # followed by other digits
            my $stem = $1;
            do_search($ctx,\@r,\%s,'person',"(uid=$query[0])",undef);
            my $extra = sub {$ARG[0]->uid =~ /^$stem\d/};
            do_search($ctx,\@r,\%s,'person',"(uid=$stem*)",$extra) 
                if $approx;
        }
        else {
            $log->notice("One word without digits") if DEBUG;
            # look for uid eq term, sn eq term, sn matches term (if approx),
            # word in displayName eq term, uid starts with term (if approx)
            do_search($ctx,\@r,\%s,'person',"(uid=$query[0])",undef);
            do_search($ctx,\@r,\%s,'person',"(sn=$query[0])",undef,"sn");
            do_search($ctx,\@r,\%s,'person',"(sn~=$query[0])",undef,"sn") 
                if $approx;
            my $extra = sub {$ARG[0]->displayName =~ /\b$query[0]\b/i};
            do_search($ctx,\@r,\%s,'person',"(displayName=*$query[0]*)",$extra,
                     "displayName");
            do_search($ctx,\@r,\%s,'person',"(uid=$query[0]*)",undef) 
                if $approx;
        }
    } elsif ($n_words > 1) {
        $log->notice("Two or more words") if DEBUG;
        # look for sn eq all terms, displayName eq all terms
        my $all = join(' ', @query);
        do_search($ctx,\@r,\%s,'person',"(sn=$all)",undef,"sn");
        do_search($ctx,\@r,\%s,'person',"(displayName=$all)",
                  undef,"displayName");

        if ($approx) {
            # look for sn eq final term and displayName matches all
            # other terms, displayName matches final term and
            # displayName matches all other terms, sn matches final
            # term
            my $last = pop @query;
            my $filter = "";
            foreach my $arg (@query) {
                $filter .= "(displayName~=$arg)";
            }
            do_search($ctx,\@r,\%s,'person',"(&$filter(sn=$last))",undef,
                     "sn","displayName");
            do_search($ctx,\@r,\%s,'person',"(&$filter(displayName~=$last))",
                      undef,"displayName");
            do_search($ctx,\@r,\%s,'person',"(sn~=$last)",undef,"sn");
        }
    }
    return @r;

}

###

sub inst_search {

    my ($m,$ctx,$query,$approx) = @_;

    my $log    = $ctx->{log};

    $log->notice("inst_search - looking for '$query'") if DEBUG;

    my @query = map { lc } split (/\s+/,$query);
    my $n_words = scalar(@query);
    my %s;
    my @r = ();

    my @stopwords = 
        ( 'a', 
          'in',
          'for',
          'and', 
          'of', 
          'the',
     );
    my %stopwords = map { $ARG => $ARG } @stopwords;

    if ($n_words == 1) {
        my $acro = $query[0];
        $acro =~ s/\.//g;
        if ($query[0] =~ /([a-z]+)\d+/) {
            $log->notice("One word with digits") if DEBUG;
            # look for instID eq term, acronym eq term less full
            # stops, and (if approx) instID is alpha part of term
            # followed by other digits
            my $stem = $1;
            do_search($ctx,\@r,\%s,'inst',"(instID=$query[0])",undef);
            do_search($ctx,\@r,\%s,'inst',"(acronym=$acro)",undef);
            my $extra = sub {$ARG[0]->instID =~ /^$stem\d/};
            do_search($ctx,\@r,\%s,'inst',"(instID=$stem*)",$extra) if $approx;
        } else {
            $log->info("One word without digits") if DEBUG;
            # look for instID eq term, word in ou eq term, acronym eq
            # term less full stops, ou matches term (if approx)
            do_search($ctx,\@r,\%s,'inst',"(instID=$query[0])",undef);
            my $extra = sub {$ARG[0]->ou =~ /\b$query[0]\b/i};
            do_search($ctx,\@r,\%s,'inst',"(ou=*$query[0]*)",$extra);
            do_search($ctx,\@r,\%s,'inst',"(acronym=$acro)",undef);
            do_search($ctx,\@r,\%s,'inst',"(ou~=$query[0])",undef) if $approx;;
        }
    } elsif ($n_words > 1) {
        $log->info("Two or more words") if DEBUG;

        # look for ou eq all terms
        my $all = join(' ', @query);
        do_search($ctx,\@r,\%s,'inst',"(ou=$all)",undef);

        # look for ou contains each term as a word
        my $filter = "(&";
        foreach my $arg (@query) {
            $filter .= "(ou=*$arg*)";
        }
        $filter .= ")";
        my $extra = sub {
            grep { return 0 unless $ARG[0]->ou =~ /\b$_\b/i } @query;
            return 1
        };
        do_search($ctx,\@r,\%s,'inst',$filter,$extra);

        if ($approx) {
            # look for ou matches each term as a word, except stopwords
            $filter = "(&";
            foreach my $arg (@query) {
                next if $stopwords{$arg} || $arg =~ /\d\d\d\d/;
                $filter .= "(ou~=$arg)";
            }
            $filter .= ")";
            do_search($ctx,\@r,\%s,'inst',$filter,undef);
        }
    }

    return @r;

}

###

sub group_search {

    my ($m,$ctx,$query,$approx) = @_;

    my $log    = $ctx->{log};

    $log->notice("group_search - looking for '$query'") if DEBUG;

    my @query = map { lc } split (/\s+/,$query);
    my $n_words = scalar(@query);
    my %s;
    my @r = ();

    if ($n_words == 1) {
        if ($query[0] =~ /\d/) {
            $log->notice("All digits") if DEBUG;
            # Look for groupID eq term
            do_search($ctx,\@r,\%s,'group',"(groupID=$query[0])",undef);
        } else {
            $log->info("One word without digits") if DEBUG;
            # look for word in groupTitle eq term, groupTitle matches
            # term (if approx)
            my $extra = sub {$ARG[0]->groupTitle =~ /\b$query[0]\b/i};
            do_search($ctx,\@r,\%s,'group',"(groupTitle=*$query[0]*)",$extra);
            do_search($ctx,\@r,\%s,'group',"(groupTitle~=$query[0])",undef)
                if $approx;
        }
    } elsif ($n_words > 1) {
        $log->info("Two or more words") if DEBUG;

        # look for groupTitle eq all terms
        my $all = join(' ', @query);
        do_search($ctx,\@r,\%s,'group',"(groupTitle=$all)",undef);

        # look for groupTitle contains each term as a word
        my $filter = "(&";
        foreach my $arg (@query) {
            $filter .= "(groupTitle=*$arg*)";
        }
        $filter .= ")";
        my $extra = sub {
            grep { return 0 unless $ARG[0]->groupTitle =~ /\b$_\b/i } @query;
            return 1
        };
        do_search($ctx,\@r,\%s,'group',$filter,$extra);

        if ($approx) {
            # look for groupTitle matches each terms
            $filter = "(&";
            foreach my $arg (@query) {
                $filter .= "(groupTitle~=$arg)";
            }
            $filter .= ")";
            do_search($ctx,\@r,\%s,'group',$filter,undef);
        }
    }

    return @r;

}

###

sub do_search {

    my ($ctx,$results,$seen,$what,$ldap_filter,$perl_filter,@depends) = @_;

    my $dir    = $ctx->{dir};
    my $log    = $ctx->{log};

    my (@entries,$key);
    if ($what eq 'person') {
        @entries = $dir->search_for_people($ldap_filter);
        $key = 'uid';
    }
    elsif ($what eq 'inst') {
        @entries = $dir->search_for_insts($ldap_filter);
        $key = 'instID';
    }
    elsif ($what eq 'group') {
        @entries = $dir->search_for_groups($ldap_filter);
        $key = 'groupID';
    }

  ENTRY:
    foreach my $entry (@entries) {
        next ENTRY if $seen->{$entry->get(attr => $key)};
        next ENTRY if $perl_filter and !&$perl_filter($entry);
        foreach my $attr (@depends) {
            next ENTRY unless $entry->is_visible($attr);
        }
        $seen->{$entry->get(attr => $key)} = '1';
        push(@{$results},$entry);
    }
}

###

sub generic_edit_complete {

    my ($m,$ctx) = @_;
    my $log    = $ctx->{log};
    my $viewer = $ctx->{viewer};
    my $data   = $ctx->{data};

    # Just claim completion on cancel
    return 1 if defined($m->apache_req->param('cancel'));

    # Decline to edit if the browser is known dangerous
    if (is_unreliable($m->apache_req->headers_in->{'User-Agent'})) {
        $ctx->{component} = 'unreliable_browser.mason';
        return 0;
    }

    # If there is a set-pwd CGI argument then generate a new
    # userPassword and re-display the form
    if (defined($m->apache_req->param('set-pwd'))) {

        $log->info("set-pwd request") if DEBUG;
        my %args;

        ### pro tem log what we got
        my $args = args_validate
            ($m, 
             { optional => ['submit', 'pref_mail'],
               optional_regexp => qr/.*/,
          });
        if (DEBUG) {
            $log->info("Validated values:");
            foreach my $parm (sort $args->valid) {
                $log->info("  $parm: " . $args->valid($parm));
            }
        }

        # Copy through existing params
        my $charset = $m->apache_req->param(ARG_CHARSET);
        foreach my $key ($m->apache_req->param()) {
            $args{$key} = 
                fix_charenc(scalar($m->apache_req->param($key)),
                            $charset,$log);
        }

        $args{userpassword_0} = generate_password();
        $args{userpassword_count} = 1;

        ### pro tem log what we got
        if (DEBUG) {
            $log->info("Regenerated values:");
            foreach my $key (sort keys %args) {
                $log->info("  $key: " . $args{$key});
            }
        }

        # Setup ctx - component must be set by whatever calls generic edit
        $ctx->{errors}    = {};
        $ctx->{args}      = \%args;
        return 0;

    }

    # If there is a 'submit' CGI argument then it's a form submission
    # and we need to apply the edits requested, if we can. If we can,
    # we do so and return complete. If not, we populate
    # $ctx->{errors} with explanations and $ctx->{args} with the
    # original args and return fail
    if (defined($m->apache_req->param('submit'))) {

        $log->info("Processing edit submission") if DEBUG;

        ### Build a list of acceptable arguments and check them
        ###my @regex;
        ###foreach my $attr ($data->supportedattrs) {
        ###  my $attrlc = lc($attr);
        ###  push @regex, "${attrlc}_sup", "${attrlc}_\\d+",
        ###    "${attrlc}_\\d+_del"
        ###	unless $attrlc eq 'mail' or $attrlc eq 'mailAlternate';
        ###}
        ###push @regex, "allmail_sup", "allmail_\\d+", "allmail_\\d+_del";
        ###my $regex = '(' . join(')|(',@regex) . ')';

        my $args = args_validate
            ($m, 
             { optional => ['submit', 'pref_mail'],
               optional_regexp => qr/.*/,
          });

        ### pro tem log what we got
        if (DEBUG) {
            $log->info("Validated values:");
            foreach my $parm (sort $args->valid) {
                $log->info("  $parm: " . $args->valid($parm));
            }
        }

        my %errors = ();

        # Extract data from the CGI parameters
        my (%aa, %add);
        foreach my $param ($args->valid) {
            my $value = 
                fix_charenc(scalar($args->valid($param)),
                            scalar($args->valid(ARG_CHARSET)),
                            $log);
            if ($param =~ /^(.*?)_new_(\d+)$/) {
                $add{$1}->[$2]->{val} = $value;
            } elsif ($param =~ /^(.*?)_new_cmt_(\d+)$/) {
                $add{$1}->[$2]->{cmt} = $value;
            } elsif ($param =~ /^(.*)_(\d+)$/) {
                $aa{$1}->{new}->[$2]->{val} = $value;
            } elsif ($param =~ /^(.*)_(\d+)_cmt$/) {
                $aa{$1}->{new}->[$2]->{cmt} = $value;
            } elsif ($param =~ /^(.*)_(\d+)_orig$/) {
                $aa{$1}->{orig}->[$2] = $value;
            } elsif ($param =~ /^(.*)_sup$/) {
                $aa{$1}->{sup} = $value;
            } elsif ($param =~ /^(.*)_(\d+)_del$/) {
                $aa{$1}->{del}->[$2] = 1;
            }
        }

        if (DEBUG) {
            $log->info("Un-munged values");
            $log->info(Dumper(\%aa,\%add));
        }

        # Apply assorted fixups
        my %allkeys = map { $ARG => 1 } keys %aa, keys %add, 'allmail';
        foreach my $key (keys %allkeys) {

            # Ensure {orig}, {new} and are at least defined (if empty)
            $aa{$key}->{orig}    = [] unless $aa{$key}->{orig};
            $aa{$key}->{new}     = [] unless $aa{$key}->{new};

            # merge in add(itional) data onto the end of new
            if ($add{$key}) {
                push(@{$aa{$key}->{new}},@{$add{$key}});
            }

            # blank any deleted values
            foreach my $i (0..scalar(@{$aa{$key}->{new}})) {
                if (defined($aa{$key}->{del}->[$i])) {
                    $aa{$key}->{new}->[$i] = { val => '', cmt => ''};
                }
            }

        }

        # Fixup allmail
        my $pref_mail = $args->valid('pref_mail');
        if ($pref_mail and $pref_mail > 0 and defined($aa{'allmail'}->{new})) {
            $log->info("Preferred mail processing: want address $pref_mail")
                if DEBUG;
            my $new_pref = $aa{'allmail'}->{new}->[$pref_mail];
            if ($new_pref and defined($new_pref->{val})) {
                for (my $i = $pref_mail; $i > 0; --$i) {
                    $aa{'allmail'}->{new}->[$i] = $aa{'allmail'}->{new}->[$i-1]
                }
                $aa{'allmail'}->{new}->[0] = $new_pref;
            }
        }

        # Remove any {new} lines that have blank values
        foreach my $key (keys %aa) {
            my @old = @{$aa{$key}->{new}};
            my @new;
            foreach my $value (@old) {
                push(@new,$value) if $value->{val} !~ /^\s*$/;
            }
            $aa{$key}->{new} = \@new;
        }

        # Construct full new values for attributes with comments
        if (defined($aa{labeleduri})) {
            foreach my $i (0..scalar(@{$aa{labeleduri}->{new}})-1) {
                $log->info("Considering labeleduri $i") if DEBUG;
                my $val = new Ucam::Directory::LabeledURI
                    (label => $aa{labeleduri}->{new}->[$i]->{cmt},
                     uri   => $aa{labeleduri}->{new}->[$i]->{val});
                $log->info("  constructed $val") if DEBUG;
                $aa{labeleduri}->{new}->[$i]->{val} = "$val";
            }
        }
        if (defined($aa{telephonenumber})) {
            foreach my $i (0..scalar(@{$aa{telephonenumber}->{new}})-1) {
                $log->info("Considering telephonenumber $i") if DEBUG;
                $log->info("  ", $aa{telephonenumber}->{new}->[$i]->{cmt},
                           "!!", $aa{telephonenumber}->{new}->[$i]->{val})
                    if DEBUG;
                eval {
                    my $val = new Ucam::Directory::LabeledPhone
                        (label => $aa{telephonenumber}->{new}->[$i]->{cmt},
                         phone => $aa{telephonenumber}->{new}->[$i]->{val});
                    $log->info("  constructed '$val'") if DEBUG;
                    $aa{telephonenumber}->{new}->[$i]->{val} = "$val";
                };
                if ($EVAL_ERROR) {
                    # Just propagate the error if we didn't expect it
                    die $EVAL_ERROR unless
                        Ucam::Directory::Exception::invalidattrval->caught();
                    $errors{telephonenumber} = $EVAL_ERROR->as_string;
                }
            }
        }
        ### Hm, gratuitous duplication here...
        if (defined($aa{facsimiletelephonenumber})) {
            foreach my $i
                (0..scalar(@{$aa{facsimiletelephonenumber}->{new}})-1) {
                if (DEBUG) {
                    $log->info("Considering facsimiletelephonenumber $i");
                    $log->info("  ",
                             $aa{facsimiletelephonenumber}->{new}->[$i]->{cmt},
                             "!!",
                             $aa{facsimiletelephonenumber}->{new}->[$i]->{val})
                }
                eval {
                    my $val = new Ucam::Directory::LabeledPhone
                        (label
                         => $aa{facsimiletelephonenumber}->{new}->[$i]->{cmt},
                         phone
                         => $aa{facsimiletelephonenumber}->{new}->[$i]->{val});
                    $log->info("  constructed '$val'") if DEBUG;
                    $aa{facsimiletelephonenumber}->{new}->[$i]->{val} = "$val";
                };
                if ($EVAL_ERROR) {
                    # Just propagate the error if we didn't expect it
                    die $EVAL_ERROR unless
                        Ucam::Directory::Exception::invalidattrval->caught();
                    $errors{facsimiletelephonenumber} = $EVAL_ERROR->as_string;
                }
            }
        }

        # Move ...{new}->[*]->{val} to {new}->[*]
        foreach my $key (keys %aa) {
            foreach my $i (0..scalar(@{$aa{$key}->{new}})-1) {
                $aa{$key}->{new}->[$i] = $aa{$key}->{new}->[$i]->{val};
            }
        }

        if (DEBUG) {
            $log->info("Munged values");
            $log->info(Dumper(\%aa));
        }

        foreach my $attr ($data->supportedattrs) {
            my $attrlc = lc($attr);

            $log->info("### checking $attr") if DEBUG;

            # No point on proceeding if we don't know something new about
            # this attribute, unless it's required
            if (!$aa{$attrlc}) {
                $errors{$attrlc} = "Requires a value"
                    if $data->is_required($attrlc);
                next;
            }

            # Suppression
            $log->info("  Testing $attr for suppression") if DEBUG;
            if (defined($aa{$attrlc}->{sup})) {
                $log->info("  $attr suppression defined: " .
                           $aa{$attrlc}->{sup}) if DEBUG;

                # Complain if the user can't suppress this
                permission_exception("$viewer trying to (un)suppress $attr")
                    unless $data->is_suppressible(attr => $attr);

                if ($aa{$attrlc}->{sup} eq 'on') {
                    $data->suppress(attr => $attr);
                } elsif ($aa{$attrlc}->{sup} eq 'off') {
                    $data->unsuppress(attr => $attr);
                } else {
                    argument_exception("Invalid suppression value " .
                                       "for $attr: '" .
                                       $aa{$attrlc}->{sup} . "'")
                }

            }

            # We'll do photos later
            next if $attrlc eq 'jpegphoto';
            next if $attrlc eq 'collationorder';

            # Get the current view of the data from the directory
            my @current = $data->get(attr => $attr);
            if (DEBUG) {
                $log->info("     orig ", join('!',@{$aa{$attrlc}->{orig}}));
                $log->info("      new ", join('!',@{$aa{$attrlc}->{new}}));
                $log->info("  current ", join('!',@current));
            }

            # No values to change unless the new and old values
            # differ, and at least one new value is not blank (to
            # allow for new records)
            next if list_eq($aa{$attrlc}->{orig}, $aa{$attrlc}->{new});

            $log->info("  OK, looks like $attr changed") if DEBUG;

            # Shouldn't get here unless we are allowed to edit this attribute
            permission_exception("$viewer trying to edit $attr")
                unless $data->is_editable(attr => $attr);

            # Detect and log clashing updates but otherwise plough on
            $log->warn("Clashing update - data in the directory has changed")
                if !list_eq($aa{$attrlc}->{orig},\@current);

            # Validate and if OK, update
            my $msg = $data->validate(attr => $attr,
                                      val => $aa{$attrlc}->{new});
            if ($msg) {
                $errors{$attrlc} = $msg;
            } else {
                $log->info("Replacing $attr, values '",
                           join('|',@{$aa{$attrlc}->{new}}), "'") if DEBUG;
                $data->replace(attr => $attr, val => $aa{$attrlc}->{new});
                $log->info("$attr is now '",
                           join('|',$data->get(attr => $attr)), "'") if DEBUG;
            }

        }                       # end foreach

        # Photograph

        $log->info("Upload params: ", join('!',$m->apache_req->upload()))
            if DEBUG;
        # Photos anyone?
        if (defined($aa{'jpegphoto'}->{del}->[0])) {
            $log->info("Deleting photo") if DEBUG;
            $data->replace(attr => "jpegPhoto", val => undef);
        } else {
            my $photo = $m->apache_req->upload('jpegphoto_0');
            if (defined($photo) and $photo->size > 0) {
                $log->info("Got a photo, type ", $photo->type, " size ",
                           $photo->size) if DEBUG;
                $log->info("  photo filename: ", $photo->tempname) if DEBUG;
                if ($photo->size > 1024 * 1024 * 2) {
                    $errors{'jpegphoto'} =
                        "Photographs must be less than 2MB in size. " .
                            "The file you have selected seems to be " .
                                "larger than this.";
                } else {
                    my $photodata;
                    {
                        local $INPUT_RECORD_SEPARATOR = undef;
                        my $fh = $photo->fh;
                        $photodata = <$fh>;
                    }
                    ;
                    $log->info("Actual length of photodata: ",
                               length($photodata)) if DEBUG;
                    my $msg = $data->validate(attr => "jpegPhoto",
                                              val => $photodata);
                    if ($msg) {
                        $errors{jpegphoto} = $msg;
                    } else {
                        $log->info("Replacing jpegPhoto") if DEBUG;
                        $data->replace(attr => "jpegPhoto", val => $photodata);
                    }
                }
            }
        }

        # Collation order ('co' in what follows). collationorder_0 is the
        # cn of the row currently occupying the position to which we want
        # to move this one. Rows need to be shuffled to make space for
        # any change. If we are dealing with a new row then we are only
        # called if a position other than last was selected (becasue last
        # is represented as '' and if collationorder_0 is '' it's not
        # included in args. The latter gets fixed up elsewhere

        if (defined($aa{'collationorder'})) {
            my $new     = $aa{'collationorder'}->{new}->[0];
            my $orig    = $aa{'collationorder'}->{orig}->[0];
            my $current = $data->get(attr => 'cn');

            $log->info("Collation: new/orig/current: $new/$orig/$current")
                if DEBUG;

            if ($new ne $orig) {

                $log->info("  OK, looks like collation order changed")
                    if DEBUG;

                # Shouldn't get here unless we are allowed to edit
                # this attribute
                permission_exception("$viewer trying to edit collationorder")
                    unless $data->is_editable(attr => 'collationorder');

                # Detect and log clashing updates but otherwise plough on
                $log->info("Clashing update - data in the directory " .
                           "has changed")
                    if $orig ne $current;

                my @row = $data->inst->contactRow;
                my $nrows = scalar(@row);

                # Find the start and end position of the block we have to move
                my ($from,$to);
                foreach my $i (0..$nrows-1) {
                    $log->info(" row $i - cn: ", $row[$i]->cn, " co: ",
                               $row[$i]->collationOrder) if DEBUG;
                    $from = $i if $row[$i]->cn eq $data->cn;
                    $to   = $i if $row[$i]->cn eq $new;
                }
                $log->info("  from/to: $from/$to") if DEBUG;
                argument_exception("Failed to find new collate position")
                    unless defined($to);

                # Remember the co we are trying to get to
                my $to_co = $row[$to]->collationOrder;

                # If we have $from move all the rows between $from and $to
                # 'towards' our current position to create space in our new
                # position
                if (defined($from)) {

                    if ($from < $to) {
                        for (my $i = $to; $i > $from; --$i) {
                            $log->info("Setting co for ", 
                                       $row[$i]->rowTitle, 
                                       " to what was co of ",
                                       $row[$i-1]->rowTitle,
                                       " (",
                                       $row[$i-1]->collationOrder, 
                                       ")") if DEBUG;
                            $row[$i]->replace(attr => 'collationorder', 
                                              val
                                              => $row[$i-1]->collationOrder);
                            $row[$i]->commit;
                        }
                    } else {
                        for (my $i = $to; $i < $from; ++$i) {
                            $log->info("Setting co for ", 
                                       $row[$i]->rowTitle, 
                                       " to what was co of ",
                                       $row[$i+1]->rowTitle,
                                       " (",
                                       $row[$i+1]->collationOrder, ")")
                                if DEBUG;
                            $row[$i]->replace(attr => 'collationorder', 
                                              val
                                              => $row[$i+1]->collationOrder);
                            $row[$i]->commit;
                        }
                    }

                }
                # Without $from we are inserting a new record at $to. Move all
                # the others out of the way
                else {

                    for (my $i = $to; $i < $nrows; ++$i) {
                        $log->info("Incrementing co for ", 
                                   $row[$i]->rowTitle) if DEBUG;
                        $row[$i]->replace(attr => 'collationorder', 
                                          val => $row[$i]->collationOrder+1);
                        $row[$i]->commit;
                    }

                }

                # Update our co (this gets committed in a moment)
                $log->info("Setting our co to $to_co") if DEBUG;
                $data->replace(attr => 'collationorder', val => $to_co);

                # Clear the contactRow cache to avoid a circular reference
                $data->inst->clear_contactrow_cache;
            }

        }

        # For a contact row, actually set co if it is currently '' -
        # implying both that this is a new record and that we want it at
        # the end
        if ($data->can('collationOrder') and !defined($data->collationOrder)) {
            my @row = $data->inst->contactRow;
            my $pos = @row ? $row[-1]->collationOrder+1 : 1;
            $data->replace(attr => 'collationorder', val => $pos);
            $log->info("Forced co to be ", $pos) if DEBUG;
            # Clear the contactRow cache to avoid a circular reference
            $data->inst->clear_contactrow_cache;
        }

        # If there were validation errors, populate %args and redisplay
        # the edit form
        if (%errors) {
            $log->info("There were errors") if DEBUG;
            my %args;
            my $charset = $m->apache_req->param(ARG_CHARSET);
            foreach my $key ($m->apache_req->param()) {
                $args{$key} = 
                    fix_charenc(scalar($m->apache_req->param($key)),
                                $charset,$log);
            }
            ### pro tem log what we got
            if (DEBUG) {
                $log->info("Regenerated values:");
                foreach my $key (sort keys %args) {
                    $log->info("  $key: " . $args{$key});
                }
            }
            # Setup ctx - component must be set by whatever calls generic edit
            $ctx->{args}      = \%args;
            $ctx->{errors}    = \%errors;

            return 0;

        }

        # Otherwise commit and return complete
        $log->info("Committing...") if DEBUG;
        $data->commit;
        return 1;
    }

    # Otherwise then this must be an initial edit form request so we
    # need to populate $ctx->{args} from $data.

    $log->info("Initial edit") if DEBUG;

    # If we are creating a new contact row then $data will be an
    # as-yet uncommitted Ucam::ContactRow object. Unfortunatly it will
    # have a proto-cn which may not be unique and which, if not, will
    # cause problems for the collationOrder shuffling algorithm. So
    # remove it. With prejudice.
    if ($ctx->{newcontactrow}) {
        $log->info("Clearing cn on new contact row") if DEBUG;
        $data->replace(attr => "cn", val => undef, internal=>1);
    }

    my ($ctr, %args);
    foreach my $attr ($data->supportedattrs) {
        my $attrlc = lc($attr);

        # setup editing data and a remembered copy 
        $ctr = 0;
        foreach my $val ($data->get(attr => $attr)) {
            if ($attrlc eq 'labeleduri') {
                $args{"${attrlc}_${ctr}"} = $val->uri;
                $args{"${attrlc}_${ctr}_cmt"} = $val->label || '';
                $args{"${attrlc}_${ctr}_orig"} = $val;
            } elsif ($attrlc eq 'telephonenumber' or 
                     $attrlc eq 'facsimiletelephonenumber' ) {
                $args{"${attrlc}_${ctr}"} = $val->phone;
                $args{"${attrlc}_${ctr}_cmt"} = $val->label || '';
                $args{"${attrlc}_${ctr}_orig"} = $val;
            } elsif ($attrlc eq 'collationorder') {
                if (defined($data->collationOrder)) {
                    $args{"${attrlc}_${ctr}"} = $data->cn;
                    $args{"${attrlc}_${ctr}_orig"} = $data->cn;
                }
            } elsif ($attrlc eq 'rowstyle') {
                foreach my $style ($data->rowStyle) {
                    if ($style eq 'bold') {
                        $args{"${attrlc}_0"} = 'bold';
                        $args{"${attrlc}_0_orig"} = 'bold';
                    } elsif ($style eq 'italic') {
                        $args{"${attrlc}_1"} = 'italic';
                        $args{"${attrlc}_1_orig"} = 'italic';
                    }
                }
            } elsif ($attrlc eq 'userpassword') {
                # Indicate that we had one, but not what it is
                if (defined($data->userPassword)) {
                    $args{"${attrlc}_${ctr}"} = '??';
                    $args{"${attrlc}_${ctr}_orig"} = '??';
                }
            } elsif ($attrlc ne 'jpegphoto') { # skip photo data
                $args{"${attrlc}_${ctr}"} = $val;
                $args{"${attrlc}_${ctr}_orig"} = $val;
            }
            ++$ctr;
        }
        $args{"${attrlc}_count"} = $ctr;

        # suppression status
        if ($data->is_suppressible(attr => $attr)) {
            if ($data->is_suppressed(attr => $attr)) {
                $args{"${attrlc}_sup"} = 'on';
            } else {
                $args{"${attrlc}_sup"} = 'off';
            }
        }

    }                       # end foreach $attr

    $args{"pref_mail"} = 0;

    if (DEBUG) {
        $log->info("Initial form data:");
        foreach my $key (sort keys %args) {
            $log->info("  $key: $args{$key}");
        }
    }

    # Setup ctx - component must be set by whatever calls generic_edit
    $ctx->{args} = \%args;
    $ctx->{errors} = {};

    return 0;

}

###

# Validate the submitted args against a Data::FormValidator profile
# Missing or unexpected arguments throw errors, dealing with invalid
# ones is left for later

sub args_validate {

    my ($m,$profile) = @_;

    # Detect repeating arguments, since we are not using them (yet?)
    foreach my $key ($m->apache_req->param()) {
        my @args = $m->apache_req->param($key);
        argument_exception("Unexpected repeating argument: $key")
            if @args > 1;
    }

    my $result = Data::FormValidator->check($m->apache_req, $profile);

    argument_exception(error => "Missing required argument(s): " .
                       join(" ",$result->missing),
                       args => $result) if $result->has_missing;
    argument_exception(error => "Unexpected argument(s): " .
                       join(" ",$result->unknown),
                       args => $result) if $result->has_unknown;

    return $result;

}

###

sub get_fullprefix {

    my ($m) = @_;
    my $r = $m->apache_req;
    my $path = $m->dhandler_arg;

    # (re)-construct our full URL
    my $full_prefix = $r->construct_url;
    # Strip query string if we got it
    $full_prefix =~ s|\?.*||;
    # Crush multiple '/' to one since Mason seems to do this to dhandler_arg
    $full_prefix =~ s|(?<!:)/+|/|g;
    # Canonicalise on no trailing slash.
    $full_prefix =~ s|/$||;
    $path =~ s|/$||;

    $full_prefix =~ s|/$path$||;
    return $full_prefix;

}

###

sub get_prefix {

    my ($m) = @_;

    my $prefix = get_fullprefix($m);
    $prefix =~ s{^.*?://.*?(/|$)}{$1};
    return $prefix;
}

###

sub redirect {

    my ($m,$ctx,$target) = @_;
    my ($full_prefix) =  $ctx->{full_prefix};
    my $r = $m->apache_req;

    $r->headers_out->{Location} =  "$full_prefix/$target";
    $r->content_type("text/html");
    $m->clear_buffer;
    $m->abort(HTTP_SEE_OTHER);

}

###

# Test if two arrays have the same elements in the same order

sub list_eq {

    my ($a,$b) = @_;

    # They are different if the lengths differ.
    return 0 unless scalar(@{$a}) == scalar(@{$b});

    # They are different if any of the elements differ. Force
    # strigification since some elements may be objects - let's hope
    # they stringify OK
    for (my $i = 0; $i < scalar(@{$a}); ++$i) {
        return 0 if "$a->[$i]" ne "$b->[$i]";
    }

    # Otherwise they are the same
    return 1;

}

###

# Coerce $bytes into a text string, using information from $charset as
# a hint about what character encoding is in use

sub fix_charenc {

    my ($bytes,$charset,$log) = @_;
    my $result;

    $log->info("Decoding $bytes using $charset") if DEBUG;

    # character encoding defaults to utf-8 'cos it's a good guess
    $charset ||= 'utf-8';

    # Try to decode using $charset. This will die if $charset is not
    # recognised. Malformed characters will be replaces by a
    # substitution character
    eval {
        no warnings 'utf8';
        $result = decode($charset,$bytes);
    };

    # As a fallback, decode using ascii to remove everything that we
    # don't understand. This shouldn't be able to fail!!
    if ($EVAL_ERROR) {
        $log->warn("Unrecognised character encoding '$charset' ".
                   "- falling back to 'ascii'");
        $result = decode('ascii',$bytes);
        $result =~ s/\x{fffd}/?/g;
    }

    $log->info("Got result '$result'") if DEBUG;
    return $result;

}

###

sub read_photo {

    my ($fn) = @_;
    general_exception("No default image file") unless $fn;
    open(my $img, $fn) or
        general_exception("Failed to open default image '$fn': $!");
    my $photo;
    {
        local $INPUT_RECORD_SEPARATOR = undef;
        $photo = <$img>;
    }
    ;
    close($img);
    return $photo;

}

###

sub timestamp {

    my ($log,$start_time,$msg) = @_;

    $log->notice(sprintf("TS - %s: %6.3fs", $msg, tv_interval($start_time)));

}

###

# Tests if the browser in use _appears_ to be one that is unreliable 
# in its handling of extended characters when editing

sub is_unreliable {

    my ($agent) = @_;

    return 
        ($agent =~ m{Mozilla/4\.0 \(compatible; MSIE \d+\.\d+; Mac_PowerPC\)}
         or
         $agent =~ m{Mozilla/4\.78 \[en\] \(X11; U; Linux});

}

###

sub generate_password {

    my $pw;
    for (1..PWLEN) {
        $pw .= substr(PWCHARS,int(rand(length(PWCHARS))),1);
    }

    return $pw;

}

1;
__END__

=head1 NAME

Ucam::Lookup - master controller logic for the directory web interface

=head1 SYNOPSIS

  use Ucam::Lookup::Controler;
  Ucam::Lookup::Controler->run($m)

=head1 DESCRIPTION

Wind it up and let it run.

Takes a single argument, the Mason request object for the current request.

=head2 EXPORT

None by default.

=head1 AUTHOR

Jon Warbrick, E<lt>jw35@cam.ac.ukE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2005 by the University of Cambridge Computing Service

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.5 or,
at your option, any later version of Perl 5 you may have available.

=cut

# Local Variables:
# mode:cperl
# cperl-close-paren-offset: -4
# cperl-continued-statement-offset: 4
# cperl-indent-level: 4
# cperl-indent-parens-as-block: t
# cperl-tab-always-indent: t
# End:
