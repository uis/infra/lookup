# $Id$

package Ucam::Lookup::Handler;

# A simple Apache handler for use in place of
# HTML::Mason::ApacheHandler so that we can impose an upload file size
# limit.

# See http://www.masonhq.com/?LimitUploadFileSize

use strict;
use warnings;

use Apache2::Request;

use base 'HTML::Mason::ApacheHandler';

sub prepare_request {
  my $self = shift;
  my $r     = shift;
  my @args;

  push @args, POST_MAX => 1024 * 1024 * 50; # 50Mb backstop

  Apache2::Request->new( $r, @args );

  return $self->SUPER::prepare_request( $r, @_ );

}

1;

