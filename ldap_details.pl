#!/usr/bin/perl -w

use strict;

use constant HOST => "ldap.lookup.cam.ac.uk";

use Net::LDAP;

my $ldap = Net::LDAP->new(HOST) or die $@;

my $mesg = $ldap->bind;
die $mesg->error if $mesg->is_error;

my $schema = $ldap->schema;

my %syntax_desc;
print "Syntax:\n\n";

my @syntaxes = $schema->all_syntaxes;
foreach (sort by_name @syntaxes) {
  print $_->{name}, " ", $_->{desc}, "\n";
  $syntax_desc{$_->{name}} = $_->{desc};
}

print "\n";

foreach (qw/camAcUkPerson camAcUkOrganization camAcUkOrganizationalUnit 
	 contactRow camAcUkGroup/) {

  my @must = $schema->must($_);
  my @may = $schema->may($_);

  print "$_, must:\n\n";
  foreach (sort by_name @must) {
    describe_atribute($_);
  }
  print "\n";

  print "$_, may:\n\n";
  foreach (sort by_name @may) {
    describe_atribute($_);
  }

}

sub by_name {
  lc($a->{name}) cmp lc($b->{name});
}
  
sub describe_atribute {

  my ($attr) = @_;

  print "  ", $attr->{name}, "\n";
  print "    Syntax: ", syntax($attr), "\n";

  print "\n"

}

sub syntax {
  
  my ($attr) = @_;

  return "" unless $attr;

  if ($attr->{syntax}) {
    return $syntax_desc{$attr->{syntax}} 
      ? $syntax_desc{$attr->{syntax}} 
      : $attr->{syntax};
  }
  elsif ($attr->{sup} and $attr->{sup}[0]) {
    return syntax($schema->attribute($attr->{sup}[0]));
  }
	
}
