# $Id$

package Ucam::Directory::Person;

use 5.008001;
use strict;
use warnings;
use Encode;

use Net::LDAP::Util qw/escape_filter_value/;
use Unicode::Collate;
use MIME::QuotedPrint ();
use Ucam::Directory::Entry ':all';
use Ucam::Directory::Exception
    qw/general_exception invalidattrval_exception permission_exception/;
use Exporter;

our $VERSION = '0.01';

our @ISA = qw(Ucam::Directory::Entry Exporter);
our @EXPORT_OK = qw(map_title);

sub ldap_base_dn { Ucam::Directory::ldap_people_dn() };
use constant ldap_filter => '(objectClass=inetOrgPerson)';

# Attributes we'll fetch if asked nicely
use constant supportedattrs =>
# Officially-supported attributes
    (qw(instID mailAlternative displayName jpegPhoto uid labeledURI
	mail ou postalAddress telephoneNumber sn cn suppression
	allmail cancelled unreal title jdName jdInst jdCamMail jdExDir jdColl
        misAffiliation),
# camAcUkPerson attributes not officially supported
     qw(CamSISID libraryCardNumber universityCardNumber universityCardPhoto USN
       suppressAttribute
       lastUpdated));
use constant supattrhash => {map((lc($_) => 1), supportedattrs)};

use constant attrdeschash =>
    { instid          => "Institution",
      displayname     => "Display name",
      jpegphoto       => "Photograph",
      uid             => "CRSid",
      labeleduri      => "Web page",
      allmail         => "Email address",
      postaladdress   => "Address",
      telephonenumber => "Phone number",
      sn              => "Surname",
      cn              => "Registered name",
      group           => "Group",
      suppression     => "Default suppression",
      ou              => "Institution",
      title           => "R\x{00f4}le",
      jdname          => "UCS registered name",
      jdinst          => "UCS registered institution",
      jdcoll          => "UCS registered college",
      misaffiliation  => "MIS status",
#
      insts           => "Institution",
      jdinsts         => "UCS registered institution",
      jdcolls         => "UCS registered college",
};

# Attributes to fetch by default
use constant fastattrs =>
    grep(!/^(jpegPhoto|universityCardPhoto)$/, supportedattrs);

# Attributes that can't be suppressed
our @nosuppress = qw(uid instID jdInst ou suppression misAffiliation);
our %nosuppress = map((lc($_) => 1), @nosuppress);

for my $field (supportedattrs) {
    my $slot = $field;
    no strict "refs";
    *$field = sub {
	my $self = shift;
	return $self->get(attr => $slot);
    };
}

use constant relations =>
{
    insts =>   [ class=>"Ucam::Directory::Institution",
	         myattr=>"instID", theirattr=>"instID" ],
    jdinsts => [ class=>"Ucam::Directory::Institution",
	         myattr=>"jdInst", theirattr=>"instID" ],
    jdcolls => [ class=>"Ucam::Directory::Institution",
	         myattr=>"jdColl", theirattr=>"instID" ],
};

for my $field (keys %{relations()}) {
    my $slot = $field;
    no strict "refs";
    *$field = sub {
	my $self = shift;
	return $self->relation(@{$self->relations->{$slot}});
    };
}

# Returns the person's groups as visible to viewer
sub groups {
    my $self = shift;

    my $dir = $self->{dir};
    my @raw = $dir->search_for_entries(class => "Ucam::Directory::Group",
                                       filter => "(allUid=".$self->uid.")");
    my @cooked = grep { $_->has_member($self->uid) } @raw;

    return @cooked;
}

###

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);
    return $self;
}

sub _fillup_attrs {
    my $self = shift;
    $self->SUPER::_fillup_attrs(@_);
}    

sub is_suppressed {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    my $attr = lc $args{attr};

    if ($attr eq 'allmail') {
	return $self->is_suppressed('mail') &&
	    $self->is_suppressed('mailAlternative');
    }
    return grep(lc($_) eq $attr,
		$self->get(attr=>'suppressAttribute', internal=>1)) &&
		    !$nosuppress{$attr};
}

sub is_visible {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    my $attr = lc $args{attr};

    # If we can edit the object, we can see everything.  Otherwise,
    # suppressed attributes and suppression-controlling attributes are
    # invisible.
    return (!$self->is_suppressed($attr) &&
	$attr ne 'suppression' && $attr ne 'suppressAttribute') ||
	$self->is_editable;
    
}

# Convenience methods

# Name suitable for display purposes
sub name {
    my $self = shift;
    if (defined($self->displayName)) {
	return $self->displayName;
    } elsif (defined($self->cn)) {
	return $self->cn;
    } else {
	return $self->uid;
    }
}

# Useful oddities
use constant title_map => {
    'Dr'    => 'Dr',
    'Prof'  => 'Prof.',

    'Canon' => 'Canon',
    'Rabbi' => 'Rabbi',
    'Rev'   => 'Rev.',
    'Revd'  => 'Revd',

    'Dame'  => 'Dame',
    'Sir'   => 'Sir',
    'Lady'  => 'Lady',
    'Lord'  => 'Lord',
    'Judge' => 'Judge',
  };

my $title_re = qr/@{[join('|', keys(%{&title_map}))]}/;

sub map_title {
    return title_map->{$_[0]};
}

sub as_vcard {
    my $self = shift;

    my @card;

    push @card, "BEGIN:VCARD";
    push @card, "VERSION:2.1";

    # FN is required in V3, if not here
    push @card, "FN;CHARSET=UTF-8:" . $self->name;

    # Attempt to build N from name. Use sn as surname component if at
    # the end of name (causes e.g. De Mulder to come out right)
    my $name = $self->name;
    my $surname = $self->sn;
    my @rest;
    if ($name =~ /(.*?)\s+$surname$/) {
        @rest = split(' ',$1);
    }
    else {
        @rest = split(' ',$name);
        $surname = pop(@rest);
    }

    if (scalar(@rest) == 0) {
        push @card, "N;CHARSET=UTF-8:" . vc_escape($surname);
    }
    elsif (scalar(@rest) == 1) {
        push @card, "N:" . vc_escape($surname) . ';' .
                           vc_escape($rest[0]);
    }
    elsif (scalar(@rest) == 2) {
        push @card, "N;CHARSET=UTF-8:" . vc_escape($surname) . ';' .
                           vc_escape($rest[0])  . ';' .
                           vc_escape($rest[1]);
    }
    else {
        push @card, "N;CHARSET=UTF-8:" . vc_escape($surname) . ';' .
                           vc_escape(shift(@rest))  . ';' .
                           vc_escape(join(' ',@rest));
    }

    foreach my $addr ($self->postalAddress) {
        $addr =~ s/\n/\r\n/g;
        push @card, "LABEL;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:" .
            MIME::QuotedPrint::encode($addr,"");
    }

    foreach my $tel ($self->telephoneNumber) {
        push @card, "TEL:" . expand_phone($tel->phone);
    }

    push @card, "EMAIL;INTERNET;PREF:" . $self->mail
        if $self->mail;
    foreach my $addr ($self->mailAlternative) {
        push @card, "EMAIL;INTERNET:" . $addr;
    }

    push @card, "TITLE;CHARSET=UTF-8:" . $self->title if $self->title;

    push @card, "ORG;CHARSET=UTF-8:" . join(';','University of Cambridge', $self->ou);

    foreach my $uri ($self->labeledURI) {
        push @card, "URL:" . $uri->uri;
    }

    # Both LastUpdated itself, and its data component, may be missing..
    if ($self->lastUpdated and $self->lastUpdated->date) {
        push @card, "REV:" . $self->lastUpdated->date . "Z";
    }

    push @card, "END:VCARD";

    return join("\n",@card);

}

#
# Sorting functions
#

sub _preproc_byname {
    my ($p, $regonly) = @_;
    my $sn = $p->sn;
    $sn or ($sn) = $p->uid =~ /(\w)\d*$/;
    my $notsn = ($regonly ? 0 : $p->displayName) || $p->cn;
    if ($notsn) {
	$notsn =~ s/\s+$sn$//;
	$notsn =~ s/^(($title_re)[.\s]+)+//o;
    } else {
	($notsn) = $p->uid =~ /^(\w+)\w\d*$/;
	$notsn =~ s/(.)/$1./g;
    }
    my $key = "$sn\t$notsn";
    $key =~ s/\bMc(?=[[:upper:]])/Mac/g;
    $key =~ s/\bSt\.?(?= )/Saint/g;
    return $key
}

my $byname;
sub byname {
    return $byname ||
	($byname = new Unicode::Collate(table => 'ucamdirkeys.txt',
					variable => 'non-ignorable',
				  preprocess => sub {_preproc_byname(@_,0)}));
}

my $byregname;
sub byregname {
    return $byregname ||
	($byregname = new Unicode::Collate(table => 'ucamdirkeys.txt',
					   variable => 'non-ignorable',
				   preprocess => sub {_preproc_byname(@_,1)}));
}

sub _preproc_byuid {
    my $p = shift;
    return $p->uid;
}

my $byuid;
sub byuid {
    return $byuid ||
	($byuid = new Unicode::Collate(table => 'ucamdirkeys.txt',
				       preprocess => \&_preproc_byuid));
}


#
# Editing functions
#

# Attributes that users can edit
use constant editattrs => qw(mail mailAlternative displayName telephoneNumber
			     jpegPhoto postalAddress labeledURI
			     suppression suppressAttribute allmail title);
use constant edattrhash => {map((lc($_) => 1), editattrs)};

use constant mvattrs => qw(instID mailAlternative telephoneNumber
			   postalAddress labeledURI suppressAttribute
			   ou allmail title misAffiliation);
use constant mvattrhash => {map((lc($_) => 1), mvattrs)};

sub is_suppressible {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;

    if ($self->is_editable('suppressAttribute')) {
	return exists($args{attr}) ?
	    $self->supattrhash->{lc($args{attr})} &&
	    !$nosuppress{lc($args{attr})} : 1;
    } else {
	return 0;
    }
}


use constant suppress_equivalents => {
    mail => ['mailAlternative'],
};

sub suppress {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    my $attr = $args{attr};
    if ($attr eq 'allmail') {
	$self->suppress('mail');
    } else {
	return if $self->is_suppressed($attr);
	my @suppressed =
	    grep($self->is_suppressible($_), $self->suppressAttribute);
	push(@suppressed, $attr);
	$self->replace(attr => "suppressAttribute", val => \@suppressed);
	if (exists(suppress_equivalents->{$attr})) {
	    foreach my $subattr (@{suppress_equivalents->{$attr}}) {
		$self->suppress($subattr);
	    }
	}
    }
}

sub unsuppress {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    my $attr = $args{attr};
    if ($attr eq 'allmail') {
	$self->unsuppress('mail');
    } else {
	return unless $self->is_suppressed($attr);
	my @suppressed =
	    grep($self->is_suppressible($_), $self->suppressAttribute);
	@suppressed = grep(lc($_) ne lc($attr), @suppressed);
	$self->replace(attr => "suppressAttribute", val => \@suppressed);
	if (exists(suppress_equivalents->{$attr})) {
	    foreach my $subattr (@{suppress_equivalents->{$attr}}) {
		$self->unsuppress($subattr);
	    }
	}
    }
}

sub is_editable {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;

    if (!exists($self->{dir}) || $self->{dir}->{godmode}) {
	return 1;
    } elsif (!exists($self->{dir}->{viewer})) {
	return 0;
    } elsif (exists($args{attr}) && lc($args{attr}) eq 'instid') {
	return 1; # validate() handles the complexity
    } elsif ($self->{dir}->{viewer} eq $self->uid) {
 	return exists($args{attr}) ? $self->edattrhash->{lc($args{attr})} : 1;
    } elsif (grep($_->is_editable, $self->insts) ||
	     $self->{dir}->viewer_in_group_zero) {
	if (exists($args{attr}) &&
	    (lc $args{attr} eq 'suppression' ||
	     lc $args{attr} eq 'suppressattribute')) {
	    return 0;
	} else {
	    return exists($args{attr}) ? $self->edattrhash->{lc($args{attr})} : 1;
	}
    } else {
	return 0;
    }
}

sub validate {
    my $self = shift;
    my %args = @_;
    my $attr = lc $args{attr};
    my @val = ref($args{val}) ? @{$args{val}} : ($args{val});
    my %val;
    my @failed;

    if ($attr eq 'instid') {
	eval {
	    # The only operations allowed are to add or delete institutions
	    # that the viewer is allowed to edit.
	    my %added = map(($_=>1), @val);
	    map(delete $added{$_}, $self->instID);
	    my %deleted = map(($_=>1), $self->instID);
	    map(delete $deleted{$_}, @val);
	    my %changed = (%added, %deleted);
	    @failed = grep(!$self->{dir}->lookup_inst($_)->is_editable,
			      keys %changed);
	};
	$@ && return $@;
	if (@failed > 1) {
	    return "You may not manage the following institutions: " .
		join(' ', @failed);
	} elsif (@failed) {
	    return "You may not manage the institution " .
		join(' ', @failed);
	}
    }

    return $self->SUPER::validate(@_);
}

# replace_hook returns true if replace() should give up (e.g. because the attribute
# isn't real)
sub replace_hook {
    my $self = shift;
    my %args = @_;
    my $attr = lc($args{attr});
    my $value = $args{value};
    my @value = @{$value};

    if ($attr eq 'instid') {
	if (exists($self->{dir})) {
	    $self->replace(attr => 'ou',
		       val => [map($_->ou, grep(!defined($_->cancelled),
						$self->insts))],
		       internal => 1);
	}
    }
    return 0;
}

sub logattrs {
    my $self = shift;
    return (type => 'person', uid => $self->uid);
}

sub rdn {
    my $self = shift;
    return $self->uid;
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Ucam::Directory::Person - Perl module representing a person

=head1 SYNOPSIS

  use Ucam::Directory::Person;
  $person = new Ucam::Directory::Person;

=head1 DESCRIPTION

See L<https://intranet.csi.cam.ac.uk/index.php/Ucam::Directory::Person> for now.

=head2 EXPORT

None by default.



=head1 SEE ALSO

=head1 AUTHOR

B.J. Harris, E<lt>bjh21@cam.ac.ukE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2005 by B.J. Harris

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
