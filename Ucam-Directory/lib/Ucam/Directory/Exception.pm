package Ucam::Directory::Exception;

use 5.008001;
use strict;
use warnings;

our $VERSION = '0.01';

# Thank you HTML::Mason::Exceptions for much of how this works

my %e;

BEGIN {
  
  %e = (
	 # Generic
	
	 'Ucam::Directory::Exception' =>
	 { description => 'Generic base class for Directory exceptions',
	   alias => 'general_exception',
	 },
	
	 # Create specific exception classes like this
	 #
	 # 'Ucam::directory::Exception::proto' =>
         # { isa => 'Ucam::WebAuth::Exception',
         #   description => 'Protocol exception',
         #   fields => 'extra',
         #   alias => 'proto_exception',
         # },

	 'Ucam::Directory::Exception::notfound' =>
         { isa => 'Ucam::Directory::Exception',
	   description => 'Not found exception',
	   alias => 'notfound_exception',
	 },

	 'Ucam::Directory::Exception::argument' =>
	 { isa => 'Ucam::Directory::Exception',
	   description => 'Argument exception',
	   fields => 'args',
	   alias => 'argument_exception',
	 },
	
	 'Ucam::Directory::Exception::permission' =>
	 { isa => 'Ucam::Directory::Exception',
	   description => 'Permission denied',
	   fields => 'args',
	   alias => 'permission_exception',
	 },
	
	 'Ucam::Directory::Exception::invalidattrval' =>
	 { isa => 'Ucam::Directory::Exception',
	   description => 'Attribute value failed validation',
	   alias => 'invalidattrval_exception',
	 },
	
	 'Ucam::Directory::Exception::ldap' =>
	 { isa => 'Ucam::Directory::Exception',
	   description => 'Attribute value failed validation',
	   fields => 'ldap_message', 
	   alias => 'ldap_exception',
	 },
	
       );
  
}

use Exception::Class(%e);

# This import() method allows this:
#  use Ucam::WebAuth::Exception('error1', 'error2', ...);
# ...
#  error1 "something went wrong";

sub import {

  my ($class, @args) = @_;

  my $caller = caller;
  foreach my $name (@args) {
    no strict 'refs';
    die "Unknown exception abbreviation '$name'" unless defined &{$name};
    *{"${caller}::$name"} = \&{$name};
  }
	
}

1;
__END__

=head1 NAME

  Ucam::Directory::Exception - Exception class for Ucam::Directory

=head1 SYNOPSIS

  use Ucam::Directory::Exception qw/general_exception/;
  general_exception("It's all gone horribly wrong");

and then, in a module far, far away

  eval {
    ...
  }
  if ($@) {
    if (UNIVERSAL::isa($@,'Ucam::Directory::Exception') {
      log($@->as_string . "\n" . $@->trace);
    }
  }

=head1 DESCRIPTION

Exception class for Ucam::Directory.

=head1 EXPORT

Currently

=over

=item general_exception

The base exception from which all others inherit. Should probably only
be thrown if no other exception makes sense.

=item notfound_exception

Appropriate for reporting that the requested resource could not be
found.

=item argument_exception

Appropriate for reporting an error in the CGI arguments supplied in the
request. Supports an additional field, args, that can be used to carry
a Data::FormValidator::Result object for further reporting.

==back

Further exports can be added for more specialised exceptions.

=head1 AUTHOR

Jon Warbrick, University of Cambridge Computing Service, E<lt>jw35@cam.ac.ukE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2005 University of Cambridge Computing Service.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself. 

=cut
