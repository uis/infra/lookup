# $Id$

package Ucam::Directory::ContactRow;

use 5.008001;
use strict;
use warnings;
use Encode;

use Net::LDAP::Util qw/escape_filter_value/;
use Ucam::Directory::Entry ':all';
use Ucam::Directory::Exception qw/general_exception/;

our $VERSION = '0.01';

our @ISA = qw(Ucam::Directory::Entry);

use constant supportedattrs => qw(cn rowTitle collationOrder instID labeledURI
 lastUpdated mail ou rowStyle telephoneNumber uid postalAddress);
use constant supattrhash => {map((lc($_) => 1), supportedattrs)};

use constant attrdeschash =>
    { rowtitle        => "Description",
      collationorder  => "Position",
      instid          => "Institution",
      labeleduri      => "Web page",
      mail            => "Email address",
      rowstyle        => "Row style",
      telephonenumber => "Phone number",
      uid             => "CRSid",
      postaladdress   => "Address",
#
      people          => "Associated person",
};

for my $field (supportedattrs) {
    my $slot = $field;
    no strict "refs";
    *$field = sub {
	my $self = shift;
	return $self->get(attr => $slot);
    };
}

use constant relations =>
{
    people => [ class=>"Ucam::Directory::Person",
		myattr=>"uid", theirattr=>"uid"],
};

for my $field (keys %{relations()}) {
    my $slot = $field;
    no strict "refs";
    *$field = sub {
	my $self = shift;
	return $self->relation(@{$self->relations->{$slot}});
    };
}

sub inst {
    my $self = shift;
    return $self->{inst};
}

#
# Editing
#

# Attributes that users can edit
use constant editattrs => qw(rowTitle collationOrder labeledURI mail rowStyle
			     telephoneNumber uid postalAddress);
use constant edattrhash => {map((lc($_) => 1), editattrs)};

use constant mvattrs => qw(labeledURI mail rowStyle telephoneNumber uid 
                             postalAddress);
use constant mvattrhash => {map((lc($_) => 1), mvattrs)};

use constant rqdattrs => qw(rowtitle);
use constant rqdattrhash => {map((lc($_) => 1), rqdattrs)};

sub is_editable {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    if ($self->{dir}->{godmode}) {
	return 1;
    } elsif (exists($self->{inst}) && $self->{inst}->is_editable) {
 	return exists($args{attr}) ? $self->edattrhash->{lc($args{attr})} : 1;
    }
    return 0;
}

sub is_deletable {
    my $self = shift;
    return $self->is_editable;
}

use constant idchars => 'a' .. 'z', '0' .. '9';
sub genid {
    my $self = shift;
    my $len = length($self->cn) + 1;
    my $cn = join('',map((idchars)[rand(idchars)], 1 .. $len));
    $self->replace(attr=>'cn', val=>$cn, internal=>1);
    my $dn = Ucam::Directory::contactrow_dn($self->{inst}->instID, $cn);
    $self->{ldapentry}->dn($dn);
}

sub logattrs {
    my $self = shift;
    return (type => 'contactrow',
	    instid => $self->{inst}->instID,
	    cn => $self->cn);
}

sub rdn {
    my $self = shift;
    return $self->cn;
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Ucam::Directory::ContactRow - Perl module representing a contact point

=head1 SYNOPSIS

  use Ucam::Directory::ContactRow;
  $person = new Ucam::Directory::ContactRow;

=head1 DESCRIPTION

See L<https://intranet.csi.cam.ac.uk/index.php/Ucam::Directory::ContactRow>
for now.

=head2 EXPORT

None by default.



=head1 SEE ALSO

=head1 AUTHOR

B.J. Harris, E<lt>bjh21@cam.ac.ukE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2005 by B.J. Harris

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
