# $Id$

use warnings;
use strict;

package Ucam::Directory::LabeledPhone;

use Regexp::Common;

use Ucam::Directory::Exception qw/invalidattrval_exception/;

use overload '""' => sub {${$_[0]}}, fallback => 1;

sub new {
    my $class = shift;
    my $self;
    if (@_ == 1) {
	$$self = shift;
    } else {
	my %args = @_;
	!defined($args{label}) or
	    $args{label} =~ /^[^\(\)]*$RE{balanced}{-parens=>'()'}?[^\(\)]*$/
	    or invalidattrval_exception("unbalanced parentheses");
	substr($args{phone}, -1) ne ')' or
	    invalidattrval_exception("close-paren at end of phone number");
	$$self = defined($args{label}) ?
	    "$args{phone} ($args{label})" :
	    $args{phone};
    }
    return bless($self, $class);
}

sub split {
    my $self = shift;
    my($phone, $label) = 
	$$self =~ /^(.*?)(?: *($RE{balanced}{-parens=>'()'}))?$/;
    return ($phone, defined($label) ? substr($label, 1, -1) : undef);
}

sub phone {
    my $self = shift;
    my $phone;
    ($phone, undef) = $self->split;
    return $phone;
}

sub label {
    my $self = shift;
    my ($label);
    (undef, $label) = $self->split;
    return $label;
}

1;
