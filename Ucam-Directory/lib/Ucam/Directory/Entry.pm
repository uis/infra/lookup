# $Id$

package Ucam::Directory::Entry;

use 5.008001;
use strict;
use warnings;
use Encode;
use English;

use Image::Size;
use Net::LDAP qw/LDAP_ALREADY_EXISTS LDAP_NO_SUCH_ATTRIBUTE/;
use Net::LDAP::Extension::SetPassword;
use Net::LDAP::Util qw/escape_filter_value/;
use XML::Writer;
use DateTime;
use DateTime::Format::Strptime;

use Ucam::Directory::Exception qw/general_exception permission_exception
    invalidattrval_exception ldap_exception/;
use Ucam::Directory::LabeledPhone;
use Ucam::Directory::LabeledURI;
use Ucam::Directory::LastUpdated;

our $VERSION = '0.01';

require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Ucam::Directory::Entry ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS =
    ( 'all' => [ qw( fmt_postal fmt_dirstring fmt_binary
		     unfmt_postal unfmt_dirstring unfmt_binary vc_escape
                     expand_phone) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw( );

use constant is_visible => 1;
use constant is_editable => 0;
use constant is_suppressible => 0;
use constant is_suppressed => 0;
use constant is_deletable => 0;

use constant attrlongdeschash => {};

use constant LDAP_DATE_FORMAT => "%Y%m%d%H%M%SZ";
use constant ISO_DATE_FORMAT  => '%Y-%m-%dT%H:%M:%S';
sub fastattrs {
    my $self = shift;
    return $self->supportedattrs;
}

sub new {
    my $class = shift;
    my $self = bless({}, $class);

    (@_ == 1) && unshift(@_, 'entry');
    my %args = @_;
    my $attr;

    $self->{ldapentry} = $args{entry};
    $self->{gotattrs} = {};
    my @gotattrs = @{$args{gotattrs} || [$self->supportedattrs]};
    foreach $attr (@gotattrs) {
	$self->{gotattrs}->{lc($attr)} = 1;
    }
    $self->{dir} = $args{dir} if ($args{dir});
    return $self;
}

sub _load_attrs {
    my $self = shift;
    my $entry = $self->{ldapentry};
    my $attr;
    foreach $attr ($entry->attributes(nooptions => 1)) {
	$self->{attrs}->{lc($attr)} = [$entry->get_value($attr)];
    }
}    

sub attrok {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    return exists $self->supattrhash->{$args{attr}};
}

sub _fillup_attrs {
    my $self = shift;
    $self->{ldapentry} =
	$self->{dir}->fillup_attrs($self->{ldapentry});
    foreach ($self->supportedattrs) {
	$self->{gotattrs}->{lc($_)} = 1;
    }
}

sub _get_raw {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    my $attr = lc($args{attr});
    if (!exists $self->{gotattrs}->{$attr}) {
	if (exists($self->{dir})) {
	    $self->_fillup_attrs;
	} else {
	    return wantarray ? () : undef;
	}
    }
    return $self->{ldapentry}->get_value($attr);
}

sub get {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    my $attr = lc($args{attr});
    if (exists($self->relations->{$attr})) {
	return $self->$attr;
    }
    unless ($self->attrok($attr)) {
	general_exception("bad attribute $attr");
    }
    if (!$args{internal} && !$self->is_visible($attr)) {
	return wantarray ? () : undef;
    }
    if ($attr eq 'allmail') {
	my @mails = ($self->mail, $self->mailAlternative);
	return wantarray ? @mails : $mails[0];
    } elsif ($attr eq 'postaladdress') {
	return fmt_postal($self->_get_raw($attr));
    } elsif ($attr eq 'labeleduri') {
	return fmt_labeleduri($self->_get_raw($attr));
    } elsif ($attr eq 'telephonenumber' ||
	     $attr eq 'facsimiletelephonenumber') {
	return fmt_labeledphone($self->_get_raw($attr));
    } elsif ($attr eq 'expirytimestamp' ||
             $attr eq 'warntimestamp' ) {
        return fmt_datetime($self->_get_raw($attr));
    } elsif ($attr eq 'lastupdated') {
        return fmt_lastupdated($self->_get_raw($attr));
    } elsif ($attr eq 'jpegphoto' ||
	     $attr eq 'universitycardphoto') {
	return fmt_binary($self->_get_raw($attr))
    } else {
	return fmt_dirstring($self->_get_raw($attr));
    }
}

# Format a postal address from LDAP 1.3.6.1.4.1.1466.115.121.1.41 format into
# a UTF-8 string.
sub fmt_postal {
    @_ = @_;
    foreach (@_) {
	s/\s*\$\s*/\n/g;
	s/\\(..)/chr(hex($1))/eg;
	$_ = decode_utf8($_, Encode::FB_CROAK)
    }
    return wantarray ? @_ : $_[0];
}

sub unfmt_postal {
    @_ = @_;
    foreach (@_) {
	$_ = encode_utf8($_);
	s/([\\\$])/"\\".sprintf("%02x", ord($1))/eg;
	s/\r?\n/ \$ /g;
    }
    return wantarray ? @_ : $_[0];
}
    

# Format text from LDAP 1.3.6.1.4.1.1466.115.121.1.15 format into a UTF-8
# string.
sub fmt_dirstring {
    @_ = @_;
    foreach (@_) {
	$_ = decode_utf8($_, Encode::FB_CROAK)
    }
    return wantarray ? @_ : $_[0];
}

sub unfmt_dirstring {
    @_ = @_;
    foreach (@_) {
	$_ = encode_utf8($_)
    }
    return wantarray ? @_ : $_[0];
}    

# Generate a little object for a labeledURI.
sub fmt_labeleduri {
    @_ = @_;
    foreach (@_) {
	$_ = new Ucam::Directory::LabeledURI(decode_utf8($_,
							 Encode::FB_CROAK));
    }
    return wantarray ? @_ : $_[0];
}    

# Generate a little object for a labeledPhone.
sub fmt_labeledphone {
    @_ = @_;
    foreach (@_) {
	$_ = new Ucam::Directory::LabeledPhone(decode_utf8($_,
							   Encode::FB_CROAK));
    }
    return wantarray ? @_ : $_[0];
}    

# Generate a little object for a lastUpdated.
sub fmt_lastupdated {
    @_ = @_;
    foreach (@_) {
	$_ = new Ucam::Directory::LastUpdated(decode_utf8($_,
							   Encode::FB_CROAK));
    }
    return wantarray ? @_ : $_[0];
}    

# Generate DateTime objects for dates
sub fmt_datetime {
    @_ = @_;
    my $fmt = DateTime::Format::Strptime->new(pattern=>LDAP_DATE_FORMAT);
    foreach (@_) {
        $_ = $fmt->parse_datetime(decode_utf8($_,Encode::FB_CROAK));
        general_exception("corrupt LDAP date") unless $_;
        $_->set_time_zone('UTC');
    }
    return wantarray ? @_ : $_[0];
}

sub unfmt_datetime {
    @_ = @_;
    foreach (@_) {
        unless (ref $_ and $_->isa('DateTime')) {
            my $fmt = 
                DateTime::Format::Strptime->new(pattern=>ISO_DATE_FORMAT);
            $_ = $fmt->parse_datetime($_);
            general_exception("Invalid date format in unfmt") unless $_;
        }
        $_ = $_->strftime(LDAP_DATE_FORMAT) if ref and $_->isa('DateTime');
    }
    return wantarray ? @_ : $_[0];
}


# Null formatter for binary objects (e.g. JPEG)
sub fmt_binary {
    @_ = @_;
    return wantarray ? @_ : $_[0];
}

sub unfmt_binary {
    @_ = @_;
    return wantarray ? @_ : $_[0];
}

sub attrdesc {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    my $desc = $self->attrdeschash->{lc($args{attr})};
    if (!exists($args{plural})) {
	my $count = $#{[$self->get($args{attr})]} + 1;
	$args{plural} = $count > 1;
    }
    if ($args{plural}) {
	# Could use Lingua::EN::Inflect if things get hairy
        if ($desc !~ / by| to| for|manages|status/i) {
            $desc =~ /[sxz]$/ and $desc .= 'e';
            $desc .= 's';
        }
    }
    return $desc;
}

sub attrlongdesc {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    return $self->attrlongdeschash->{lc($args{attr})};
}

sub relation {
    my $self = shift;
    my %args = @_;
    my $myattr = $args{myattr};
    my $theirattr = $args{theirattr};
    my $class = $args{class};
    my $cachekey = "$args{myattr}/$args{theirattr}/$args{class}";

    unless (exists($self->{relationcache}->{$cachekey})) {
	if (exists($self->{dir}) && $self->get($args{myattr})) {
	    my $dir = $self->{dir};
	    my $filter = '(|' . join('',map("($args{theirattr}=".
					    escape_filter_value($_).')',
					    $self->get($args{myattr}))) . ')';
	    my @raw = $dir->search_for_entries(class => $class,
					       filter => $filter);
	    # Now re-filter to allow for our (possibly more restricted)
	    # permissions.
	    my @cooked = grep($_->is_visible($theirattr), @raw);
	    $self->{relationcache}->{$cachekey} = \@cooked;
	} else {
	    $self->{relationcache}->{$cachekey} = [];
	}
    }
    return wantarray ? @{$self->{relationcache}->{$cachekey}} :
	$self->{relationcache}->{$cachekey}->[0];
}


#
# Editing functions
#

sub stamp {
    my $self = shift;

    my $viewer = $self->{dir}->viewer
	or general_exception("editing with no viewer defined");
    my $service = $self->{dir}->service
	or general_exception("editing with no service defined");
    my $datetime = DateTime->now(time_zone=>'local')
        ->strftime("%Y-%m-%d, %H:%M %Z");
    my $lastup = "$service, $viewer, $datetime";
    $self->{ldapentry}->replace(lastUpdated => $lastup);
    $self->{changed}->{lastUpdated} = [$lastup];
}

sub commit {
    my $self = shift;

    # We may be creating the object...
    unless ($self->{ldapentry}->dn) {
        if ($self->can('genid')) {
            $self->genid;
        }
        else {
            general_exception("can't create object without genid support");
        }
    }

    if ($self->{changed}) {
	exists $self->{dir} or
	    general_exception("no directory for commit");
	UPDATE: {
	    my $mesg = $self->{ldapentry}->update($self->{dir}->{ldap});
	    if ($mesg->is_error && $mesg->code == LDAP_ALREADY_EXISTS &&
		$self->can('genid')) {
		$self->genid;
		redo UPDATE;
	    }
	    $mesg->is_error and ldap_exception(error => $mesg->error,
                                               ldap_message => $mesg);
	}
    }
    if (exists($self->{newpwd})) {
        my $ldap = $self->{dir}->{ldap};
        my $pwd = $self->{newpwd};
        my $mesg = $ldap->set_password(user => $self->{ldapentry}->dn,
                                       newpasswd => $pwd);
        $mesg->is_error and ldap_exception(error => $mesg->error,
                                           ldap_message => $mesg);
        $self->{changed}->{newpwd} =  [$self->{newpwd}];
        delete($self->{newpwd});
    }
    if ($self->{changed}) {
	if (exists $self->{dir}->{log}) {
	    my($logline);
	    my($log) = new XML::Writer(OUTPUT => \$logline);
	    $log->startTag('modify', $self->logattrs);
	    foreach my $attr (keys %{$self->{changed}}) {
		$log->startTag('replace', attr => $attr);
		foreach (@{$self->{changed}->{$attr}}) {
		    if (lc($attr) eq 'jpegphoto' ||
			lc($attr) eq 'universitycardphoto' ||
                        lc($attr) eq 'userpassword') {
			$log->emptyTag('unloggedvalue');
		    } else {
			$log->dataElement('value', $_);
		    }
		}
		$log->endTag;
	    }
	    $log->endTag;
	    syswrite($self->{dir}->{log}, "$logline\n");
	}
	delete $self->{changed};
    }
}

sub is_multivalued {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;

    return $self->mvattrhash->{lc($args{attr})};
}

use constant rqdattrhash => {};
use constant rqdattrs => ();
sub is_required {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;

    if (exists($args{attr})) {
        return exists($self->rqdattrhash->{lc($args{attr})});
    }
    else {
        return scalar($self->rqdattrs);
    }
}

sub IsAtext {
    return <<END;
0041 005A
0061 007A
0030 0039
0021
0023
0024
0025
0026
0027
002A
002B
002D
002F
003D
003F
005E
005F
0060
007B
007C
007D
007E
END
}

sub validate {
    my $self = shift;
    my %args = @_;
    my $attr = lc $args{attr};
    my @val = (ref($args{val}) eq 'ARRAY') ? @{$args{val}} : ($args{val});
    my %val;


    if (@val > 1 && !$self->is_multivalued($attr)) {
	return "Only one value permitted";
    }
    if ($self->is_required($attr) and !grep {defined($_) and $_ ne ''} @val) {
        return "Requires a value"
    }
    if ($attr eq 'mgrgroupid' && !$self->{dir}->{godmode} &&
	exists($self->{dir}) &&	exists($self->{dir}->{viewer})) {
	my $viewer = $self->{dir}->{viewer};
	my @groups = 
	    $self->{dir}->search_for_groups
	 ('(|'.join('',map("(groupID=".escape_filter_value($_).')',
			   @val,'000000','0')).')');
        grep ($_->has_member($viewer), @groups) ||
	    return "You are not a member of any of these groups";
    }
    foreach (@val) {
	$val{lc($_)} && return "Value '$_' appears more than once";
	$val{lc($_)} = 1;
	if ($attr ne 'jpegphoto' && $attr ne 'universitycardphoto' &&
	    length($_) > 1024) {
	    return "'$_' is more than 1024 characters long";
	}
	if ($attr eq 'suppressattribute' && !$self->is_suppressible(lc($_))) {
	    return "Attribute '$_' may not be suppressed";
	}
	if ($attr eq 'uid') {
	    /^[a-zA-Z0-9]+$/ or return "'$_' is not a valid CRSID";
	}
	if ($attr eq 'mail' ||
	    $attr eq 'allmail' ||
	    $attr eq 'mailAlternative') {
	    unless (/^\p{IsAtext}+(?:\.\p{IsAtext}+)*\@
		      \p{IsAtext}+(?:\.\p{IsAtext}+)*$/x) {
		return "'$_' is not in a format that is accepted here - " .
                       "should be something like fjc55\@cam.ac.uk";
	    }
	}
	if ($attr eq 'telephonenumber' ||
	    $attr eq 'facsimiletelephonenumber' ||
	    $attr eq 'pager') {
	  if (/[^A-Za-z0-9\(\)+,-\.\/:\? ]/) {
	    return "'$_' contains characters which are not " .
	      "allowed here - only letters, digits, " .
		"()+,-./:? and space are allowed";
	  }
        }
        if ($attr eq 'jpegphoto') {
	    my ($x, $y, $type) = Image::Size::imgsize(\$_);
	    if ($type ne 'JPG') {
		return "Photographs must be in JPEG format; the image you " .
		    "have selected does not appear to be in this format";
	    } elsif ($x > 300 or $y > 340) {
	       return "Photographs must be at most 340 pixels high and " .
		    "at most 300 pixels wide; the image you have selected " .
		    "appears to be $y by $x pixels";
	    }
	}
	if ($attr eq 'labeleduri') {
	    # Limit URIs to safe schemes (e.g. not javascript:)
	    # Also to schemes that can sensibly define a "Web page"
	    if (!m!^(ftp|http|gopher|https)://!) {
		return "Web page URIs must start with 'http://', 'https://', ".
		    "'ftp://', or 'gopher://'";
	    }
	}
        if ($attr eq 'expirytimestamp' ||
            $attr eq 'warntimestamp') {
            unless (ref($_) and !$_->isa('DateTime')) {
                my $fmt = 
                    DateTime::Format::Strptime->new(pattern=>ISO_DATE_FORMAT);
                my $d = $fmt->parse_datetime($_);
                return "Invalid date format" unless $d;
            }
            elsif (!$_->isa('DateTime')) {
                return "Wrong sort of date object";
            }
        }
        if ($attr eq 'userpassword') {
            return "Passwords must be at least 32 characters long" 
                unless length($_) >= 32;
        }
        if ($attr eq 'misaffiliation') {
            return "Status must be 'staff' or 'student'"
                unless $_ eq 'staff' or $_ eq 'student';
        }
    }
    return 0;
}

use constant replace_hook => 0;

sub replace {
    my $self = shift;
    my %args = @_;
    my $attr = lc($args{attr});
    my $value = exists($args{value}) ? $args{value} : $args{val};
    my @value = (ref($value) eq 'ARRAY') ? @{$value}
                                           : (defined($value) ? ($value) : ());
    my $valid;

    unless ($args{internal} || $self->is_editable($attr)) {
	permission_exception("Attribute '$attr' is not editable");
    }
    if ($valid = $self->validate(attr => $attr, val => \@value)) {
	invalidattrval_exception("$attr: $valid");
    }

    # Make sure we don't reload later.
    unless ($self->{changed}) {
	$self->_fillup_attrs;
    }

    if ($attr eq 'allmail') {
	my $mail = shift(@value);
	$self->replace(attr => 'mail', val => $mail);
	$self->replace(attr => 'mailalternative', val => \@value);
	return;
    }

    # Sidetrack setting (but not deleting) userpassword
    if ($attr eq 'userpassword' and $value[0] ne '') {
        $self->{newpwd} = $value[0];
        return;
    }

    if ($attr eq 'postaladdress') {
	@value = unfmt_postal(@value);
    } elsif ($attr eq 'expirytimestamp' ||
             $attr eq 'warntimestamp') {
        @value = unfmt_datetime(@value)
    } elsif ($attr eq 'jpegphoto' ||
	     $attr eq 'universitycardphoto') {
	@value = unfmt_binary(@value)
    } else {
	@value =  unfmt_dirstring(@value);
    }

    $self->stamp;
    $self->{ldapentry}->replace($attr => \@value);
    $self->{changed}->{$attr} = \@value;
    $self->replace_hook(attr => $attr, value => \@value);
}

# delete() deletes the whole object (if that's allowed)

sub delete {
    my $self = shift;
    $self->is_deletable || permission_exception("deletion not permitted");
    $self->{ldapentry}->delete();
    my $mesg = $self->{ldapentry}->update($self->{dir}->{ldap});
    $mesg->is_error and ldap_exception(error => $mesg->error,
                                          ldap_message => $mesg);
}


# by default only god can cancel things - overridable in other objects

sub is_cancelable {
    my $self = shift;
    return $self->{dir}->{godmode};
}

# cancel() marks the object as cancelled

sub cancel {
    my $self = shift;
    $self->is_cancelable || permission_exception("cancellation not permitted");
    my $datetime = DateTime->now->strftime("%Y%m%d%H%M%SZ");
    $self->{ldapentry}->replace(cancelled => $datetime);
    $self->{changed}->{cancelled} = [$datetime];
    $self->stamp;
    $self->commit;
}

# Get the next value from an IDPool counter
sub nextid {
    my $self = shift;
    my %args = @_;

    my $nextval;
    my $ineration_counter = 0;
  LOOP:
    {
        my $result = $self->{dir}->_ldapsearch
            ( base    => $args{ctr} . "," . Ucam::Directory::ldap_base_dn(),
              scope   => "base",
              filter  => "(objectClass=*)",
         );
        my $entry = $result->pop_entry ||
            general_exception("can't find counter");
        $nextval = $entry->get_value($args{attr}) ||
            general_exception("can't read counter");

        my $msg = $self->{dir}->{ldap}->modify
            ( $entry->dn,
              changes => [
                          delete => [
                                     $args{attr} => $nextval
                                 ],
                          add    => [
                                     $args{attr} => $nextval+1
                                 ],
                      ]
         );
        if ($msg->is_error) {
            $msg->code == LDAP_NO_SUCH_ATTRIBUTE ||
                ldap_exception(error => $msg->error, ldap_message => $msg);
            redo LOOP;
        }

    };

    return $nextval;

}

# Expand common UofC telephone abbriviations
sub expand_phone {
    my ($val) = @_;

    if ($val =~ /^3\d{4}$/) {
        return "+44 1223 3$val";
    }
    elsif ($val =~ /^[4,6]\d{4}$/) {
        return "+44 1223 7$val";
    }
    elsif ($val =~ /^\d{6}$/) {
        return "+44 1223 $val";
    }
    elsif ($val =~ /^0(.+)/) {
        return "+44 $1";
    }
    return $val;
}

# Escape a value for inclusion ina vCare compound property value
sub vc_escape {
    my ($val) = @_;

    $val =~ s/(;)/\\$1/g;

    return $val;
}

1;
