# $Id$

use warnings;
use strict;

package Ucam::Directory::LabeledURI;

use overload '""' => sub {${$_[0]}}, fallback => 1;

sub new {
    my $class = shift;
    my $self;
    if (@_ == 1) {
	$$self = shift;
    } else {
	my %args = @_;
	$$self = defined($args{label}) ?
	    "$args{uri} $args{label}" : $args{uri};
    }
    return bless($self, $class);
}

sub uri {
    my $self = shift;
    my $uri;
    ($uri) = split(/ +/, $$self);
    return $uri;
}

sub label {
    my $self = shift;
    my ($label);
    (undef, $label) = split(/ +/, $$self, 2);
    return $label;
}

1;
