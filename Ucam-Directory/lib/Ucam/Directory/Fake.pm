# $Id$
package Ucam::Directory::Fake;

use 5.008001;
use strict;
use warnings;

use Ucam::Directory;
use Ucam::Directory::Exception qw/general_exception/;
use Ucam::Directory::Person;
use Ucam::Directory::Institution;
use Ucam::Directory::ContactRow;

use Net::LDAP;
use Net::LDAP::Entry;
use Net::LDAP::LDIF;
use Net::LDAP::Util qw(escape_dn_value escape_filter_value);
use Test::MockObject;

use IO::Scalar;

our @ISA = qw(Ucam::Directory);

sub new {
    my $class = shift;
    my $self = bless({}, $class);
    my %args = @_;
    $self->{viewer} = $args{viewer} if exists $args{viewer};
    $self->{service} = $args{service} if exists $args{service};
    return $self;
}

use constant frs99 => <<'EOF';
version: 1

dn: uid=frs99,ou=people,o=University of Cambridge,dc=cam,dc=ac,dc=uk
uid: frs99
objectClass: camAcUkPerson
objectClass: inetOrgPerson
sn: Smith
ou: University Computing Service
ou: Corpus Christi College
instID: CS
instID: CORPUS
libraryCardNumber: VABCD
mail: frs99@cam.ac.uk
telephoneNumber: 34567
telephoneNumber: 567890
homePhone: +44 1223 654321
postalAddress: \55\6eiversity of Cambridge$Computing Service$Pembroke Street$CAMBRIDGE$CB2  3QH
displayName: Fred Smith
cn: F.R. Smith
lastUpdated: lookup.cam.ac.uk,bjh21,2005-8-4 at 18:10 GMT
suppressAttribute: postalAddress
jpegPhoto:: /9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsL
 DBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/
 wAALCAABAAEBAREA/8QAFAABAAAAAAAAAAAAAAAAAAAAB//EABQQAQAAAAAA
 AAAAAAAAAAAAAAD/2gAIAQEAAD8Af3//2Q==
jdName: Smith F.R.
jdInst: CS
jdCamMail: Y
jdExDir: N
misAffiliation: staff
misAffiliation: student
EOF

use constant ucs => <<'EOF';
version: 1

dn: instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
objectClass: organizationalUnit
objectClass: camAcUkOrganizationalUnit
instID: CS
street: test address
ou: University Computing Service (UCS)
parentInstID: INGBS
postalAddress: University of Cambridge$Computing Service$Pembroke Street$Cambridge$CB2 3QH
jdName: University Computing Service2
jdFather: INGBS
groupID: 100656
EOF

use constant cspeople => <<'EOF';
version: 1

dn: uid=xx123,ou=people,o=University of Cambridge,dc=cam,dc=ac,dc=uk
uid: xx123
cn: X.X.
sn: X
instID: CS
objectClass: camAcUkPerson
objectClass: inetOrgPerson

dn: uid=xx124,ou=people,o=University of Cambridge,dc=cam,dc=ac,dc=uk
uid: xx124
cn: X.X.
sn: X
instID: CS
objectClass: camAcUkPerson
objectClass: inetOrgPerson

dn: uid=xx125,ou=people,o=University of Cambridge,dc=cam,dc=ac,dc=uk
uid: xx125
cn: X.X.
sn: X
instID: CS
objectClass: camAcUkPerson
objectClass: inetOrgPerson
EOF

my $ldap_people_dn = Ucam::Directory::ldap_people_dn;
my $ldap_insts_dn = Ucam::Directory::ldap_insts_dn;
my $ldap_groups_dn = Ucam::Directory::ldap_groups_dn;

our %searchresults =
    ( "uid=frs99,$ldap_people_dn base" .
      " (objectClass=inetOrgPerson)" => frs99,

      "uid=frs99,$ldap_people_dn base" .
      " (objectClass=*)" => frs99,

      "$ldap_people_dn one" .
      ' (&(objectClass=inetOrgPerson)(|(uid=frs99)))' => frs99,

      "uid=spqr1,$ldap_people_dn base" .
      " (objectClass=inetOrgPerson)" => '',

      "instID=CS,$ldap_insts_dn base" .
      " (objectClass=organizationalUnit)" => ucs,

      "$ldap_insts_dn one" .
      " (&(objectClass=organizationalUnit)(|(instID=CS)))" => ucs,

      "instID=THING,$ldap_insts_dn base" .
      " (objectClass=organizationalUnit)" => "",

      "instID=CS,$ldap_insts_dn one" .
      " (objectClass=contactRow)" => '
version: 1

dn: cn=20,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Director
instID: CS
objectClass: contactRow
rowStyle: bold
uid: frs99
collationOrder: 20
cn: 20

dn: cn=42,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Personal Assistant to Director
instID: CS
objectClass: contactRow
rowStyle: blank
uid: lamk2
collationOrder: 42
cn: 42

dn: cn=61,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Head of Unix Division
instID: CS
objectClass: contactRow
rowStyle: bold
uid: rjd4
collationOrder: 61
cn: 61

dn: cn=60,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Head of Network and Small Systems Division
instID: CS
objectClass: contactRow
rowStyle: bold
uid: cjc1
collationOrder: 60
cn: 60

dn: cn=63,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Head of Technical User Support Division
instID: CS
objectClass: contactRow
rowStyle: bold
uid: mro2
collationOrder: 63
cn: 63

dn: cn=59,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Fax
instID: CS
objectClass: contactRow
rowStyle: blank
telephoneNumber: 34679
collationOrder: 59
cn: 59

dn: cn=58,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Website
instID: CS
objectClass: contactRow
rowStyle: bold
labeledURI: http://www.cam.ac.uk/cs
collationOrder: 58
cn: 58

dn: cn=50,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Deputy Director
instID: CS
objectClass: contactRow
rowStyle: bold
uid: sk17
collationOrder: 50
cn: 50

dn: cn=62,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Head of User Services
instID: CS
objectClass: contactRow
rowStyle: bold
uid: rjs1
collationOrder: 62
cn: 62

dn: cn=64,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Head of Institution Liason
instID: CS
objectClass: contactRow
rowStyle: bold
uid: rs12
cn: 64
collationOrder: 67

dn: cn=56,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
collationOrder: 56
rowTitle: Enquiries and Reception
instID: CS
objectClass: contactRow
rowStyle: bold
telephoneNumber: 34600
mail: reception@ucs.cam.ac.uk
cn: 56',

      "cn=20,instID=CS,$ldap_insts_dn base" .
      " (objectClass=contactRow)" => '
version: 1

dn: cn=20,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
rowTitle: Director
instID: CS
objectClass: contactRow
rowStyle: bold
uid: frs99
collationOrder: 20
cn: 20
',
      "instID=THING,$ldap_insts_dn one" .
      " (objectClass=contactRow)" => "",

      "$ldap_people_dn one" .
      " (&(objectClass=inetOrgPerson)(instID=CS))" => cspeople,
      "$ldap_people_dn one" .
      " (&(objectClass=inetOrgPerson)(|(instID=CS)))" => cspeople,

      "cn=lookup_admin,instID=CS,$ldap_insts_dn base" .
      " (objectClass=camAcUkGroup)" => '
version: 1

dn: cn=lookup_admin,instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,
 dc=uk
cn: lookup_admin
mgr: ijl20
mgr: rjd4
mgr: cjc1
adm: mro2
adm: sp253
adm: si202
adm: jw35
adm: dpc22
instID: CS
objectClass: camAcUkGroup
objectClass: top
groupTitle: directory administration for UCS
description: Members of this group can edit this institution/department\'s data
  in the University Directory',

      "cn=lookup_admin,instID=THING,$ldap_insts_dn base" .
      " (objectClass=camAcUkGroup)" => '',

      "$ldap_insts_dn one" .
      " (&(objectClass=organizationalUnit)(|(instID=CS)(instID=CORPUS)))" => '
version: 1

dn: instID=CORPUS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
objectClass: organizationalUnit
objectClass: camAcUkOrganizationalUnit
ou: Corpus Christi College
instID: CORPUS
parentInstID: COLL

dn: instID=CS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
objectClass: organizationalUnit
objectClass: camAcUkOrganizationalUnit
instID: CS
street: test address
ou: University Computing Service (UCS)
parentInstID: INGBS
postalAddress: New Museums Site $ Pembroke Street $ Cambridge $ CB2 3QH
',

      "$ldap_insts_dn one " .
      '(&(objectClass=organizationalUnit)(|(instID=INGBS)))' => '
version: 1

dn: instID=INGBS,ou=insts,o=University of Cambridge,dc=cam,dc=ac,dc=uk
objectClass: organizationalUnit
objectClass: camAcUkOrganizationalUnit
ou: Other General Board Institutions - Service and Admin.
instID: INGBS
parentInstID: INGB
',
      "$ldap_insts_dn one " .
      '(&(objectClass=organizationalUnit)(parentInstID=INGBS))' => ucs,
      "$ldap_insts_dn one " .
      '(&(objectClass=organizationalUnit)(|(parentInstID=INGBS)))' => ucs,
      "$ldap_groups_dn one " .
      "(&(objectClass=camAcUkGroup)(|(groupID=0)(groupID=000000)))" => '
version: 1

dn: groupID=000000,ou=groups,o=University of Cambridge,dc=cam,dc=ac,dc=uk
objectClass: camAcUkGroup
groupID: 000000
groupTitle: Top-level group
description: Top-level group
mgrGroupID: 000000
uid: x205
visibility: members
',
      "$ldap_groups_dn one " .
      "(&(objectClass=camAcUkGroup)(|(groupID=100656)))" => '
dn: groupID=100656,ou=groups,o=University of Cambridge,dc=cam,dc=ac,dc=uk
objectClass: camAcUkGroup
groupID: 100656
groupTitle: Editors group for "University Computing Service (UCS)".
description: The members of this group may edit the directory data for "Univer
 sity Computing Service (UCS)".
mgrGroupID: 100655
uid: x200
uid: x201
uid: x202
visibility: members
mail: foo@x.invalid
userPassword: secret
rdrGroupID: 123456
allUid: x200
allUid: x201
allUid: x202
allUid: x203
expiryTimestamp: 19600305123456Z
lastUpdated: lookup, test0001, 2005-10-11, 16:32 BST
'
      );


sub _ldapsearch {
    my $self = shift;
    my %args = @_;

    my $mesg = new Test::MockObject;
    my $ent;
    my $gotattr;

    if (exists($searchresults{"$args{base} $args{scope} $args{filter}"})) {
	my $LDIF = new IO::Scalar
	  \$searchresults{"$args{base} $args{scope} $args{filter}"};
	my $ldif = new Net::LDAP::LDIF($LDIF);
	my @results;
	while (defined($ent = $ldif->read_entry)) {
	    if (exists $args{attrs}) {
		foreach $gotattr ($ent->attributes) {
		    unless (grep(lc($_) eq lc($gotattr), @{$args{attrs}})) {
			$ent->delete($gotattr);
		    }
		}
	    }
	    push @results, $ent;
	}
	$mesg->set_false('is_error');
	$mesg->set_always('count', scalar(@results));
	$mesg->set_list('entries', @results);
	$mesg->mock('entry', sub { return $results[$_[1]]; });
    } else {
	$mesg->set_true('is_error');
	$mesg->set_always('code', 1);
	$mesg->set_always('error', "_ldapsearch invoked strangely: " .
			  "'$args{base} $args{scope} $args{filter}'");
    }
    $mesg;
}

1;
