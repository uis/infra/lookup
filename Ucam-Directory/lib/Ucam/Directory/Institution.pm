# $Id$

package Ucam::Directory::Institution;

use 5.008001;
use strict;
use warnings;
use Encode;

use Net::LDAP::Util qw/escape_filter_value/;
use MIME::QuotedPrint ();
use Ucam::Directory::Entry ':all';
use Ucam::Directory::Exception qw/general_exception/;

our $VERSION = '0.01';

our @ISA = qw(Ucam::Directory::Entry);

sub ldap_base_dn { Ucam::Directory::ldap_insts_dn() };
use constant ldap_filter => '(objectClass=organizationalUnit)';

use constant supportedattrs => qw(instID parentInstID lastUpdated jpegPhoto ou
 postalAddress telephoneNumber facsimileTelephoneNumber mail mailAlternative
 labeledURI allmail mgrGroupID acronym cancelled jdFather jdName groupID);
use constant supattrhash => {map((lc($_) => 1), supportedattrs)};

use constant attrdeschash => 
    { jpegphoto                => "Image",
      ou                       => "Name",
      postaladdress            => "Address",
      telephonenumber          => "Phone number",
      facsimiletelephonenumber => "Fax number",
      allmail                  => "Email address",
      labeleduri               => "Web page",
      groupid                  => "Group",
      mgrgroupid               => "Managing group",
      acronym                  => "Acronym",
      jdfather                 => "UCS parent institution",
      jdname                   => "UCS name",

      people                   => "Member",
      parents                  => "Parent institution",
      jdfathers                => "UCS parent institution",
      children                 => "Child institution",
      mgrgroups                => "Data manager",
      groups                   => "Group",
 };

# Attributes to fetch by default
use constant fastattrs =>
    grep(!/^(jpegPhoto|universityCardPhoto)$/, supportedattrs);

for my $field (supportedattrs) {
    my $slot = $field;
    no strict "refs";
    *$field = sub {
	my $self = shift;
	return $self->get(attr => $slot);
    };
}

use constant relations =>
{
    people =>    [ class=>"Ucam::Directory::Person",
	           myattr=>"instID", theirattr=>"instID"],
    parents =>   [ class=>"Ucam::Directory::Institution",
		   myattr=>"parentInstID", theirattr=>"instID"],
    jdfathers => [ class=>"Ucam::Directory::Institution",
		   myattr=>"jdFather", theirattr=>"instID"],
    children =>  [ class=>"Ucam::Directory::Institution",
		   myattr=>"instID", theirattr=>"parentInstID"],
    mgrgroups => [ class=>"Ucam::Directory::Group",
		   myattr=>"mgrGroupID", theirattr=>"groupID"],
    groups =>    [ class=>"Ucam::Directory::Group",
	           myattr=>"groupID", theirattr=>"groupID"],
};

for my $field (keys %{relations()}) {
    my $slot = $field;
    no strict "refs";
    *$field = sub {
	my $self = shift;
	return $self->relation(@{$self->relations->{$slot}});
    };
}

sub contactRow {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'cn');
    my %args = @_;
    if (exists($args{cn})) {
	if ($self->{dir}) {
	    my $row = $self->{dir}->lookup_contactrow(instid => $self->instID,
						      cn => $args{cn});
	    $row->{inst} = $self;
	    return $row;
	} else {
	    return undef;
	}
    }
    unless (exists $self->{contactRow}) {
	if ($self->{dir}) {
	    $self->{contactRow} =
		[ $self->{dir}->get_contactrows($self->instID) ];
	} else {
	    $self->{contactRow} = [];
	}
	map($_->{inst} = $self, @{$self->{contactRow}});
    }
    return wantarray ? @{$self->{contactRow}} : $self->{contactRow}->[0];
}

sub clear_contactrow_cache {
    my $self = shift;
    delete($self->{contactRow});
}

sub create_contactrow {
    my $self = shift;
    permission_exception("no permission to create ContactRow")
	unless $self->is_editable;
    my $row = $self->{dir}->create_contactrow;
    $row->{inst} = $self;
    $row->genid;
    return $row;
}

sub as_vcard {
    my $self = shift;

    my @card;

    push @card, "BEGIN:VCARD";
    push @card, "VERSION:2.1";

    push @card, "FN:" . $self->ou;
    push @card, "N:"  . vc_escape($self->ou);

    foreach my $addr ($self->postalAddress) {
        $addr =~ s/\n/\r\n/g;
        push @card, "LABEL;ENCODING=QUOTED-PRINTABLE:" .
            MIME::QuotedPrint::encode($addr,"");
    }

    foreach my $tel ($self->telephoneNumber) {
        push @card, "TEL:" . expand_phone($tel->phone);
    }

    push @card, "EMAIL;INTERNET;PREF:" . $self->mail
        if $self->mail;
    foreach my $addr ($self->mailAlternative) {
        push @card, "EMAIL;INTERNET:" . $addr;
    }

    foreach my $uri ($self->labeledURI) {
        push @card, "URL:" . $uri->uri;
    }

    # Both LastUpdated itself, and its data component, may be missing..
    if ($self->lastUpdated and $self->lastUpdated->date) {
        push @card, "REV:" . $self->lastUpdated->date . "Z";
    }

    push @card, "END:VCARD";

    return join("\n",@card);

}

#
# Sorting
#

sub _preproc_byname {
    my $inst = shift;
    my $name = $inst->ou;
    $name =~ s/Dept\.?/Department/g;
    $name =~ s/^St /Saint /g;
    $name =~ s{^((?:The )?(?:(?:Diploma|Sub Department|Department|Faculty|School|Institute|Research Centre|Centre|Unit) (?:for|in|of))|University of Cambridge|University|Part 3) (.+)$}
      {$2\t$1};
    return $name;
}

my $byname;
sub byname {
    return $byname ||
	($byname = new Unicode::Collate(table => 'ucamdirkeys.txt',
					variable => 'non-ignorable',
					preprocess => \&_preproc_byname));
}

#
# Editing
#

# Attributes that users can edit
use constant editattrs => qw(mail mailAlternative telephoneNumber
			     facsimileTelephoneNumber
			     jpegPhoto postalAddress labeledURI
			     allmail mgrGroupID acronym groupID);
use constant edattrhash => {map((lc($_) => 1), editattrs)};

use constant mvattrs => qw(parentInstID postalAddress mailAlternative allmail
			   telephoneNumber facsimileTelephoneNumber
			   labeledURI mgrGroupID acronym groupID);
use constant mvattrhash => {map((lc($_) => 1), mvattrs)};


sub is_editable {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    if ($self->{dir}->{godmode}) {
	return 1;
    } elsif (exists($self->{dir}->{viewer}) &&
	     (grep($_->has_member($self->{dir}->{viewer}), $self->mgrgroups) ||
	      $self->{dir}->viewer_in_group_zero)) {
 	return exists($args{attr}) ? $self->edattrhash->{lc($args{attr})} : 1;
    }
    return 0;
}

sub logattrs {
    my $self = shift;
    return (type => 'institution', instid => $self->instID);
}

sub rdn {
    my $self = shift;
    return $self->instID;
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Ucam::Directory::Person - Perl module representing a person

=head1 SYNOPSIS

  use Ucam::Directory::Institution;
  $person = new Ucam::Directory::Institution;

=head1 DESCRIPTION

See L<https://intranet.csi.cam.ac.uk/index.php/Ucam::Directory::Institution>
for now.

=head2 EXPORT

None by default.



=head1 SEE ALSO

=head1 AUTHOR

B.J. Harris, E<lt>bjh21@cam.ac.ukE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2005 by B.J. Harris

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
