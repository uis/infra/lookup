# $Id$

package Ucam::Directory::Group;

use 5.008001;
use strict;
use warnings;
use Encode;

use Ucam::Directory::Entry ':all';
use Ucam::Directory::Exception qw/general_exception permission_exception/;
use Unicode::Collate;
use List::MoreUtils qw(uniq);
use DateTime;
use DateTime::Format::Strptime;

our $VERSION = '0.01';

our @ISA = qw(Ucam::Directory::Entry);

use constant ISO_DATE_FORMAT  => '%Y-%m-%dT%H:%M:%S';

sub ldap_base_dn { Ucam::Directory::ldap_groups_dn() };
use constant ldap_filter => '(objectClass=camAcUkGroup)';

use constant supportedattrs => qw
    (groupID groupTitle description mgrGroupID uid visibility mail
     cancelled userPassword rdrGroupID allUid expiryTimestamp
     lastUpdated warnTimestamp instID);
use constant supattrhash => {map((lc($_) => 1), supportedattrs)};

use constant attrdeschash =>
    { groupid         => "GroupID",
      grouptitle      => "Title",
      description     => "Description",
      mgrgroupid      => "Managed by",
      uid             => "Visible member",
      visibility      => "Membership visibility",
      mail            => "Email address",
      cacelled        => "Cancelled",
      userpassword    => "Password",
      rdrgroupid      => "Privileged access by",
      alluid          => "Member",
      expirytimestamp => "Expires",
      lastupdated     => "Last updated",
      warntimestamp   => "Next expiry warning",
#
      mgrgroups       => "Managed by",
      rdrgroups       => "Privileged access by",
      mgdgroups       => "Manages",
      readgroups      => "Privileged access to",
      mgdinsts        => "Manages lookup data for",
      owninginsts     => "Belongs to",
};

##use constant attrlongdeschash =>
##    { groupid         => "Unique identifier for this group. Normally a number greater than 100000",
##      grouptitle      => "A one-line description for this group",
##      description     => "A multi-line description for this group",
##      mgrgroupid      => "Groups whose members can edit information about this group, add and remove members, etc.",
##      uid             => "The members of this list who have not suppressed their membership",
##      visibility      => "The general visibility of this group's membership - generally within the University, to group members and managers, or only to group managers",
##      mail            => "One or more email addresses that can be used to contact group members",
##      cacelled        => "",
##      userpassword    => "An automatically-generated password which can be used to authenticate over LDAP and so gain access to the full membership list of this group including people who have suppressed their membership",
##      rdrgroupid      => "Groups whose members can see the full membership list of this group including people who have suppressed their membership",
##      alluid          => "All the members of this group",
##      expirytimestamp => "A date after which this group will be automatically cancelled; any edit to the group's details will automatically extend this for 12 months",
##      lastupdated     => "When, and by who, this groups details or membership were last updated",
#
##      mgrgroups       => "Groups whose members can edit information about this group, add and remove members, etc.",
##      rdrgroups       => "Groups whose members can see the full membership list of this group including people who have suppressed their membership",
##      mgdgroups       => "Groups whose details and membership are managed by members of this group",
##      readgroups      => "Groups whose full membership (including people who have suppressed their membership) can be seen by members of this group",
##     mgdinsts        => "Institutions whose details and membership are managed by members of this group",
##      owninginsts     => "Institutions ultimately responsible for this group",
##};

for my $field (supportedattrs) {
    my $slot = $field;
    no strict "refs";
    *$field = sub {
	my $self = shift;
	return $self->get(attr => $slot);
    };
}

use constant relations =>
    {
     # Our managers
     mgrgroups =>   [ class=>"Ucam::Directory::Group",
                      myattr=>"mgrGroupID", theirattr=>"groupID"],
     # Our readers
     rdrgroups =>   [ class=>"Ucam::Directory::Group",
                      myattr=>"rdrGroupID", theirattr=>"groupID"],
     # Groups we manage
     mgdgroups =>   [ class=>"Ucam::Directory::Group",
                      myattr=>"groupID", theirattr=>"mgrGroupID"],
     # Groups we read
     readgroups =>  [ class=>"Ucam::Directory::Group",
                      myattr=>"groupID", theirattr=>"rdrGroupID"],
     # Insts we manage
     mgdinsts =>    [ class=>"Ucam::Directory::Institution",
                      myattr=>"groupID", theirattr=>"mgrGroupID"],
     # Insts we belong to
     owninginsts => [ class=>"Ucam::Directory::Institution",
                      myattr=>"groupID", theirattr=>"groupID"],
 };

for my $field (keys %{relations()}) {
    my $slot = $field;
    no strict "refs";
    *$field = sub {
	my $self = shift;
	return $self->relation(@{$self->relations->{$slot}});
    };
}

# Returns listable members
sub members {
    my $self = shift;

    my @members;
    if ($self->is_visible('allUid')) {
        @members = $self->relation(class=>"Ucam::Directory::Person",
                                   myattr=>"allUid",
                                   theirattr=>"uid");
    }
    elsif ($self->is_visible('uid')) {
        @members = $self->relation(class=>"Ucam::Directory::Person",
                                  myattr=>"uid",
                                  theirattr=>"uid");
    }
    # Add the viewer if they are a member and not already in the list
    if (exists($self->{dir}->{viewer}) and
        $self->has_member($self->{dir}->{viewer}) and
        !grep { $_->uid eq $self->{dir}->{viewer} } @members) {
        my $me = eval {
            $self->{dir}->lookup_person($self->{dir}->{viewer})
        };
        push @members,$me if $me;
    }
    return @members;

}

# Tests for group membership from viewer's point of view
sub has_member {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'uid');
    my %args = @_;
    my $int = $args{internal};
    # Bypass permission checks for confirming the viewer's membership.
    $int |= exists($self->{dir}->{viewer}) &&
	$self->{dir}->{viewer} eq $args{uid};
    if (grep($_ eq $args{uid},$self->get(attr=>'uid', internal=>$int))) {
        return 'unsuppressed';
    }
    elsif (grep($_ eq $args{uid},$self->get(attr=>'alluid', internal=>$int))) {
        return 'suppressed';
    }
    else {
        return;
    }
}

# Tests for group membership from a 'public' point of view (membership
# unsuppressed, group visibility cam)
sub has_public_member {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'uid');
    my %args = @_;
    return $self->visibility eq 'cam' &&
        grep($_ eq $args{uid}, $self->get(attr=>'uid'));
}

# Tests for a group manager
sub has_manager {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'uid');
    my %args = @_;
    return grep($_->has_member(%args), $self->mgrgroups);
}

# Tests for a group reader
sub has_reader {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'uid');
    my %args = @_;
    return defined($args{uid}) && 
        grep($_->has_member(%args),$self->rdrgroups);
}

sub is_visible {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    my $attr = lc $args{attr};

    return 1 unless exists $self->{dir};
    if ($attr eq 'alluid') {
        return $self->is_editable ||
            $self->has_reader($self->{dir}->{viewer});
    }
    if ($attr eq 'uid') {
	if (!defined($self->visibility)) {
	    return 0;
	} elsif ($self->visibility eq 'managers') {
	    return $self->is_editable ||
                $self->has_reader($self->{dir}->{viewer});
	} elsif ($self->visibility eq 'members') {
	    return $self->is_editable ||
                $self->has_reader($self->{dir}->{viewer}) ||
                    (exists($self->{dir}->{viewer}) &&
                     $self->has_member($self->{dir}->{viewer}));
        } elsif ($self->visibility eq 'cam') {
            return 1;
	} else {
	    return 0;
	}
    }
    return 1;
}

#
# Sorting
#

sub _preproc_byname {
    my $group = shift;
    return $group->groupTitle;
}

my $byname;
sub byname {
    return $byname ||
	($byname = new Unicode::Collate(table => 'ucamdirkeys.txt',
					variable => 'non-ignorable',
					preprocess => \&_preproc_byname));
}

#
# Editing
#

# Attributes that users can edit. uid/allUid can't be edited directly
use constant editattrs => qw(groupTitle description mgrGroupID visibility
                             mail userPassword rdrGroupID
                             expiryTimestamp warntimestamp);
use constant edattrhash => {map((lc($_) => 1), editattrs)};

use constant mvattrs => qw(mgrGroupID uid rdrGroupID allUid);
use constant mvattrhash => {map((lc($_) => 1), mvattrs)};

use constant rqdattrs => qw(groupTitle description mgrGroupID
                            visibility);
use constant rqdattrhash => {map((lc($_) => 1), rqdattrs)};

sub is_editable {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'attr');
    my %args = @_;
    if ($self->{dir}->{godmode}) {
        return 1;
    } elsif (!exists($self->{dir}->{viewer})) {
	return 0;
    } elsif ($self->has_manager($self->{dir}->{viewer}) ||
	      $self->{dir}->viewer_in_group_zero) {
 	return exists($args{attr}) ? $self->edattrhash->{lc($args{attr})} : 1;
    }
    return 0;
}


sub validate {
    my $self = shift;
    my %args = @_;
    my $attr = lc $args{attr};
    my @val = (ref($args{val}) eq 'ARRAY') ? @{$args{val}} : ($args{val});
    my %val;
    my @failed;

    if ($attr eq 'expirytimestamp' or
        $attr eq 'warntimestamp') {
        foreach my $date (@val) {
            unless (ref($date) and $date->isa('DateTime')) {
                my $fmt = 
                    DateTime::Format::Strptime->new(pattern=>ISO_DATE_FORMAT);
                $date = $fmt->parse_datetime($date);
                return "Invalid date format" unless $date;
            }
        }
    }

    if ($attr eq 'expirytimestamp') {
        foreach my $date (@val) {
            return "Expiry date can not be in the past"
               if $date < DateTime->now;
            return "Expiry date can not be more than a year in the future"
                if $date->delta_days(DateTime->now)->in_units('days') > 366;
        }
    }

    return $self->SUPER::validate(@_);

}

#FIXME# the next two could probably be managed internally by trapping
# changes to allUid with a replace hook, except that replace_hook is
# called _after_ the coresponding change and so can't work out what
# changed...

# Add new user(s) to uid and allUid as appropriate.
sub add_members {
    my $self = shift;
    my @ids = @_;

    # Only available to the group's managers and group zero
    permission_exception("membership addition not permitted")
        unless $self->has_manager($self->{dir}->{viewer}) or
            $self->{dir}->viewer_in_group_zero;

    my @new_alluid = uniq $self->get(attr=>'allUid', internal=>1), @ids;

    # Add to uid only unless $person->suppression
    my @new_uid = $self->get(attr=>'uid', internal=>1);
    foreach my $id (@ids) {
        if (my $person = eval { $self->{dir}->lookup_person($id) }) {
            my $suppression = $person->get(attr=>'suppression',internal=>1);
            @new_uid = uniq @new_uid,$id unless $suppression;
        }
    }
    $self->replace(attr=>'allUid',val=>\@new_alluid,internal=>1);
    $self->replace(attr=>'uid',   val=>\@new_uid,internal=>1);
}

# Remove user(s) from uid and allUid as appropriate.
sub remove_members {
    my $self = shift;
    my @ids = @_;

    # Only available to the group's managers and group zero
    permission_exception("membership removal not permitted")
        unless $self->has_manager($self->{dir}->{viewer}) or
            $self->{dir}->viewer_in_group_zero;

    my @new_alluid = $self->get(attr=>'allUid', internal=>1);
    my @new_uid    = $self->get(attr=>'uid', internal=>1);
    foreach my $id (@ids) {
         @new_alluid = grep { $_ ne $id } @new_alluid;
         @new_uid    = grep { $_ ne $id } @new_uid;
     }
    $self->replace(attr=>'allUid',val=>\@new_alluid,internal=>1);
    $self->replace(attr=>'uid',   val=>\@new_uid,internal=>1);

}

# Hide viewer by removing them from uid.
sub suppress_membership {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'uid');
    my %args = @_;

    # Only god and self can alter suppression status
    permission_exception("you can't change membership suppression")
        unless $self->{dir}->{godmode} or 
            ($self->{dir}->{viewer} and $self->{dir}->{viewer} eq $args{uid});

    my @new_uid = grep { $_ ne $args{uid} } 
        $self->get(attr=>'uid', internal=>1);
    $self->replace(attr=>'uid', val=>\@new_uid, internal=>1);
}

# Make viewer visible by adding them to uid.
sub unsuppress_membership {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'uid');
    my %args = @_;

    # Only god and self can alter suppression status
    permission_exception("you can't change membership suppression")
        unless $self->{dir}->{godmode} or 
            ($self->{dir}->{viewer} and $self->{dir}->{viewer} eq $args{uid});

    # Can only be used if uid is already a member
    permission_exception("can't un-suppress non-member of a group")
        unless grep { $_ eq $args{uid} }
            $self->get(attr=>'allUid', internal=>1);

    my @new_uid = uniq $self->get(attr=>'uid', internal=>1), $args{uid};
    $self->replace(attr=>'uid', val=>\@new_uid,internal=>1);

}

sub is_cancelable {
    my $self = shift;
    return $self->is_editable;
}

sub genid {
    my $self = shift;

    my $nextval = $self->nextid(ctr  => "cn=groupIDPool,ou=groups",
                                attr => "groupID");

    $self->replace(attr=>'groupID', val=>$nextval, internal=>1);
    my $dn = Ucam::Directory::group_dn($nextval);
    $self->{ldapentry}->dn($dn);
}



sub logattrs {
    my $self = shift;
    return (type => 'group', groupid => $self->groupID);
}

sub rdn {
    my $self = shift;
    return $self->groupID;
}

1;
