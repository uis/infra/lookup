# $Id$

use warnings;
use strict;

package Ucam::Directory::TestServer;

use Ucam::Directory::TestServerSchemas;
use File::Temp;

our $slaptest;

our @slaptests = ("/usr/sbin/slaptest -u", "/usr/sbin/slaptest");

our $slapd;

our @slapds = ("/usr/sbin/slapd", "/usr/lib/openldap/slapd");

sub testslaptest {
    foreach (@slaptests) {
	$slaptest = $_;
	return 1 unless system "$slaptest -f /dev/null >/dev/null 2>&1";
    }
    return 0;
}

our $slapdfrag;

our @slapdfrags = ("backend bdb\n",
		   "modulepath /usr/lib/ldap\nmoduleload back_bdb\nbackend bdb\n");

sub testslapd {
    foreach (@slapds) {
	$slapd = $_;
	return 1 if -x $slapd;
    }
    return 0;
}

sub testslapdconf {
    $slaptest or testslaptest or return 0;
    foreach (@slapdfrags) {
	$slapdfrag = $_;
	my $slapdconf = new File::Temp;
	print $slapdconf $_;
	close $slapdconf;
	return 1 unless system "$slaptest -f $slapdconf >/dev/null 2>&1";
    }
    return 0;
}

sub new {
    my $class = shift;
    my $self = bless({}, $class);

    $slapd or testslapd or die "can't find slapd\n";
    $slapdfrag or testslapdconf or die "can't get working slapd.conf\n";

    my $basedir = $self->{basedir} = File::Temp::tempdir;

    my $slapdconf = "$basedir/slapd.conf";

    open my $conf, "> $slapdconf" or die "$slapdconf: $!\n";
    print $conf Ucam::Directory::TestServerSchemas::core;
    print $conf Ucam::Directory::TestServerSchemas::cosine;
    print $conf Ucam::Directory::TestServerSchemas::inetorgperson;
    print $conf Ucam::Directory::TestServerSchemas::camacuk;
    print $conf $slapdfrag;
    print $conf <<EOF;

pidfile		$basedir/slapd.pid
schemacheck     off
loglevel        0
checkpoint 512 30
database        bdb
suffix          "o=University of Cambridge,dc=cam,dc=ac,dc=uk"
directory       "$basedir"
index           objectClass eq
access to attrs=userPassword
        by dn="cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk" write
        by anonymous auth
        by self write
        by * none
access to dn.base="" by * read
access to *
        by dn="cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk" write
        by * read
EOF
    close $conf or die "$slapdconf: $!\n";

    system "/usr/sbin/slapadd -l $_[0] -f $basedir/slapd.conf"
        and die "slapadd failed";

    my $url = $self->url;
    my @args = ("$slapd -f $slapdconf -h $url");
    system @args and die "slapd startup failed\n";
    sleep 5;
    return $self;
}

sub basedir {
    my $self = shift;
    return $self->{basedir};
}

sub url {
    my $self = shift;
    my $socket = $self->basedir . "/ldapi";
    (my $baseurl = $socket) =~ s!/!%2f!g;
    return "ldapi://$baseurl";
}

sub DESTROY {
    my $self = shift;
    my $basedir = $self->basedir;

    if ($basedir) {
        if (open my $pidfile, "< $basedir/slapd.pid") {
            my $pid = <$pidfile>;
            chomp($pid);
            kill TERM => $pid;
        }
        system "rm -rf $basedir";
    }
}

1;
