# $Id$

use warnings;
use strict;

package Ucam::Directory::LastUpdated;

use Ucam::Directory::Exception qw/general_exception/;

use DateTime::Format::Strptime;

use overload '""' => sub {${$_[0]}}, fallback => 1;

use constant LU_DATE_FORMAT     => "%Y-%m-%d, %H:%M %Z";
use constant ALT_LU_DATE_FORMAT => "%Y-%m-%d, %H:%M %z";

sub new {
    my $class = shift;
    my $self;
    if (@_ == 1) {
	$$self = shift;
    } else {
	my %args = @_;
	$$self = "$args{service}, $args{user}, " .
            $args{date}->strftime(LU_DATE_FORMAT);
    }
    return bless($self, $class);
}

sub service {
    my $self = shift;
    return (split(/, /, $$self))[0];
}

sub user {
    my $self = shift;
    my $dir  = shift;
    my $user = (split(/, /, $$self))[1];
    my $person = eval { $dir->lookup_person($user) } if $dir;
    return $person || $user;
}

sub date {
    my $self = shift;
    my $date = (split(/, /, $$self,3))[2];

    return undef unless $date;

    my $fmt = DateTime::Format::Strptime->new(pattern => LU_DATE_FORMAT);
    my $result = $fmt->parse_datetime($date);

    # DateTime::Format::Strftime considers 'BST' to be ambiguous, so for our
    # purposes try replacing it with an explicit offset and re-parsing
    if (!$result and $date =~ /BST$/) {
        $date =~ s/BST$/+0100/;
        my $alt_fmt = 
            DateTime::Format::Strptime->new(pattern => ALT_LU_DATE_FORMAT);
        $result = $alt_fmt->parse_datetime($date);
    }

    general_exception("invalid lastUpdated date format") unless $result;
    $result->set_time_zone('UTC');
    return $result;
}

1;
