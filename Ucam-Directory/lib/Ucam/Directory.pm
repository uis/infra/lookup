# $Id$
package Ucam::Directory;

use 5.008001;
use strict;
use warnings;

use Ucam::Directory::Exception qw/general_exception notfound_exception
    ldap_exception/;
use Ucam::Directory::Person;
use Ucam::Directory::Institution;
use Ucam::Directory::ContactRow;
use Ucam::Directory::Group;

use Net::LDAP qw/LDAP_NO_SUCH_OBJECT/;
use Net::LDAP::Entry;
use Net::LDAP::Filter;
use Net::LDAP::Util qw(escape_dn_value escape_filter_value);

require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Ucam::Directory::Entry ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw( escape_filter_value ldap_base_dn ) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw( );

our $VERSION='1.57';

use constant ldap_server_name => 'ldap.lookup.cam.ac.uk';
use constant ldap_base_dn => 'o=University of Cambridge,dc=cam,dc=ac,dc=uk';
use constant ldap_people_dn => 'ou=people,' . ldap_base_dn;
use constant ldap_insts_dn => 'ou=insts,' . ldap_base_dn;
use constant ldap_groups_dn => 'ou=groups,' . ldap_base_dn;
use constant ldap_timeout => 30;

sub new {
    my $class = shift;
    my $self = bless({}, $class);
    my %args = @_;
    my $mesg;
    if (exists $args{fake} && $args{fake}) {
	die "Use Ucam::Directory::Fake!";
    }
    my $server = $args{ldapserver} || ldap_server_name;
    my $timeout = $args{timeout}   || ldap_timeout;
    my $ldap = Net::LDAP->new($server, timeout => $timeout);
    general_exception("Failed to connect to LDAP server at $server: '$@'")
        unless $ldap;
    if ($args{cafile} or $args{capath}) {
        my %tlsarg;
        $tlsarg{cafile} = $args{cafile} if $args{cafile};
        $tlsarg{capath} = $args{capath} if $args{capath};
        $mesg = $ldap->start_tls(verify => 'require', %tlsarg);
        $mesg->is_error && ldap_exception(error => $mesg->error,
                                          ldap_message => $mesg);
    }
    if ($args{binddn}) {
	$mesg = $ldap->bind($args{binddn}, password => $args{password})
    } else {
	$mesg = $ldap->bind;
    }
    $mesg->is_error && ldap_exception(error => $mesg->error,
				      ldap_message => $mesg);
    $self->{ldap} = $ldap;
    $self->{viewer} = $args{viewer} if exists $args{viewer};
    $self->{service} = $args{service} if exists $args{service};
    $self->{log} = $args{log} if exists $args{log};
    $self->{godmode} = $args{godmode} if exists $args{godmode};
    return $self;
}

sub person_dn {
    return "uid=" . escape_dn_value($_[0]) . "," . ldap_people_dn;
}

sub inst_dn {
    return "instID=" . escape_dn_value($_[0]) . "," . ldap_insts_dn;
}

sub contactrow_dn {
    return "cn=" . escape_dn_value($_[1]) . "," .
	"instID=" . escape_dn_value($_[0]) . "," . ldap_insts_dn;
}

sub group_dn {
    return "groupID=" . escape_dn_value($_[0]) . "," . ldap_groups_dn;
}

sub viewer {
    return $_[0]->{viewer};
}

sub service {
    return $_[0]->{service};
}

sub viewer_in_group_zero {
    my $self = shift;
    return grep($_->has_member($self->viewer),
		$self->search_for_groups('(|(groupID=0)(groupID=000000))'));
}


sub lookup_person {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'uid');
    my %args = @_;
    exists($args{uid}) or general_exception("no uid provided");
    $args{uid} =~ /^[0-9a-zA-Z]+$/ or general_exception("bad uid");
    my @attrs = Ucam::Directory::Person->fastattrs;
    my $mesg =
      $self->_ldapsearch(base => person_dn($args{uid}),
			 scope => 'base',
			 filter => '(objectClass=inetOrgPerson)',
			 attrs => \@attrs);
    $mesg->is_error && $mesg->code == LDAP_NO_SUCH_OBJECT &&
	notfound_exception("uid '$args{uid}' not found");
    $mesg->is_error && ldap_exception(error => $mesg->error,
				      ldap_message => $mesg);
    if ($mesg->count == 0) {
	notfound_exception("uid '$args{uid}' not found");
    } else {
	return new Ucam::Directory::Person(dir => $self,
					   entry => $mesg->entry(0),
					   gotattrs => \@attrs);
    }
}

sub check_uids {
    my $self = shift;
    # Check that all people exist
    my @people =
	$self->search_for_people
	('(|'.join('',map("(uid=".escape_filter_value($_).')',@_)).')');
    if (@people < @_) {
	# At least one of them doesn't exist.
	# We'll have to work out which.
	my %people = map(($_->uid, 1), @people);
	return grep(!exists($people{lc($_)}), @_);
    }
    return ();
}

sub check_groupids {
    my $self = shift;
    # Check that all groups exist
    my @groups =
	$self->search_for_groups
	('(|'.join('',map("(groupid=".escape_filter_value($_).')',@_)).')');
    if (@groups < @_) {
	# At least one of them doesn't exist.
	# We'll have to work out which.
	my %groups = map(($_->groupID, 1), @groups);
	return grep(!exists($groups{$_}), @_);
    }
    return;
}

sub create_person {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'uid');
    my %args = @_;
    exists($args{uid}) or general_exception("no uid provided");
    $args{uid} =~ /^[0-9a-zA-Z]+$/ or general_exception("bad uid");
    my $entry = new Net::LDAP::Entry;
    $entry->dn(person_dn($args{uid}));
    $entry->add(objectClass=>['inetOrgPerson', 'camAcUkPerson']);
    $entry->add(uid=>$args{uid});
    my $person = new Ucam::Directory::Person(dir => $self,
					     entry => $entry);
    # We set this again to ensure that the Person object knows that it's
    # been changed.
    $person->{changed}->{uid} = [$args{uid}];
    return $person;
}

sub lookup_inst {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'instid');
    my %args = @_;
    exists($args{instid}) or general_exception("no instid provided");
    $args{instid} =~ /^[0-9a-zA-Z]+$/ or general_exception("bad instid");
    my @attrs = Ucam::Directory::Institution->fastattrs;
    my $mesg =
      $self->_ldapsearch(base => inst_dn($args{instid}),
			 scope => 'base',
			 filter => '(objectClass=organizationalUnit)',
			 attrs => \@attrs);
    $mesg->is_error && $mesg->code == LDAP_NO_SUCH_OBJECT &&
	notfound_exception("instid '$args{instid}' not found");
    $mesg->is_error && ldap_exception(error => $mesg->error,
				      ldap_message => $mesg);
    if ($mesg->count == 0) {
	notfound_exception("instid '$args{instid}' not found");
    } else {
	return new Ucam::Directory::Institution(dir => $self,
						entry => $mesg->entry(0),
						gotattrs => \@attrs);
    }
}

sub create_inst {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'instid');
    my %args = @_;
    exists($args{instid}) or general_exception("no instid provided");
    $args{instid} =~ /^[0-9a-zA-Z]+$/ or general_exception("bad instid");
    my $entry = new Net::LDAP::Entry;
    $entry->dn(inst_dn($args{instid}));
    $entry->add(objectClass=>['organizationalUnit',
			      'camAcUkOrganizationalUnit']);
    $entry->add(instid=>$args{instid});
    my $inst = new Ucam::Directory::Institution(dir => $self,
						entry => $entry);
    # We set this again to ensure that the Institution object knows that it's
    # been changed.
    $inst->{changed}->{instid} = [$args{instid}];
    return $inst;
}

sub lookup_contactrow {
    my $self = shift;
    my %args = @_;
    exists($args{instid}) or general_exception("no instid provided");
    $args{instid} =~ /^[0-9a-zA-Z]+$/ or general_exception("bad instid");
    exists($args{cn}) or general_exception("no cn provided");
    $args{cn} =~ /^[0-9a-zA-Z]+$/ or general_exception("bad cn");
    my @attrs = Ucam::Directory::ContactRow->fastattrs;
    my $mesg =
      $self->_ldapsearch(base => contactrow_dn($args{instid},$args{cn}),
			 scope => 'base',
			 filter => '(objectClass=contactRow)',
			 attrs => \@attrs);
    $mesg->is_error && $mesg->code == LDAP_NO_SUCH_OBJECT &&
	notfound_exception("contactrow '$args{cn}' of " .
			   "instid '$args{instid}' not found");
    $mesg->is_error && ldap_exception(error => $mesg->error,
				      ldap_message => $mesg);
    if ($mesg->count == 0) {
	notfound_exception("contactrow '$args{cn}' of " .
			   "instid '$args{instid}' not found");
    } else {
	return new Ucam::Directory::ContactRow(dir => $self,
					       entry => $mesg->entry(0),
					       gotattrs => \@attrs);
    }
}

sub create_contactrow {
    my $self = shift;
    my $entry = new Net::LDAP::Entry;
    $entry->add(objectClass=>['contactRow']);
    $entry->add(cn=>"");
    my $person = new Ucam::Directory::ContactRow(dir => $self,
						 entry => $entry);
    # We set this again to ensure that the object knows that it's
    # been changed.
    $person->{changed}->{cn} = [""];
    return $person;
}

sub lookup_group {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'groupid');
    my %args = @_;
    exists($args{groupid}) or general_exception("no groupid provided");
    $args{groupid} =~ /^[0-9]+$/ or general_exception("bad groupid");
    my @attrs = Ucam::Directory::Group->fastattrs;
    my $mesg =
      $self->_ldapsearch(base => group_dn($args{groupid}),
			 scope => 'base',
			 filter => '(objectClass=camAcUkGroup)',
			 attrs => \@attrs);
    $mesg->is_error && $mesg->code == LDAP_NO_SUCH_OBJECT &&
	notfound_exception("groupid '$args{groupid}' not found");
    $mesg->is_error && ldap_exception(error => $mesg->error,
				      ldap_message => $mesg);
    if ($mesg->count == 0) {
	notfound_exception("groupid '$args{groupid}' not found");
    } else {
	return new Ucam::Directory::Group(dir => $self,
						entry => $mesg->entry(0),
						gotattrs => \@attrs);
    }
}

sub create_group {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'groupid');
    my %args = @_;
    exists($args{groupid}) and general_exception("giving groupid not allowed");
    my $entry = new Net::LDAP::Entry;
    $entry->add(objectClass=>['camAcUkGroup']);
    my $group = new Ucam::Directory::Group(dir => $self,
					   entry => $entry);
    # We set this again to ensure that the Group object knows that it's
    # been changed.
    $group->{changed}->{groupid} = [""];
    return $group;
}

sub get_contactrows {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'instid');
    my %args = @_;
    exists($args{instid}) or general_exception("no instid provided");
    $args{instid} =~ /^[0-9a-zA-Z]+$/ or general_exception("bad instid");
    my $mesg =
	$self->_ldapsearch(base => inst_dn($args{instid}),
			   scope => 'one',
			   filter => '(objectClass=contactRow)');
    $mesg->is_error && ldap_exception(error => $mesg->error,
				      ldap_message => $mesg);
    return sort { $a->collationOrder <=> $b->collationOrder }
      map(new Ucam::Directory::ContactRow(dir => $self, entry => $_),
	  $mesg->entries);
}

# Given a Net::LDAP::Entry, fetch a new copy with all its attributes.
sub fillup_attrs {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'entry');
    my %args = @_;
    my $mesg =
	$self->_ldapsearch(base => $args{entry}->dn,
			   scope => 'base',
			   filter => '(objectClass=*)');
    $mesg->is_error && ldap_exception(error => $mesg->error,
				      ldap_message => $mesg);
    $mesg->count == 1 or die general_error("base LDAP lookup went strange");
    return $mesg->entry(0);
}

sub search_for_entries {
    my $self = shift;
    my %args = @_;
    my $class = $args{class};
    exists($args{filter}) or general_exception("no filter provided");
    my($filter) = new Net::LDAP::Filter($args{filter});
    defined($filter) or general_exception("bad filter '$args{filter}'");
    my @attrs = $class->fastattrs;
    my $mesg =
      $self->_ldapsearch(base => $class->ldap_base_dn,
			 scope => 'one',
			 filter => '(&'.$class->ldap_filter.$filter->as_string .')',
			 attrs => \@attrs);
    $mesg->is_error && ldap_exception(error => $mesg->error,
				      ldap_message => $mesg);
    return map($class->new(dir => $self, entry => $_, gotattrs => \@attrs),
	       $mesg->entries);
}

sub search_for_people {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'filter');
    return $self->search_for_entries(class => "Ucam::Directory::Person", @_);
}

sub search_for_insts {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'filter');
    return $self->search_for_entries(class => "Ucam::Directory::Institution", @_);
}

sub search_for_groups {
    my $self = shift;
    (@_ == 1) and unshift(@_, 'filter');
    return $self->search_for_entries(class => "Ucam::Directory::Group", @_);
}

sub _ldapsearch {
    my $self = shift;
    return $self->{ldap}->search(@_);
}

# Preloaded methods go here.

1;
__END__
=head1 NAME

Ucam::Directory - Perl module abstracting a connection to the Directory

=head1 SYNOPSIS

 use Ucam::Directory qw/escape_filter_value/;
 
 $dir = new Ucam::Directory
    (ldapserver => "cantor.csi.cam.ac.uk",
     binddn =>
       "cn=fjc55,ou=people,o=University of Cambridge,dc=cam,dc=ac,dc=uk",
     password => "secret", viewer => "fjc55");
 
 $fakedir = new Ucam::Directory::Fake;
 
 $person = $dir->lookup_person('fjc55');
 
 $inst = $dir->lookup_inst('CS');
 
 @people = $dir->search_for_people('(InstID=CS)');
 
 @insts = $dir->search_for_insts('(parentInstID=' .
				 escape_filter_value('CS') . ')');
 
  use Ucam::Directory;
  blah blah blah

=head1 DESCRIPTION

A Ucam::Directory object encapsulates a connection to the
Directory.  Just calling C<new> gets you an anonymous
connection, but you can also specify more useful things.

One operation on a directory is to look up a person by CRSID, using
the C<lookup_person> method.  It throws
L<Ucam::Directory::Exception>::notfound if the person doesn't exist.

Similarly, institutions can be looked up using C<lookup_inst>.

General searches for people and institutions can be done using
C<search_for_people>, C<search_for_insts>, and
C<search_for_groups>.  Each one takes an LDAP text-format
filter, and returns a (possibly-empty) list of
L<Ucam::Directory::Person>, L<Ucam::Directory::Institution>, or
L<Ucam::Directory::Group> objects as appropriate.  See
L<Net::LDAP::Filter> for details of the LDAP filter syntax.
C<escape_filter_value> can be used to quote metacharacters in
strings being filtered on.  It's imported directly from
L<Net::LDAP::Util>.

A fake directory, which can only cope with certain queries, is
available by creating a Ucam::Directory::Fake instead of a
Ucam::Directory.  This is useful for testing.

=head2 EXPORT

None by default.  C<escape_filter_value> is exportable.

=head1 SEE ALSO

L<https://intranet.csi.cam.ac.uk/index.php/Ucam::Directory>

=head1 AUTHOR

Ben Harris, E<lt>bjh21@cam.ac.ukE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2005 Ben Harris.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself. 

=cut
