# $Id$
# -*- cperl -*-
# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 46;

use Ucam::Directory::Person;
use Net::LDAP::Entry;

use Encode;
use utf8;

use Unicode::Collate;

sub newent {
    my $entry = new Net::LDAP::Entry;
    $entry->add(map(encode_utf8($_), @_));
    return new Ucam::Directory::Person($entry);
}

# Tests of the (internal) preprocessor

sub is_preproc {
    my $got = Ucam::Directory::Person::_preproc_byname(newent(@{$_[0]}), 1);
    is($got, $_[1], $_[2]);
}

is_preproc([sn=>"Smith", cn=>"J. Smith"], "Smith\tJ.");
is_preproc([sn=>"St John", cn=>"J. St John"], "Saint John\tJ.");
is_preproc([sn=>"McCall", cn=>"J. McCall"], "MacCall\tJ.");
is_preproc([sn=>"Mcz", cn=>"J. Mcz"], "Mcz\tJ.");
is_preproc([sn=>"Strange", cn=>"J. Strange"], "Strange\tJ.");
is_preproc([sn=>"God", cn=>"Prof. J. God"], "God\tJ.");
is_preproc([sn=>"God", cn=>"Dr Q. God"], "God\tQ.");

sub sorted {
    my @u = map(newent(@$_), @_);
    my @s = $c->sort(@u);
    is_deeply(\@s, \@u);
}

ok($c = Ucam::Directory::Person->byname);
sorted([sn=>"Harris", cn=>"A. Harris"],
       [sn=>"Harry", cn=>"A. Harry"]);
sorted([sn=>"Harris", cn=>"A. Harris"],
       [sn=>"Harris", cn=>"B. Harris"]);
sorted([sn=>"Harris", cn=>"A. Harris"],
       [sn=>"Harris", cn=>"A. Z. Harris"]);
sorted([uid=>"bjh21"],
       [uid=>"zzh21"]);
sorted([uid=>"zzh20"],
       [uid=>"abck"]);
sorted([sn=>"Harris", cn=>"B.J. Harris", displayName=>"Ben Harris"],
       [sn=>"Harris", cn=>"B. Harris", displayName=>"Bill Harris"]);
sorted([sn=>"Harris", cn=>"B.J. Harris"],
       [sn=>"Harris", cn=>"B. Harris", displayName=>"Bill Harris"]);
sorted([sn=>"Harri", cn=>"Z. Harri"],
       [sn=>"Harris", cn=>"B. Harris"]);
sorted([sn=>"Griffiths", cn=>"Fred Griffiths"],
       [uid=>"bjh21", displayName=>"Ben Harris"],
       [sn=>"Harris", cn=>"B.J. Harris"]);
sorted([sn=>"Muller", cn=>"A. Muller"],
       [sn=>"Müller", cn=>"B. Müller"],
       [sn=>"Muller", cn=>"C. Muller"]);
sorted([sn=>"Smith", cn=>"Prof. A.A. Smith"],
       [sn=>"Smith", cn=>"A.B. Smith"],
       [sn=>"Smith", cn=>"Dr A.C. Smith"]);
sorted([sn=>"Smith", cn=>"Z. Smith"],
       [sn=>"Smith Jones", cn=>"A. Smith Jones"]);
sorted([sn=>"McCall Smith", cn=>"A. McCall Smith"],
       [sn=>"Mayo Smith", cn=>"A. Mayo Smith"]);
sorted([sn=>"McDermott", cn=>"H. McDermott"],
       [sn=>"Macdonald", cn=>"D.W. Macdonald"],
       [sn=>"McKimmie", cn=>"C. McKimmie"]);
sorted([sn=>"ten Wolde", cn=>"M. ten Wolde"],
       [sn=>"Yellop", cn=>"K.W. Yellop"]);
sorted([sn=>"A", cn=>"A. A"],
       [sn=>"B", cn=>"A. B"]);
sorted([sn=>"a", cn=>"a. a"],
       [sn=>"B", cn=>"A. B"]);


ok($c = Ucam::Directory::Person->byregname);
sorted([sn=>"Harris", cn=>"A. Harris"],
       [sn=>"Harry", cn=>"A. Harry"]);
sorted([sn=>"Harris", cn=>"A. Harris"],
       [sn=>"Harris", cn=>"B. Harris"]);
sorted([sn=>"Harris", cn=>"A. Harris"],
       [sn=>"Harris", cn=>"A. Z. Harris"]);
sorted([uid=>"bjh21"],
       [uid=>"zzh21"]);
sorted([uid=>"zzh20"],
       [uid=>"abck"]);
sorted([sn=>"Harris", cn=>"B. Harris", displayName=>"Bill Harris"],
       [sn=>"Harris", cn=>"B.J. Harris", displayName=>"Ben Harris"]);
sorted([sn=>"Harris", cn=>"B. Harris", displayName=>"Bill Harris"],
       [sn=>"Harris", cn=>"B.J. Harris"]);
sorted([sn=>"Harri", cn=>"Z. Harri"],
       [sn=>"Harris", cn=>"B. Harris"]);
sorted([sn=>"Griffiths", cn=>"Fred Griffiths"],
       [uid=>"bjh21", displayName=>"Ben Harris"],
       [sn=>"Harris", cn=>"B.J. Harris"]);
sorted([sn=>"Muller", cn=>"A. Muller"],
       [sn=>"Müller", cn=>"B. Müller"],
       [sn=>"Muller", cn=>"C. Muller"]);
sorted([sn=>"Smith", cn=>"Prof. A.A. Smith"],
       [sn=>"Smith", cn=>"A.B. Smith"],
       [sn=>"Smith", cn=>"Dr A.C. Smith"]);
sorted([sn=>"Smith", cn=>"Z. Smith"],
       [sn=>"Smith Jones", cn=>"A. Smith Jones"]);
sorted([sn=>"McCall Smith", cn=>"A. McCall Smith"],
       [sn=>"Mayo Smith", cn=>"A. Mayo Smith"]);
sorted([sn=>"McDermott", cn=>"H. McDermott"],
       [sn=>"Macdonald", cn=>"D.W. Macdonald"],
       [sn=>"McKimmie", cn=>"C. McKimmie"]);
sorted([sn=>"ten Wolde", cn=>"M. ten Wolde"],
       [sn=>"Yellop", cn=>"K.W. Yellop"]);
sorted([sn=>"A", cn=>"A. A"],
       [sn=>"B", cn=>"A. B"]);
sorted([sn=>"a", cn=>"a. a"],
       [sn=>"B", cn=>"A. B"]);

ok($c = Ucam::Directory::Person->byuid);
sorted([uid=>"bjh21"],
       [uid=>"zzh21"]);
sorted([uid=>"abck"],
       [uid=>"zzh20"]);
