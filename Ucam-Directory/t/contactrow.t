# $Id$
# -*- cperl -*-
# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 19;

use Ucam::Directory;
use Ucam::Directory::Fake;
use Net::LDAP::Entry;

use Encode;
use utf8;

BEGIN {
    use_ok('Ucam::Directory::ContactRow');
};

$entry = new Net::LDAP::Entry;
$entry->add(objectClass => ["contactRow"],
	    cn => ["20"],
	    collationOrder => ["20"],
	    instID => ["CS"],
	    rowStyle => ["bold"],
            rowTitle => ["Director"],
            uid => ["ijl20"]);

ok($cr = new Ucam::Directory::ContactRow($entry), "new object");
isa_ok($cr, 'Ucam::Directory::ContactRow');

# Contact rows in directory
$dir = new Ucam::Directory::Fake;

ok(@rows = $dir->get_contactrows("CS"), "look up rows");
is(scalar(@rows), 11);
isa_ok($rows[0], 'Ucam::Directory::ContactRow');
is_deeply([map($_->collationOrder, @rows)],
	  [20, 42, 50, 56, 58, 59, 60, 61, 62, 63, 67]);

is_deeply([$dir->get_contactrows(instid => "THING")], [], "failed lookup");

# Contact rows attached to an institution

$inst = $dir->lookup_inst('CS');

ok(@rows = $inst->contactRow, "get rows from inst");
is(scalar(@rows), 11);
isa_ok($rows[0], 'Ucam::Directory::ContactRow');
is_deeply([map($_->collationOrder, @rows)],
	  [20, 42, 50, 56, 58, 59, 60, 61, 62, 63, 67]);
ok(@direct = $rows[0]->people);
isa_ok($direct[0], 'Ucam::Directory::Person');
ok($direct = $rows[0]->people);
isa_ok($direct, 'Ucam::Directory::Person');

ok($row = $inst->contactRow($rows[0]->cn));
is($row->cn, $rows[0]->cn);
is($row->rdn, $rows[0]->cn);


