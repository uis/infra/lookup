# $Id$ -*- cperl -*-
# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 6;
use Test::Exception;

BEGIN { use_ok('Ucam::Directory', qw(escape_filter_value)); };
BEGIN { use_ok('Ucam::Directory::Fake'); };

# ok($dir = new Ucam::Directory);
# isa_ok($dir, 'Ucam::Directory');

throws_ok { new Ucam::Directory(ldapserver => 'nonsuch.invalid') }
                               qr/failed to connect/i;

ok($fakedir = new Ucam::Directory::Fake);
isa_ok($fakedir, 'Ucam::Directory');

is(escape_filter_value('foo'), 'foo', 'escape_filter_value');
