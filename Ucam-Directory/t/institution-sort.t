# $Id$
# -*- cperl -*-
# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 2;

use Ucam::Directory;
use Ucam::Directory::Fake;
use Ucam::Directory::Institution;
use Net::LDAP::Entry;

use Encode;
use utf8;

use Unicode::Collate;

sub newent {
    my $entry = new Net::LDAP::Entry;
    $entry->add(map(encode_utf8($_), @_));
    return new Ucam::Directory::Institution($entry);
}

sub sorted {
    my @u = map(newent(ou=>$_), @_);
    my @s = $c->sort(@u);
    is_deeply(\@s, \@u);
}

ok($c = Ucam::Directory::Institution->byname);
sorted("Faculty of That", "Department of This");
