# -*- perl -*-
# $Id$

use Test::More tests => 25;

use_ok('Ucam::Directory::LabeledPhone');
ok($lu = new Ucam::Directory::LabeledPhone('34728'));
isa_ok($lu, 'Ucam::Directory::LabeledPhone');
is("$lu", "34728");
is($lu->phone, "34728");
is($lu->label, undef);

ok($lu = new Ucam::Directory::LabeledPhone('34728 (Unix Support)'));
is("$lu", "34728 (Unix Support)");
is($lu->phone, "34728");
is($lu->label, "Unix Support");

ok($lu = new Ucam::Directory::LabeledPhone(phone => "(01223) 334728",
					   label => "Unix Support"));
is("$lu", "(01223) 334728 (Unix Support)");
is($lu->phone, "(01223) 334728");
is($lu->label, "Unix Support");

ok($lu = new Ucam::Directory::LabeledPhone(phone => "(01223) 334728",
					   label => undef));
is("$lu", "(01223) 334728");
is($lu->phone, "(01223) 334728");
is($lu->label, undef);

eval {
  new Ucam::Directory::LabeledPhone(phone => "(01223) 334728",
				    label => "mis(matched");
};
is ($@, 'unbalanced parentheses','mismatched open');

eval {
  new Ucam::Directory::LabeledPhone(phone => "(01223) 334728",
				    label => "mis)matched");
};
is ($@, 'unbalanced parentheses', 'mismatched close');

eval {
  new Ucam::Directory::LabeledPhone(phone => "(01223) 334728)",
				    label => "stuff");
};
is ($@ , 'close-paren at end of phone number', 'trailing close paren');

ok($lu = new Ucam::Directory::LabeledPhone(phone => "(01223) 334728",
					   label => "(stuff in brackets)"),
  'ballanced parens');
is("$lu", "(01223) 334728 ((stuff in brackets))");
is($lu->phone, "(01223) 334728");
is($lu->label, "(stuff in brackets)");
