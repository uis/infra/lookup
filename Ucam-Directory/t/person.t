# $Id$
# -*- cperl -*-
# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 62;

use Ucam::Directory;
use Ucam::Directory::Fake;
use Net::LDAP::Entry;

use Encode;
use utf8;

BEGIN {
    use_ok('Ucam::Directory::Person', qw(map_title));
};

$entry = new Net::LDAP::Entry;
$entry->add(objectClass => ["camAcUkPerson", "inetOrgPerson"],
	    instID => ["CS", "CORPUS"],
	    libraryCardNumber => ["VABCD"],
	    displayName => [encode_utf8("Fred Smïth")],
	    uid => ["frs99"],
	    mail => ['frs99@cam.ac.uk'],
	    mailAlternative => ['frs99@cus.cam.ac.uk', 'fred@hotmail.com'],
	    telephoneNumber => ['34567', '567890'],
	    postalAddress => ['\55\6eiversity of Cambridge$Computing Service$Pembroke Street$CAMBRIDGE$CB2  3QH'],
	    sn => [encode('iso8859-1', 'Smïth')], # Deliberately bad
	    cn => ['F.R. Smith'],
	    homePhone => ['+44 1223 654321'],
            suppressAttribute => ['postalAddress', 'uid'],
            jdName => ['Smith F.R.'],
            jdInst => ['CS'],
            jdCamMail => ['Y'],
            jdExDir => ['N'],
            jdColl => ['BOT'],
            misAffiliation => ['staff','student'],
            );

ok($person = new Ucam::Directory::Person($entry), "new object");
isa_ok($person, 'Ucam::Directory::Person');

is($person->get(attr => "uid"), "frs99", "single-value");
is($person->rdn,"frs99");
is($person->get(attr => "instID"), "CS", "multi-value scalar");
is_deeply([$person->get(attr => "instID")], ['CS', 'CORPUS'],
	  "multi-value list");
is($person->get(attr => "postalAddress"), "University of Cambridge\n" .
   "Computing Service\nPembroke Street\nCAMBRIDGE\nCB2  3QH",
   "postal address");
eval { $person->get(attr => "homePhone"); }; ok($@, "exception on bad attr");
eval { $person->homePhone; }; ok($@, "exception with convenience method");

ok(!defined($person->get(attr => "labeledURI")), "no value scalar");
is_deeply([$person->get(attr => "labeledURI")], [], "no value list");

is($person->mail, 'frs99@cam.ac.uk', "convenience scalar");
is_deeply([$person->instID], ['CS', 'CORPUS'], "convenience list");

is($person->displayName, "Fred Smïth", "unicode attr");
eval { $person->sn; }; ok($@, "bad UTF-8 from server");

is($person->name, "Fred Smïth", "name pseudo-attr");
is_deeply([$person->allmail], ['frs99@cam.ac.uk', 'frs99@cus.cam.ac.uk',
	'fred@hotmail.com'], "allmail pseudo-attr");

is_deeply([$person->jdName], ['Smith F.R.'], "jdName");
is_deeply([$person->jdInst], ['CS'], "jdInst");
is_deeply([$person->jdCamMail], ['Y'], "jdCamMail");
is_deeply([$person->jdExDir], ['N'], "jdExDir");
is_deeply([$person->jdColl], ['BOT'], "jdColl");

is_deeply([$person->misAffiliation], ['staff','student'], "Status");

$dir = new Ucam::Directory::Fake(viewer => "frs98");

ok($person = $dir->lookup_person(uid => "frs99"), "look up person");
isa_ok($person, 'Ucam::Directory::Person');
is_deeply([$person->instID], ['CS', 'CORPUS'], "default attr");
ok($person->jpegPhoto, "get non-default attr");
is_deeply([$person->postalAddress], [], "suppressed attr");
is_deeply([$person->get(attr=>'postalAddress')], [], "suppressed attr");

is_deeply([$person->postalAddress], [], "suppressed attr");
is_deeply([$person->telephoneNumber], ['34567', '567890'],
	"unsuppressed attr");
is_deeply([$person->uid], ['frs99'], "unsuppressed attr");

is($person->jdName, 'Smith F.R.', "jdName 1");
is_deeply([$person->jdName], ['Smith F.R.'], "jdName 2");
is($person->jdInst, 'CS', "jdInst 1");
is_deeply([$person->jdInst], ['CS'], "jdInst 2");
is_deeply([$person->jdCamMail], ['Y'], "jdCamMail");
is_deeply([$person->jdExDir], ['N'], "jdExDir");
is_deeply([$person->misAffiliation], ['staff','student'], "Status");

my @insts = $person->jdinsts;
is(scalar(@insts), 1, "right number of insts");
isa_ok($insts[0],'Ucam::Directory::Institution');
is(($insts[0])->instID,'CS',"institution is CS");

$dir = new Ucam::Directory::Fake(viewer => "frs99");
ok($person = $dir->lookup_person(uid => "frs99"), "look up person");
is_deeply([$person->postalAddress], ["University of Cambridge\n" .
   "Computing Service\nPembroke Street\nCAMBRIDGE\nCB2  3QH"],
	"suppressed attr as self");
ok($person->is_suppressed('postalAddress'));
ok(!$person->is_suppressed('uid'));

eval { $dir->lookup_person(uid => "spqr1") }; ok($@, "failed lookup");

ok(@insts = $person->insts, "Person::insts");
isa_ok($insts[0], Ucam::Directory::Institution);
is(scalar(@insts), 2, "right number of insts");
ok($inst = $person->insts, "Person::insts scalar");
isa_ok($inst, Ucam::Directory::Institution);
is_deeply([$person->insts], [$person->get('insts')]);

is($person->attrdesc("labeledURI"), "Web page");
is($person->attrdesc("labeleduri"), "Web page");
is($person->attrdesc(attr=>"labeleduri",plural=>0), "Web page");
is(Ucam::Directory::Person->attrdesc(attr=>"postalAddress",plural=>1),
   "Addresses");
is($person->attrdesc("cn"), "Registered name");

ok(map_title('Prof'));
ok(!map_title('Muppet'));

ok($person->as_vcard);
