# $Id$ -*- cperl -*-
# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 117;
use Test::Exception;

use Ucam::Directory;
use Ucam::Directory::Fake;
use Ucam::Directory::Person;
use Net::LDAP::Entry;

use Encode;
use utf8;

$jpg400x1 = unpack('u', <<'end');
M_]C_X``02D9)1@`!`0$`2`!(``#_X0`617AI9@``34T`*@````@```````#_
M_@`70W)E871E9"!W:71H(%1H92!'24U0_]L`0P`%`P0$!`,%!`0$!04%!@<,
M"`<'!P</"PL)#!$/$A(1#Q$1$Q8<%Q,4&A41$1@A&!H='1\?'Q,7(B0B'B0<
M'A\>_]L`0P$%!04'!@<."`@.'A01%!X>'AX>'AX>'AX>'AX>'AX>'AX>'AX>
M'AX>'AX>'AX>'AX>'AX>'AX>'AX>'AX>'AX>_\``$0@``0&0`P$B``(1`0,1
M`?_$`!4``0$````````````````````(_\0`%!`!````````````````````
M`/_$`!0!`0````````````````````#_Q``4$0$`````````````````````
M_]H`#`,!``(1`Q$`/P"RP```````````````````````````````````````
*``````````?_V0``
`
end
$jpg1x1 = unpack('u', <<'end');
M_]C_X``02D9)1@`!`0$`2`!(``#_X0`617AI9@``34T`*@````@```````#_
M_@`70W)E871E9"!W:71H(%1H92!'24U0_]L`0P`%`P0$!`,%!`0$!04%!@<,
M"`<'!P</"PL)#!$/$A(1#Q$1$Q8<%Q,4&A41$1@A&!H='1\?'Q,7(B0B'B0<
M'A\>_]L`0P$%!04'!@<."`@.'A01%!X>'AX>'AX>'AX>'AX>'AX>'AX>'AX>
M'AX>'AX>'AX>'AX>'AX>'AX>'AX>'AX>'AX>_\``$0@``0`!`P$B``(1`0,1
M`?_$`!4``0$````````````````````(_\0`%!`!````````````````````
M`/_$`!0!`0````````````````````#_Q``4$0$`````````````````````
3_]H`#`,!``(1`Q$`/P"RP`?_V0``
`
end

sub groups_are {
    my ($person,$expected) = @_;

    my $ok = 1;
    my %found_groups;
    my %expected_groups = map { $_ => 1 } @$expected;
    foreach my $group ($person->groups) {
        $found_groups{$group->groupID} = 1;
        unless (defined($expected_groups{$group->groupID})) {
            diag "groups_are: ", $group->groupID, " found but not expected";
            $ok = 0;
        }
    }
    foreach my $group (@$expected) {
        unless (defined($found_groups{$group})) {
            diag "groups_are: $group expected but not foud";
            $ok = 0;
        }
    }

    return $ok;
}


$dir = new Ucam::Directory::Fake(viewer => "frs98", service=>"test");
$person = $dir->lookup_person(uid => "frs99");
ok(!$person->is_editable('displayName'));
ok(!$person->is_editable('uid'));
ok(!$person->is_editable);
ok(!$person->is_cancelable);
ok(!$person->is_suppressible);
ok(!$person->is_suppressible('displayName'));
ok(!$person->is_suppressible('sn'));
ok(!$person->is_suppressible('uid'));

$dir = new Ucam::Directory::Fake(viewer => "frs99", service => "test");
$person = $dir->lookup_person(uid => "frs99");
ok($person->is_editable('displayName'));
ok($person->is_editable('suppressAttribute'));
ok(!$person->is_editable('uid'));
ok(!$person->is_editable('jdName'));
ok(!$person->is_editable('jdInst'));
ok(!$person->is_editable('jdCamMail'));
ok(!$person->is_editable('jdExDir'));
ok(!$person->is_editable('misAffiliation'));
ok($person->is_editable);
ok(!$person->is_cancelable);
ok($person->is_suppressible);
ok($person->is_suppressible('displayName'));
ok($person->is_suppressible('sn'));
ok(!$person->is_suppressible('uid'));
ok($person->is_suppressible('jdName'));
ok(!$person->is_suppressible('jdInst'));
ok($person->is_suppressible('jdCamMail'));
ok($person->is_suppressible('jdExDir'));
ok(!$person->is_suppressible('misAffiliation'));

ok(!$person->validate(attr=>'displayName', val=>['Fred Smith']));
ok($person->validate(attr=>'displayName', val=>['foo', 'bar']));
ok(!$person->validate(attr=>'postalAddress', val=>['foo', 'bar']));
ok($person->validate(attr=>'suppressAttribute',
	val=>['displayName', 'uid', 'mail']));
ok(!$person->validate(attr=>'suppressAttribute',
	val=>['displayName']));
ok($person->validate(attr=>'postalAddress', val=>['foo', 'foo']));
ok($person->validate(attr=>'jpegPhoto', val=>"foo"));
ok($person->validate(attr=>'jpegPhoto', val=>$jpg400x1));
ok(!$person->validate(attr=>'jpegPhoto', val=>$jpg1x1));
ok(!$person->validate(attr => 'displayName', val => '벤 하룃'));
ok($person->validate(attr => 'mail', val => '벤 하룃'));
ok($person->validate(attr => 'allmail', val => '벤 하룃'));
ok(!$person->validate(attr => 'mail', val => decode("utf8", 'foo@bar')));
ok(!$person->validate(attr => 'mail', val => 'bjh21@cam.ac.uk'));
ok($person->validate(attr => 'mail', val => "foo"));
ok(!$person->validate(attr => 'labeledURI', val => 'http://www.cam.ac.uk'));
ok($person->validate(attr => 'labeledURI', val => 'javascript:hello'));

ok(!$person->validate(attr => 'displayName', val => '벤' x 1024));
ok($person->validate(attr => 'displayName', val => '벤' x 1025));
ok(!$person->validate(attr => 'displayName', val => '!' x 1024));
ok($person->validate(attr => 'displayName', val => '!' x 1025));

ok(!$person->validate(attr => 'misAffiliation', val => 'staff'));
ok(!$person->validate(attr => 'misAffiliation', val => 'student'));
ok(!$person->validate(attr => 'misAffiliation', val => ['staff','student']));
ok($person->validate(attr => 'misAffiliation', val => 'muppet'));
ok($person->validate(attr => 'misAffiliation', val => ['muppet','student']));

is_deeply([$person->suppressAttribute], ['postalAddress']);
$person->suppress('postalAddress');
is_deeply([$person->suppressAttribute], ['postalAddress']);
$person->suppress('telephoneNumber');
is_deeply([$person->suppressAttribute], ['postalAddress', 'telephoneNumber']);
$person->unsuppress('postalAddress');
is_deeply([$person->suppressAttribute], ['telephoneNumber']);
$person->suppress('allmail');
is_deeply([$person->suppressAttribute],
	['telephoneNumber', 'mail', 'mailAlternative']);

$person->suppress('suppressAttribute');
lives_ok {
    $person->suppress('suppressAttribute');
} 'resuppressing suppressAttribute';

$person->replace(attr=>'displayName', val=>['Frod Smoth']);
$person->replace(attr=>'postalAddress', val=>["a\nb\nc", "d\ne\nf"]);
is($person->displayName, "Frod Smoth");
is_deeply([$person->postalAddress], ["a\nb\nc", "d\ne\nf"]);
$person->replace(attr=>'displayName', val=>undef);
is_deeply([$person->displayName], []);
ok($person->lastUpdated =~
   /^test, frs99, \d\d\d\d-\d\d-\d\d, \d\d\:\d\d ...$/);

use Ucam::Directory::TestServer;

$server = eval { new Ucam::Directory::TestServer("t/fakedir.ldif") };

SKIP: {
    skip "Can't start test server - slapd not available?", 54, if !$server;

    ok($dir = new Ucam::Directory(ldapserver => $server->url, viewer => 'x200',
                                  service => "test",
                                  binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage"));
    ok($person = $dir->lookup_person('x200'));
    ok(!$person->is_suppressed('cn'));
    $person->suppress('cn');
    $person->replace(attr => 'suppression', val => 'on');
    $person->replace(attr => 'displayName', val => '벤 하룃');
    lives_ok {$person->commit};
    ok($person = $dir->lookup_person('x200'));
    ok($person->is_suppressed('cn'));
    is($person->suppression, 'on');
    is($person->displayName, '벤 하룃');
    # Check that a null change doesn't throw an exception
    $person->suppress('cn');
    lives_ok {$person->commit};
    pass;
    ok(groups_are($person,["100656","200656"])); # viewer x200, person x200

    ok($person = $dir->lookup_person('x200'));
    $person->replace(attr=>'displayName', val=>'Jon foo Warbrick');
    $person->jpegPhoto;
    is($person->displayName, 'Jon foo Warbrick');

    $person->replace(attr=>'allmail',
                     value=>['foo@example.com', 'bar@example.com']);
    is($person->mail, 'foo@example.com');
    is($person->mailAlternative, 'bar@example.com');
    $person->replace(attr=>'allmail', value=>['foo@example.com']);
    is($person->mail, 'foo@example.com');
    is_deeply([$person->mailAlternative], []);
    $person->replace(attr=>'allmail', value=>[]);
    is($person->mail, undef);
    is_deeply([$person->mailAlternative], []);

    ok($person = $dir->lookup_person('x201'));
    ok(!$person->is_editable);
    ok(!$person->is_cancelable);
    lives_ok {$person->commit};

    ok(groups_are($person,["100656"])); # viewer x200, person x201

    ok($person = $dir->lookup_person('fjc55'));
    ok($person->is_editable);
    ok(!$person->is_cancelable);
    ok(!$person->is_editable('suppressAttribute'));

    ok(groups_are($person,[]));

    ok($person = $dir->lookup_person('x212'));
    ok(groups_are($person,["123456"])); # viewer x200, person x212, vis cam
        ok($person = $dir->lookup_person('x213'));
    ok(groups_are($person,[])); # viewer x200, person x213 hidden

    ok($dir = new Ucam::Directory(ldapserver => $server->url, viewer => 'x200',
                                  service => "test",
                                  binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
                                  log => new IO::Scalar(\$logdata)));
    ok($person = $dir->lookup_person('x200'));
    $person->replace(attr=>'displayName', val=>'Jon foo Warbrick');
    $person->replace(attr=>'jpegPhoto', val=>$jpg1x1);
    lives_ok {$person->commit};
    like($logdata, qr!<modify type="person" uid="x200"><replace attr="lastUpdated"><value>test, x200, \d\d\d\d-\d\d-\d\d, \d\d:\d\d ...</value></replace><replace attr="displayname"><value>Jon foo Warbrick</value></replace><replace attr="jpegphoto"><unloggedvalue /></replace></modify>\n!);

    ok($dir = new Ucam::Directory(ldapserver => $server->url, viewer => 'x205',
                                  service => "test",
                                  binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
                                  log => new IO::Scalar(\$logdata)));
    ok($person = $dir->lookup_person('x200'));
    ok($person->is_editable);
    ok(!$person->is_cancelable);

    ok(groups_are($person,["100656","200656"]));

    ok($dir = new Ucam::Directory(ldapserver => $server->url, godmode => 1,
                                  viewer => 'root', service => "test",
                                  binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
                                  log => new IO::Scalar(\$logdata)));
    ok($person = $dir->create_person('ta999'));
    $person->replace(attr=>'sn', val=>'Atkins');
    $person->replace(attr=>'cn', val=>'T. Atkins');
    lives_ok {$person->commit};
    lives_ok {$person = $dir->lookup_person('ta999')};
    lives_ok {$person = $dir->create_person('ta998')};
    $person->replace(attr=>'cn', val=>'T. Atkins');
    $person->replace(attr=>'instID', val=>['CS', 'CSOLD']);
    $person->replace(attr=>'sn', val=>'Atkins');
    $person->replace(attr=>'allmail', val=>'ta998@cam.ac.uk');
    lives_ok {$person->commit};
    ok($person = $dir->lookup_person('ta998'));
    is($person->sn, 'Atkins');
    is_deeply([$person->ou], ["University Computing Service (UCS)"]);

    ok($person->is_editable);
    ok($person->is_cancelable);

    is_deeply([$dir->check_uids('x201', 'spqr1', 'ta998')], ['spqr1']);

}
