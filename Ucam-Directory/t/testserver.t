# $Id$ -*- cperl -*-

use Test::More tests => 4;

use Ucam::Directory;
use_ok("Ucam::Directory::TestServer");

$server = eval { new Ucam::Directory::TestServer("t/fakedir.ldif") };

SKIP: {
    skip "Can't start test server - slapd not available?", 3, if !$server;

    ok($dir = new Ucam::Directory(ldapserver => $server->url));

    ok($inst = $dir->lookup_inst('CS'));
    isa_ok($inst, "Ucam::Directory::Institution");

}
