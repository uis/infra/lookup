# $Id$ -*- cperl -*-

use Test::More;
use Test::Exception;

use Ucam::Directory;
use Ucam::Directory::Fake;
use Ucam::Directory::Institution;
use Net::LDAP::Entry;

use Encode;
use utf8;

use Ucam::Directory::TestServer;

$server = eval { new Ucam::Directory::TestServer("t/fakedir.ldif") };
if ($server) {
   plan(tests => 53)
}
else {
   plan(skip_all => "Can't start test server - slapd not available?");
}

ok($dir = new Ucam::Directory(ldapserver => $server->url, godmode => 1,
	viewer => 'root', service => "test",
	binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
	log => new IO::Scalar(\$logdata)));
ok($inst = $dir->lookup_inst('CS'));
ok($inst->is_editable);
ok($inst->is_cancelable);
lives_ok {$inst->commit};
$inst->replace(attr=>'allmail', val => 'enquiries@ucs.cam.ac.uk');
lives_ok {$inst->commit};
ok($inst = $dir->lookup_inst('CS'));
is($inst->mail, 'enquiries@ucs.cam.ac.uk');

ok($dir = new Ucam::Directory(ldapserver => $server->url,
	viewer => 'x201', service => "test",
	binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
	log => new IO::Scalar(\$logdata)));
ok($inst = $dir->lookup_inst('CS'));
ok($inst->is_editable);
ok(!$inst->is_cancelable);
lives_ok {$inst->commit};
$inst->replace(attr=>'allmail', val => ['enquiries@ucs.cam.ac.uk',
	'help-desk@ucs.cam.ac.uk', 'director@ucs.cam.ac.uk']);
lives_ok {$inst->commit};
ok($inst = $dir->lookup_inst('CS'));
is($inst->mail, 'enquiries@ucs.cam.ac.uk');

ok(!$inst->validate(attr => 'mgrGroupID', val => [100656, 100655]));
ok($inst->validate(attr => 'mgrGroupID', val => [100655]));

ok($row = $inst->contactRow);
ok($row->is_editable);
ok($row->is_editable('uid'));

ok($row = $inst->create_contactrow);
$row->replace(attr=>'uid', val=>'x201');
$row->replace(attr=>'collationOrder', val=>'36');
$row->replace(attr=>'rowTitle', val=>'Test contact row');
lives_ok {$row->commit};

# Force a collision in cn
ok($row1 = $inst->create_contactrow);
$row1->replace(attr=>'uid', val=>'x201');
$row1->replace(attr=>'collationOrder', val=>'37');
$row1->replace(attr=>'cn', val=>$row->cn, internal=>1);
$row1->replace(attr=>'rowTitle', val=>'Second test contact row');
$row1->{ldapentry}->
	dn(Ucam::Directory::contactrow_dn($row1->{inst}->instID, $row1->cn));
is($row1->cn, $row->cn);
lives_ok {$row1->commit};
isnt($row1->cn, $row->cn);

lives_ok {$row->delete};

ok($person = $dir->lookup_person("fjc55"));
ok(!$person->validate(attr=>'instID', val=>[$person->instID]),
	"null instid change");
ok($person->validate(attr=>'instID', val=>[$person->instID, "INGBS"]),
	"add wrong instid");
ok(!$person->validate(attr=>'instID', val=>["CAKE"]),
	"delete right instid");
ok($person->validate(attr=>'instID', val=>[$person->instID, "SPONGE"]),
	"add bogus instid");
$person->replace(attr=>'instID', val=>["CAKE"]);
lives_ok {$person->commit};
ok($person = $dir->lookup_person("x200"));
ok($person->is_multivalued('instID'));
$person->replace(attr=>'instID', val => [$person->instID, 'CS']);
lives_ok {$person->commit};
is_deeply([sort($person->ou)],
	['CS Course Identifiers', 'University Computing Service (UCS)']);

ok($dir = new Ucam::Directory(ldapserver => $server->url,
	viewer => 'x206', service => "test",
	binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
	log => new IO::Scalar(\$logdata)));
ok($inst = $dir->lookup_inst('CS'));
ok(!$inst->is_editable);
ok(!$inst->is_cancelable);
ok($row = $inst->contactRow);
ok(!$row->is_editable);

ok(exists($inst->{contactRow}));
$inst->clear_contactrow_cache;
ok(!exists($inst->{contactRow}));

$logdata=$logdata; # Avoid a warning

ok($dir = new Ucam::Directory(ldapserver => $server->url, godmode => 1,
	viewer => 'root', service => "test",
	binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
	log => new IO::Scalar(\$logdata)));
ok($inst = $dir->create_inst('THING'));
isa_ok($inst, "Ucam::Directory::Institution");
$inst->replace(attr=>'ou', val=>'Department of Thing');
lives_ok {$inst->commit};
lives_ok {$inst = $dir->lookup_inst('THING')};
isa_ok($inst, "Ucam::Directory::Institution");
is($inst->instID, 'THING');
