# $Id$ -*- cperl -*-
# change 'tests => 1' to 'tests => last_test_to_print';

use strict;

use Test::More tests => 202;

use Ucam::Directory;
use Ucam::Directory::Fake;
use Ucam::Directory::TestServer;
use Net::LDAP::Entry;

use Encode;
use utf8;

BEGIN {
    use_ok('Ucam::Directory::Group');
};

sub members_are {
    my ($group,$expected) = @_;

    my $ok = 1;
    my %found_members;
    my %expected_members = map { $_ => 1 } @$expected;
    foreach my $member ($group->members) {
        $found_members{$member->uid} = 1;
        unless (defined($expected_members{$member->uid})) {
            diag "members_are: ", $member->uid, " found but not expected";
            $ok = 0;
        }
    }
    foreach my $member (@$expected) {
        unless (defined($found_members{$member})) {
            diag "members_are: $member expected but not foud";
            $ok = 0;
        }
    }

    return $ok;
}

my $entry = new Net::LDAP::Entry;
$entry->add(objectClass => ["camAcUkGroup"],
	    groupID => ["1"],
	    groupTitle => ["Lookup administrators"],
	    description => ["People with super-user rights"],
	    mgrGroupID => ["1"],
            uid => ["uid001", "uid002"],
	    visibility => ["members"],
            mail => ["super\@x.invalid"],
            instID => ["CS"],
            cancelled => ["ex-parot"],
            userPassword => ["secret"],
            rdrGroupID => ["CSX"],
            allUid => ["uid001", "uid002", "uid003"],
            expiryTimestamp => ["19600503123456Z"],
        );

ok(my $group = new Ucam::Directory::Group($entry), "new object");
isa_ok($group, 'Ucam::Directory::Group');


my $server = eval { new Ucam::Directory::TestServer("t/fakedir.ldif") };

SKIP: {
    skip "Can't start test server - slapd not found?", 199, if !$server;

    my $dir = new Ucam::Directory(ldapserver => $server->url, viewer=>'x203');

    ok($group = $dir->lookup_group('100656'));

    is($group->groupID, "100656");
    is($group->rdn,"100656");
    is($group->groupTitle, 'Editors group for "University Computing Service (UCS)".');
    is($group->description,'The members of this group may edit the directory data for "University Computing Service (UCS)".');
    is($group->mgrGroupID,"100655");
    ok($group->has_member("x203"));

    ok(!$group->has_member("x999"));
    ok(!$group->has_manager("x211"));
    ok($group->has_manager(uid=>"x211",internal=>1));
    ok(!$group->has_manager("x999"));
    ok(!$group->has_manager(uid=>"x999",internal=>1));
    ok(!$group->has_reader("x213"));
    ok($group->has_reader(uid=>"x213",internal=>1));
    ok(!$group->has_reader("x999"));
    ok(!$group->has_reader(uid=>"x999",internal=>1));

    is_deeply([$group->uid],["x200","x201", "x202"]);
    is($group->visibility,"members");
    is($group->mail,"foo\@x.invalid");
    isa_ok($group->owninginsts,'Ucam::Directory::Institution');
    is(($group->owninginsts)[0]->instID,'CS');
    is($group->userPassword,undef);
    is($group->rdrGroupID,"123456");
    is_deeply([$group->allUid],["x200","x201", "x202","x203","x204"]);
    is($group->expiryTimestamp,"1960-03-05T12:34:56");
    isa_ok($group->expiryTimestamp,'DateTime');
    is($group->lastUpdated,"lookup, fjc55, 2005-10-11, 16:32 BST");
    isa_ok($group->lastUpdated,'Ucam::Directory::LastUpdated');
    is($group->lastUpdated->service,'lookup');
    is($group->lastUpdated->user,'fjc55');
    isa_ok($group->lastUpdated->user($dir),'Ucam::Directory::Person');
    is($group->lastUpdated->user($dir)->uid,'fjc55');
    is($group->lastUpdated->date,'2005-10-11T15:32:00'); # time in UTC
    is($group->lastUpdated->date->set_time_zone('local'),
       '2005-10-11T16:32:00'); # time in local
    isa_ok($group->lastUpdated->date,'DateTime');

    ok(my $g2 = $group->mgrgroups);
    is($g2->groupID, '100655');
    ok($g2->has_member('x203'));
    ok(!$g2->has_member('spqr1'));

    ok(my $g3 = $group->rdrgroups);
    is($g3->groupID,"123456");

    ok(my $inst = $group->owninginsts);
    is_deeply([$inst->instID], ['CS']);

    # Visibility tests
    isnt($group->uid, undef);
    isnt($g2->uid, undef);
    ok($g3 = $dir->lookup_group('200656'));
    isnt($g3->uid, undef);
    ok($g3 = $dir->lookup_group('000000'));
    is($g3->uid, undef);
    ok($g3 = $dir->lookup_group('123456'));
    is_deeply([$g3->uid], ["x203","x205","x212"]);
    ok($g3->has_member('x203'));
    ok($g3->has_member('x205'));

    # extended uid/allUid visibility tests

    # as x200
    print STDERR "# As x200\n";
    $dir = new Ucam::Directory(ldapserver => $server->url, viewer=>'x200');

    # ... a visible member of 100656 which has visibility members
    print STDERR "# group 100656\n";
    ok(my $g = $dir->lookup_group('100656'));
    ok($g->has_member('x200'));
    ok($g->has_member('x201'));
    ok(!$g->has_member('x203'));
    ok(!$g->has_member('x204'));
    is($g->visibility,'members');
    ok(!$g->has_public_member('x200'));
    ok(!$g->has_public_member('x201'));
    ok(!$g->has_public_member('x203'));
    ok(!$g->has_public_member('x204'));

    is($g->has_member('x200'),'unsuppressed');
    is($g->has_member('x201'),'unsuppressed');
    isnt($g->has_member('x203'),'unsuppressed');
    isnt($g->has_member('x204'),'unsuppressed');

    ok($g->is_visible(attr => 'uid'));
    ok(!$g->is_visible(attr => 'alluid'));
    ok(!$g->is_editable(attr => 'uid')); # uid is never editable
    ok(!$g->is_editable(attr => 'alluid')); # nor is allUid
    is_deeply([$g->uid],["x200","x201", "x202"]);
    is_deeply([$g->allUid],[]);
    ok(members_are($g,["x200","x201","x202"]));

    # ... a visible member of 200656 which has visibility managers
    print STDERR "# group 200656\n";
    ok($g = $dir->lookup_group('200656'));
    ok($g->has_member('x200'));
    ok(!$g->has_member('x201'));
    ok(!$g->has_member('x203'));
    ok(!$g->has_member('x204'));
    is($g->visibility,'managers');
    ok(!$g->has_public_member('x200'));
    ok(!$g->has_public_member('x201'));
    ok(!$g->has_public_member('x203'));
    ok(!$g->has_public_member('x204'));

    is($g->has_member('x200'),'unsuppressed');
    isnt($g->has_member('x201'),'unsuppressed');
    isnt($g->has_member('x203'),'unsuppressed');
    isnt($g->has_member('x204'),'unsuppressed');

    ok(!$g->is_visible(attr => 'uid'));
    ok(!$g->is_visible(attr => 'alluid'));
    ok(!$g->is_editable(attr => 'uid'));
    ok(!$g->is_editable(attr => 'alluid'));
    is_deeply([$g->uid],[]);
    is_deeply([$g->allUid],[]);
    ok(members_are($g,["x200"]));

    # ... not a member of 123456 which has visibility cam
    print STDERR "# group 123456\n";
    ok($g = $dir->lookup_group('123456'));
    ok(!$g->has_member('x200'));
    ok($g->has_member('x203'));
    ok(!$g->has_member('x213'));
    is($g->visibility,'cam');
    ok(!$g->has_public_member('x200'));
    ok($g->has_public_member('x203'));
    ok(!$g->has_public_member('x213'));

    isnt($g->has_member('x200'),'unsuppressed');
    is($g->has_member('x203'),'unsuppressed');
    isnt($g->has_member('x213'),'unsuppressed');

    ok($g->is_visible(attr => 'uid'));
    ok(!$g->is_visible(attr => 'alluid'));
    ok(!$g->is_editable(attr => 'uid'));
    ok(!$g->is_editable(attr => 'alluid'));
    is_deeply([$g->uid],["x203","x205","x212"]);
    is_deeply([$g->allUid],[]);
    ok(members_are($g,["x203","x205","x212"]));

    # as x204
    print STDERR "# As x204\n";
    $dir = new Ucam::Directory(ldapserver => $server->url, viewer=>'x204');

    # ...an INvisible member of 100656 which has visibility members
    print STDERR "# group 100656\n";
    ok($g = $dir->lookup_group('100656'));
    ok($g->has_member('x200'));
    ok($g->has_member('x201'));
    ok(!$g->has_member('x203'));
    ok($g->has_member('x204'));
    is($g->visibility,'members');

    is($g->has_member('x200'),'unsuppressed');
    is($g->has_member('x201'),'unsuppressed');
    isnt($g->has_member('x203'),'unsuppressed');
    isnt($g->has_member('x204'),'unsuppressed');

    ok($g->is_visible(attr => 'uid'));
    ok(!$g->is_visible(attr => 'alluid'));
    ok(!$g->is_editable(attr => 'uid'));
    ok(!$g->is_editable(attr => 'alluid'));
    is_deeply([$g->uid],["x200","x201", "x202"]);
    is_deeply([$g->allUid],[]);
    ok(members_are($g,["x200","x201","x202","x204"]));


    # ...an INvisible member of 200656 which has visibility managers
    print STDERR "# group 200565\n";
    ok($g = $dir->lookup_group('200656'));
    ok(!$g->has_member('x200'));
    ok(!$g->has_member('x201'));
    ok(!$g->has_member('x203'));
    ok($g->has_member('x204'));
    is($g->visibility,'managers');

    isnt($g->has_member('x200'),'unsuppressed');
    isnt($g->has_member('x201'),'unsuppressed');
    isnt($g->has_member('x203'),'unsuppressed');
    isnt($g->has_member('x204'),'unsuppressed');

    ok(!$g->is_visible(attr => 'uid'));
    ok(!$g->is_visible(attr => 'alluid'));
    ok(!$g->is_editable(attr => 'uid'));
    ok(!$g->is_editable(attr => 'alluid'));
    is_deeply([$g->uid],[]);
    is_deeply([$g->allUid],[]);
    ok(members_are($g,["x204"]));

    # as x211
    print STDERR "# As x211\n";
    $dir = new Ucam::Directory(ldapserver => $server->url, viewer=>'x211');

    # ...a manager of 100656 which has visibility members
    print STDERR "# group 100565\n";
    ok($g = $dir->lookup_group('100656'));
    ok($g->has_member('x200'));
    ok($g->has_member('x201'));
    ok($g->has_member('x203'));
    ok($g->has_member('x204'));
    is($g->visibility,'members');
    ok($g->is_visible(attr => 'uid'));
    ok($g->is_visible(attr => 'alluid'));
    ok(!$g->is_editable(attr => 'uid'));
    ok(!$g->is_editable(attr => 'alluid'));
    is_deeply([$g->uid],["x200","x201", "x202"]);
    is_deeply([$g->allUid],["x200","x201","x202","x203","x204"]);
    ok(members_are($g,["x200","x201","x202","x203","x204"]));

    # ...a manager of 200656 which has visibility managers
    print STDERR "# group 200565\n";
    ok($g = $dir->lookup_group('200656'));
    ok($g->has_member('x200'));
    ok($g->has_member('x201'));
    ok($g->has_member('x203'));
    ok($g->has_member('x204'));
    is($g->visibility,'managers');
    ok($g->is_visible(attr => 'uid'));
    ok($g->is_visible(attr => 'alluid'));
    ok(!$g->is_editable(attr => 'uid'));
    ok(!$g->is_editable(attr => 'alluid'));
    is_deeply([$g->uid],["x200","x201", "x202"]);
    is_deeply([$g->allUid],["x200","x201", "x202","x203","x204"]);
    ok(members_are($g,["x200","x201","x202","x203","x204"]));

    # as x213
    print STDERR "# As x213\n";
    $dir = new Ucam::Directory(ldapserver => $server->url, viewer=>'x213');

    # ...a manager of 100656 which has visibility members
    print STDERR "# group 100565\n";
    ok($g = $dir->lookup_group('100656'));
    ok($g->has_member('x200'));
    ok($g->has_member('x201'));
    ok($g->has_member('x203'));
    ok($g->has_member('x204'));
    is($g->visibility,'members');
    ok($g->is_visible(attr => 'uid'));
    ok($g->is_visible(attr => 'alluid'));
    ok(!$g->is_editable(attr => 'uid'));
    ok(!$g->is_editable(attr => 'alluid'));
    is_deeply([$g->uid],["x200","x201", "x202"]);
    is_deeply([$g->allUid],["x200","x201", "x202","x203","x204"]);
    ok(members_are($g,["x200","x201","x202","x203","x204"]));

    # ...a manager of 200656 which has visibility managers
    print STDERR "# group 200565\n";
    ok($g = $dir->lookup_group('200656'));
    ok($g->has_member('x200'));
    ok($g->has_member('x201'));
    ok($g->has_member('x203'));
    ok($g->has_member('x204'));
    is($g->visibility,'managers');
    ok($g->is_visible(attr => 'uid'));
    ok($g->is_visible(attr => 'alluid'));
    ok(!$g->is_editable(attr => 'uid'));
    ok(!$g->is_editable(attr => 'alluid'));
    is_deeply([$g->uid],["x200","x201", "x202"]);
    is_deeply([$g->allUid],["x200","x201", "x202","x203","x204"]);
    ok(members_are($g,["x200","x201","x202","x203","x204"]));

}

