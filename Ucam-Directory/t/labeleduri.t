# -*- perl -*-
# $Id$

use Test::More tests => 22;

use_ok('Ucam::Directory::LabeledURI');
ok($lu = new Ucam::Directory::LabeledURI('http://www.cam.ac.uk'));
isa_ok($lu, 'Ucam::Directory::LabeledURI');
is("$lu", "http://www.cam.ac.uk");
is($lu->uri, "http://www.cam.ac.uk");
is($lu->label, undef);
ok($lu = new Ucam::Directory::LabeledURI('http://www.cam.ac.uk '));
is("$lu", "http://www.cam.ac.uk ");
is($lu->uri, "http://www.cam.ac.uk");
is($lu->label, '');
ok($lu = new Ucam::Directory::LabeledURI
	('http://www.cam.ac.uk  University of Cambridge Web site'));
is("$lu", "http://www.cam.ac.uk  University of Cambridge Web site");
is($lu->uri, "http://www.cam.ac.uk");
is($lu->label, 'University of Cambridge Web site');

ok($lu = new Ucam::Directory::LabeledURI
	(uri => 'http://www.cam.ac.uk',
	 label => 'University of Cambridge Web site'));
is("$lu", "http://www.cam.ac.uk University of Cambridge Web site");
is($lu->uri, "http://www.cam.ac.uk");
is($lu->label, 'University of Cambridge Web site');

ok($lu = new Ucam::Directory::LabeledURI
	(uri => 'http://www.cam.ac.uk',
	 label => undef));
is("$lu", "http://www.cam.ac.uk");
is($lu->uri, "http://www.cam.ac.uk");
is($lu->label, undef);
