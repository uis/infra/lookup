# -*- perl -*-
# $Id$

use Test::More tests => 18;

use Ucam::Directory;
use Ucam::Directory::TestServer;

use_ok('Ucam::Directory::LastUpdated');
ok($lu = Ucam::Directory::LastUpdated->new('lookup, test0001, 2006-05-03, 16:12 GMT'));
isa_ok($lu, 'Ucam::Directory::LastUpdated');
is("$lu", "lookup, test0001, 2006-05-03, 16:12 GMT");
is($lu->service,"lookup");
is($lu->user,"test0001");
isa_ok($lu->date,"DateTime");
is($lu->date,"2006-05-03T16:12:00");

ok($lu2 = Ucam::Directory::LastUpdated->new('lookup, test0001, 2006-05-03, 16:12 BST'));
is("$lu2", "lookup, test0001, 2006-05-03, 16:12 BST");
is($lu2->date,"2006-05-03T15:12:00"); # 'cos the _date_ is always in UT


$server = eval { new Ucam::Directory::TestServer("t/fakedir.ldif") };

SKIP: {
    skip "Can't start test server - slapd not found?", 7, if !$server;

    $dir = new Ucam::Directory(ldapserver => $server->url, viewer=>'x203');

    ok($lu3 = Ucam::Directory::LastUpdated->new('lookup, x200, 2006-05-03, 16:12 BST'));
    is($lu3->user,'x200');
    isa_ok($lu3->user($dir),'Ucam::Directory::Person');
    is($lu3->user($dir)->uid,'x200');

    ok($lu4 = Ucam::Directory::LastUpdated->new('lookup, x999, 2006-05-03, 16:12 BST'));
    is($lu4->user,'x999');
    ok(!ref($lu4->user($dir)),"Non-existent user isn't an object");

}
