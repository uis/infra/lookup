#!/usr/bin/perl -w

use strict;

use Test::More 'no_plan';

my @exceptions =         qw/notfound_exception argument_exception/;

use_ok('Ucam::Directory::Exception', 'general_exception', @exceptions);

eval { general_exception('Foo bar') };
ok($@,                                             'getting anything');
isa_ok($@,"Ucam::Directory::Exception");
is($@->error,'Foo bar',                           'expected message?');

foreach (@exceptions) {

  eval { no strict 'refs'; &$_('Foo bar') };

  ok($@,                                           'getting anything');

  my $m = $_;
  $m =~ s/_exception//;
  isa_ok($@,"Ucam::Directory::Exception");
  isa_ok($@,"Ucam::Directory::Exception::$m");
  #ok(isa_webauth_exception($@,$m),           'the right one of ours?');

  is($@->error,'Foo bar',                         'expected message?');

}

