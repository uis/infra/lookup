# $Id$ -*- cperl -*-

use strict;

use Test::More;
use Test::Exception;

use Ucam::Directory;
use Ucam::Directory::Fake;
use Ucam::Directory::Group;
use Net::LDAP::Entry;
use DateTime;

use Encode;
use utf8;

#use Data::Dumper;

use Ucam::Directory::TestServer;

my $server = eval { new Ucam::Directory::TestServer("t/fakedir.ldif") };
if ($server) {
   plan(tests => 95)
}
else {
   plan(skip_all => "Can't start test server - slapd not found?");
}

my $logdata;
ok(my $dir = new Ucam::Directory(ldapserver => $server->url, godmode => 1,
	viewer => 'root', service => "test",
	binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
	log => new IO::Scalar(\$logdata)));

ok(my $group = $dir->create_group());
$group->replace(attr => "groupTitle", val => "My new group");
$group->replace(attr => "description", val => "a group for testing");
$group->replace(attr => "mgrGroupID", val => "5");
$group->replace(attr => "visibility", val => "cam");
lives_ok {$group->commit};
is($group->groupID, 200000);

ok($group = $dir->create_group());
$group->replace(attr => "groupTitle", val => "XXX");
$group->replace(attr => "description", val => "YYY");
$group->replace(attr => "mgrGroupID", val => "5");
$group->replace(attr => "visibility", val => "cam");
lives_ok {$group->commit};
is($group->groupID, 200001);

lives_ok {$group->replace(attr=>'mgrgroupid', val=>'12345')};

# Assorted invalid dates
like ($group->validate(attr=>'expirytimestamp', val=>'foo'),qr/date format/);
dies_ok  { $group->replace(attr=>'expirytimestamp', val=>'foo') };
dies_ok  { $group->replace(attr=>'expirytimestamp', val=>'1960-01-99T12:34:56') };
like ($group->validate(attr=>'warntimestamp', val=>'foo'),qr/date format/);
dies_ok  { $group->replace(attr=>'warntimestamp', val=>'foo') };
dies_ok  { $group->replace(attr=>'warntimestamp', val=>'1960-01-99T12:34:56') };

my $now = DateTime->now;
my $yesterday = DateTime->now->subtract(days=>1);
my $nextyear = DateTime->now->add(years=>1);
my $wideblueyonder = DateTime->now->add(years=>5);
isa_ok($now,'DateTime');
is     ($group->validate(attr=>'expirytimestamp', val=>$now),'0');
is     ($group->validate(attr=>'expirytimestamp', val=>"$now"),'0');
lives_ok { $group->replace(attr=>'expirytimestamp',val=>$now) };
lives_ok { $group->replace(attr=>'expirytimestamp',val=>"$now") };

is     ($group->validate(attr=>'warntimestamp', val=>$now),'0');
is     ($group->validate(attr=>'warntimestamp', val=>"$now"),'0');
lives_ok { $group->replace(attr=>'warntimestamp',val=>$now) };
lives_ok { $group->replace(attr=>'warntimestamp',val=>"$now") };

is     ($group->validate(attr=>'expirytimestamp', val=>$nextyear),'0');
is     ($group->validate(attr=>'expirytimestamp', val=>"$nextyear"),'0');
is     ($group->validate(attr=>'warntimestamp', val=>$nextyear),'0');

like   ($group->validate(attr=>'expirytimestamp', val=>$yesterday),qr/past/);
like   ($group->validate(attr=>'expirytimestamp', val=>"$yesterday"),qr/past/);
is     ($group->validate(attr=>'warntimestamp', val=>$yesterday),'0');


like   ($group->validate(attr=>'expirytimestamp', val=>$wideblueyonder),qr/future/);
like   ($group->validate(attr=>'expirytimestamp', val=>"$wideblueyonder"),qr/future/);
is     ($group->validate(attr=>'warntimestamp', val=>$wideblueyonder),'0');

like ($group->validate(attr=>'userpassword', val=>'foo'),qr/at least/);
dies_ok  { $group->replace(attr=>'userpassword', val=>'foo') };
my $thirty_two = '12345678901234567890123456789012';
is ($group->validate(attr=>'userpassword', val=>$thirty_two),0);
lives_ok  { $group->replace(attr=>'userpassword', val=>$thirty_two) };

# required fields
like ($group->validate(attr=>'grouptitle', val=>undef),         qr/requires/i);
like ($group->validate(attr=>'grouptitle', val=>[]),            qr/requires/i);
like ($group->validate(attr=>'grouptitle', val=>''),            qr/requires/i);
throws_ok { $group->replace(attr=>'grouptitle', val=>undef) }   qr/requires/i;
throws_ok { $group->replace(attr=>'grouptitle', val=>[]) }      qr/requires/i;
throws_ok { $group->replace(attr=>'grouptitle', val=>'') }      qr/requires/i;

throws_ok { $group->replace(attr=>'description', val=>undef)}qr/requires/i;
throws_ok { $group->replace(attr=>'mgrgroupid', val=>undef)} qr/requires/i;
throws_ok { $group->replace(attr=>'visibility', val=>undef)} qr/requires/i;

ok($group->is_editable,'group is editable (in god mode)');
ok($group->is_cancelable,'group is cancelable (in god mode)');

lives_ok { $group->commit };


ok($dir = new Ucam::Directory(ldapserver => $server->url,
	viewer => 'x203', service => "test",
	binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
	log => new IO::Scalar(\$logdata)));

lives_ok { $group = $dir->lookup_group('100656') }; # x203 manages 100656
ok($group->is_editable,'group is editable by x203');
ok($group->is_cancelable,'group is cancelable by x203');

ok($group->has_member("x200"));
is($group->has_member("x200"),'unsuppressed');
ok(!$group->has_member("x210"));
isnt($group->has_member("x210"),'unsuppressed');

lives_ok { $group->remove_members("x200","x210") };

ok(!$group->has_member("x200"));
isnt($group->has_member("x200"),'unsuppressed');
ok(!$group->has_member("x210"));
isnt($group->has_member("x210"),'unsuppressed');

lives_ok { $group->add_members("x200","x210") };

ok($group->has_member("x200"));
is($group->has_member("x200"),'unsuppressed');
ok($group->has_member("x210"));
isnt(!$group->has_member("x210"),'unsuppressed'); # x210 has 'suppression: yes'

lives_ok { $group->remove_members("x210") };

ok($group->has_member("x200"));
is($group->has_member("x200"),'unsuppressed');
ok(!$group->has_member("x210"));
isnt($group->has_member("x210"),'unsuppressed');



ok($dir = new Ucam::Directory(ldapserver => $server->url,
	viewer => 'x200', service => "test",
	binddn => "cn=root,ou=ldap_admin,o=University of Cambridge,dc=cam,dc=ac,dc=uk", password => "ossifrage",
	log => new IO::Scalar(\$logdata)));

lives_ok { $group = $dir->lookup_group('100656') }; # x200 doesn't manage100656
ok(!$group->is_editable,'group is NOT editable by x200');
ok(!$group->is_cancelable,'group is NOT cancelable by x200');

ok($group->has_member("x200"));
is($group->has_member("x200"),'unsuppressed');
ok(!$group->has_public_member("x200")); # also considers group visibility
is($group->has_member("x201"),'unsuppressed');
isnt($group->has_member("x205"),'unsuppressed');

lives_ok { $group->suppress_membership("x200") };
ok($group->has_member("x200"));
isnt($group->has_member("x200"),'unsuppressed');

lives_ok { $group->unsuppress_membership("x200") };
ok($group->has_member("x200"));
is($group->has_member("x200"),'unsuppressed');

throws_ok { $group->suppress_membership("x999") } qr/you can't change/;
throws_ok { $group->unsuppress_membership("x999") } qr/you can't change/;

lives_ok { $group = $dir->lookup_group('100655') }; # x200 isn't in 100655
ok(!$group->has_member("x200"));
throws_ok { $group->unsuppress_membership("x200") } qr/non-member/;

ok($group->is_required);
ok($group->is_required(attr=>'groupTitle'));
ok($group->is_required('groupTitle'));
ok(!$group->is_required('rdrGroupID'));
