# $Id$ -*- cperl -*-
# change 'tests => 1' to 'tests => last_test_to_print';

use strict;

#BEGIN { $Exporter::Verbose = 1}

use Test::More tests => 32;

use Ucam::Directory;
use Ucam::Directory::Fake;
use Net::LDAP::Entry;

use Encode;
use utf8;

BEGIN {
    use_ok('Ucam::Directory::Institution');
};

my $entry = new Net::LDAP::Entry;
$entry->add(objectClass => ["camAcUkOrganizationalUnit", "organizationalUnit"],
	    instID => ["CS"],
	    postalAddress => ['\55\6eiversity of Cambridge $ Computing Service $ Pembroke Street $ CAMBRIDGE $ CB2  3QH'],
	    ou => ['University Computing Service'],
	    parentInstID => ['INGBS'],
	    labeledURI => ['http://www.cam.ac.uk/cs/ UCS web site',
			   'http://www.cam.ac.uk/cs/courses/ UCS courses'],
            jdName => ['University Computing Service2'],
            jdFather => ['INGBS'],               
	    );

ok(my $inst = new Ucam::Directory::Institution($entry), "new object");
isa_ok($inst, 'Ucam::Directory::Institution');

is($inst->get(attr => "instID"), "CS", "single-value");
is($inst->rdn,"CS");
is($inst->get(attr => "postalAddress"), "University of Cambridge\n" .
   "Computing Service\nPembroke Street\nCAMBRIDGE\nCB2  3QH",
   "postal address");
eval { $inst->get(attr => "homePhone"); }; ok($@, "exception on bad attr");
eval { $inst->homePhone; }; ok($@, "exception with convenience method");
ok(my $lu = $inst->labeledURI, "get labeledURI");
is($lu->uri, "http://www.cam.ac.uk/cs/", "labeledURI works");

is($inst->jdName, "University Computing Service2");
is($inst->jdFather, "INGBS");

my $dir = new Ucam::Directory::Fake;

ok($inst = $dir->lookup_inst("CS"), "look up inst");
isa_ok($inst, 'Ucam::Directory::Institution');

is($inst->jdName, "University Computing Service2");
is($inst->jdFather, "INGBS");
my @fathers = $inst->jdfathers;
is(scalar(@fathers),1);
isa_ok($fathers[0],'Ucam::Directory::Institution');
is($fathers[0]->instID,'INGBS');

ok(my @people = $inst->people);
isa_ok($people[0], 'Ucam::Directory::Person');
is(scalar(@people), 3, "right number of people");

is($inst->groupID,'100656');
my @groups = $inst->groups;
is(scalar(@groups),1,"right number of groups");
isa_ok($groups[0],'Ucam::Directory::Group');
is($groups[0]->groupID,'100656',"group is 100656");

eval { $dir->lookup_inst(instid => "THING") }; ok($@, "failed lookup");

ok(my $pinst = $inst->parents);
is($pinst->instID, 'INGBS');
ok(my($cinst) = $pinst->children);
is($cinst->instID, 'CS');

ok($inst->as_vcard);

