<%doc>

This component knows (or is at least intended to know or to know how
to find out) everything necessary to display a series of attributes as 
cells in a listing. Call it like

  <table>
    <tr>
      <& table-row.mason, prefix=>$prefix, data=>$obj, attrs=> ['cn','uid'] &>
    </tr>
    ...
  </table>

</%doc>

% foreach my $attr (@attrs) {

<%perl>
$attr = lc($attr);
my @items;
if ($attr eq 'name') {
  @items = ($data->name);
}
else {
  @items = $data->get(attr=>$attr);
}

# Strip out group 000000 (or 0) as a manager if there is any alternative
if ($attr eq 'mgrgroups' and scalar(@items) > 1) {
  @items = grep { $_->groupID ne '000000' and $_->groupID ne '0' } @items;
}

# Sort lists of objects in appropriate ways
if (@items and $items[0] and ref($items[0])) {
  if (($items[0])->isa('Ucam::Directory::Person')) {
    @items = Ucam::Directory::Person->byregname->sort(@items);
  }	   
  elsif (($items[0])->isa('Ucam::Directory::Institution')) { 
    @items = Ucam::Directory::Institution->byname->sort(@items);
  }
  elsif (($items[0])->isa('Ucam::Directory::Group')) {
    @items = Ucam::Directory::Group->byname->sort(@items);	
  }
}

# Set up '[' and ']' as needed
my ($open, $close) = ('', '');
($open, $close) = ('[ ', ' ]') 
                    if $attr ne 'name' and $data->is_suppressed($attr);

</%perl>

<td class="<% $attr %>">
% my $ctr = 0;
% foreach my $item (@items) {
% ++$ctr;
%#
%# - name
%#
%   if ($attr eq 'name') {
%     my $priv_name = priv_name($data);
%     if ($priv_name) {
  [
% }
  <a href="<% "$prefix/person/" |n %><% $data->uid |u %>"><% $data->name |h %></a>
%     if ($priv_name) {
  ]
% }
%     if ($item !~ /^\p{InReadable}*$/) {
%       my ($open, $close) = ('', '');
%       ($open, $close) = ('[ ', ' ]')
%                           if ($data->cn and $data->is_suppressed('cn'));
  <br />
  (<% $open %><% $data->cn || $data->uid |h %><% $close %>)
%     }
%   }  
%#
%# - ou
%#
%   elsif ($attr eq 'ou') {
  <a href="<% "$prefix/inst/" |n %><% $data->instID |u %>"><% $item |h
  %></a>
%   }
%#
%# - group title
%#
%   elsif ($attr eq 'grouptitle') {
  <a href="<% "$prefix/group/" |n %><% $data->groupID |u %>"><% $item |h %></a>
%   }
%#
%# - insts/parents/children/mgdinsts
%#
%   elsif ($attr eq 'insts' or 
%          $attr eq 'parents' or 
%          $attr eq 'children' or
%          $attr eq 'mgdinsts' or
%          $attr eq 'owninginsts') {
  <a href="<% "$prefix/inst/" . $item->instID |n %>"><% $item->ou |h %></a><br />
%#
%# - mgrgroups/mgdgroups
%#
%   } elsif ($attr eq 'mgrgroups' or
%            $attr eq 'mgdgroups') {
  <a href="<% "$prefix/group/" . $item->groupID |n %>"><% $item->groupTitle |h %></a><br />
%#
%# - mail/allmail
%#
%   } elsif ($attr eq 'allmail' or
%            $attr eq 'mail') {
  <% $open %>
  <a href="mailto:<% $item |h %>"><% $item |h %></a>
  <% $close %>
  <br />
%#
%# - labeleduri
%#
%   } elsif ($attr eq 'labeleduri') {
  <% $open %>
%     if ($item->label){ 
  <a href="<% $item->uri %>"><% $item->label %></a>
%     } else {
  <a href="<% $item->uri %>"><% $item->uri %></a>
%     }
  <% $close %>
  <br />    
%#
%# - postaladdress
%#
%   } elsif  ($attr eq 'postaladdress') {
%     foreach my $line (split /\n/, $item) {
  <% $open %><% $line |h %><% $close %><br />
%     }
%     if ($ctr != scalar(@items)) {
   <br />
%     }	 
%#
%# - everything else
%#
%   } else {
   <% $open %><% $item |h %><% $close %><br />
%   }
% }
</td>

% }

<%args>
$data
@attrs
$prefix
</%args>

<%once>
use Ucam::Directory::Person;
use Ucam::Directory::Institution;
use Ucam::Directory::Group;
use Ucam::Lookup::Helpers qw/InReadable priv_name/;
</%once>


